# Структура проекта
- **android** - Android-проект
- **ios** - iOS-проект

 - **src/app** - React Native проект
    - **api** - содержит реализацию всех эндпоинтов
	-  **modules** - содержит реализацию основных модулей приложения, в том числе использующихся в качестве экранов.
	    -  Большинство модулей делится на domain и ui.
	        -  **domain** - содержит логику работы модуля - эндпоинты, сторы, интерфейсы и т.д.
	        -  **ui** - содержит визуальную часть модуля, с которой работаем непосредственно пользователь.
	            - **components** - содержит реализацию всех вспомогательных компонентов.
	            - **screens** - сами экраны.
	    - **navigation** - содержит реализацию навигации приложения между экранами.
	- **store** - содержит реализацию состояния приложений, с помощью которого можно связать все компоненты между собой.
	- **utils** - содержит дополнительные функции, которые могут использоваться в компонентах проекта.

Используемый для обращения к API baseUrl задается в файле **"modules/core/infrastructure/httpResource"** в константе BASE_URL

# Настройка окружения для работы с React Native
Для работы должны быть установлены следующие компонеты: React Native CLI, Node.js, JDK, Android Studio (для Android), 
Xcode (для iOS)
Подробную инструкцию по установке можно посмотреть [здесь](https://reactnative.dev/docs/environment-setup)

# Установка зависимостей
- перейти в директорию с проектом, запустить команду `npm install`

*для iOS необходимо дополнительно выполнить команду `cd  ios && pod install`

# Билд 
### Android
**Debug**:
- подключить устройство с включеным режимом разработчика (также должны быть включены пункты "Отладка по USB", 
  "Установка по USB"). Подключенный устройства можно посмотреть через `adb devices`
- включить RN server `npx react-native start` (или `react-native start`)
- запуск процесса билда `npx react-native run-android` (или `react-native run-android`)

**Release**: 
- перейти `cd android`
- выполнить `./gradlew assembleRelease` - билд release-версии
- apk-файл будет находиться в `/android/app/build/outputs/apk/release/apk-release.apk`

### iOS
Открыть файл `.xcworkspace` в Xcode
- Навести на кнопку запуска
- Выбрать цель
- Выбрать устройство или эмулятор
- Нажать на пуск

![1](./readmeFiles/readmeImage/readmeImage1.png)
  
Для сборки release-версии необходимо изменить схему в **Product** -> **Scheme** -> **Edit scheme...** и выбрать тип сборки (**Build Configuration**), нажать на кнопку Close.
![2](./readmeFiles/readmeImage/readmeImage2.png)

# Решение возможных ошибок
## iOS
- ошибка связанная с `FBReactNativeSpec`:
в дереве проекта выбрать **Pods**, в центральной части экрана выбрать вкладку **Build Phases**, **target -> FBReactNativeSpec**, убедиться что `[CP-User] Generate Spec` находится выше `Headers` (возникает после команды `pod install` , баг версии RN 0.64)

![3](./readmeFiles/readmeImage/readmeImage3.png)
- ошибка **"Undefined symbol: __swift_FORCE_LOAD_$_swiftWebKit"**
перейти в  build setting - > Library search path -> добавить переменную $(SDKROOT)/usr/lib/swift



import { formatLastActivity } from './../src/app/modules/ud-ui/helpers/dateFormat';
import 'react-native';
import React from 'react';

it('Test today', () => {
  expect(formatLastActivity('2020-01-15T07:32:16+0000')).toBe('был(а) 15 янв. в 10:32');
});
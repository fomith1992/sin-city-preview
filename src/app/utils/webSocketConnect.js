import {BASE_URL} from '../modules/core/infrastructure/httpResource'
import {
  Alert
} from 'react-native'
//import ReconnectingWebSocket from 'react-native-reconnecting-websocket';
import {object} from 'prop-types';
import ReconnectingWebSocket from 'reconnecting-websocket';

var ws;
var oldToken = '88';
const getWsHost = () => {
  //return 'ws' + BASE_URL.slice(i, BASE_URL.length);
  return( 'wss' + BASE_URL.slice(BASE_URL.indexOf(':'), BASE_URL.length));
};

const isOpen = () => {

  return (!!ws && (!!ws.readyState) )

};
const options = {
 // reconnectDecay: 1,
 // debug: true,
};
window.passToken = (token) => {
  if (oldToken !== token) {
    oldToken = token;
   // console.log('window.passToken', token);
    if (isOpen()) {
    //  console.log('isOpen', ' nu da');
      ws.close();
    }

    wsConnect(token);
  }


};

export const wsConnect = (token) => {
 // console.log('wsConnect', token);
  // ws = new WebSocket(getWsHost() + '/ws/' + token + '/');
   ws = new ReconnectingWebSocket(getWsHost() + '/ws/' + token + '/', null, options);


  // if (isOpen()) {
  //   ws = null;
  //   Alert.alert('isOpen');
  //   ws = new WebSocket(getWsHost() + '/ws/' + token + '/');
  // } else {
  //   Alert.alert('isClosed');
  //   ws = new WebSocket(getWsHost() + '/ws/' + token + '/');
  // }


  window.closeWsConnect = () => {
    try {
      ws.close();
      oldToken = '88';
    }
    catch(e)
    {

    }
   // console.log('closeWsConnecet');
  };
  window.sendMessage = (value) => {
   // ws.close();
    if (!!ws.send && ws.readyState === 1) {
      ws.send(value);
     // console.log('onWsSend', JSON.parse(value));
    } else {
    //  console.log('onWsSendFAILED', JSON.parse(value)['action']);
  //    ws.close();
   //   window.passToken(oldToken);
      //ws = new ReconnectingWebSocket(getWsHost() + '/ws/' + token + '/', null, options);
    }
    // // Alert.alert('onSend');
   // console.log('READY_STATE', ws.readyState, ws.readyState === 1);

  };

  // if (!isOpen()) {
  //   ws = new WebSocket(getWsHost() + '/ws/' + token + '/');
  // } else {
  //   Alert.alert('close');
  //  // ws = new WebSocket(getWsHost() + '/ws/' + token + '/');
  // }
  //var ws = new WebSocket('wss://sc-dating-dev.truemachine.ru/api/v0/ws/d183418c22d70d778a240b37d690e09848b6426f/');
  //alert(String(getWsHost() + '/ws/' + token));
  //Alert.alert("onEntry");

  ws.onopen = () => {
   // console.log('onOpen');
  };

  ws.onmessage = (e) => {
  //  console.log('SOBAKA_onmessage', e, 'PENA_data', JSON.parse(e.data));
     let obj  = JSON.parse(e.data);
   //  console.log('onmessage', obj['action'], obj['message']);
   //  console.log('onmessage', obj['action']);

    if (obj['type'] === "chat_message") {
     // console.log("entry", obj['message']);
     // window.onGetPress(JSON.stringify(obj['message']));
     //  if (!(obj['action'] === 'photo_sent' && myPhoto_SentSent)) {
      if (obj['action'] === 'message_sent' || obj['action'] === 'photo_sent') {
        if (!!window.onGetPress) {
          window.onGetPress(obj['action'], obj['message']);
        }
        if (!!window.refreshChatList) {
          window.refreshChatList();
        } else if (!!window.searchScreenChatLoad) {
          window.searchScreenChatLoad();
        }
      } else if ( obj['action'] === "messages_read" ) {
        if (!!window.onGetPress) {
          window.onGetPress(obj['action'], obj['message']);
        }
        if (!!window.refreshChatList) {
          window.refreshChatList();
        }
      } else if (obj['action'] === "subscription_error") {
       // console.log('sub_error');
        window.onGetPaymentError();
      } else {
        console.log('onmessage obj', obj);
      }

      // } else {
      //   myPhoto_SentSent = false;
      // }

    } else {
      //Alert.alert('not chatMes');
     // window.onGetPress();
    }
    //Alert.alert('e.data', e.data);
    //console.log(e.data);
   // console.log(JSON.parse(e.message));
  };

  ws.onerror = (e) => {
    // an error occurred
    //console.log(e.message);
   // console.log('onerror', e.message, e.status);
    // Alert.alert(String(isOpen()));
  };
  ws.onclose = (e) => {
    // connection opened
   // console.log("onClose", e.code, ' + ', e.reason);
  };
  return 0;
};

// class WebSocketDevice extends Component {
//   constructor(props) {
//     super(props);
//
//     this.state = {
//       ws: null,
//       isOpen: false,
//     };
//   }
//
//   // single websocket instance for the own application and constantly trying to reconnect.
//
//   componentDidMount() {
//     this.connect();
//   }
//
//   timeout = 250; // Initial timeout duration as a class variable
//
//   /**
//    * @function connect
//    * This function establishes the connect with the websocket and also ensures constant reconnection if connection closes
//    */
//   connect = () => {
//    // var ws = new WebSocket("ws://localhost:3000/ws");
//     var ws = new WebSocket(getWsHost());
//     let that = this; // cache the this
//     var connectInterval;
//
//     // websocket onopen event listener
//     ws.onopen = () => {
//       Alert.alert("connected websocket main component");
//
//       this.setState({ ws: ws });
//
//       that.timeout = 250; // reset timer to 250 on open of websocket connection
//       clearTimeout(connectInterval); // clear Interval on on open of websocket connection
//     };
//
//     // websocket onclose event listener
//     ws.onclose = e => {
//       console.log(
//         `Socket is closed. Reconnect will be attempted in ${Math.min(
//           10000 / 1000,
//           (that.timeout + that.timeout) / 1000
//         )} second.`,
//         e.reason
//       );
//
//       that.timeout = that.timeout + that.timeout; //increment retry interval
//       connectInterval = setTimeout(this.check, Math.min(10000, that.timeout)); //call check function after timeout
//     };
//
//     // websocket onerror event listener
//     ws.onerror = err => {
//       Alert.alert(
//         "Socket encountered error: ",
//         err.message,
//         "Closing socket"
//       );
//
//       ws.close();
//     };
//   };
//
//   /**
//    * utilited by the @function connect to check if the connection is close, if so attempts to reconnect
//    */
//   check = () => {
//     const { ws } = this.state;
//     if (!ws || ws.readyState == WebSocket.CLOSED) this.connect(); //check if websocket instance is closed, if so call `connect` function.
//   };
//
//   // render() {
//   //   return <ChildComponent websocket={this.state.ws} />;
//   // }
// }

// import React, {Component,} from "react";
// import {AsyncStorage} from 'react-native';
// import PropTypes from "prop-types";
// import RemoteSVGImage from 'react-native-remote-svg';
// import {Cache} from "react-native-cache";
// import RNFetchBlob from "rn-fetch-blob";


// export class FastRemoteSVGImage extends Component {

//   static propTypes = {
//     source   : PropTypes.shape({
//       uri: PropTypes.string.isRequired
//     }),
//     namespace: PropTypes.string.isRequired
//   };

//   constructor(props) {
//     super(props);
//     this.state = {
//       uri: null
//     };
//     this.cache = new Cache({
//       namespace: props.namespace,
//       policy   : {maxEntries: 1000},
//       backend  : AsyncStorage
//     });
//   }

//   componentDidMount() {
//     const uri = this.props.source && this.props.source.uri;
//     if (uri) {
//       this._loadRemoteSvgUri(uri);
//     }
//   }

//   shouldComponentUpdate(nextProps) {
//     const prevUri = this.props && this.props.source && this.props.source.uri;
//     const nextUri = nextProps && nextProps.source && nextProps.source.uri;
//     if (nextUri && prevUri !== nextUri) {
//       this._loadRemoteSvgUri(nextUri);
//     }
//     return true;
//   }

//   /**
//    * returns data uri string
//    * @param {string} text
//    * @return {string}
//    * @private
//    */
//   static _createSvgDataUri = (text) => {
//     return `data:image/svg+xml;utf8,${text}`;
//   };

//   /**
//    *
//    * @param {string} remoteUri
//    * @private
//    */
//   _loadRemoteSvgUri = (remoteUri) => {
//     const encodedRemoteUri = encodeURI(remoteUri);
//     this.setState({uri: null});
//     this.cache.peekItem(encodedRemoteUri, (error, localUri = null) => {
//       if (error) {
//         console.log('Failed getting remote uri from cache', {error, remoteUri, encodedRemoteUri});
//         this.setState({ uri: null });
//         return;
//       }
//       if (localUri) {
//         this.setState({ uri: localUri });
//         return;
//       }

//       this.setState({ uri: null });
//       RNFetchBlob.fetch('GET', remoteUri).then((res) => {
//         if (res.info().status === 200) {
//           this.cache.setItem(encodedRemoteUri, FastRemoteSVGImage._createSvgDataUri(res.text()), (error) => {
//             if (error) console.log('Failed setting remote uri to cache', {error, remoteUri, encodedRemoteUri});
//           })
//         } else {
//           this.cache.removeItem(encodedRemoteUri, (error) => {
//             if (error) console.log('Failed removing remote uri from cache', {error, remoteUri, encodedRemoteUri});
//           });
//         }
//       });
//     });
//   };

//   render() {
//     const {source, ...otherProps} = this.props;
//     if (this.state.uri) {
//       return (<RemoteSVGImage
//         source={{ uri: this.state.uri }}
//         {...otherProps}
//       />)
//     } else {
//       return (<RemoteSVGImage
//         source={{...source, uri: source.uri}}
//         {...otherProps}
//       />)
      
//     }
    
//   }
// }

// export default FastRemoteSVGImage;

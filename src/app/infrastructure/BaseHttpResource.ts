import { HttpClient } from './interfaces/HttpClient';
import { HttpClientOptions, DefaultHttpClientOptions } from './interfaces/HttpClientOptions';

export class BaseHttpResource {
  private baseUrl: string;
  private httpClient: HttpClient;
  private defaultOptions: HttpClientOptions;
  private

  constructor(baseUrl: string,
              httpClient: HttpClient,
              defaultOptions = DefaultHttpClientOptions) {
    this.baseUrl = baseUrl;
    this.httpClient = httpClient;
    this.defaultOptions = defaultOptions;
  }

  post(url: string, body: any, options: HttpClientOptions) {
   // console.log('BODY', body);
    const { requestUrl, requestOptions } = this.resolveRequestData(url, options);
    const errorHandler = this.resolveErrorHandler(options);
    return this.httpClient.post(requestUrl, body, requestOptions)
      .catch((e) => this.processError(e, errorHandler));
  }

  put(url: string, body: any, options: HttpClientOptions) {
    const { requestUrl, requestOptions } = this.resolveRequestData(url, options);
    const errorHandler = this.resolveErrorHandler(options);
    return this.httpClient.put(requestUrl, body, requestOptions)
      .catch((e) => this.processError(e, errorHandler));
  }

  patch(url: string, body: any, options: HttpClientOptions) {
    const { requestUrl, requestOptions } = this.resolveRequestData(url, options);
    const errorHandler = this.resolveErrorHandler(options);
    return this.httpClient.patch(requestUrl, body, requestOptions)
      .catch((e) => this.processError(e, errorHandler));
  }

  get(url: string, body: any, options: HttpClientOptions) {
    const { requestUrl, requestOptions } = this.resolveRequestData(url, options);
    const getRequestOptions = Object.assign({}, { params: body }, requestOptions);
    const errorHandler = this.resolveErrorHandler(options);
    return this.httpClient.get(requestUrl, getRequestOptions)
      .catch((e) => this.processError(e, errorHandler));
  }

  delete(url: string, options: HttpClientOptions) {
    const { requestUrl, requestOptions } = this.resolveRequestData(url, options);
    const errorHandler = this.resolveErrorHandler(options);
    return this.httpClient.delete(requestUrl, requestOptions)
      .catch((e) => this.processError(e, errorHandler));
  }

  setHeaders(headers: HttpClientOptions['headers']) {
    this.defaultOptions.headers = Object.assign({}, this.defaultOptions.headers, headers);
  }

  clearHeaders() {
    delete this.defaultOptions.headers;
  }

  /**
   * @private
   * @param url
   * @param options
   * @returns {{requestUrl, requestOptions: *}}
   */
  resolveRequestData(url: string, options: HttpClientOptions) {
    const requestUrl = this.resolveRequestUrl(url);
    const requestOptions = this.resolveRequestOptions(options);
    return { requestUrl, requestOptions };
  }

  /**
   * @private
   * @param url
   * @returns {string}
   */
  resolveRequestUrl(url: string) {
    const urlPart = `/${url}`;
    return this.baseUrl + urlPart.replace('//', '/');
  }

  /**
   * @private
   * @param options
   * @returns {*}
   */
  resolveRequestOptions(options: HttpClientOptions) {
    const result = { ...this.defaultOptions, ...options };
    delete result.handleError;
    return result;
  }

  resolveErrorHandler(options: HttpClientOptions) {
    if (options.handleError != null && typeof options.handleError === 'function') {
      return options.handleError;
    }
    return this.defaultOptions.handleError;
  }

  processError(e: any, handleError: any) {
   // console.log('processERROR', e);
    if (handleError != null && typeof handleError === 'function') {
      return handleError(e);
    }
    throw e;
  }

  setErrorHandler(errorHandler: (e: any) => any) {
    this.defaultOptions.handleError = errorHandler as any;
  }

}

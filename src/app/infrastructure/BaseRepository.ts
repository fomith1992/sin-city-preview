import { BaseRestResource } from './BaseRestResource';

function isObject(obj: any) {
  return obj === Object(obj);
}

export class BaseRepository {
  entityIdName = 'id';
  protected restResource: BaseRestResource;

  constructor(restResource: BaseRestResource) {
    this.restResource = restResource;
  }

  save(entity: any): Promise<any> {
    return this[this.isEntityNew(entity) ? 'create' : 'update'](entity);
  }

  create(entity: any): Promise<any> {
    const entityData = this.prepareEntityForRequest(entity);
    return this.resource().create(entityData).then((res) => this.processResponse(res));
  }

  update(entity: any, entityData?: any): Promise<any> {
    if (entityData != null) {
      return this.resource(entity)
        .update(entityData).then((res) => this.processResponse(res));
    }

    if (this.isEntityNew(entity)) {
      return new Promise((resolve, reject) => reject(new Error('Repository#updarte(): Entity is new')));
    }

    const entityBody = this.prepareEntityForRequest(entity);
    return this.resource(entity)
      .update(entityBody).then((res) => this.processResponse(res));
  }

  patch(entity: any): Promise<any> {
    if (this.isEntityNew(entity)) {
      return new Promise((resolve, reject) => reject(new Error('Repository#patch(): Entity is new')));
    }
    const entityData = this.prepareEntityForRequest(entity);
    return this.resource(entity)
      .patch(entityData).then((res) => this.processResponse(res));
  }

  load(params = {}) {
    return this.resource().get(params)
      .then((res) => this.processResponse(res));
  }

  loadById(id: number, params?: any): Promise<any> {
    if (id == null) {
      return new Promise((resolve, reject) => reject(new Error('Repository#loadById(): id is undefined')));
    }
    return this.resource(id).get(params).then((res) => this.processResponse(res));
  }

  delete(entity: any): Promise<any> {
    if (this.isEntityNew(entity)) {
      return new Promise((resolve, reject) => reject(new Error('Repository#delete(): you can not update a new entity')));
    }
    return this.resource(entity).delete().then((res) => this.processResponse(res));
  }

  search(params: any): Promise<any> {
    const searchParams = this.resolveSearchParams(params);
    const requestBody = Object.assign({}, searchParams);
    return this.resource().get(requestBody)
      .then((res) => this.processResponse(res));
  }

  isEntityNew(entity: any): boolean {
    return entity && entity[this.entityIdName] == null;
  }

  resource(...params: any[]) {
    if (!params || params.length === 0) {
      return this.restResource;
    } else if (params.length === 1) {
      return this.restResource.child(this.resolveResourceParamLiteral(params[0]));
    }

    const resourcePath = params.reduce((result, param, index) => {
      if (index !== 0) {
        result += '/';
      }
      result += this.resolveResourceParamLiteral(param);
      return result;
    }, '');

    return this.restResource.child(resourcePath);
  }

  prepareEntityForRequest(entity: any) {
    // if (this.DataMapper) {
    //   return this.DataMapper.decode(entity);
    // }
    return entity;
  }

  async processResponse(response: any) {
    let result = { meta: null } as any;

    let responseJson;
    try {
      responseJson = response && await response.json();
    } catch(e) {}
    if (!responseJson) {
      return;
    }

    let data;
    let meta = null;

    if (responseJson.data && Array.isArray(responseJson.data)) {
      data = responseJson.data;
      meta = {
        page: responseJson.page,
        per_page: responseJson.size,
        total: responseJson.total_count
      };
    } else if (responseJson.data) {
      data = responseJson.data;
    } else {
      data = responseJson;
    }

    if (Array.isArray(data)) {
      result = data.map((entityData) => this.encodeEntity(entityData));
    } else {
      result = this.encodeEntity(data);
    }

    if (result != null) {
      result['meta'] = meta;
    }
  //  console.log('RESPONSE_RESULT', result);
  //  console.log('RESPONSE_RESULT');
    return result;
  }

  /**
   * Private helpers methods
   */

  resolveResourceParamLiteral(param: any) {
    if (isObject(param)) {
      return param[this.entityIdName];
    }
    return param;
  }

  resolveSearchParams(params: any) {
    if (params) {
      const result = Object.assign({}, params);
      if (params.page) {
        result.page = Number(result.page);
      }
      if (params.per_page) {
        result.size = Number(result.per_page);
        delete result.per_page;
      }

      return result;
    }
    return {};
  }

  encodeEntity(entityData: any) {
    // if (this.DataMapper) {
    //   return this.DataMapper.encode(entityData);
    // }

    return entityData;
  }
}

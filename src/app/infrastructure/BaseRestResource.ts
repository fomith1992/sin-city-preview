import { BaseHttpResource } from './BaseHttpResource';
import { HttpClientOptions } from './interfaces/HttpClientOptions';
export class BaseRestResource {

  Constructor = BaseRestResource;

  constructor(private httpResource: BaseHttpResource,
              private resourceUrl: string,
              private options?: HttpClientOptions) {
  }

  create(body?: any, options?: Partial<HttpClientOptions>) {
    return this.httpResource.post(this.resourceUrl, body, this.createOptions(options));
  }

  update(body?: any, options?: Partial<HttpClientOptions>) {
    return this.httpResource.put(this.resourceUrl, body, this.createOptions(options));
  }

  patch(body?: any, options?: Partial<HttpClientOptions>) {
    return this.httpResource.patch(this.resourceUrl, body, this.createOptions(options));
  }

  get(body?: any, options?: Partial<HttpClientOptions>) {
    return this.httpResource.get(this.resourceUrl, body, this.createOptions(options));
  }

  delete(options?: Partial<HttpClientOptions>) {
    return this.httpResource.delete(this.resourceUrl, this.createOptions(options));
  }

  child(...routeParts: string[]) {
    const baseUrl = this.resolveChildUrl(routeParts);
    return new this.Constructor(this.httpResource, baseUrl, this.options);
  }

  setBaseUrl(baseUrl: string) {
    this.resourceUrl = baseUrl;
  }

  resolveChildUrl(routeParts: any[]) {
    return routeParts.reduce((resultUrl, routePart) =>
      `${resultUrl}/${routePart}`, this.resourceUrl);
  }

  createOptions(options?: Partial<HttpClientOptions>) {
    return Object.assign({}, this.options, options);
  }
}

export class BaseRepositoryBuilder {
  constructor(repository, resource) {
    this.repository = repository;
    this.resource = resource;
  }

  build(resource = this.resource) {
    if (!resource) {
      throw new TypeError('BaseRepositoryBuilder#build(): Resource should be defined');
    }

    return new (this.repository)(resource);
  }
}

export class FetchHttpClient {

  /**
   * @public
   * @param {string} url
   * @param {any} body
   * @param {HttpClientOptions} options
   * @returns {Promise<Response> | *}
   */
  post(url, body, options) {
    const requestOptions = this._createRequestOptions('post', options, body);
    return this._fetchHandleCode(url, requestOptions);
  }

  /**
   * @public
   * @param {string} url
   * @param {any} body
   * @param {HttpClientOptions} options
   * @returns {Promise<Response> | *}
   */
  put(url, body, options) {
    const requestOptions = this._createRequestOptions('put', options, body);
    return this._fetchHandleCode(url, requestOptions);
  }

  /**
   * @public
   * @param {string} url
   * @param {any} body
   * @param {HttpClientOptions} options
   * @returns {Promise<Response> | *}
   */
  patch(url, body, options) {
    const requestOptions = this._createRequestOptions('patch', options, body);
    return this._fetchHandleCode(url, requestOptions);
  }

  /**
   * @public
   * @param {string} url
   * @param {HttpClientOptions} options
   * @returns {Promise<Response> | *}
   */
  get(url, options) {
    const query = options.params ? this._getQueryString(options.params) : '';
    const requestOptions = this._createRequestOptions('get', options);
    return this._fetchHandleCode(`${url}?${query}`, requestOptions);
  }

  /**
   * @public
   * @param {string} url
   * @param {HttpClientOptions} options
   * @returns {Promise<Response> | *}
   */
  delete(url, options) {
    const requestOptions = this._createRequestOptions('delete', options);
    return this._fetchHandleCode(url, requestOptions);
  }

  // Private helpers

  _fetchHandleCode(url, options) {
    // console.log(url, options)
    return new Promise((resolve, reject) => {
      fetch(url, options)
        .then(response => {
          if (response && response.status <= 206) {
            resolve(response);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  _getQueryString(params) {
    return Object
      .keys(params)
      .map(k => {
        if (params[k] === null || params[k] === undefined) { return ''; }
        if (Array.isArray(params[k])) {
          return `${encodeURIComponent(k)}=${params[k].join(',')}`;
        }
        if (k === 'purpose_datings') {
          return `${encodeURIComponent(k)}=${params[k]}`
        }
        return `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`

      })
      .filter(Boolean)
      .join('&');
  }

  /**
   * @private
   * @param {string} method
   * @param {HttpClientOptions} options
   * @param {any} body
   * @returns {any}
   */
  _createRequestOptions(method, options, body) {
    const result = {
      method,
      headers: this._resolveHeaders(options),
      mode: options.mode,
      cache: options.cache,
      credentials: options.credentials,
      redirect: options.redirect,
      referrer: options.referrer
    };

    if (body) {
      result['body'] = this._decodeBody(body, options)
    }

    return result;
  }

  /**
   * @private
   * @param {any} body
   * @param {HttpClientOptions} options
   */
  _decodeBody(body, options) {
    if (options.contentType === 'json') {
      return JSON.stringify(body);
    }

    // todo other content types
    return body;
  }

  _resolveHeaders(options) {
    const additionalHeaders = {};
    if (options.contentType === 'json') {
      additionalHeaders['Content-Type'] = 'application/json';
    } else if (options.contentType === 'form-data') {
      additionalHeaders['Content-Type'] = 'multipart/form-data';
    }
    if (options.responseType === 'json') {
      additionalHeaders['Accept'] = 'application/json';
    }

    return Object.assign({}, options.headers, additionalHeaders);
  }

}

export interface Response<T> {
  count: number
  next: any
  previous: any
  results: Array<T>
}
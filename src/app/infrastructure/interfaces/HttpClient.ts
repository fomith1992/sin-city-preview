import { HttpClientOptions } from './HttpClientOptions';

export interface HttpClient {
  post: (url: string, body: Object, options: HttpClientOptions) => Promise<any>;

  put: (url: string, body: Object, options: HttpClientOptions) => Promise<any>;

  patch: (url: string, body: Object, options: HttpClientOptions) => Promise<any>;

  get: (url: string, options: HttpClientOptions) => Promise<any>;

  delete: (url: string, options: HttpClientOptions) => Promise<any>;
}
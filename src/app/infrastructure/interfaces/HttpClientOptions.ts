/**
 * @typedef {Object} HttpClientOptions
 * @property {object} headers
 * @property {'json'} responseType    // todo add other types
 * @property {'json'} contentType    // todo add other types
 * @property {'cors'|'no-cors'|'same-origin'} mode [same-origin]
 * @property {'default'|'no-cache'|'reload'|'force-cache'|'only-if-cached'} cache [default]
 * @property {'same-origin'|'include'|'omit'} credentials [same-origin]
 * @property {'manual'|'follow'|'error'} redirect [follow]
 * @property {'no-referrer'|'client'} referrer [client]
 * @property {boolean} timeOffset - add timeOffset into headers
 */

export type HttpClientOptions = {
  headers?: Object
  responseType: string
  contentType: string
  'Content-Type'?: string
  accessType: string
  mode?: 'cors'|'no-cors'|'same-origin'
  cache?: 'default'|'no-cache'|'reload'|'force-cache'|'only-if-cached'
  credentials?: 'same-origin'|'include'|'omit'
  redirect?: 'manual'|'follow'|'error'
  referrer?: 'no-referrer'|'client'
  timeOffset: boolean
  handleError: () => any
}

export const DefaultHttpClientOptions: HttpClientOptions = {
  headers: {},
  responseType: 'json',
  contentType: 'json',
  accessType: 'json',
  mode: 'same-origin',
  cache: 'default',
  credentials: 'same-origin',
  redirect: 'follow',
  referrer: 'client',
  timeOffset: true,
  handleError: null
};

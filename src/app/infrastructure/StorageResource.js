import { AsyncStorage } from "react-native";

export class StorageResource {

  _storage = AsyncStorage;

  constructor(storageKey) {
    this._storageKey = storageKey;
  }

  load() {
    return this._storage.getItem(this._storageKey).then((response) => JSON.parse(response));
  }

  save(data) {
    return this._storage.setItem(this._storageKey, JSON.stringify(data));
  }

}
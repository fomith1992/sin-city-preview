import React, { useState, useEffect } from 'react';
// import { Dimensions, Platform } from "react-native";
// import { getStatusBarHeight } from 'react-native-status-bar-height';

export default (handler, interval) => {
  const [intervalId, setIntervalId] = useState();
  useEffect(() => {
    const id = setInterval(handler, interval);
    setIntervalId(id);
    return () => clearInterval(id);
  }, []);
  return () => clearInterval(intervalId);
};

export function onePromisePerTime() { 
  let isHandling = false; 
  return (promise: Promise<any>) => {
    if (!isHandling) {
      isHandling = true;
      return promise.finally(() => { isHandling = false; }) 
    } else {
      return new Promise(res => res());
    }
  };
}

// export function arrayToObjectById(items, idSelector = 'id') {
//   return items.reduce((r, item) => {
//     r[item[idSelector]] = item;
//     return r;
//   }, {});
// }

// export function updateItemInArray(array, item, index) {
//   const newArray = [...array];
//   newArray[index] = item;

//   return newArray;
// }

// export function debounce(f, ms) {
//   let timer = null;
//   return function (...args) {
//     const onComplete = () => {
//       f.apply(this, args);
//       timer = null;
//     };

//     if (timer) {
//       clearTimeout(timer);
//     }

//     timer = setTimeout(onComplete, ms);
//   }
// }

// export function makeCancelable(promise) {
//   let hasCanceled_ = false;

//   const wrappedPromise = new Promise((resolve, reject) => {
//     promise.then(
//       val => hasCanceled_ ? reject({isCanceled: true}) : resolve(val),
//       error => hasCanceled_ ? reject({isCanceled: true}) : reject(error)
//     );
//   });

//   return {
//     promise: wrappedPromise,
//     cancel() {
//       hasCanceled_ = true;
//     },
//   };
// }

// export function debounceMax(f, ms, maxMs) {
//   let maxTimer;
//   let timer = null;
//   return function (...args) {
//     const onComplete = () => {
//       f.apply(this, args);
//       if (timer) clearTimeout(timer);
//       if (maxTimer) clearTimeout(timer);
//       timer = null;
//       maxTimer = null;
//     };
//     if (!maxTimer) {
//       maxTimer = setTimeout(onComplete, maxMs);
//     }

//     if (timer) {
//       clearTimeout(timer);
//     }

//     timer = setTimeout(onComplete, ms);
//   }
// }

// export function getSizeContain(originalSize, containerSize) {
//   const K = originalSize.width / containerSize.width;
//   let width = Math.round(originalSize.width / K);
//   let height = Math.round(originalSize.height / K);

//   if (height > containerSize.height) {
//     const K2 = originalSize.height / containerSize.height;
//     height = containerSize.height;
//     width = Math.round(originalSize.width / K2);
//   }

//   return { width, height };
// }

// export function isPortrait() {
//   const dim = Dimensions.get('screen');
//   return dim.height >= dim.width;
// }

// export function getScreenSize() {
//   const wW = Dimensions.get('window').width;
//   // const hH = Platform.OS === 'android' ? Dimensions.get('window').height - getStatusBarHeight() : Dimensions.get('window').height;
//   const hH = Dimensions.get('window').height - getStatusBarHeight();
//   return { wW, hH };
// }

// export function formatCount(value) {
//   let count = value || '1247';
//   if (count.length > 3) {
//     count = count.split('');
//     const preparingCount = [];
//     count.reverse().forEach((number, index) => {
//       if (index !== 0 && index % 3 === 0) preparingCount.unshift(',');
//       preparingCount.unshift(number);
//     });
//     count = preparingCount.join('');
//   }
//   return count;
// }

// export function mayAskPermissions(response) {
//   if (Platform.OS === 'ios') {
//     return response !== 'denied' && response !== 'restricted';
//   } else {
//     return  response === 'denied' || response === 'undetermined';
//   }
// }
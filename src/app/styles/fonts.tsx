export default function fontFamily(name: 'fontTitleBold' | 'fontBaseMedium' | 'fontBaseRegular') {
  let fontFamily;
  switch (name) {
    case 'fontTitleBold':
      fontFamily = 'Montserrat-SemiBold';
      break;
    case 'fontBaseMedium':
      fontFamily = 'Montserrat-Medium';
      break;
    case 'fontBaseRegular':
      fontFamily = 'Montserrat-Regular';
      break;
  }

  return { fontFamily };
}
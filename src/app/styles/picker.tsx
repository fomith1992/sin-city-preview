import fontFamily from './fonts';

const input = {
  color: '#fff',
  fontSize: 16,
  marginLeft: 12,
  ...fontFamily('fontBaseMedium')
};

export default {
  viewContainer: { 
    flex: 1, 
    justifyContent: 'center'
  },
  inputIOS: input,
  inputAndroid: input
}
import React, {useRef, useState, useEffect} from 'react';
import {StatusBar, BackHandler} from 'react-native';
import {locale} from 'moment';
import 'moment/locale/ru';
import {NavigationActions} from 'react-navigation';
import {Provider} from 'mobx-react';
import FlashMessage, {showMessage} from 'react-native-flash-message';
import firebase, {RNFirebase} from 'react-native-firebase';
import Geolocation from 'react-native-geolocation-service';
import 'react-native-gesture-handler';
import notifications from './modules/notifications/domain/services/Notifications';
import {Settings, AppEventsLogger} from 'react-native-fbsdk-next';
import appsFlyer from 'react-native-appsflyer';

import * as Sentry from '@sentry/react-native';

import AppNavigator from './modules/navigation/navigators';
import RootStore from './rootStore';
import {wsConnect} from './utils/webSocketConnect';
// import { enableLogging } from 'mobx-logger';

import LayoutComponentsOverflowLoader from './modules/layout/ui/components/overflow-loader';
import LayoutComponentModalMessage from './modules/layout/ui/components/modal-message';
import {FirebaseManager} from './utils/FIrebaseManager';

// if (__DEV__) {
//   const config = {
//     predicate: () => __DEV__,
//     action: true,
//     reaction: true,
//     transaction: true,
//     compute: true,
//   };
//   enableLogging(config);
// }
var userExists = true;
window.setUserExists = (quantor) => {
  userExists = quantor;
  // alert(quantor);
};
window.getUserExists = () => {
  return userExists;
};
const App = (props) => {
  //добавить сокет
  //обратбока () Х
  // рарарар ЧИ
  // Ъ

  // Sentry.init({
  //   dsn: 'https://2bfb982a3b474385a160ccb9efc6ca34@sentry.io/5172872',
  // });
  //throw new Error("My first Sentry error!");
  firebase.crashlytics().enableCrashlyticsCollection();
  const FM = FirebaseManager();
  // Ask for consent first if necessary
  appsFlyer.initSdk(
    {
      devKey: 'GULegkZsG2ifj5ZQ4ZkidP',
      isDebug: false,
      appId: 'id1492086999',
      onInstallConversionDataListener: true, //Optional
      onDeepLinkListener: true, //Optional
      timeToWaitForATTUserAuthorization: 10 //for iOS 14.5
    },
    (result) => {
     // console.log("appsFlyer.initSdk_success", result);
    },
    (error) => {
     // console.error("appsFlyer.initSdk_error", error);
    }
  );
  // Possibly only do this for iOS if no need to handle a GDPR-type flow
  Settings.initializeSDK();
  Settings.setAdvertiserTrackingEnabled(true)
    .then((res) => {
     // console.log('setAdTracking_then', res);
    })
    .catch((err) => {
     // console.log('setAdTracking_err', err);
    });

  // AppEventsLogger.logEvent(AppEventsLogger.AppEvents.CompletedRegistration, {
  //   [AppEventsLogger.AppEventParams.RegistrationMethod]: 'email',
  // });

  const navigatorRef = useRef(null);
  const [rootStore] = useState(new RootStore());
  const storesProviders = useRef(rootStore.getStoresProviders());
  const getActiveRouteName = (navigationState) => {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
      return getActiveRouteName(route);
    }
    return route.routeName;
  };
  const onBackPress = () => {
    navigatorRef.current.dispatch(NavigationActions.back());
    return true;
  };

  //firebase.analytics().setAnalyticsCollectionEnabled(true);
  useEffect(() => {
    locale('ru');

    //wsConnect('bad', 'dab');
    //window.dialogsMainOnScreenFocus()

    Geolocation.setRNConfiguration({
      authorizationLevel: 'whenInUse',
      skipPermissionRequests: false,
    });
    rootStore.emitAfterStoreInit();
    rootStore.navigationStore.createRef(navigatorRef.current);
    BackHandler.addEventListener('hardwareBackPress', onBackPress);
    notifications.checkIsNotificationTapped();
    return () => {
      notifications.destroy();
      BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    };
  }, [rootStore.navigationStore]);
  return (
    <>
      <Provider {...storesProviders.current}>
        <StatusBar backgroundColor="black" barStyle="light-content" />
        <AppNavigator
          ref={navigatorRef}
          {...props}
          onNavigationStateChange={(prevState, newState) => {
            const newRoute = getActiveRouteName(newState);
            const prevRoute = getActiveRouteName(prevState);

            if (prevRoute != newRoute) {
              if (newRoute === 'USERS_SCREENS_PAYMENT_REQUIRED_SCREEN') {
                window.fLogEvent('app_warningScreen', 'subscription');
              } else if (newRoute === 'VERIFICATION_REQUIRED') {
                window.fLogEvent('app_warningScreen', 'verification');
              } else if (newRoute === 'WELCOME') {
                window.fLogEvent('onboarding_viewScreen', 1);
                window.fLogEvent('app_viewScreen', newRoute);
              } else if (newRoute === 'PROFILE_SECOND_STEP_SCREEN') {
                window.fLogEvent('registration_viewScreen', 'reg_screen_2');
              } else {
                window.fLogEvent('app_viewScreen', newRoute);
              }
            }
          }}
        />
        <FlashMessage duration={6000} position="top" />
        <LayoutComponentsOverflowLoader />
        <LayoutComponentModalMessage />
      </Provider>
    </>
  );
};

export default App;

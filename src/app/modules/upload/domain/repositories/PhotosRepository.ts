import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import photosResource from "../resources/PhotosResource";

class PhotosRepository extends BaseRepository {
  constructor() {
    super(photosResource)
  }
}

const photosRepository = new PhotosRepository();

export default photosRepository; 
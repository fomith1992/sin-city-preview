import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../../modules/core/infrastructure/httpResource";

class PhotosResource extends BaseRestResource {
    constructor() {
        super(httpResource, 'photos');
    }
}

const photosResource = new PhotosResource();

export default photosResource;
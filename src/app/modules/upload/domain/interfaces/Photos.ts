export interface CreatePhotosResponse {
  id: number
  photo: string
  photoUrl: string
  photoBlurUrl: string
  photoThumbnailUrl: string
  photoThumbnailBlurUrl: string
  createdAt: string
}
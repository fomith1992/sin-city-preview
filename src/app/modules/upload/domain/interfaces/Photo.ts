export interface Photo {
  id: number
  photo: string
  photoUrl: string
  photoBlurUrl: string
  photoThumbnailUrl: string
  photoThumbnailBlurUrl: string
  countLikes?: number
  iLikedIt: boolean
  createdAt: string
  isAvatar: boolean
}
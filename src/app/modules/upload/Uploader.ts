import { Platform } from 'react-native';
import { ImagePickerResponse } from 'react-native-image-picker';
import { CreatePhotosResponse } from './domain/interfaces/Photos';
const mime = require('mime');
import photosResource from './domain/resources/PhotosResource';

class Uploader {

  public upload(photo: ImagePickerResponse): Promise<CreatePhotosResponse> {
    const data = this.prepareFormData(photo);
    return photosResource.create(data, { contentType: 'multipart/form-data' })
      .then((response: any) => response.json())
  }

  public prepareFormData(photo: ImagePickerResponse) {
    const hasExt = mime.getType(photo.uri);

    const data = new FormData();
    let name = photo.uri.split('/').pop();
    let uri = Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "");

    if (!hasExt) {
      const ext = mime.getExtension(photo.type);
      name = `${name}.${ext}`;
    }

    data.append("photo", {
      type: photo.type,
      name,
      uri
    });
    return data;
  }
}

const uploader = new Uploader();

export default uploader;
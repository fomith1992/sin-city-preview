import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../../modules/core/infrastructure/httpResource";
import { City } from './../interfaces/City';

interface data {
  count:	number
  next:	string
  previous:	string
  results: City[]
}

class CitiesResource extends BaseRestResource {
  constructor() {
    super(httpResource, 'cities', {});
  }
  loadCities(): Promise<data> {
    return this.get().then((res: any) => res.json());
  }
}

const citiesResource = new CitiesResource();

export default citiesResource;
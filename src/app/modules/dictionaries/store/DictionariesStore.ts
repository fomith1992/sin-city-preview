import { observable, computed } from 'mobx';
import citiesResource from '../domain/resources/CitiesResource';

import { City } from './../domain/interfaces/City';

export class DictionariesStore {
  @observable cities: City[] = [];
  constructor() {
    this.loadData();
  }

  @computed get getCities() {
    return this.cities.map((city: City) => ({ label: city.title, value: city.id }));
  }

  private loadData() {
    citiesResource.loadCities()
      .then((response) => { this.cities = response.results; })
  }
}

export default DictionariesStore;
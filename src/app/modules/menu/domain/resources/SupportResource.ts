import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../../modules/core/infrastructure/httpResource";

class SupportResource extends BaseRestResource {
  constructor() {
    super(httpResource, 'feedback');
  }
}

const supportResource = new SupportResource();

export default supportResource;
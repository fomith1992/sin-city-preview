import { CommunicationType } from './../interfaces/CommunicationType';
import { showMessage } from 'react-native-flash-message';
import supportRepository from '../repositories/SupportRepository';

class SupportService {
  public sendMessage(message: string, typeCommunication: CommunicationType) {
    return supportRepository.create({
      message,
      typeCommunication
    });
  }
}

const supportService = new SupportService();
export default supportService;

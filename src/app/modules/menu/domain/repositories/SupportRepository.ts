import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import supportResource from "../resources/SupportResource";

class SupportRepository extends BaseRepository {
  constructor() {
    super(supportResource)
  }
}

const supportRepository = new SupportRepository();

export default supportRepository; 
import { StyleSheet } from 'react-native';
import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    marginTop: 10,
    marginRight: 20
  },
  infoRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: 2
  },
  label: {
    ...fontFamily('fontBaseRegular'),
    color: '#959595',
    marginRight: 6,
  },
  value: {
    ...fontFamily('fontBaseRegular'),
    color: '#fff'
  },
})
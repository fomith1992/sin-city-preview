import React  from 'react';
import { View, Text } from 'react-native';
import { getVersion, getBuildNumber } from 'react-native-device-info';

import styles from "./style";

const MenuComponentsAppInfo = () => {
    const version = getVersion();
    const buildNumber = getBuildNumber();
    return (
      <View style={styles.container}>
        <View style={styles.infoRow}>
          <Text style={styles.label}>version</Text>
          <Text style={styles.value}>{version}</Text>
        </View>
        <View style={styles.infoRow}>
          <Text style={styles.label}>build number</Text>
          <Text style={styles.value}>{buildNumber}</Text>
        </View>
      </View>
    );
  };

export default MenuComponentsAppInfo;
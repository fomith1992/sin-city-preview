import React, { useRef, memo, useCallback } from 'react';
import { Text, TouchableOpacity, Image } from 'react-native';

import icons from '../../../../icons';
import styles from './style';

type props = {
  value: any
  label: string
  onPress: (p: any) => void
  textColor?: string
};

const MenuItemsListItem = memo(({ label, onPress, textColor, value }: props) => {
  const color = textColor ? useRef({ color: textColor }) : useRef({});
  const onPressed = useCallback(() => { onPress(value) }, [value]);
  return (
    <TouchableOpacity style={styles.container} onPress={onPressed}>
      <Text style={[styles.label, color.current]}>{label}</Text>
      <Image source={icons.navBack} style={styles.arrow}/>
    </TouchableOpacity>
  );
});

export default MenuItemsListItem;
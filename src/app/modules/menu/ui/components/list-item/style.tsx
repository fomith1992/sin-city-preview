import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: { backgroundColor: '#000', flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 15, paddingHorizontal: 20, borderBottomColor: '#ffffff33', borderBottomWidth: 1 },
  label: { color: '#fff', fontSize: 16, ...fontFamily('fontBaseMedium') },
  arrow: { transform: [{ rotate: '180deg' }]}
})
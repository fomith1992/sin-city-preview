import React, { useCallback } from 'react';
import { Alert, SafeAreaView, ScrollView, Linking, Platform, View } from 'react-native';
import { inject, observer } from "mobx-react";
import Navigation from "react-navigation-mobx-helpers";
import { getVersion, getBuildNumber } from 'react-native-device-info';

import Header from '../../../../layout/ui/components/header';
import MenuItemsListItem from '../../components/list-item';

import * as screenNames from '../../../../navigation/screen-names';

import styles from "./style";
import icons from '../../../../icons';
import { AuthUserStore } from "../../../../auth/store/auth-user";
import { TYPES } from "../../../../../store/store-types";
// import MenuComponentsAppInfo from "../../components/app-info";

const userManualLink = {
  uri: 'https://scinfo.ru/SC_user_manual.pdf',
  cache: true
};
const politicLink = {
  uri: 'https://scinfo.ru/privacy_policy.pdf',
  cache: true
};

const listItems = [
  { label: 'Мой профиль', path: screenNames.MY_PROFILE_SCREEN, name: 'myprofile' },
  { label: 'Подтверждение аккаунта', path: screenNames.VERIFICATION_FIRST_STEP_SCREEN, name: 'verification' },
  { label: 'Оформление подписки', path: screenNames.GET_PREMIUM_SCREEN, name: 'premium' },
  // { label: 'Восстановление покупки ', path: '' }, // next epic
  // { label: 'Избранные пользователи', path: screenNames.FAVOURITES_SCREEN, name: 'favorite' },
  //{ label: 'Симпатии', path: screenNames.CONNECTIONS_SCREEN, name: 'sympathy' },
  { label: 'Черный список', path: screenNames.BLACK_LIST_SCREEN, name: 'blacklist' },
  { label: 'Руководство пользователя', link: userManualLink, name: 'manual' },
  { label: 'Настройка push-уведомлений', path: screenNames.MENU_NOTIFICATIONS_SETTINGS, name: 'push' },
  //{ label: 'Последние уведомления', path: screenNames.NOTIFICATIONS_SCREEN, name: 'notifications' },
  { label: 'Обратная связь', path: screenNames.SUPPORT_SCREEN, name: 'feedback' },
  { label: 'Политика конфиденциальности', link: politicLink, name: 'privacy_policy' },
  { label: 'Выйти из аккаунта', textColor: '#e84747', name: 'logout' },
  { label: 'Удалить аккаунт', textColor: '#e84747', path: screenNames.REMOVE_PROFILE_SCREEN, name: 'delete_account' },
];

type props = {
  authUserStore: AuthUserStore,
  navigationStore: Navigation,
  navigation: any
}

const MenuScreensIndex = inject(TYPES.AuthUser, TYPES.Navigation)
  (observer(({ authUserStore, navigationStore, navigation }: props) => {
    const onMenuItemPressed = useCallback((listItem) => {
      if (listItem.name !== 'delete_account') {
        window.fLogEventObj('menu_clickItem', {item_name: listItem.name})
      } else {
        window.fLogEventObj('menu_clickDelete', {})
      }
      if (listItem.path) { navigationStore.navigate(listItem.path); }
      else if (listItem.link) {
        Platform.OS === 'android'
          ? navigation.navigate(screenNames.PDF_VIEWER, {url: listItem.link})
          : Linking.openURL(listItem.link.uri).then(() => {})
      }
    },[]);

    const onProfilePressed = useCallback(() => {
      window.fLogEventObj('menu_clickItem', {item_name: 'myprofile'});
      navigation.navigate(screenNames.MY_PROFILE_SCREEN); //EVENTS_TAB
    }, []);

    const onEventsPressed = useCallback(() => {
      navigation.navigate(screenNames.EVENTS_TAB);
    }, []);

    const onNotificationPressed = useCallback(() => {
      window.fLogEventObj('menu_clickItem', {item_name: 'notifications'});
      navigation.navigate(screenNames.NOTIFICATIONS_SCREEN);
    }, []);

    const onLogoutPressed = useCallback(() => {
      window.fLogEventObj('menu_clickExit', {});
      authUserStore.logout();
    },[]);

    return (
      <SafeAreaView style={styles.container}>
        <Header
          title="Меню"
          onPress={() => onProfilePressed()}
          icon={icons.profileMenu}
          leftIcon={icons.notificationIcon}
          onBack={() => onNotificationPressed()}
          leftIconSecond={icons.eventsIcon}
          onBackSecond={() => onEventsPressed()}
        />
        <ScrollView
          style={styles.scrollView}
          bounces={false}
          showsHorizontalScrollIndicator={false}>
          {listItems.map((listItem, index) => {
            const textColor = listItem.textColor ? listItem.textColor : undefined;

            if (listItem.name === 'logout') {
              return (
                <MenuItemsListItem
                  value={listItem}
                  key={listItem.label}
                  label={listItem.label}
                  textColor={textColor}
                  onPress={onLogoutPressed}/>
              );
            }

            return (
              <MenuItemsListItem
                value={listItem}
                key={listItem.label}
                label={listItem.label}
                textColor={textColor}
                onPress={onMenuItemPressed}/>
            );
          })}
        </ScrollView>
      </SafeAreaView>
    );
  }));

(MenuScreensIndex as any).navigationOptions = {
  // tabBarOnLongPress: () => {
  // //  Alert.alert('Application info.', `innerVersion: 1.4.3\nversion: ${getVersion()}\nbuild number: ${getBuildNumber()}`)
  // }
};

export default MenuScreensIndex;

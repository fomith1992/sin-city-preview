import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000'
  },
  multilineTextInputContainer: {
    maxHeight: 160,
    paddingHorizontal: 20,
    paddingTop: 16
  },
  multilineTextInput: {
    height: '100%',
    backgroundColor: '#fff',
    borderRadius: 17,
    paddingHorizontal: 15,
    paddingTop: 15,
    paddingBottom: 35,
    fontSize: 14, ...fontFamily('fontBaseMedium')
  },
  optionsContainer: { paddingHorizontal: 20, marginTop: 25, flex: 1 },
  checkboxContainer: { flexDirection: 'row', paddingVertical: 10 },
  checkboxLabel: { fontSize: 16, color: '#fff', ...fontFamily('fontBaseMedium') },
  checkBox: {
    width: 19,
    height: 19,
    borderRadius: 50,
    borderColor: '#ffbd21',
    borderWidth: 2,
    marginRight: 18,
    justifyContent: 'center',
    alignItems: 'center'
  },
  checkBoxInner: { width: 9, height: 9, borderRadius: 50 },
  charactersCounter: {
    position: 'absolute',
    bottom: 13,
    right: 35,
    color: '#999',
    fontSize: 9, ...fontFamily('fontBaseMedium')
  },
  bottomButton: {
    marginHorizontal: 40,
    marginTop: 10,
    marginBottom: 20
  },
})
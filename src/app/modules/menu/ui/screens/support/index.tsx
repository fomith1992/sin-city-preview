import React, {useState, useCallback, useRef, useEffect} from 'react';
import { ScrollView, SafeAreaView, TextInput, View, Text, TouchableOpacity, KeyboardAvoidingView, Platform } from 'react-native';
import { inject, observer } from 'mobx-react';
import { TYPES, ITYPES } from '../../../../../store/store-types';

import styles from './style';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import { CommunicationType } from '../../../domain/interfaces/CommunicationType';
import { beautifyPhoneNumber } from '../../../../ud-ui/helpers/beautifyString';

const placeholder = 'Если у вас есть предложения по улучшению приложения, вы можете оставить их здесь';

const options = [
  { label: (number: string) => `SMS (${number})`, type: 'PHONE' },
  { label: (number: string) => `WhatsApp (${number})`, type: 'WHATSAPP' },
  { label: (number: string) => `Telegram (${number})`, type: 'TELEGRAM' },
  { label: () => 'Без связи', type: 'OTHER' }
] as { label: (number: string) => string, type: CommunicationType }[];

const SupportScreen = inject(TYPES.CurrentUserStore)(observer(
  ({ currentUserStore, navigation }: { currentUserStore: ITYPES['CurrentUserStore'], navigation: any }) => {
    useEffect(() => {
      window.fLogEventObj('feedback_openScreen', {screen_name: 'feedback'});
    }, []);
    const [message, setMessage] = useState('');
    const [selectedOption, setSelectedOption] = useState(0);
    const onMessageChange = useCallback((message) => { message.length <= 200 && setMessage(message); }, []);
    const onBackPress = useCallback(() => { navigation.pop(); }, []);
    const onSendPress = useCallback(() => {
      currentUserStore.sendFeedback(message, options[selectedOption].type);
      if (message === '') {
        window.fLogEventObj('feedback_clickSendBtn', {status_feedback: 'error'});
      }
    }, [message, selectedOption]);

    return (
      <SafeAreaView style={styles.container}>
        <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                              enabled
                              style={styles.container}>
          <Header title="Обратная связь" onBack={onBackPress}/>
          <ScrollView keyboardShouldPersistTaps="never">
            <View style={styles.multilineTextInputContainer}>
              <TextInput
                value={message}
                multiline
                textAlignVertical="top"
                numberOfLines={10}
                placeholderTextColor="#999"
                style={styles.multilineTextInput}
                placeholder={placeholder}
                onChangeText={onMessageChange}>
              </TextInput>
              <Text style={styles.charactersCounter}>{`${message.length} / 200`}</Text>
            </View>
            <View style={styles.optionsContainer}>
            {options.map(({ label }, index) =>
              <TouchableOpacity key={index}
                                activeOpacity={.9}
                                onPress={() => { setSelectedOption(index); }}
                                style={styles.checkboxContainer}>
                <View  style={styles.checkBox}>
                  <View style={[styles.checkBoxInner, { backgroundColor: selectedOption === index ? '#ffbd21' : '#000' }]} />
                </View>
                <Text style={styles.checkboxLabel}>{label(beautifyPhoneNumber(currentUserStore.user?.phone))}</Text>
              </TouchableOpacity>
            )}
            </View>

          </ScrollView>
        </KeyboardAvoidingView>
        <UDButton title="Отправить"
                  onPress={onSendPress}
                  style={styles.bottomButton}/>
      </SafeAreaView>
    );
  }
));

export default SupportScreen;

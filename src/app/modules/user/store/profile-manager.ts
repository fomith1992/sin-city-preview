import { PurposeDating } from './../domain/enums/PurposeDating';
import { PurposeGenderEnum } from './../../user/domain/enums/PurposeGender';
import { NavigationParams } from 'react-navigation';
import { observable, action, computed, comparer } from 'mobx';
import { FamilyStatusEnum } from './../domain/enums/FamilyStatus';
import { GendersEnum } from './../domain/enums/Gender';
import * as _ from 'lodash';
import { formatLastActivity } from './../../ud-ui/helpers/dateFormat';
import { ProfileService } from './../domain/services/ProfileService';
import * as screenNames from '../../navigation/screen-names';
import { BaseStore } from './../../../store/BaseStore';
import { TYPES, ITYPES } from './../../../store/store-types';
import { ProfileManager } from './../domain/interfaces/ProfileManager';
import { User } from './../domain/interfaces/User';
import { showMessage } from 'react-native-flash-message';
import { onePromisePerTime } from '../../../infrastructure/helpers';

import userRepository from '../domain/repositories/UserRepository';

export class ProfileActionManager extends BaseStore implements ProfileManager {
  protected likePhotoRequestWrapper = onePromisePerTime();
  protected likeRequestWrapper = onePromisePerTime();
  protected favouriteRequestWrapper = onePromisePerTime();

  public currentUserId: number = -1;
  public userCoordinates: any = null;
  @observable isCurrentUserNotVerified: boolean = false;
  @observable isLoading: boolean = false;

  protected navigation: ITYPES['Navigation'];
  protected layout: ITYPES['Layout'];
  protected chatsStore: ITYPES['ChatsStore'];
  private authStore: ITYPES['AuthUser'];

  storeInit() {
    this.navigation = this.root.resolve(TYPES.Navigation);
    this.layout = this.root.resolve(TYPES.Layout);
    this.chatsStore = this.root.resolve(TYPES.ChatsStore);
    this.authStore = this.root.resolve(TYPES.AuthUser);
  }

  get users(): User[] {
    return [];
  }

  set users(users: User[]) {

  }

  @action
  protected updateUser(user: User) {
    if (this.root.users[user.id]) {
      Object.assign(this.root.users[user.id], user);
    } else {
      Object.assign(this.root.users, {
        [user.id]: user
      });
    }
  }

  protected removeUser(id: number) {
    delete this.root.users[id];
  }

  protected getUser(id: number) {
    return this.root.users[id];
  }

  protected getUserPhotos(id: number) {
    return this.root.users[id].photos;
  }

  public openConnectionsWithFieldsError() {
    this.navigation.navigate(screenNames.CONNECTIONS_SCREEN);
    this.navigation.navigate(screenNames.USERS_SCREENS_INFO_FIELDS_REQUIRED_SCREEN);
  }

  public openProfile(id: number, param: NavigationParams) {
    this.currentUserId = id;
    try {
      this.loadUserInformation(id);
      this.navigation.navigate(screenNames.PROFILE_SCREEN, param);
    } catch(e) {}
  }
    // переход к определенному юзеру на карте по пушу
    public openUserOnMap(userCoordinates, param: NavigationParams) {
      console.log('openUserOnMap coords', userCoordinates);
      this.userCoordinates = userCoordinates;
        this.navigation.navigate(screenNames.LOCATION_TAB);
        // this.currentUserId = id;
        // try {
        //     this.loadUserInformation(id);
        //     this.navigation.navigate(screenNames.PROFILE_SCREEN, param);
        // } catch(e) {}
    }

  @action
  public async loadUserInformation(id: number) {
    this.isLoading = true;
    try {
      const user = await userRepository.loadById(id);
      const { results } = await userRepository.loadPhotos(id);
      const photos = {};
      results.forEach((photo: User['photo']) => Object.assign(photos, { [photo.id]: photo }));
      this.updateUser({
        ...user,
        photos
      });
    } catch(err) {
      if (err.code == 102) {
        this.isCurrentUserNotVerified = true;
      } else if (err.code == 103) {
        window.fLogEvent('app_errorScreen',
          err.detail);
        if (!!err.detail && err.detail !== 'None') {
          showMessage({ message: err.detail, type: 'danger' });
        }
        this.navigation.pop();
      } else {
        window.fLogEvent('app_errorScreen',
          err.detail || 'неизвестная ошибка');
        if (!!err.detail && err.detail !== 'None') {
          showMessage({message: err.detail || 'неизвестная ошибка', type: 'danger'});
        }
        this.navigation.pop();
      }
    } finally {
      this.isLoading = false;
    }
  }

  doAction(actionIndex: number, isComedFromChat?: boolean) {
    if (actionIndex === 0) {
      window.fLogEventObj('profile_clickBlockBtn', {user_id: this.currentUserId});
      this.blockUser(this.currentUserId, { isComedFromChat });
    }
  }

  sendMessage() {
    this.chatsStore.createChat(this.currentUserId);
  }

  @action
  public async setLike(id: number) {
    await this.likeRequestWrapper(this.updateLike(id));
  }

  @action
  public async setPhotoLike(id: number) {
    await this.likePhotoRequestWrapper(this.updatePhotoLike(id));
  }

  @action
  public async setFavourite(id: number) {
    await this.favouriteRequestWrapper(this.updateFavourite(id));

  }

  public async blockUser(id: number, { isComedFromChat } = { isComedFromChat : false }) {
    this.layout.showOverflowLoader();
    try {
      await userRepository.blockUser(id);
      isComedFromChat
        ? this.root.navigationStore.navigate(screenNames.MESSAGES_TAB)
        : this.root.navigationStore.pop();
        this.removeUser(id);
    } catch(e) {
    } finally {
      this.layout.hideOverflowLoader();
    }
  }

  public async unblockUser(id: number) {
    this.layout.showOverflowLoader();
    try {
      await userRepository.unblockUser(id);
      this.removeUser(id);
    } catch(e) {
    } finally {
      this.layout.hideOverflowLoader();
    }
  }
  // определение падежа для ед. измерения возраста
  private getAgeCountUnit(age: number) {
    const n = age % 10;
    if (age > 10 && age < 20 || n === 0 || n > 4) {
      return 'лет'
    } else if (n === 1) {
      return 'год'
    } else {
      return 'года'
    }
  }

  @computed
  public get userInformation() {
    const userInfo = this.getUser(this.currentUserId);
    const cityTitle = _.get(userInfo, 'city.title');
    const userPhotos = !_.isEmpty(userInfo?.photos) ? Object.values(userInfo.photos) : [];
    var userPhotosSorted = JSON.parse(JSON.stringify(userPhotos));
    userPhotosSorted.map((photo, i) => {
      if (photo.isAvatar && i !== 0) {
        userPhotosSorted[0] = photo;
        userPhotosSorted[i] = userPhotos[0];
      }
    });
    if (userInfo) {
      return {
        ...userInfo,
        iLikedIt: userInfo.iLikedIt,
        photos: userPhotosSorted,
        isBlocked: userInfo.iBlockedIt,
        lastActivity: formatLastActivity(userInfo.lastActivity),
        city: !!cityTitle ? `г. ${cityTitle}` : `Не указано`,
        hasChildren: userInfo.hasChildren != null ? userInfo.hasChildren ? 'Дети есть' : 'Детей нет' : 'Не указано',
        profiles: userInfo.profiles ? userInfo.profiles : [],
        purposeDating: userInfo.purposeDating ? PurposeDating[userInfo.purposeDating] : 'Не указано',
        purposeGender: userInfo.purposeGender ? `${PurposeGenderEnum[userInfo.profiles.length === 1 ? userInfo.purposeGender : 'p' + userInfo.purposeGender]} (${userInfo.interestingAgeMin} - ${userInfo.interestingAgeMax})` : '',
        gender: userInfo.profiles?.map(({ gender, age }) => `${GendersEnum[gender]} (${age} ${this.getAgeCountUnit(age)})`).join(' & '),
        familyStatus: userInfo.familyStatus ? FamilyStatusEnum[userInfo.familyStatus] : 'Не указано',
      };
    }
    return null;
  }

  @action
  private async updateLike(id: number): Promise<any> {
    try {
      await ProfileService.changeLikeStatus(this.getUser(id));
    } catch(error) {
      if (error.code == 102) {
        this.navigation.navigate(screenNames.VERIFICATION_REQUIRED);
      }
    }
  }

  @action
  private async updateFavourite(id: number): Promise<any> {
    try {
      await ProfileService.changeFavouriteStatus(this.getUser(id), this.authStore.userId);
    } catch(error) {
      if (error.code == 102) {
        this.navigation.navigate(screenNames.VERIFICATION_REQUIRED);
      }
    }
  }

  @action
  private async updatePhotoLike(id: number): Promise<any> {
    try {
      const user = this.getUser(this.currentUserId);
      await ProfileService.changePhotoLikeStatus(user, user.photos[id]);
    } catch(error) {
      if (error.code == 102) {
        this.navigation.navigate(screenNames.VERIFICATION_REQUIRED);
      }
    }
  }
}

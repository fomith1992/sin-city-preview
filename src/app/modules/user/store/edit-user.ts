import { action, computed, observable, toJS } from "mobx";
import { showMessage } from "react-native-flash-message";
import Navigation from "react-navigation-mobx-helpers";
import * as _ from 'lodash';

import { BaseStore } from "../../../store/BaseStore";
import * as screenNames from "../../navigation/screen-names";
import { ProfileFactory } from "../domain/factories/ProfileFactory";
import userRepository from "../domain/repositories/UserRepository";
import { User } from "../domain/interfaces/User";
import { Profile } from "../domain/interfaces/Profile";
import { TYPES } from "../../../store/store-types";
import { AuthUserStore } from "../../auth/store/auth-user";
import { CurrentUserStore } from "../../auth/store/current-user";
import LayoutStore from "../../layout/store/LayoutStore";
import userEditionValidator from '../domain/validators/UserEditionValidator';
import profileEditionValidator from '../domain/validators/ProfileEditionValidation';
const errorMessages = require('./error-messages.json');

export class EditUserStore extends BaseStore {
  @observable tempUser: User;
  @observable tempProfiles: Profile[];

  private currentUser: CurrentUserStore;
  private authUser: AuthUserStore;
  private navigation: Navigation;
  private layout: LayoutStore;

  storeInit() {
    this.authUser = this.root.resolve(TYPES.AuthUser);
    this.navigation = this.root.resolve(TYPES.Navigation);
    this.currentUser = this.root.resolve(TYPES.CurrentUserStore);
    this.layout = this.root.resolve(TYPES.Layout);
  }

  @action
  public editCurrentAccount(): void {
    this.navigation.navigate(screenNames.EDIT_PROFILE_SCREEN);
    this.tempUser = toJS(this.currentUser.user);
    this.tempProfiles = this.currentUser.profiles.map((p) => toJS(p));
  }

  @action
  public editCancel(): void {
    this.currentUser.syncCurrentUser();
    this.navigation.pop();
  }

  @action
  public changeTempUser(changes: any): void {
    Object.assign(this.tempUser, changes);
  }

  @action
  public changeTempUserGender(gender: string): void {
    if (gender) {
      this.tempProfiles = [];
      gender.split('').forEach((g) => {
        const profile = ProfileFactory.create({ gender: (g as 'M' | 'W') });
        this.tempProfiles.push(profile);
      });
    }
  }

  @action
  public saveProfileTempChanges(): void {
    if (this.tempUserErrors) {
      window.fLogEvent('app_errorScreen',
        this.tempUserErrors.join('\n'));
      showMessage({ message: this.tempUserErrors.join('\n'), type: 'danger' });
    } else {
      const city = _.get(this.tempUser, 'city.id');
      const cityObj = _.get(this.tempUser, 'city');
      console.log('city_cityObj_', city, cityObj)
      if (!!cityObj && cityObj.hasOwnProperty('title') && cityObj.title === "") {
        this.tempUser.city = null;
      }
      const user = {
          ...toJS(this.tempUser),
        profiles: toJS(this.tempProfiles),
        city: city ? city : this.tempUser.city != -1 ? this.tempUser.city : null
      };
      console.log('saveProfileTempChanges', user);
      userRepository.update(this.authUser.userId, user)
        .then((user: User) => {
          this.currentUser.user = user;
          this.currentUser.profiles = toJS(this.tempProfiles);
          this.navigation.pop();
          showMessage({ message: 'Изменения сохранены', type: 'success' });
        });
    }
  }

  @computed
  public get tempProfileInfo(): any {
    const user = (this.tempUser || {}) as User;
    const profiles = this.tempProfiles || [];
    const gender = profiles.map((p) => p.gender).join('');
    return {
      login: user.login,
      city: typeof user.city !== 'number' ? !!user.city ? user.city.id : user.city : user.city,
      familyStatus: user.familyStatus,
      interestingAgeMin: user.interestingAgeMin,
      interestingAgeMax: user.interestingAgeMax,
      description: user.description,
      purposeGender: user.purposeGender,
      purposeDating: user.purposeDating,
      hasChildren: user.hasChildren,
      profiles,
      gender
    }
  }

  @computed
  public get tempUserErrors(): any {
    const result = [];
    const errors = userEditionValidator.validate(this.tempUser, errorMessages);
    if (errors) {
      result.push(...Object.keys(errors).reduce((result, key) => {
        result.push(...errors[key]);
        return result;
      }, []));
    }
    this.profileEditErrors && result.push(...this.profileEditErrors);
    return result.length ? result : null;
  }

  @computed
  public get profileEditErrors(): any {
    let result: any[] = [];
    this.tempProfiles.forEach((profile, index) => {
      const errors = profileEditionValidator.validate(profile, errorMessages);
      if (errors) {
       // result.push(`Профиль ${index + 1}`)
        const errorsList = Object.keys(errors).reduce((res, key) => {
          res.push(...errors[key]);
          return res;
        }, []);
        result.push(...errorsList);
      }
    })
    return result.length ? result : null;
  }
}

import { observable, action } from 'mobx';
import { TYPES, ITYPES } from '../../../store/store-types';
import SearchStore from '../../search/store/SearchStore';
import userRepository from '../domain/repositories/UserRepository';

export class FavouritesStore extends SearchStore {
  protected nextPage = 1;

  @action
  search() {
    this.nextPage = 1;
    this.loadNextPage();
  }

  @action
  loadNextPage() {
    if (this.nextPage) {
      userRepository.loadFavourites(this.authStore.userId, this.nextPage)
        .then(({ results, next }: any) => {
          this.nextPage = next ? this.nextPage + 1 : 0;
          this.resultList = results.map((result: any) => {
            result.heFavorite = true;
            return result;
          });
        })
    }
  }

  @action
  setFavourite(id: number) {
    return super.setFavourite(id)
      .then(() => { this.resultList = this.resultList.filter(user => user.id !== id); })
  }

}

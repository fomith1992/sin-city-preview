import { IConnectionCategory } from './../domain/enums/ConnectionCategory';
import { action, computed, observable } from "mobx";
import { UserConnectionsFilterEnum } from "../domain/enums/FilteredProfiles";
import * as screenNames from '../../navigation/screen-names';
import userRepository from "../domain/repositories/UserRepository";
import { IConnection } from "../domain/interfaces/Connections";
import SearchStore from "../../search/store/SearchStore";
import { TYPES, ITYPES } from '../../../store/store-types';
import { ConnectionCategoriesFactory } from '../domain/factories/ConnectionCategoriesFactory';

export default class ConnectionsStore extends SearchStore {
  @observable filterKey: UserConnectionsFilterEnum = UserConnectionsFilterEnum.WhoMyFavorites;

  protected repository: any = userRepository;

  protected isServerHandlingLike: boolean = false;
  protected isServerHandlingFavourite: boolean = false;

  @observable connections: Record<UserConnectionsFilterEnum, IConnectionCategory> = ConnectionCategoriesFactory.create();

  @observable whomILike: IConnection[] = [];
  @observable whoLikedMe: IConnection[] = [];
  @observable whoVisitorsMe: IConnection[] = [];
  @observable whoMyFavorites: IConnection[] = [];

  private currentUserStore: ITYPES['CurrentUserStore'];

  storeInit() {
    super.storeInit();
    this.currentUserStore = this.root.resolve(TYPES.CurrentUserStore);
  }


  @computed get ids(): number[] {
    return this.connections[this.filterKey].ids;
  }
  set ids(ids: number[]) {
    this.connections[this.filterKey].ids = [...ids];
  }

  @computed get isLoading() {
    return this.connections[this.filterKey].isLoading;
  }

  set isLoading(value) {
    //mock
  }

  @action onRefresh(){
    this.connections[this.filterKey].options.page = 1;
    this.loadNextPageByType(this.filterKey, true);
  }

  @action
  setFilter(filter: UserConnectionsFilterEnum) {
    this.filterKey = filter;
  }

  @action
  setLike(id: number){
    if (this.iAmNotVerifiedAndVisitorCategory && false) {
      this.navigationStore.navigate(screenNames.VERIFICATION_REQUIRED);
    } else {
      return super.setLike(id)
        .then(() => {
          if(this.filterKey === UserConnectionsFilterEnum.WhomILike) {
            this.connections[this.filterKey].ids =
              this.connections[this.filterKey].ids.filter(connectionId => connectionId !== id);
          }
        })
        .catch(() => {});
    }
  }

  @action
  setFavourite(id: number) {
    if (this.iAmNotVerifiedAndVisitorCategory && false) {
      this.navigationStore.navigate(screenNames.VERIFICATION_REQUIRED);
    } else {
      return super.setFavourite(id);
    }
  }

  @computed get isVisitorsCategory() {
    return this.filterKey === UserConnectionsFilterEnum.WhoVisitorsMe;
  }

  @computed get iAmNotVerifiedAndVisitorCategory() {
    const amINotVerified = !this.currentUserStore.user?.isVerified;
    return this.isVisitorsCategory && amINotVerified;
  }

  @computed get iAmNotPayedAndVisitorCategory() {
    const amINotPayed = !this.currentUserStore.user?.isPayed;
    return this.isVisitorsCategory && amINotPayed;
  }

  public openProfile(item: any) {
    if (item.notAvailable) {
      this.navigationStore.navigate(screenNames.VERIFICATION_FIRST_STEP_SCREEN);
   // defeature
   } else if (this.iAmNotPayedAndVisitorCategory && false) {
     this.navigationStore.navigate(screenNames.GET_PREMIUM_SCREEN);
    //  this.navigationStore.navigate(screenNames.USERS_SCREENS_PAYMENT_REQUIRED_SCREEN);
    } else {
      this.profileManager.openProfile(item.id);
    }
  }

  @computed get refreshing() {
    return this.isLoading;
  }

  @computed
  public get resultList(): any[] {
    return this.ids.map((id) => this.users[id]).filter(Boolean);
  }

  public set resultList(result: any[]) {
    result.forEach((result) => {
      this.users[result.id] = {...this.users[result.id], ...result};
    });
  }

  @action
  loadAllConnections(){
    this.connections = ConnectionCategoriesFactory.create();
    Object.values(UserConnectionsFilterEnum).forEach((filter) => {
      this.loadNextPageByType(filter);
    })
  }

  @action
  async loadNextPageByType(filter: UserConnectionsFilterEnum, cleanBefore: boolean = false) {
    const connection = this.connections[filter];
    type IResponse = { next: string, results: IConnection[] };
    if (connection.options.page && !connection.isLoading) {
      connection.isLoading = true;
      try {
        const { next, results }: IResponse = await userRepository.loadByPath(this.authStore.userId, filter, connection.options)
            .catch((error) => {
              if (error.code === '106') {
                this.navigationStore.navigate(screenNames.USERS_SCREENS_INFO_FIELDS_REQUIRED_SCREEN);
                this.setFilter(UserConnectionsFilterEnum.WhoMyFavorites);
              } else {
                console.log('ERROR', error)
              }
            });
        connection.options.page = Boolean(next) ? connection.options.page + 1 : 0;
        if (cleanBefore) { connection.ids = []; }
        connection.ids = [...connection.ids, ...results.map(conn => conn.id)];
        this.resultList = [...results];
      } finally {
        connection.isLoading = false;
      }
    }
  }
}

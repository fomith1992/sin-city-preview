import AsyncStorage from '@react-native-community/async-storage';
import { TYPES, ITYPES } from './../../../store/store-types';
import { Alert, Platform, Linking } from 'react-native';
import { BaseStore } from '../../../store/BaseStore';
import * as RNIap from 'react-native-iap';
import firebase from 'react-native-firebase'
import paymentRepository from '../domain/repositories/PaymentRepository';
import * as Sentry from '@sentry/react-native';
import appsFlyer from 'react-native-appsflyer';
import { subscriptionsTypes } from '../ui/screens/premium';

export class PurchasesStore extends BaseStore {
  private storageKey = '@purchaseDate';
  private productsId = Platform.select({
    ios: ['sub_month_one', 'sub_month_12', 'sub_month_6'],
    android: ['sub_month', 'sub_month_6', 'sub_month_12']
  });

  private layout: ITYPES['Layout'];
  private currentUser: ITYPES['CurrentUserStore'];

  storeInit() {
   this.layout = this.root.resolve(TYPES.Layout);
   this.currentUser = this.root.resolve(TYPES.CurrentUserStore);
  }

  public getSubscription = async (subType: subscriptionsTypes) => {
    console.log('getSubscription', subType)
   // Sentry.captureMessage('getSubscription');
    await RNIap.initConnection();
    this.layout.showOverflowLoader();
    setTimeout(() => {
      this.layout.hideOverflowLoader();
    }, 3000)
    RNIap.getAvailablePurchases()
        .then((availablePurchases) => {
            console.log('getAvailablePurchases_then', availablePurchases);
        })
        .catch((err) => {
            console.log('getAvailablePurchases_catch', err);
        })
    const subs = await RNIap.getSubscriptions(this.productsId);
    const sub = subs.find(product => product.productId === subType);
    console.log('subs, sub', subs, sub)
    let purchase;
    try {
      purchase = await RNIap.requestSubscription(sub.productId, false)
          .then(async (res) => {
            console.log('RNIap.requestSubscription THEN', res)
            const receipt = res.transactionReceipt;
            if (receipt) {
              try {
                await RNIap.finishTransaction(res)
                    .then((finRes) => {
                        console.log('RNIap.finishTransaction_then', finRes)
                    })
                    .catch((err) => {
                        console.log('RNIap.finishTransaction_then', err)
                    })
              } catch (ackErr) {
                console.warn('ackErr', ackErr);
              }
            }
          //  Sentry.captureMessage('RNIap.requestSubscription THEN ' + JSON.stringify(res))
            AsyncStorage.setItem(this.storageKey, JSON.stringify(res));
           // await RNIap.finishTransaction(res, false, '')
            this.tryUpdatePurchase();
          })
          .catch((err) => {
            console.log('RNIap.requestSubscription CATCH', err);
          //  Sentry.captureMessage('RNIap.requestSubscription CATCH ' + JSON.stringify(err))
          })
      console.log('purchase = RNIap.requestSubscription', JSON.stringify(purchase))
    //  Sentry.captureMessage('purchase = RNIap.requestSubscription' + JSON.stringify(purchase));
     // AsyncStorage.setItem(this.storageKey, JSON.stringify(purchase));
     // this.tryUpdatePurchase();
      window.fLogEventObj('premium_successBuySubscribe', {});
    } catch(e) {
     // Sentry.captureMessage('getSubscription_CATCH' + JSON.stringify(e));
      if (e.code !== RNIap.IAPErrorCode.E_USER_CANCELLED) {
        firebase.crashlytics().recordCustomError(e.code, e.message);
      }
      this.layout.hideOverflowLoader();
    } finally {
      this.layout.hideOverflowLoader();
    }
  }

  public tryUpdatePurchase = async () => {
    let purchase = await AsyncStorage.getItem(this.storageKey);
   // Sentry.captureMessage('tryUpdatePurchase ' + JSON.stringify(purchase));
    console.log('tryUpdatePurchase', purchase);
    if (purchase) {
      purchase = JSON.parse(purchase);
      try {
        if (Platform.OS === 'ios') {
         // Sentry.captureMessage('updateIosPayment ' + JSON.stringify(purchase));
          console.log('updateIosPayment', purchase)
          await paymentRepository.updateIosPayment(purchase as any);
        } else if (Platform.OS === 'android') {
        //  Sentry.captureMessage('updateAndroidPayment ' + JSON.stringify(purchase));
          await paymentRepository.updateAndroidPayment(purchase as any);
        }
       // Sentry.captureMessage('this.currentUser.user.isPayed = true;');
        console.log('this.currentUser.user.isPayed = true;');
        this.currentUser.user.isPayed = true;
        appsFlyer.logEvent('sc_subscription');
        AsyncStorage.removeItem(this.storageKey);
        this.layout.hideOverflowLoader();
      } catch(e) {
       // Sentry.captureMessage('tryUpdatePurchase_CATCH ' + JSON.stringify(e));
        console.log('tryUpdatePurchase_CATCH', e)
        if (e.status !== 403) {
          AsyncStorage.setItem(this.storageKey, JSON.stringify(purchase));
          setTimeout(this.tryUpdatePurchase, 1000 * 60);
        }
      } finally { this.layout.hideOverflowLoader(); }
    }

  }

  public removeSubscription = () => {
    if (Platform.OS === 'ios') {
      Linking.openURL('https://apps.apple.com/account/subscriptions');
    } else if (Platform.OS === 'android') {
      Linking.openURL(`https://play.google.com/store/account/subscriptions?package=com.sincityapp&sku=${this.productsId}`);
    }
    window.fLogEventObj('premium_successCancelSubscribe', {});
  }
}

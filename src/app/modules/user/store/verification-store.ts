import { BaseStore } from './../../../store/BaseStore';
import { observable, action } from 'mobx';
import { TYPES, ITYPES } from '../../../store/store-types';
import * as screenNames from '../../navigation/screen-names';
import uploader from '../../upload/Uploader';
import { ImagePickerResponse } from 'react-native-image-picker';
import { verificationStatus, IVerificationStatusType } from '../domain/enums/VerificationStatus';
import userRepository from "../domain/repositories/UserRepository";
import {showModalAndTakePhoto} from "../../core/domain/services/MediaTakingService";

const CODE_EXPIRATION_TIME = 900;

export class VerificationStore extends BaseStore {
  @observable cause = '';
  @observable verificationStatus: IVerificationStatusType | null = null;
  @observable verificationCode: string = '';
  @observable adminId: string = null;
  @observable codeExpirationTime = CODE_EXPIRATION_TIME;
  private timer: number | null = null;
  private isScreenFocused: boolean = false;

  private authUserStore: ITYPES['AuthUser'];
  private layout: ITYPES['Layout'];
  private navigation: ITYPES['Navigation'];

  storeInit() {
    this.authUserStore = this.root.resolve(TYPES.AuthUser);
    this.layout = this.root.resolve(TYPES.Layout);
    this.navigation = this.root.resolve(TYPES.Navigation);
  }

  @action
  tryVerifyAgain() {
    this.verificationStatus = verificationStatus.unknown;
  }

  @action
  checkVerification() {
    userRepository.getVerificationInfo(this.authUserStore.userId)
      .then((response) => {
        if (response.status === 'NEW') {
          this.verificationStatus = verificationStatus.processing;
          this.adminId = response.adminId;
          console.log('response.status === NEWE', response.adminId)
        }
        else if (response.status === 'VRF') { this.verificationStatus = verificationStatus.ready; }
        else if (response.status === 'CAN') {
          this.cause = response.cause;
          this.verificationStatus = verificationStatus.denied;
          this.adminId = response.adminId;
        }
        else {
          this.verificationStatus = verificationStatus.unknown;
          this.adminId = response.adminId;
        }
      })
      .catch(() => {})
  }

  @action
  openVerificationScreen() {
    if (this.codeExpirationTime < CODE_EXPIRATION_TIME) {
      return this.navigation.navigate(screenNames.VERIFICATION_CONFIRMATION_SCREEN);
    }
    this.loadVerificationCode();
  }

  private loadVerificationCode() {
    userRepository.getVerificationCode(this.authUserStore.userId)
      .then(({ message }: { message: string }) => {
        this.startTimer();
        this.verificationCode = message;
        this.navigation.navigate(screenNames.VERIFICATION_CONFIRMATION_SCREEN);
      })
      .catch((e) => { console.log(e) })
  }

  public choicePhoto() {
    this.layout.showOverflowLoader(false);
    showModalAndTakePhoto()
      .then((response) => { this.uploadPhoto(response as any); })
      .catch((err) => { this.layout.hideOverflowLoader() });
  }

  public screenWasUnfocused(): void {
    this.isScreenFocused = false;
  }

  public screenWasFocused(): void {
    this.isScreenFocused = true;
  }

  @action
  uploadPhoto(photo: ImagePickerResponse) {
    this.layout.showOverflowLoader(true);
    const { userId } = this.authUserStore;
    userRepository.uploadVerificationPhoto(userId, uploader.prepareFormData(photo))
      .then((response: any) => {
        this.resetTimer();
        window.fLogEventObj('verification_successLoadPhoto', {});
        this.verificationStatus = verificationStatus.processing;
      //  this.navigation.pop(2);
      })
      .catch((e: any) => {
        if(e.code === 105){
          this.loadVerificationCode();
        }
        this.startTimer();
      })
      .finally(() => { this.layout.hideOverflowLoader(); })
  }

  private startTimer( ) {
    this.resetTimer();
    this.timer = setInterval(() => {
      if (this.codeExpirationTime) {
        this.codeExpirationTime--;
      }
      else {
        this.timer && clearInterval(this.timer);
        this.codeExpirationTime = CODE_EXPIRATION_TIME;
        if (this.isScreenFocused) {
          this.loadVerificationCode();
        }
      }
    }, 1000);
  }
  private resetTimer() {
    this.timer && clearInterval(this.timer);
    this.codeExpirationTime = CODE_EXPIRATION_TIME;
  }
}

export default VerificationStore;

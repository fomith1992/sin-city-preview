import { action } from 'mobx';
import { BlockedUser } from './../domain/interfaces/BlockedUser';
import SearchStore from '../../search/store/SearchStore';
import { TYPES, ITYPES } from '../../../store/store-types';
import userRepository from '../../user/domain/repositories/UserRepository';

import * as screenNames from '../../navigation/screen-names';
import { showMessage } from "react-native-flash-message";


export class BlockedUsersStore extends SearchStore {
  protected nextPage = 1;
  public nextPageLoading = false;

  @action
  unblockUser(userId: number) {
    this.profileManager.unblockUser(userId);
  }

  @action
  search() {
    this.nextPage = 1;
    this.resultList = [];
    this.loadNextPage();
  }

  @action
  loadNextPage() {
    if (this.nextPage && !this.nextPageLoading) {
      this.layout.showOverflowLoader();
      userRepository.loadBlacklist(this.authStore.userId, this.nextPage)
        .then(({ results, next }: { results: BlockedUser[], next?: string }) => {
          this.nextPage = next ? this.nextPage + 1 : 0;
          this.resultList = [...this.resultList, ...results];
        })
        .catch(() => {})
        .finally(() => { this.layout.hideOverflowLoader(); })
    }
  }

  openUserProifile() {
    window.fLogEvent('app_errorScreen',
      "Вы заблокировали данного пользователя. Для просмотра профиля необходимо его разблокировать");
    showMessage({message: "Вы заблокировали данного пользователя. Для просмотра профиля необходимо его разблокировать" , type: 'danger'});
  }
}

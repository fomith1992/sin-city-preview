import { User } from './../domain/interfaces/User';
import { observable, action } from 'mobx';
import SearchStore from '../../search/store/SearchStore';
import { TYPES } from '../../../store/store-types';
import userRepository from '../domain/repositories/UserRepository';
import {CurrentUserStore} from "../../auth/store/current-user";

export class PhotoLikesStore extends SearchStore {
  protected nextPage = 1;
  @observable photoId = 0;

  private currentUserStore: CurrentUserStore;

  storeInit() {
    super.storeInit();
    this.currentUserStore = this.root.resolve(TYPES.CurrentUserStore);
  }

  @action
  public search() {
    this.nextPage = 1;
    this.loadNextPage()
  }

  @action
  public loadNextPage() {
    if (this.nextPage && this.photoId) {
      userRepository.loadPhotoLikes(this.authStore.userId, this.photoId, this.nextPage)
      .then(({ results, next }: any) => {
        this.nextPage = next ? this.nextPage + 1 : 0;
        this.resultList = results;
      })
    }
  }

  @action blockUser(userId: number): Promise<void> {
    return super.blockUser(userId)
      .then(() => {
        this.currentUserStore.syncCurrentUserPhotos();
      });
  }
}

export const Attitudes = [
 // { label: 'Не выбрано', value: null },
  { label: 'Положительное', value: 'POSITIVE', key: 'POSITIVE' },
  { label: 'Нейтральное', value: 'NEUTRAL', key: 'NEUTRAL' },
  { label: 'Отрицательное', value: 'NEGATIVE', key: 'NEGATIVE' },
];

export const AttitudesEnum = {
 // null: 'Не выбрано',
  'POSITIVE': 'Положительное',
  'NEUTRAL': 'Нейтральное',
  'NEGATIVE': 'Отрицательное',
};

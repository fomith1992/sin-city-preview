export const PurposeGenderForPicker = [
  { value: 'M', label: 'Мужчину' },
  { value: 'W', label: 'Женщину' },
  { value: 'MW', label: 'Пару (м + ж)' },
  { value: 'WW', label: 'Пару (ж + ж)' },
  { value: 'MM', label: 'Пару (м + м)' },
]

export enum PurposeGenderEnum {
  'M' = 'Ищу мужчину',
  'W' = 'Ищу женщину',
  'MW' = 'Ищу пару м+ж',
  'WW' = 'Ищу пару ж+ж',
  'MM' = 'Ищу пару ж+ж',
  'pM' = 'Ищем мужчину',
  'pW' = 'Ищем женщину',
  'pMW' = 'Ищем пару м+ж',
  'pWW' = 'Ищем пару ж+ж',
  'pMM' = 'Ищем пару ж+ж'
}

export const PurposeDatingForPicker = [
  { value: 'SWING_OPEN', label: 'Свинг открытый' },
  { value: 'SWING_CLOSED', label: 'Свинг закрытый' },
  { value: 'SWING_SOFT', label: 'Мягкий свинг' },
  { value: 'GROUP_SEX', label: 'Групповой секс' }
]

export enum PurposeDating {
  "SWING_OPEN" = "Для открытого свинга",
  "SWING_CLOSED" = "Для закрытого свинга",
  "SWING_SOFT" = "Для мягкого свинга",
  "GROUP_SEX" = "Для группового секса",
}

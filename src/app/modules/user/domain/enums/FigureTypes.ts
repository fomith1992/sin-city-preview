export const FigureTypesEnum = {
  'TRIANGLE': 'Тип фигуры - Треугольник',
  'INVERTED TRIANGLE': 'Тип фигуры - Перевернутый треугольник',
  'HOURGLASS': 'Тип фигуры - Песочные часы ',
  'RECTANGLE': 'Тип фигуры - Прямоугольник',
  'CIRCLE': 'Тип фигуры - Круг',
  'TRAPEZE': 'Тип фигуры - Трапеция',
  'OVAL': 'Тип фигуры - Овал',
};

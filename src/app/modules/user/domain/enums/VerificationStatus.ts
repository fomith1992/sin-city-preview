export const verificationStatus = {
  ready: 'ready',
  denied: 'denied',
  processing: 'processing',
  unknown: 'unknown'
} as {
  ready: IVerificationStatusType
  denied: IVerificationStatusType
  processing: IVerificationStatusType
  unknown: IVerificationStatusType
}

export type IVerificationStatusType = 'ready' | 'denied' | 'processing' | 'unknown';
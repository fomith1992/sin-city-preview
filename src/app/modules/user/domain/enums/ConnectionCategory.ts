export type IConnectionCategory = { 
  isLoading: boolean, 
  ids: number[] ,
  options: {
    page: number
  }
};
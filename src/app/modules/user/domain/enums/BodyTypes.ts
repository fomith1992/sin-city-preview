export const BodyTypesEnum = {
  'ECTOMORPH': 'Тип тела - Худощавое',
  'MESOMORPH': 'Тип тела - Среднее',
  'ENDOMORPH': 'Тип тела - Полное',
}

export enum UserConnectionsFilterEnum {
  WhomILike = 'liked',
  WhoLikedMe = 'who-liked-me',
  WhoVisitorsMe = 'visitors',
  WhoMyFavorites = 'favorites'
}

export const FacialTypes = {
  'NOT': 'Нет',
  'BRISTLES': 'Щетина',
  'BEARD': 'Борода',
  'MUSTACHE': 'Усы',
  'MUSTACHE BEARD': 'Усы и борода',
}
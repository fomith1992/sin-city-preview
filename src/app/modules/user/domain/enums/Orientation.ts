export const OrientationsEnum = {
 // null: "Не выбрано",
  "HETERO": "Гетеросексуал",
  "HOMO": "Гомосексуал",
  "BI": "Бисексуал",
}

export const Orientations = [
 // { label: "Не выбрано", value: null },
  { label: "Гетеросексуал", value: 'HETERO', key: 'HETERO' },
  { label: "Гомосексуал", value: 'HOMO', key: 'HOMO' },
  { label: "Бисексуал", value: 'BI', key: 'BI' },
]

export const GendersEnum = {
  "M": "Мужчина",
  "W": "Женщина",
};

export const ProfileGendersEnum = {
  "M": "Мужчина",
  "W": "Женщина",
};

export const GendersList = [
  { label: 'Мужчина', value: 'M', key: 'M' },
  { label: 'Женщина', value: 'W', key: 'W' },
  { label: 'Пара (м+ж)', value: 'MW', key: 'MW' },
  { label: 'Пара (ж+ж)', value: 'WW', key: 'WW' },
  { label: 'Пара (м+м)', value: 'MM', key: 'MM' },
];

export const GenderFirstLetter = {
  'M': 'м',
  'W': 'ж'
}

export const FamilyStatus = [
  { label: 'В браке', value: 'MARRIED', key: 'MARRIED'  },
  { label: 'Не в браке', value: 'UNMARRIED', key: 'UNMARRIED' },
  { label: 'В гражданском браке', value: 'CIVIL_MARRIAGE', key: 'CIVIL_MARRIAGE' },
]

export enum FamilyStatusEnum  {
  'MARRIED' = 'В браке',
  'UNMARRIED' = 'Не в браке',
  'CIVIL_MARRIAGE' = 'В гражданском браке'
}

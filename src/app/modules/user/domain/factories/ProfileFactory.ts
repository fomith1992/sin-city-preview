import { Profile } from "../interfaces/Profile";

export class ProfileFactory {
  constructor() {}

  static create({ gender }: any): Profile {
    return {
      gender,
      height: null,
      weight: null,
      typeFigure: null,
      typePhysiques: null,
      facialHair: null,
      orientation: null,
      attitudeToAlcohol: null,
      attitudeToSmoking: null
    };

  // static create({ gender }: any): Profile {
  //   return {
  //     gender,
  //     height: 115,
  //     weight: 30,
  //     typeFigure: gender === 'W' ? 'HOURGLASS' : 'INVERTED TRIANGLE',
  //     typePhysiques: 'ECTOMORPH',
  //     facialHair: 'NOT',
  //     orientation: 'HETERO',
  //     attitudeToAlcohol: 'NEGATIVE',
  //     attitudeToSmoking: 'NEGATIVE'
  //   };
  }
}

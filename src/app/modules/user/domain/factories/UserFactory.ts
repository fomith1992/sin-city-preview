import { User } from './../interfaces/User';

export class UserFactory {
  constructor() {}

  static createEmpty(): User {
    return {
      login: '',
      interestingAgeMin: 18,
      interestingAgeMax: 80,
      online: false,
      description: '',
      purposeGender: '',
    }
  }

  // deprecated
  static create(): User {
    return {
      login: '',
      interestingAgeMin: 18,
      interestingAgeMax: 80,
      online: false,
      description: ''
    }
  }
}

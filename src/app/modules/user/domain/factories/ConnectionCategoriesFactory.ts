import { ConnectionCategoryFactory } from './ConnectionCategoryFactory';
import { IConnectionCategory } from './../enums/ConnectionCategory';
import { UserConnectionsFilterEnum } from './../enums/FilteredProfiles';

export class ConnectionCategoriesFactory {
  constructor() {}

  static create(): Record<UserConnectionsFilterEnum, IConnectionCategory> {
    return {
      'liked': ConnectionCategoryFactory.create(),
      'who-liked-me': ConnectionCategoryFactory.create(),
      'visitors': ConnectionCategoryFactory.create(),
      'favorites': ConnectionCategoryFactory.create()
    };
  }
}

import { IConnectionCategory } from './../enums/ConnectionCategory';

export class ConnectionCategoryFactory {
  constructor() {}

  static create(): IConnectionCategory {
    return {
      isLoading: false,
      ids: [],
      options: {
        page: 1
      }
    };
  }
}
import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import profileResource from "../resources/ProfileResource";
import { Profile } from "../interfaces/Profile";

class ProfileRepository extends BaseRepository {
  constructor() {
    super(profileResource)
  }

  public createMultipleProfiles(profiles: Profile[]): Promise<Profile[]> {
    const promises = profiles.map((p: Profile) => {
      return this.create(p).catch(() => {
        throw new Error('Ошибка сохранения профиля');
      });
    });
    return Promise.all(promises);
  }
}

const profileRepository = new ProfileRepository();

export default profileRepository;
import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import avatarResource from "../resources/AvatarResource";
import uploader from "../../../upload/Uploader";
import { ImagePickerResponse } from "react-native-image-picker";
import { CreatePhotosResponse } from "../../../upload/domain/interfaces/Photos";

class AvatarRepository extends BaseRepository {
  constructor() {
    super(avatarResource)
  }

  public uploadAndSaveAvatar(pickedPhoto: ImagePickerResponse): Promise<any> {
    return uploader.upload(pickedPhoto)
      .then((response: CreatePhotosResponse) => this.setAvatarByPhotoId(response.id))
      .then((res: any) => this.processResponse(res))
  }

  public setAvatarByPhotoId(photoId: number): Promise<any> {
    return this.resource('change')
      .update({ avatar: photoId })
      .then((res: any) => this.processResponse(res))
      .then((photo) => { return {...photo, id: photo.avatar }; })
  }
}

const avatarRepository = new AvatarRepository();

export default avatarRepository;
import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import userResource from "../resources/UserResource";
import { Push } from "../../../notifications/domain/interfaces/Push";
import { IVerificationInfo } from "../interfaces/VerificationResponse";

class UserRepository extends BaseRepository {
  constructor() {
    super(userResource)
  }

  loadFavourites(userId: number, page: number) {
    return this.resource(userId, 'favorites')
      .get({ page })
      .then((res: any) => this.processResponse(res));
  }

  public loadPush(userId: number) {
    console.log('loadPush', userId)
    return this.resource(userId, 'set-push')
      .get()
      .then((res: any) => this.processResponse(res));
  }

  public updatePush(userId: number, push: any) {
    return this.resource(userId, 'set-push')
      .update(push)
      .then((res: any) => this.processResponse(res));
  }

  public uploadVerificationPhoto(id: number, data: any): Promise<any> {
    return this.resource(id, 'verified-photo')
      .update(data, { contentType: 'multipart/form-data' })
      .then((res: any) => this.processResponse(res));
  }

  public getVerificationCode(id: number): Promise<any> {
    return this.resource(id, 'verified-getcode').create()
      .then((res: any) => this.processResponse(res));
  }

  public getVerificationInfo(id: number): Promise<IVerificationInfo> {
    return this.resource(id, 'verified-info').get()
      .then((res: any) => (this.processResponse(res) as any));
  }

  public loadPhotos(userId: number): Promise<any> {
    return this.resource(userId, 'photos')
      .get()
      .then((res: any) => this.processResponse(res))
  }

  public loadPhotoLikes(userId: number | null, photoId: number, page: number = 1 ): Promise<any> {
    return this.resource(userId, 'photos', photoId, 'likes').get({ page })
      .then((res: any) => this.processResponse(res))
  }

  public saveMapVisibility(userId: number | null, visibility: boolean): Promise<any> {
    return this.resource(userId, 'show-me').update({ showMe: visibility })
      .then((res: any) => this.processResponse(res))
  }

  public getMapVisibility(userId: number | null): Promise<any> {
    return this.resource(userId, 'show-me').get()
      .then((res: any) => this.processResponse(res))
  }

  public saveIntention(userId: number | null, intention: string): Promise<any> {
    return this.resource(userId, 'intention').update({ intention })
      .then((res: any) => this.processResponse(res));
  }

  public getIntention(userId: number | null): Promise<any> {
    return this.resource(userId, 'intention').get()
      .then((res: any) => this.processResponse(res))
  }

  public setLike(id: number, action: 'like' | 'unlike'): Promise<any> {
    return this.resource(id, action)
      .create()
      .then((res: any) => this.processResponse(res));
  }

  public savePushSettings(userId: number | null, pushes: Push): Promise<any> {
    return this.resource(userId, 'set-push').update(pushes)
      .then((res: any) => this.processResponse(res));
  }

  public getPushSettings(userId: number): Promise<any> {
    return this.resource(userId, 'set-push').get()
      .then((res: any) => this.processResponse(res));
  }

  public setPhotoLike(id: number, photoId: number, action: 'like' | 'unlike'): Promise<any> {
    return this.resource(id, 'photos', photoId, action)
      .create()
      .then((res: any) => this.processResponse(res));
  }

  public setFavourite(id: number, action: 'add-favorite' | 'delete-favorite'): Promise<any> {
    return this.resource(id, action)
      .create()
      .then((res: any) => this.processResponse(res));
  }

  public loadBlacklist(id: number, page: number): Promise<any> {
    return this.resource(id, 'blacklists')
      .get({ page })
      .then((res: any) => this.processResponse(res));
  }

  public blockUser(id: number): Promise<any> {
    return this.resource(id, 'blocked')
      .create()
      .then((res: any) => this.processResponse(res));
  }

  public removeMyProfile(id: number): Promise<any> {
    return this.resource(id)
      .delete()
      .then((res: any) => this.processResponse(res));
  }

  public unblockUser(id: number): Promise<any> {
    return this.resource(id, 'unblocked')
      .create()
      .then((res: any) => this.processResponse(res));
  }

  public loadByPath(id: number, path: string, params?: any) {
    return this.resource(id, path)
      .get(params, { handleError: null }) // cache error & error payload
      .then((res: any) => this.processResponse(res));
  }
}

const userRepository = new UserRepository();

export default userRepository;

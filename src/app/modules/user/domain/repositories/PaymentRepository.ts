import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import paymentResource from "../resources/PaymentResource";
import { Purchase } from 'react-native-iap';

class PaymentRepository extends BaseRepository {
  constructor() {
    super(paymentResource)
  }

  updateIosPayment(purchase: Purchase) {
    return super.update('verified-ios', {
      receipt_data: purchase.transactionReceipt
    });
  }
  updateAndroidPayment(purchase: Purchase) {
    let receipt_data = {};
    try {
      receipt_data = JSON.parse(purchase.dataAndroid);
    } catch(e) { }
    return super.update('verified-android', { receipt_data });
  }
}

const paymentRepository = new PaymentRepository();

export default paymentRepository;

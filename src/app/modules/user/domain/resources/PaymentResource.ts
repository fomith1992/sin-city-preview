import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../../modules/core/infrastructure/httpResource";

class PaymentResource extends BaseRestResource {
  constructor() {
    super(httpResource, 'payments');
  }
}

const paymentResource = new PaymentResource();

export default paymentResource;
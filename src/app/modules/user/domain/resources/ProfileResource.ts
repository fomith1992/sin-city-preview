import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../../modules/core/infrastructure/httpResource";

class ProfileResource extends BaseRestResource {
    constructor() {
        super(httpResource, 'profiles');
    }
}

const profileResource = new ProfileResource();

export default profileResource;
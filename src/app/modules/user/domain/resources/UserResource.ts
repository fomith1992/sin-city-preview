import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../../modules/core/infrastructure/httpResource";

class UserResource extends BaseRestResource {
    constructor() {
        super(httpResource, 'users', {});
    }
}

const userResource = new UserResource();

export default userResource;
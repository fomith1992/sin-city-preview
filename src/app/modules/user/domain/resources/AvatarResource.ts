import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../../modules/core/infrastructure/httpResource";

class AvatarResource extends BaseRestResource {
  constructor() {
    super(httpResource, 'avatars');
  }
}

const avatarResource = new AvatarResource();

export default avatarResource;
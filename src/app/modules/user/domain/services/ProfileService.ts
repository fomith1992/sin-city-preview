import { User } from './../interfaces/User';
import { action } from 'mobx';

import userRepository from '../repositories/UserRepository';
import {showMessage} from "react-native-flash-message";
import {Push} from "../../../notifications/domain/interfaces/Push";

export class ProfileService {
  @action
  public static async changeLikeStatus(user: User) {
    let urlEndPoint: 'like' | 'unlike' = user.iLikedIt ? 'unlike' : 'like';
    ProfileService.changeUserFieldStatus(user, 'iLikedIt');
    try {
      await userRepository.setLike(user.id, urlEndPoint);
    } catch(error) {
      this.changeUserFieldStatus(user, 'iLikedIt');
      throw error;
    }
  }
  @action
  public static async changeFavouriteStatus(user: User, userId: any) {
    let urlEndPoint: 'delete-favorite' | 'add-favorite' = user.heFavorite ? 'delete-favorite' : 'add-favorite';
    ProfileService.changeUserFieldStatus(user, 'heFavorite');
    try {
      await userRepository.setFavourite(user.id, urlEndPoint);
      if (urlEndPoint === 'add-favorite') {
        await userRepository.loadPush(userId)
            .then((push: Push) => {
              if (push.isPushFavorLoggedIn) {
                showMessage({ message: 'Когда пользователь будет онлайн, вы получите пуш уведомление', type: 'success', duration: 3000 });
              }
            })
      }
    } catch(error) {
      this.changeUserFieldStatus(user, 'heFavorite');
      throw error;
    }
  }
  public static async changePhotoLikeStatus(user: User, photo: User['photo']) {
    let urlEndPoint: 'like' | 'unlike' = photo.iLikedIt ? 'unlike' : 'like';
    ProfileService.changeUserPhotoStatus(photo);
    try {
      await userRepository.setPhotoLike(user.id, photo.id, urlEndPoint);
    } catch(error) {
      ProfileService.changeUserPhotoStatus(photo);
      throw error;
    }
  }
  private static changeUserPhotoStatus(photo: User['photo']) {
    photo.iLikedIt = !photo.iLikedIt;
  }
  @action.bound
  private static changeUserFieldStatus = (user: User, userField: 'iLikedIt' | 'heFavorite', ) => {
    user[userField] = !user[userField];
  };
}

import profileRepository from '../repositories/ProfileRepository';

export default class MyProfileInfo {
  private userId: number;
  constructor(userId: number) {
    this.userId = userId;
  }

  getMyFavourites(page: number) {
    return profileRepository.resource(this.userId, 'favorites').get({ page })
      .then((response: any) => profileRepository.processResponse(response));
  }

}
import { Profile } from "./Profile";

export interface ActionParticipant {
  coordinates?: {
    latitude: number
    longitude: number
  }
  heFavorite: boolean
  heLikedMe: boolean
  iLikedIt: boolean
  id: number
  lastActivity: string
  login: string
  online: boolean
  photo?: {
    id: number,
    photoUrl: string,
    photoThumbnailUrl: string
  }
  profiles: Profile[]
}
import { Profile } from "./Profile";
import { Photo } from "../../../upload/domain/interfaces/Photo";

export interface User {
  id?: number
  phone?: string
  login:	string
  familyStatus?:	'MARRIED' | 'UNMARRIED' | 'CIVIL_MARRIAGE'
  interestingAgeMin:	number
  interestingAgeMax:	number
  description?:	string
  hasChildren?:	boolean
  isVerified?: boolean,
  lastActivity?: string,
  online: boolean,
  isPayed?: boolean
  intention?: "MEETING" | "PARTY",
  showMe?: boolean,
  iLikedIt?: boolean,
  heLikedMe?: boolean,
  heFavorite?: boolean,
  photo?: Photo,
  photos: Photo[]
  city?: {
    title: string
    id: number
  } | number | null
  profiles?: Profile[],
  iBlockedIt?: boolean,
  purposeDating: 'SWING_OPEN' | 'SWING_CLOSED' | 'SWING_SOFT' | 'GROUP_SEX',
  purposeGender: 'M' | 'W' | 'MW' | 'WW' | 'MM'
}

import { User } from './User';
import { NavigationParams } from 'react-navigation';

export interface ProfileManager {
  isLoading: boolean
  userInformation: ShownUserInformation
  isCurrentUserNotVerified: boolean
  currentUserId: number
  loadUserInformation: (id: number) => Promise<any>
  doAction: (buttonIndex: number, isComedFromChat: boolean) => void
  setLike: (id: number) => Promise<any>
  setPhotoLike: (id: number) => Promise<any>
  setFavourite: (id: number) => Promise<any>
  blockUser: (id: number, options: { isComedFromChat: boolean }) => Promise<any>
  unblockUser: (id: number) => Promise<any>
  sendMessage: () => void
  openProfile: (id: number, param?: NavigationParams) => void
  userLatitude: string
}

export type ShownUserInformation = User & {
  familyStatus: string
}

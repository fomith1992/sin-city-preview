export interface Profile {
  id:	number
  age: number
  birthday:	string
  height:	number
  weight:	number
  typeFigure:	'TRIANGLE'| 'INVERTED TRIANGLE'| 'HOURGLASS'| 'RECTANGLE'| 'CIRCLE'| 'TRAPEZE'| 'OVAL' | null
  typePhysiques: 'ECTOMORPH' | 'MESOMORPH' | 'ENDOMORPH' | null
  facialHair?: 'NOT' | 'BRISTLES' | 'BEARD' | 'MUSTACHE' | 'MUSTACHE BEARD' | null
  orientation?: 'BI' | 'HETERO' | 'HOMO' | null
  gender: 'M' | 'W' | null
  attitudeToSmoking?: 'POSITIVE' | 'NEUTRAL' | 'NEGATIVE' | null
  attitudeToAlcohol?: 'POSITIVE' | 'NEUTRAL' | 'NEGATIVE' | null
}

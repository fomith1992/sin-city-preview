import { Coordinates } from './../../../search/domain/interface/Coordinates';
import { Photo } from './../../../events/domain/interfaces/Photo';

export interface BlockedUser {
  id: number,
  login: "Florence Grant",
  photo: Photo,
  coordinates?: Coordinates,
  profiles: Profile[],
  iLikedIt: boolean,
  heLikedMe: boolean,
  heFavorite: boolean,
  lastActivity?: string,
  online: boolean
}

interface Profile {
  id: number,
  age: number,
  gender: "M" | "W"
}
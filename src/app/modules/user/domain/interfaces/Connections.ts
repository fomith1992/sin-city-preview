import { Photo } from './../../../events/domain/interfaces/Photo';

export interface IConnection {
  id: number
  login: string
  photo: Photo
  profiles: any
  createdAt: string
  iLikedIt: boolean
  heLikedMe: boolean
  heFavorite: boolean
  lastActivity: any
  online: boolean
}

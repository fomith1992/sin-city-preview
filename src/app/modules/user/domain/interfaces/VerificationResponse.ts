export interface IVerificationInfo {
  status: 'NEW' | 'VRF' | 'CAN'
  cause: string
  adminId: string
}

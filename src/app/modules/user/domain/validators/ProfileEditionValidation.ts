import { Validator } from "../../../validation/Validator";
import { Validators } from "../../../validation/Validators";

class ProfileEditionValidator extends Validator {
  protected schema = {
    birthday: [Validators.required],
  };
}

const profileEditionValidator = new ProfileEditionValidator();
export default profileEditionValidator;
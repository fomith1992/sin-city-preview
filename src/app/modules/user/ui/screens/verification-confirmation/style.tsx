import { StyleSheet, Dimensions } from 'react-native';

const isMediumScreen = Dimensions.get('window').height < 700;
const isSmallScreen = Dimensions.get('window').height < 600;
import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({

  content: {
   // paddingTop: 10,
    flex: 1,
    justifyContent: 'flex-start',
  //  paddingHorizontal: isSmallScreen ? 20 : 35,
    marginBottom: isMediumScreen ? 5 : 20,
    },
  stepsCont: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 25,
    marginRight: 10,
    textAlign: 'center',
  },
  imageStyle: {
    // marginLeft: 25,
   // backgroundColor: "white",
    padding: 0,
    margin: 0,
   // width: isSmallScreen ? '50%' : '60%',
    height: isSmallScreen ? 160 : isMediumScreen ? 230 : 280,
  //  height: '60%',
  //  height: 'auto',
    resizeMode: 'contain',
  //  marginTop: isSmallScreen ? 0 : 15,
    // marginRight: 'auto',
    // marginLeft: 'auto'
  },
  title: {
    ...fontFamily('fontTitleBold'),
    fontSize: isSmallScreen ? 18 : 24,
    color: '#fff',
    textAlign: 'center',
    marginTop: isSmallScreen ? 10 : 20,
    marginBottom: isSmallScreen ? 15 : 30,
  },
  subtitle: {
    ...fontFamily('fontBaseMedium'),
    fontSize: isSmallScreen ? 12 : 14,
    color: '#3e3e3e',
    lineHeight: 20,
    textAlign: 'center',
    marginBottom: 10
  },
  upperSubtitle: {
    marginBottom: 10
  },
  time: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 14,
    color: '#fff',
    lineHeight: 20,
    textAlign: 'center',
  },
  button: {
    marginHorizontal: 40,
    marginTop: isSmallScreen ? 10 : 20,
    marginBottom: 20,
  }
});

export const verificationStepStyles = {
  style: { marginBottom: isSmallScreen ? 8 : isMediumScreen ? 12 : 16, alignItems: 'flex-start' },
  textStyle: isSmallScreen ? { fontSize: 11, textAlign: 'left', letterSpacing: -0.7}
  :
      { fontSize: 13, textAlign: 'left', lineHeight: 15.8, letterSpacing: -0.7}
}

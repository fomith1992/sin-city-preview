import React, {useCallback, useEffect} from 'react';
import {Alert, SafeAreaView, Text, View, Image, ScrollView, StyleSheet, ImageBackground} from 'react-native';
import { inject, observer } from 'mobx-react';

import styles, { verificationStepStyles } from './style';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import VerificationStep from '../../components/verification-step';

import { TYPES, ITYPES } from '../../../../../store/store-types';
import { useFocusEffect } from "react-navigation-hooks";
import icons from "../../../../icons";

const backgroundImg = require('./img/pic_verification.png');

const VerificationConfirmationScreen = inject(TYPES.VerificationStore)(observer(
  ({ navigation, verificationStore, onTryAgainPress }: { navigation: any, verificationStore: ITYPES['VerificationStore'] }) => {
    useEffect(() => {
      verificationStore.checkVerification();
      window.fLogEventObj('verification_openScreen', {verif_screen: 'verification_screen_3'});
    }, []);
    const choicePhoto = useCallback(() => {
      window.fLogEventObj('verification_clickLoadPhoto', {});
      verificationStore.choicePhoto();
    }, []);

    const onScreenFocus = useCallback(() => {
      verificationStore.screenWasFocused();
      return () => {
        verificationStore.screenWasUnfocused();
      };
    }, []);

    useFocusEffect(onScreenFocus);

    let getMinuses = (minutes: number) => {
      if(minutes % 60 < 10){
        return '0' + minutes
      }
      return minutes;
    };

    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#000' }}>
        <ImageBackground
            source={icons.rain}
            style={{...StyleSheet.absoluteFillObject, backgroundColor: '#000'}}
        />

        <ScrollView>
            {/*<Header  onBack={() => { navigation.pop() }} withoutBorder/>*/}
        <View>

          {/*<Image source={require('./../../../../icons/code.png')}*/}
          {/*       style={styles.imageStyle}*/}
          {/*       resizeMode="contain"/>*/}
          <View style={{
            alignItems: 'center',
          }}>
            <Image style={styles.imageStyle} source={require('./img/confirm_verif.png')}/>
              <Text style={styles.title}>{`Зачем подтверждать \nаккаунт?`}</Text>
          </View>

          <View style={styles.content}>
              <View style={styles.stepsCont}>
            <VerificationStep {...verificationStepStyles} index={1} check
                              label="На вашем профиле появится желтая галочка" />
            <VerificationStep {...verificationStepStyles} index={2} check
                              label="Галочка подтверждает, что вы не фейк"  />
            <VerificationStep {...verificationStepStyles} index={3} check
                                label="Если вы не фейк, вам пишут и отвечают другие пользователи"  />
            <VerificationStep {...verificationStepStyles} index={3} check
                                    label="Количество знакомств вырастает на 75%"  />
              </View>
            <View style={{ flex: 1, justifyContent: 'flex-end', marginTop: 0}}>

              <UDButton style={styles.button}
                        title='Подтвердить аккаунт'
                      //  onPress={choicePhoto}
                        onPress={() => onTryAgainPress(false)}
              />
            </View>
          </View>
        </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
));

export default VerificationConfirmationScreen;

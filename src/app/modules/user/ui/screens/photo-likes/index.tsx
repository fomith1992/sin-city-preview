import React, { useEffect, useCallback } from 'react';
import { SafeAreaView, View, FlatList } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import Header from '../../../../layout/ui/components/header';
import SearchListItem from '../../../../search/ui/components/list-item';
import { TYPES, ITYPES } from '../../../../../store/store-types';
import { Observer, inject, observer } from 'mobx-react';

const UserScreensPhotoLikes = inject(TYPES.PhotoLikesStore)(observer(
  ({ navigation, photoLikesStore }: { navigation: NavigationScreenProp<NavigationState>, photoLikesStore: ITYPES['PhotoLikesStore'] }) => {
    useEffect(() => { 
      photoLikesStore.photoId = navigation.getParam('photoIndex');
      photoLikesStore.search();
    }, []);
    const onEndReached = useCallback(() => { photoLikesStore.loadNextPage(); }, []);
    const onItemPress = useCallback((item) => {
      photoLikesStore.openProfile(item.id);
    }, []);
    const renderItem = ({ item }: any) => (
      <Observer>
        {() => <SearchListItem {...item}
                                onLikePress={() => { photoLikesStore.setLike(item.id); }}
                                onFavouritePress={() => { photoLikesStore.setFavourite(item.id); }}
                                onPress={() => { onItemPress(item); }}/>}
      </Observer>
    )
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#000'}}>
        <Header title="Кому понравилась фотография" onBack={() => { navigation.goBack(); }}/>
        <View style={{ flex: 1 }}>
          <FlatList data={photoLikesStore.resultList}
                    keyExtractor={(item, index) => String(index)}
                    onEndReached={onEndReached}
                    renderItem={renderItem}/>
        </View>
      </SafeAreaView>
    );
  }
));

export default UserScreensPhotoLikes;

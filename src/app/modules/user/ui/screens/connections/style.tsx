import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  button: { borderRadius: 12, marginRight: 12, marginBottom: 30, marginTop: 20, paddingHorizontal: 20, paddingVertical: 10, justifyContent: 'center', minHeight: 38 }
})

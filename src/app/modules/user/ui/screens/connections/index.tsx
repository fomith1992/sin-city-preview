import React, { useEffect, useCallback } from 'react';
import { SafeAreaView, ScrollView, FlatList, ActivityIndicator, RefreshControl } from 'react-native';
import { Observer, inject, observer } from "mobx-react";
import { ITYPES, TYPES } from '../../../../../store/store-types';

import * as screenNames from '../../../../navigation/screen-names';
import styles from './style';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import SearchListItem from '../../../../search/ui/components/list-item';
import { NavigationScreenProp, NavigationState } from "react-navigation";
import { UserConnectionsFilterEnum } from "../../../domain/enums/FilteredProfiles";
import icons from "../../../../icons";

type props = {
  connectionStore: ITYPES['ConnectionsStore']
  navigation: any
}

const categories = [
    {
        label: 'Мои избранные',
        key: UserConnectionsFilterEnum.WhoMyFavorites,
    },
  {
      label: 'Мои гости',
      key: UserConnectionsFilterEnum.WhoVisitorsMe,
  },
  {
    label: 'Мне понравились',
    key: UserConnectionsFilterEnum.WhomILike,
  },
  {
    label: 'Я понравился',
    key: UserConnectionsFilterEnum.WhoLikedMe,
  },

];

const ConnectionsScreen = inject(TYPES.ConnectionStore, 'usersSearchStore')(observer(
  ({ connectionStore, navigation }: props) => {
    const { isLoading } = connectionStore;

    useEffect(() => {
      window.fLogEventObj('sympathy_openScreen',
        {screen_name: 'sympathy_users', list_type: connectionStore.filterKey});
      onRefresh();
     // connectionStore.loadAllConnections();
    }, []);

    const onLikePress = useCallback((item) => {
      connectionStore.setLike(item.id)
      window.fLogEventObj('sympathy_clickLikeBtn',
        {user_id: item.id});
    }, []);
    const onFavouritePress = useCallback((item) => {
      connectionStore.setFavourite(item.id);
      window.fLogEventObj('sympathy_clickFavoriteBtn',
        {user_id: item.id});
    }, []);
    const onProfilePress = useCallback((item) => {
      window.fLogEventObj('sympathy_clickProfile',
        {user_id: item.id});
      connectionStore.openProfile(item);
    }, []);

    const onEndReached = () => {
      connectionStore.loadNextPage();
    };
    // обработка нажатия на иконку профиля
    const onProfilePressed = useCallback(() => {
        window.fLogEventObj('connections_clickItem', {item_name: 'myprofile'});
        navigation.navigate(screenNames.MY_PROFILE_SCREEN); //EVENTS_TAB
    }, []);
    // обработка нажатия на иконку уведомлений
    const onNotificationPressed = useCallback(() => {
        window.fLogEventObj('menu_clickItem', {item_name: 'notifications'});
        navigation.navigate(screenNames.NOTIFICATIONS_SCREEN);
    }, []);
    // обработка нажатия на иконку событий
    const onEventsPressed = useCallback(() => {
        navigation.navigate(screenNames.EVENTS_TAB);
    }, []);

    const onFilterPressed = useCallback((filter: UserConnectionsFilterEnum) => {
      connectionStore.setFilter(filter);
      window.fLogEventObj('sympathy_openScreen',
        {screen_name: 'sympathy_users', list_type: connectionStore.filterKey});
      onRefresh();
    }, []);

    const onRefresh = useCallback(() => {
      connectionStore.onRefresh()
    }, []);

    const renderListItem = ({ item }: any) => {
      return (
        <Observer>
          {() => <SearchListItem {...item}
                             onLikePress={() => onLikePress(item)}
                             onFavouritePress={() => onFavouritePress(item)}
                             onPress={() => onProfilePress(item)}/>}
        </Observer>
      );
    };

    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#000' }}>
        <Header title="Симпатии"
                onPress={() => onProfilePressed()}
                icon={icons.profileMenu}
                leftIcon={icons.notificationIcon}
                onBack={() => onNotificationPressed()}
                leftIconSecond={icons.eventsIcon}
                onBackSecond={() => onEventsPressed()}
        />


        <ScrollView style={{ flexGrow: 0, marginLeft: 20 }}
                    horizontal
                    showsHorizontalScrollIndicator={false}>
          {categories.map((button, index) =>
            <UDButton style={[styles.button, connectionStore.filterKey === button.key ? {} : { backgroundColor: '#fff' }]}
                      key={index}
                      fontSize={14}
                      title={button.label}
                      onPress={() => onFilterPressed(button.key)}
            />
          )}
        </ScrollView>

        {isLoading
          ? <ActivityIndicator style={{ flex: 1 }} size="large"/>
          : <FlatList
              contentContainerStyle={{ flexGrow: 1 }}
              data={connectionStore.resultList}
              refreshControl={
                <RefreshControl
                  refreshing={isLoading}
                  onRefresh={onRefresh}/>
              }
              renderItem={renderListItem}
              keyExtractor={(item) => String(item.id)}
              onEndReachedThreshold={0.5}
              onEndReached={onEndReached}
            />
        }

      </SafeAreaView>
    );
  }
));

export default ConnectionsScreen;

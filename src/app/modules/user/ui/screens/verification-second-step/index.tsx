import React, {useCallback, useEffect} from 'react';
import { SafeAreaView, Text, View, ImageBackground, StyleSheet, Image, ScrollView } from 'react-native';
import { inject, observer } from 'mobx-react';

import icons from '../../../../icons';
import styles from './style';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';

const exampleImage = require('./img/example.png');

import { TYPES, ITYPES } from '../../../../../store/store-types';
import VerificationStep from '../../components/verification-step';
import {verificationStepStyles} from '../verification-second-step/style';
import fontFamily from '../../../../../styles/fonts';
const steps1 = [
  'Ваше настоящее фото.',
  'Для пар на аватаре должны быть оба человека',
];
const steps2 = [
  'Хорошо видно лицо',
  'Без очков, масок и аксессуаров, скрывающих лицо',
];
const steps = [
    'Если вы пара, то и на аватаре вас должно быть двое',
    'Ваши лица должны быть хорошо различимы',
    'Фото должно быть без очков, масок и других аксессуаров'
];
const UserScreensVerificationSecondStep = inject(TYPES.VerificationStore)(observer(
  ({ navigation, verificationStore }: { navigation: any, verificationStore: ITYPES['VerificationStore'] }) => {
    useEffect(() => {
      verificationStore.checkVerification();
      window.fLogEventObj('verification_openScreen', {verif_screen: 'verification_screen_2'});
    }, []);
    const onVerifyPress = useCallback(() => {
      window.fLogEventObj('verification_clickNext', {verif_screen: 'verification_screen_2'});
      verificationStore.openVerificationScreen();
    }, []);
    return (<>
      <ImageBackground source={icons.rain} style={{ ...StyleSheet.absoluteFillObject, backgroundColor: '#000' }}/>
      <SafeAreaView style={{ flex: 1 }}>
        <Header title="Подтверждение: Шаг 2 из 3" onBack={() => { navigation.pop() }}/>
        <ScrollView
          //contentContainerStyle={{flex: 1}}
        >
          <View style={styles.content}>
            <Image style={styles.image} source={require('./img/verification.png')}/>
            <Text style={styles.title}>{`Требования к аватару`}</Text>
              {steps.map((label, index) => (
                  <VerificationStep
                      key={index}
                      label={label}
                      {...verificationStepStyles}
                      index={index + 1}
                  />
              ))}
            {/*<Text style={styles.titleText}>{`Чтобы получить статус "Подтверждённый" ваше лицо/лица должны быть хорошо различимы на аватаре`}</Text>*/}
            {/*<VerificationStep key={1} label={steps1[1]} boldText={steps1[0]}
                                                            {...verificationStepStyles} index={1} isBold/>*/}
            {/*<Image style={styles.imageStyle} resizeMode='contain' source={exampleImage}/>*/}
            {/*<View style={{*/}
            {/*  overflow: 'hidden',*/}
            {/*  alignSelf: 'stretch',*/}
            {/*  flexDirection: 'row',*/}
            {/*  justifyContent: 'space-between',*/}
            {/*  alignItems: 'flex-start',*/}
            {/* // height: 100,*/}
            {/* // backgroundColor: 'green',*/}
            {/*}}>*/}
            {/*  <Image style={styles.imageStyleAvatarExm} source={require('./../../../../icons/avatarRight.png')}/>*/}
            {/*  <Image style={styles.imageStyleAvatarExm}  source={require('./../../../../icons/avatarLeft.png')}/>*/}
            {/*</View>*/}
            {/*<View style={{*/}
            {/*  alignSelf: 'stretch',*/}
            {/*  flexDirection: 'row',*/}
            {/*  alignItems: 'flex-start',*/}
            {/*  // height: 100,*/}
            {/*  // backgroundColor: 'green',*/}
            {/*}}>*/}
            {/*  <View style={{alignItems: 'center', width: '50%', paddingRight: 10}}>*/}
            {/*    <Text style={styles.subPhotoText}>правильно</Text>*/}
            {/*  </View>*/}
            {/*  <View style={{alignItems: 'center', width: '50%', marginLeft: 5,}}>*/}
            {/*    <Text style={styles.subPhotoText}>неправильно</Text>*/}
            {/*  </View>*/}

            {/*</View>*/}

            {/*{steps2.map((label, index) => <VerificationStep key={index} label={label} {...verificationStepStyles} index={index+2}/>)}*/}
          </View>
          <View style={styles.button}>
            <UDButton title='Продолжить' onPress={onVerifyPress}/>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>);
  }
));

export default UserScreensVerificationSecondStep;

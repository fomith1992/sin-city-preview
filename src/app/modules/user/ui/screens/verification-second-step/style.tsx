import {StyleSheet, Dimensions} from 'react-native';

const isSmallScreen = Dimensions.get('window').height < 600;
const isMediumScreen = Dimensions.get('window').height < 700;
import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  content: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    // marginHorizontal: 20,
    paddingHorizontal: 20,
    paddingTop: 20,
    // marginBottom: 60
  },
  image: {
    // marginLeft: 25,
    resizeMode: 'contain',
    width: '100%',
    marginTop: isSmallScreen ? -10 : 0,
    // marginRight: 'auto',
    // marginLeft: 'auto'
  },
  titleText: {
    ...fontFamily('fontTitleBold'),
    fontSize: 16,
    lineHeight: 20,
    color: '#fff',
    textAlign: 'center',
    marginTop: 30,
  },
  title: {
    ...fontFamily('fontTitleBold'),
    fontSize: isSmallScreen ? 18 : 24,
    color: '#fff',
    textAlign: 'center',
    marginTop: isSmallScreen ? 15 : 30,
    marginBottom: 30,
  },
  subtitle: {
    ...fontFamily('fontTitleBold'),
    fontSize: isSmallScreen ? 14 : 16,
    color: '#fff',
    textAlign: 'center',
    lineHeight: 20,
    letterSpacing: -1,
    marginBottom: 40,
  },
  subtext: {
    textAlign: 'center',
    ...fontFamily('fontBaseMedium'),
    fontSize: 16,
    lineHeight: 20,
    flexWrap: 'wrap',
    color: '#fff',
  },
  subPhotoText: {
    //  maxWidth: '40%',
    // textAlign: 'center',
    ...fontFamily('fontBaseMedium'),
    fontSize: 11,
    lineHeight: 16,
    //  flexWrap: 'wrap',
    color: 'rgba(255,255,255,0.73)',
    marginBottom: 30,
  },
  imageStyleAvatarExm: {
    // backgroundColor: 'red',
    width: '47%',
    height: 110,
    //resizeMode: 'contain',
    borderRadius: 15,
    marginBottom: 5,
  },
  button: {
    paddingHorizontal: 40,
    marginBottom: 50,
    marginTop: isSmallScreen ? 15 : isMediumScreen ? 25 : 30,
  },
});

export const verificationStepStyles = {
  style: isSmallScreen ? {marginBottom: 10} : isMediumScreen ? {marginBottom: 25} : {marginBottom: 30},
  labelStyle: isSmallScreen ? {marginRight: 30} : null,
  textStyle: isSmallScreen ? {fontSize: 14} : null,
  //textStyle: isSmallScreen ? { fontSize: 14, textAlign: 'left' } : { textAlign: 'left'}
};

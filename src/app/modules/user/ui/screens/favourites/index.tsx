import React, { useEffect, useCallback } from 'react';
import { SafeAreaView, FlatList } from 'react-native';
import { Observer, inject, observer } from 'mobx-react';

import Header from '../../../../layout/ui/components/header';
import SearchListItem from '../../../../search/ui/components/list-item';

import { TYPES, ITYPES } from '../../../../../store/store-types';

interface props {
  navigation: any,
  favouritesStore: ITYPES['FavouritesStore']
}

const FavouritesScreen = inject(TYPES.FavouritesStore)(observer(
  ({ navigation, favouritesStore }: props) => {
    useEffect(() => {
      favouritesStore.search();
    }, []);
    const onFavouritePress = useCallback((id: number) => {
      favouritesStore.setFavourite(id);
    }, []);
    const onLikePress = useCallback((id: number) => {
      favouritesStore.setLike(id);
    }, []);
    const onPress = useCallback((item) => {
      favouritesStore.openProfile(item.id);
    }, []);
    const onBackPress = useCallback((item) => {
      navigation.pop();
    }, []);
    const onScroll = useCallback(({ nativeEvent }) => {
      const contentHeight = nativeEvent.contentSize.height - nativeEvent.layoutMeasurement.height;
      if (nativeEvent.contentOffset.y === contentHeight) {
        favouritesStore.loadNextPage();
      }
    }, []);
    const renderItem = ({ item }: any) => {
      return (
        <Observer>
          {() => <SearchListItem {...item}
                                  onFavouritePress={() => { onFavouritePress(item.id) }} 
                                  onLikePress={() => { onLikePress(item.id) }} 
                                  onPress={() => { onPress(item) }}/>}
        </Observer>
      );
    }
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#000' }}>
        <Header title='Избранные пользователи' 
                onBack={onBackPress}/>
        <FlatList data={favouritesStore.resultList}
                  keyExtractor={(item) => String(item.id)}
                  onScroll={onScroll}
                  bounces={false}
                  renderItem={renderItem}/>
      </SafeAreaView>
    );
  }
))

export default FavouritesScreen;
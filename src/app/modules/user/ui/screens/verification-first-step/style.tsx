import { StyleSheet, Dimensions } from 'react-native';

const isMediumScreen = Dimensions.get('window').height < 700;
const isSmallScreen = Dimensions.get('window').height < 600;
import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 40,
    marginVertical: 30,
  },
  title: {
    ...fontFamily('fontTitleBold'),
    fontSize: isSmallScreen ? 16 : 24,
    color: '#fff',
    textAlign: 'center',
  },
  secondTitle: {
    marginBottom: isSmallScreen ? 10 : isMediumScreen ? 36 : 46
  },
  subtitle: {
    ...fontFamily('fontTitleBold'),
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
    marginBottom: isSmallScreen ? 0 : 25
  },
  text: {
    textAlign: 'center',
    ...fontFamily('fontBaseMedium'),
    fontSize: isMediumScreen ? 12 : 14,
    lineHeight: 20,
    flexWrap: 'wrap',
    color: '#fff'
  }
});

export const verificationStepStyles = {
  style: isSmallScreen ? { marginBottom: 25 } : isMediumScreen ? { marginBottom: 30 } : null,
  labelStyle: isSmallScreen ? { marginRight: 30 } : null,
  textStyle: isSmallScreen ? { fontSize: 14 } : null,
  //textStyle: isSmallScreen ? { fontSize: 14, textAlign: 'left' } : { textAlign: 'left'}
};

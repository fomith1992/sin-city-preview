import React, {useCallback, useEffect} from 'react';
import {
  ActivityIndicator,
  SafeAreaView,
  Text,
  View,
  ImageBackground,
  StyleSheet,
  ScrollView,
} from 'react-native';
import {inject, observer} from 'mobx-react';

import icons from '../../../../icons';
import styles, {verificationStepStyles} from './style';
import * as screenNames from '../../../../navigation/screen-names';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import {verificationStatus} from '../../../domain/enums/VerificationStatus';
import VerificationStep from '../../components/verification-step';

import {TYPES, ITYPES} from '../../../../../store/store-types';

import VerificationDenied from '../../components/verification-denied';
import VerificationProcessing from '../../components/verification-processing';
import VerificationReady from '../../components/verification-ready';
import VerificationConfirmationScreen from "../verification-confirmation";

const steps = [
  'На 75% больше внимания от\u00A0других пользователей',
  'В 3 из 5 случаев знакомство переходит во\u00A0встречу',
  'В 2 раза больше пользователей в\u00A0твоем доступе',
  'Риск общения с\u00A0фейками сводится\u00A0к\u00A00',
];

const VerificationStatusLoading = () => {
  return <ActivityIndicator size="large" style={{flex: 1}} />;
};

const UserScreensVerificationFirstStep = inject(TYPES.VerificationStore, TYPES.ChatsStore)(
  observer(
    ({
      navigation,
      verificationStore,
      chatsStore,
    }: {
      navigation: any;
      verificationStore: ITYPES['VerificationStore'];
      chatsStore: ITYPES['ChatsStore'];
    }) => {
      useEffect(() => {
        verificationStore.checkVerification();
        window.fLogEventObj('verification_openScreen', {
          verif_screen: 'verification_screen_1',
        });
      }, [verificationStore]);
      const onBackPress = useCallback(() => {
        navigation.pop(2);
      }, [navigation]);
      const tryAgainPress = useCallback((withAutofill = true) => {
       // verificationStore.tryVerifyAgain();
        console.log('tryAgainPress', Number(verificationStore.adminId))
        if (withAutofill) {
          chatsStore.createChat(Number(verificationStore.adminId), "Привет. Помогите получить статус Подтверждённого пользователя");
        } else {
          chatsStore.createChat(Number(verificationStore.adminId), "Привет. Помогите получить статус Подтверждённого пользователя");
        }
      }, [chatsStore]);
      const onNextPress = useCallback(() => {
        navigation.navigate(screenNames.VERIFICATION_SECOND_STEP_SCREEN);
        window.fLogEventObj('verification_clickNext', {
          verif_screen: 'verification_screen_1',
        });
      }, [navigation]);
      const FirstStepContentComponent = () => {
        return (
          <ScrollView>
            <View style={styles.content}>
              <Text style={styles.title}>Что даёт статус</Text>
              <Text
                style={StyleSheet.flatten([styles.title, styles.secondTitle])}>
                {' '}
                "Подтверждённый"?
              </Text>
              {steps.map((label, index) => (
                <VerificationStep
                  key={index}
                  label={label}
                  {...verificationStepStyles}
                  index={index + 1}
                />
              ))}
              <Text style={[styles.text]}>
                Для успешного потдверждения аккаунта мы сравним ваш аватар с
                проверочным фото на Шаге №3
              </Text>
            </View>
            <View style={{paddingHorizontal: 40, marginBottom: 50}}>
              <UDButton title="Начать подтверждение" onPress={onNextPress} />
            </View>
          </ScrollView>
        );
      };
      const firstStepContent = () => {
        switch (verificationStore.verificationStatus) {
          case verificationStatus.ready: {
            return <VerificationReady />;
          }
          case verificationStatus.denied: {
            return (
              <VerificationDenied
                reasonText={verificationStore.cause}
                onTryAgainPress={tryAgainPress}
              />
            );
          }
          case verificationStatus.processing: {
            return <VerificationProcessing navigation={navigation} />;
          }
          case verificationStatus.unknown: {
           // return <FirstStepContentComponent />;
            return <VerificationConfirmationScreen onTryAgainPress={tryAgainPress}/>;
          }
          default: {
            return <VerificationStatusLoading />;
          }
        }
      };
      return (
        <>
          <ImageBackground
            source={icons.rain}
            style={{...StyleSheet.absoluteFillObject, backgroundColor: '#000'}}
          />
          <SafeAreaView style={{flex: 1}}>
            <Header

              onBack={onBackPress}
              withoutBorder
            />
            {firstStepContent()}
          </SafeAreaView>
        </>
      );
    },
  ),
);

export default UserScreensVerificationFirstStep;

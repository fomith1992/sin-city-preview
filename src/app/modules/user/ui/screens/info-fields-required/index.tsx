import React, { useCallback } from 'react';
import { ImageBackground, SafeAreaView, Text, View } from 'react-native';
import { ITYPES } from '../../../../../store/store-types';

import * as screenNames from '../../../../navigation/screen-names';
import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';

const UserScreensInfoFieldsRequired = ({ navigation }: { navigation: ITYPES['Navigation']['navigation'] }) => {
  const onGetPremiumPress = useCallback(() => {
      navigation.goBack();
      navigation.navigate(screenNames.MY_PROFILE_SCREEN);
  }, []);
  const onBack = useCallback(() => {
    const goBack = navigation.getParam('goBack');
    goBack && typeof goBack === 'function' ? goBack(navigation.goBack) : navigation.goBack();
  }, []);
  return (
    <ImageBackground source={icons.rain} style={styles.backgroundImage}>
      <SafeAreaView style={{flex: 1}}>
        <Header title="" onBack={onBack} withoutBorder/>
        <View style={styles.textContainer}>
          <Text style={styles.label}>Чтобы получить доступ к просмотру ваших гостей и лайков, заполните остальные поля в вашем профиле.</Text>
        </View>
        <UDButton style={styles.verifyButton} onPress={onGetPremiumPress} title="Перейти в Профиль"/>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default UserScreensInfoFieldsRequired;

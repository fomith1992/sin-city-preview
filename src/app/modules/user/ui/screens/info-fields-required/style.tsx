import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  backgroundImage: { 
    flex: 1, 
    width: '100%', 
    backgroundColor: '#000'
  },
  textContainer: {
    flex: 1, 
    justifyContent: 'center', 
    marginBottom: 100
  },
  label: {
    marginHorizontal: 20, 
    color: '#fff', 
    ...fontFamily('fontTitleBold'), 
    fontSize: 24, 
    textAlign: 'center' 
  },
  labelItem: { 
    marginTop: 60,
    marginHorizontal: 20,
    color: '#fff',
    ...fontFamily('fontTitleBold'), 
    fontSize: 24, 
    textAlign: 'center' 
  },
  verifyButton: { 
    position: 'absolute', 
    bottom: 64, 
    left: 40, 
    right: 40 
  }
})

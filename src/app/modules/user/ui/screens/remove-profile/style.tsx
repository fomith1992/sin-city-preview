import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  backgroundImage: {
    backgroundColor: '#000',
    width: '100%',
    height: '100%'
  },
  safeArea: {
    zIndex: 2,
    flex: 1,
    width: '100%'
  },
  backButton: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  remove: {
    color: '#e84747',
    fontSize: 16,
    ...fontFamily('fontTitleBold')
  },
  header: {
    height: 70,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  removeContainer: {
    height: 50,
    paddingHorizontal: 10,
    justifyContent: 'center'
  },
  description: {
    color: '#fff',
    fontSize: 24,
    ...fontFamily('fontTitleBold'),
    textAlign: 'center',
    marginTop: 150,
    marginHorizontal: 20
  },
  cancelButton: {
    position: 'absolute',
    bottom: 64,
    left: 40,
    right: 40
  },
})

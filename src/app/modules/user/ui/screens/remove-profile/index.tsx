import React from 'react';
import { ImageBackground, View, SafeAreaView, Image, Text, TouchableOpacity } from 'react-native';
import { inject } from 'mobx-react';

import styles from './style';
import icons from '../../../../icons';
import UDButton from '../../../../ud-ui/components/ud-button';

import { CurrentUserStore } from "../../../../auth/store/current-user";

interface props {
  navigation: any,
  currentUserStore: CurrentUserStore
}

const RemoveProfileScreen = inject('currentUserStore')(({ navigation, currentUserStore }: props) => {
  const cancel = React.useCallback(() => { navigation.goBack(); }, []);
  return (
    <ImageBackground source={icons.rain} style={styles.backgroundImage}>
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.header}>
          <TouchableOpacity onPress={cancel}
                            style={styles.backButton}>
            <Image source={icons.navBack} resizeMode='cover'/>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {currentUserStore.removeMyProfile()
            window.fLogEvent('menu_clickDeleteApply', {});
          }}
                            style={styles.removeContainer}>
            <Text style={styles.remove}>Удалить</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.description}>Анкета будет безвозвратно удалена. Вы действительно хотите удалить анкету?</Text>
        <UDButton style={styles.cancelButton} title="Отменить" onPress={cancel}/>
      </SafeAreaView>
    </ImageBackground>
  );
})

export default RemoveProfileScreen;

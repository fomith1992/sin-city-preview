import React, {useCallback, useEffect, useState} from 'react';
import {
  SafeAreaView,
  ImageBackground,
  Text,
  View,
  Platform,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {observer, inject} from 'mobx-react';
import {TYPES, ITYPES} from '../../../../../store/store-types';

import icons from '../../../../icons';
import styles from './style';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import AdvantagesListItem from '../../components/advantages-list-item';

const backgroundImg = require('./img/background.png');

/* const advantages = [
  {
    label: 'Просматривать на карте анкеты пользователей',
    icon: icons.iconPremium1,
  },
  {label: 'Поиск анкет на карте по намерениям', icon: icons.iconPremium2},
  {label: 'Просмотр гостей своего профиля', icon: icons.iconPremium3},
  {label: 'Просмотр  и создание событий', icon: icons.iconPremium4},
  {label: 'Просмотр событий по намерениям', icon: icons.iconPremium5},
]; */

const purchasesItems = [
  {
    name: '1 месяц',
    cost: Platform.OS === 'android' ? '699 руб/мес' : '949 руб/мес',
    discount: null,
    isMostPopular: false,
    itemId:
      Platform.OS === 'android'
        ? 'sub_month'
        : ('sub_month_one' as subscriptionsTypes),
  },
  {
    name: '12 месяцев',
    cost: Platform.OS === 'android' ? '199 руб/мес' : '269 руб/мес',
    discount: '-72%',
    isMostPopular: true,
    itemId:
      Platform.OS === 'android'
        ? 'sub_month_12'
        : ('sub_month_12' as subscriptionsTypes),
  },
  {
    name: '6 месяцев',
    cost: Platform.OS === 'android' ? '349 руб/мес' : '499 руб/мес',
    discount: '-50%',
    isMostPopular: false,
    itemId:
      Platform.OS === 'android'
        ? 'sub_month_6'
        : ('sub_month_6' as subscriptionsTypes),
  },
];

const descriptionPurchasesItems = {
  sub_month: `Оплата регулярная. За 1 месяц\nавтоматически будут списаны 699 руб.\nОтмена в любое время через Google Play\nи App Store`,
  sub_month_one: `Оплата регулярная. За 1 месяц\nавтоматически будут списаны 949 руб.\nОтмена в любое время через Google Play\nи App Store`,
  sub_month_12: `Оплата регулярная. За 12 месяцев\nавтоматически будут списаны ${
    Platform.OS === 'android' ? 2388 : 3250
  } руб.\nОтмена в любое время через Google Play\nи App Store`,
  sub_month_6: `Оплата регулярная. За 6 месяцев\nавтоматически будут списаны ${
    Platform.OS === 'android' ? 2094 : 2990
  } руб.\nОтмена в любое время через Google Play\nи App Store`,
};

export type subscriptionsTypes = keyof typeof descriptionPurchasesItems;

interface props {
  navigation: any;
  purchasesStore: ITYPES['PurchasesStore'];
  currentUserStore: ITYPES['CurrentUserStore'];
}

const GetPremiumScreen = inject(
  TYPES.PurchasesStore,
  TYPES.CurrentUserStore,
)(
  observer(({navigation, purchasesStore, currentUserStore}: props) => {
    const [subType, setSubType] = useState<subscriptionsTypes | null>(
      Platform.OS === 'android' ? 'sub_month_12' : 'sub_month_12',
    );

    useEffect(() => {
      window.fLogEventObj('premium_openScreen', {
        screen_name: 'premium_subscribe',
      });
    }, []);

    const isPayed = currentUserStore?.user?.isPayed;

    useEffect(() => {
      isPayed && setSubType(null);
    }, [isPayed]);

    const onGetSubPress = useCallback(() => {
      window.fLogEventObj(
        isPayed ? 'premium_clickCancelSubscribe' : 'premium_clickBuySubscribe',
        {},
      );
      isPayed
        ? purchasesStore.removeSubscription()
        : purchasesStore.getSubscription(subType);
    }, [isPayed, subType]);
    return (
      <>
        <ImageBackground
          source={backgroundImg}
          style={styles.backgroundImage}
        />
        <SafeAreaView style={{flex: 1, height: '100%'}}>
          <ScrollView contentContainerStyle={{flexGrow: 1}}>
            <Header
              title=""
              onBack={() => {
                navigation.pop(2);
              }}
              withoutBorder
            />
            <View style={styles.mainContainer}>
              <Text style={styles.contentText}>
                Выберете план подписки после 3 дней бесплатного периода
              </Text>
              <View style={{justifyContent: 'center'}}>
                {purchasesItems.map((item) => (
                  <TouchableOpacity
                    disabled={isPayed}
                    key={item.itemId}
                    onPress={() => setSubType(item.itemId)}>
                    {item.isMostPopular && subType === item.itemId && (
                      <View style={styles.iSMostPpularContainer}>
                        <Text style={styles.iSMostPpular}>
                          самый популярный
                        </Text>
                      </View>
                    )}
                    <View
                      style={[
                        styles.cardItem,
                        subType === item.itemId
                          ? styles.cardItemActive
                          : undefined,
                      ]}>
                      <View
                        style={[styles.card, !item.discount && {marginTop: 0}]}>
                        <Text
                          style={[
                            styles.notice,
                            subType === item.itemId
                              ? styles.noticeActive
                              : undefined,
                          ]}>
                          {item.name}
                        </Text>
                        <Text
                          style={[
                            styles.notice,
                            subType === item.itemId
                              ? styles.noticeActive
                              : undefined,
                          ]}>
                          {item.cost}
                        </Text>
                      </View>
                      {item.discount && (
                        <View style={[styles.discount]}>
                          <Text
                            style={[
                              styles.discountNotice,
                              ,
                              subType === item.itemId
                                ? styles.discountNoticeActive
                                : undefined,
                            ]}>
                            {item.discount}
                          </Text>
                        </View>
                      )}
                    </View>
                  </TouchableOpacity>
                ))}
              </View>
              <View style={{marginBottom: 40}}>
                <Text style={styles.description}>
                  {descriptionPurchasesItems[subType]}
                </Text>
              </View>
              {/* <View style={styles.content}>
              {advantages.map((advantage, index) => (
                <AdvantagesListItem key={index} {...advantage} />
              ))}
            </View> */}
              <UDButton
                style={[
                  styles.button,
                  isPayed ? styles.inactiveButton : styles.activeButton,
                ]}
                title={isPayed ? 'Отменить подписку' : 'Попробовать бесплатно'}
                onPress={onGetSubPress}
              />
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }),
);

export default GetPremiumScreen;
2;

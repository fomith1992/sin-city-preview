import {StyleSheet} from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
  content: {
    flex: 1,
    paddingLeft: 50,
    paddingRight: 57,
    justifyContent: 'space-evenly',
    alignItems: 'flex-start',
  },
  backgroundImage: {
    ...StyleSheet.absoluteFillObject,
    width: '100%',
    height: '100%',
  },
  title: {
    ...fontFamily('fontTitleBold'),
    fontSize: 24,
    color: '#fff',
    marginTop: 30,
    marginBottom: 5,
    textAlign: 'center',
  },
  contentText: {
    ...fontFamily('fontBaseRegular'),
    // fontSize: 14,
    fontSize: 19,
    color: '#fff',
    marginTop: 30,
    marginBottom: 5,
    textAlign: 'center',
    paddingHorizontal: '6%',
    //alignSelf: 'stretch',
  },
  notice: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 22,
    lineHeight: 27,
    color: '#C7C7C7',
    textAlign: 'center',
  },
  noticeActive: {
    ...fontFamily('fontBaseMedium'),
    color: '#fff',
  },
  /* priceSubtitle: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 14,
    color: '#979797',
    textAlign: 'center',
    marginVertical: 20,
    width: '100%',
  }, */
  button: {
    marginHorizontal: 40,
    marginBottom: 40,
  },
  inactiveButton: {
    backgroundColor: '#aaa',
  },
  activeButton: {
    backgroundColor: '#ffbd21',
  },
  cardItem: {
    justifyContent: 'center',
    height: 86,
    marginHorizontal: 19,
    paddingHorizontal: 11,
    borderRadius: 17,
    borderWidth: 3,
    overflow: 'hidden',
    borderColor: '#ffffff00',
  },
  card: {
    marginVertical: 0,
    marginTop: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  discount: {
    alignItems: 'flex-end',
  },
  discountNotice: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 22,
    lineHeight: 27,
    color: '#C7C7C7',
  },
  iSMostPpularContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iSMostPpular: {
    ...fontFamily('fontTitleBold'),
    fontSize: 13,
    lineHeight: 16,
    color: '#000',
    paddingHorizontal: 6,
    backgroundColor: '#ffbd21',
    borderRadius: 3,
    overflow: 'hidden',
    textAlign: 'center',
    position: 'absolute',
    top: 1,
  },
  discountNoticeActive: {
    color: '#ffbd21',
  },
  cardItemActive: {
    borderRadius: 17,
    borderWidth: 3,
    overflow: 'hidden',
    borderColor: '#ffbd21',
  },
  description: {
    ...fontFamily('fontBaseRegular'),
    fontSize: 13,
    lineHeight: 16,
    letterSpacing: 1,
    color: '#fff',
    textAlign: 'center',
  },
});

import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export const activeColor = '#ffbd21';
export const inactiveColor = '#ffffff33';

export default StyleSheet.create({
  pickerInput: {
    color: '#fff',
    fontSize: 16,
    ...fontFamily('fontBaseMedium')
  },
  marker: {
    height: 19,
    width: 19,
    backgroundColor: '#fff'
  },
  contentContainer: {
    backgroundColor: '#000', 
    marginTop: 15, 
    paddingLeft: 20, 
    paddingRight: 0 
  },
  viewContainer: { 
    flex: 1, 
    justifyContent: 'center',
    paddingLeft: 7
  },
  listItemText: {
    color: '#fff',
    fontSize: 16,
    ...fontFamily('fontBaseMedium')
  },
  multilineTextInput: {
    color: '#fff',
    flex: 1,
    minHeight: 70,
    fontSize: 14,
    lineHeight: 24,
    paddingVertical: 0,
    marginRight: 40,
    marginBottom: 26,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: 10,
    textAlignVertical: 'top',
    ...fontFamily('fontBaseRegular')
  },
  multilineTextInputCounter: {
    position: 'absolute',
    bottom: 0, 
    right: 20,
    color: '#999',
    fontSize: 9,
    textAlign: 'right',
    marginRight: 40,
    marginBottom: 13
  },
  listItemContent: {
    color: '#fff',
    fontSize: 16,
    marginLeft: 12,
    ...fontFamily('fontBaseMedium')
  },
  bottomButtonContainer: {
    marginLeft: 10,
    marginRight: 30,
    marginBottom: 50,
  },
  bottomButton: { 
    marginTop: 30,
  },
  profileContainer: { 
    height: 30, 
    alignItems: 'flex-end', 
    paddingRight: 20 
  },
  multilineTextInputInvalid: {
    color: '#ff2020'
  },
  multilineTextInputCounterInvalid: {
    color: '#ff2020'
  },
});


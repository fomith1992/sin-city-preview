import React, {useCallback, useRef, useEffect} from 'react';
import {
  TextInput,
  TouchableOpacity,
  Text,
  SafeAreaView,
  Image,
  Dimensions,
  Platform,
  View,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {inject, observer} from 'mobx-react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {City} from '../../../../dictionaries/domain/interfaces/City';

import icons from '../../../../icons';
import styles, {activeColor, inactiveColor} from './style';
import pickerStyles from '../../../../../styles/picker';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import ListItem from '../../../../auth/ui/components/profile-list-item';
import AuthComponentsProfileForm from '../../../../auth/ui/components/profile-form';
import {Profile} from '../../../domain/interfaces/Profile';
import Navigation from 'react-navigation-mobx-helpers';
import {TYPES} from '../../../../../store/store-types';
import DictionariesStore from '../../../../dictionaries/store/DictionariesStore';
import {FamilyStatus} from '../../../domain/enums/FamilyStatus';
import {EditUserStore} from '../../../store/edit-user';
import {PurposeDatingForPicker} from '../../../domain/enums/PurposeDating';
import {PurposeGenderForPicker} from '../../../domain/enums/PurposeGender';

const childrenOptions = [
  {label: 'Нет', value: false},
  {label: 'Есть', value: true},
];

const wW = Dimensions.get('window').width;

const genders = [
  {label: 'Мужчина', value: 'M'},
  {label: 'Женщина', value: 'W'},
  {label: 'Пара (м+ж)', value: 'MW'},
  {label: 'Пара (ж+ж)', value: 'WW'},
  {label: 'Пара (м+м)', value: 'MM'},
];

type props = {
  editUserStore: EditUserStore;
  navigationStore: Navigation;
  dictionariesStore: DictionariesStore;
};

const UserScreensEditProfile = inject(
  TYPES.Navigation,
  TYPES.Dictionaries,
  TYPES.EditUserStore,
)(
  observer(({navigationStore, dictionariesStore, editUserStore}: props) => {
    const profiles = editUserStore.tempProfiles;
    const profileInfo = editUserStore.tempProfileInfo;
    const {cities} = dictionariesStore;
    const descriptionTextInputRef = useRef(null);

    // useEffect(() => {
    //   if(!cities.some(city => city.id === -1)) {
    //     const unselectedCity: City = {
    //       title: 'Не выбрано',
    //       id: -1
    //     };
    //     !!cities.length && cities.unshift(unselectedCity);
    //   }
    // }, []);

    const onSavePressed = useCallback(() => {
      window.fLogEventObj('myprofile_clickSaveProfile', {});
      editUserStore.saveProfileTempChanges();
    }, []);
    const onBackPressed = useCallback(() => {
      editUserStore.editCancel();
    }, []);

    const onDescriptionTextChange = useCallback((description) => {
      editUserStore.changeTempUser({description});
    }, []);

    const onInterestedYearsValuesChange = useCallback(
      ([interestingAgeMin, interestingAgeMax]: [number, number]) => {
        editUserStore.changeTempUser({interestingAgeMin, interestingAgeMax});
      },
      [],
    );

    const onFamilyStatusChanged = useCallback((familyStatus) => {
      editUserStore.changeTempUser({familyStatus});
    }, []);

    const onGenderChanged = useCallback((gender) => {
      editUserStore.changeTempUserGender(gender);
      onPurposeGenderChange(null);
    }, []);

    const onPurposeDatingChange = useCallback((purposeDating) => {
      editUserStore.changeTempUser({purposeDating});
    }, []);

    const onPurposeGenderChange = useCallback((purposeGender) => {
      editUserStore.changeTempUser({purposeGender});
    }, []);

    const onCityChanged = useCallback((city) => {
        console.log('onCityChanged', city)
      editUserStore.changeTempUser({city});
    }, []);

    const onLoginChanged = useCallback((login) => {
      editUserStore.changeTempUser({login});
    }, []);

    const onChildrenChanged = useCallback((hasChildren) => {
      editUserStore.changeTempUser({hasChildren});
    }, []);

    const descriptionCounterStyles = [styles.multilineTextInputCounter] as any;
    const descriptionStyles = [styles.multilineTextInput] as any;
    // определение ед/мн числа
    const iLookFor = () => {
      if (!!profileInfo.gender && profileInfo.gender.length > 1) {
        return 'Ищем';
      } else {
        return 'Ищу';
      }
    };

    if (profileInfo.description?.length > 200) {
      descriptionCounterStyles.push(styles.multilineTextInputCounterInvalid);
      descriptionStyles.push(styles.multilineTextInputInvalid);
    }

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#000'}}>
        <Header title="Профиль" height={70} onBack={onBackPressed} />
        <KeyboardAwareScrollView
          keyboardOpeningTime={1}
          enableResetScrollToCoords={false}
          contentContainerStyle={styles.contentContainer}
          bounces={false}
          extraScrollHeight={100}>
          <ListItem title="ВВЕДИТЕ ВАШ НИКНЕЙМ">
            <Image source={icons.username} />
            <TextInput
              style={[
                styles.pickerInput,
                {paddingRight: 20, marginLeft: 12, flex: 1},
              ]}
              value={profileInfo.login}
              onChangeText={onLoginChanged}
            />
          </ListItem>
          <ListItem title="ГОРОД">
            <Image source={icons.location} />
            <RNPickerSelect
              placeholder={{label: 'Выберите...', value: null}}
              value={profileInfo.city || cities[0]}
              style={pickerStyles}
              onValueChange={onCityChanged}
              items={cities.map((city: City) => ({
                label: city.title,
                value: city.id,
              }))}
            />
          </ListItem>
          <ListItem title="КТО ВЫ">
            <Image source={icons.gender} />
            <RNPickerSelect
              placeholder={{}}
              value={profileInfo.gender}
              style={pickerStyles}
              onValueChange={onGenderChanged}
              items={genders}
            />
          </ListItem>
          <ListItem title="СЕМЕЙНОЕ ПОЛОЖЕНИЕ">
            <Image source={icons.marriage} />
            <RNPickerSelect
              placeholder={{label: 'Выбор семейного положения...', value: null}}
              value={profileInfo.familyStatus}
              style={pickerStyles}
              onValueChange={onFamilyStatusChanged}
              items={FamilyStatus}
            />
          </ListItem>
          <ListItem title="ДЕТИ">
            <Image source={icons.kids} />
            <RNPickerSelect
              placeholder={{label: 'Наличие детей...', value: null}}
              value={profileInfo.hasChildren}
              style={pickerStyles}
              onValueChange={onChildrenChanged}
              items={childrenOptions}
            />
          </ListItem>
          <ListItem title={iLookFor().toUpperCase()}>
            <Image source={icons.gender} />
            <RNPickerSelect
              placeholder={{label: `${iLookFor()}...`, value: null}}
              value={profileInfo.purposeGender}
              style={pickerStyles}
              onValueChange={onPurposeGenderChange}
              items={PurposeGenderForPicker}
            />
          </ListItem>
          <ListItem title="ЦЕЛЬ ЗНАКОМСТВА">
            <Image source={icons.targetWhite} />
            <RNPickerSelect
              placeholder={{label: 'Цель знакомства...', value: null}}
              value={profileInfo.purposeDating}
              style={pickerStyles}
              onValueChange={onPurposeDatingChange}
              items={PurposeDatingForPicker}
            />
          </ListItem>
          <ListItem
            title="ИНТЕРЕСУЮЩИЙ ВОЗРАСТ"
            postTitle={`от ${profileInfo.interestingAgeMin} до ${profileInfo.interestingAgeMax}`}
            withoutBorder={true}>
            <MultiSlider
              values={[
                profileInfo.interestingAgeMin,
                profileInfo.interestingAgeMax,
              ]}
              sliderLength={wW - 61}
              min={18}
              max={80}
              step={1}
              snapped
              selectedStyle={{backgroundColor: activeColor}}
              unselectedStyle={{backgroundColor: inactiveColor}}
              markerStyle={styles.marker}
              containerStyle={{paddingTop: 25, paddingHorizontal: 10}}
              onValuesChange={onInterestedYearsValuesChange}
            />
          </ListItem>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => {
              descriptionTextInputRef.current.focus();
            }}>
            <ListItem title="РАССКАЖИТЕ О СЕБЕ">
              <View style={{alignSelf: 'flex-start', marginTop: 13}}>
                <Image source={icons.info} style={{marginRight: 12}} />
              </View>
              <View style={{flexDirection: 'row'}}>
                <TextInput
                  ref={(input) => {
                    descriptionTextInputRef.current = input;
                  }}
                  multiline={true}
                  numberOfLines={5}
                  value={profileInfo.description}
                  onChangeText={onDescriptionTextChange}
                  style={descriptionStyles}
                />
                <Text style={descriptionCounterStyles}>
                  {`${
                    profileInfo.description ? profileInfo.description.length : 0
                  }/200`}
                </Text>
              </View>
            </ListItem>
          </TouchableOpacity>
          {profiles.map((profile: Profile, index: number) => (
            <View key={profile.id}>
              {profiles.length === 2 && (
                <View style={styles.profileContainer}>
                  <Text style={styles.listItemContent}>
                    {profile.gender === 'M' ? 'Мужчина' : 'Женщина'}
                  </Text>
                </View>
              )}
              <AuthComponentsProfileForm profile={profile} />
            </View>
          ))}
          <View style={styles.bottomButtonContainer}>
            <UDButton
              title="Сохранить"
              style={styles.bottomButton}
              onPress={onSavePressed}
            />
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }),
);

export default UserScreensEditProfile;

import React, { useCallback } from 'react';
import { ImageBackground, SafeAreaView, Text } from 'react-native';
import { ITYPES } from '../../../../../store/store-types';

import * as screenNames from '../../../../navigation/screen-names';
import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';

const UserScreensVerificationRequired = ({ navigation }: { navigation: ITYPES['Navigation']['navigation'] }) => {
  const onVerifyPress = useCallback(() => {
    navigation.navigate(screenNames.VERIFICATION_FIRST_STEP_SCREEN);
  }, []);
  const onBack = useCallback(() => { 
    const goBack = navigation.getParam('goBack');
    goBack && typeof goBack === 'function' ? goBack(navigation.goBack) : navigation.goBack();
  }, []);
  return (
    <ImageBackground source={icons.rain} style={styles.backgroundImage}>
      <SafeAreaView style={{ flex: 1 }}>
        <Header title="" onBack={onBack} withoutBorder/>
        <Text style={styles.label}>Доступно только для верифицированных пользователей</Text>
        <UDButton style={styles.verifyButton} onPress={onVerifyPress} title="Верифицироваться сейчас"/>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default UserScreensVerificationRequired;
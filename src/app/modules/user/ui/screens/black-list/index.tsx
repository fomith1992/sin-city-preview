import React, { useEffect, useCallback } from 'react';
import { SafeAreaView, FlatList } from 'react-native';
import { inject, observer } from 'mobx-react';
import { TYPES, ITYPES } from '../../../../../store/store-types';

import Header from '../../../../layout/ui/components/header';
import UserAsListItem from '../../components/user-as-list-item';

interface props {
  blockedUsersStore: ITYPES['BlockedUsersStore']
  navigation: any
}

const BlackListScreen = inject(TYPES.BlockedUsersStore)(observer(
  ({ blockedUsersStore, navigation }: props) => {
    useEffect(() => {
      blockedUsersStore.search();
      window.fLogEventObj('blacklist_openScreen',
        {screenName: 'blacklist'});
      }, []);
    const onUnblockPress = useCallback((userId) => {
      blockedUsersStore.unblockUser(userId);
      window.fLogEventObj('blacklist_clickDeleteFromBlack',
        {user_id: userId});
      }, []);
    const onUserPress = useCallback(() => { blockedUsersStore.openUserProifile(); }, []);
    const onEndReached = useCallback(() => { blockedUsersStore.loadNextPage(); }, []);
    const onBackPressed = useCallback(() => { navigation.pop(); }, []);
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#000' }}>
        <Header title="Черный список" onBack={onBackPressed}/>
        <FlatList data={blockedUsersStore.resultList}
                  onEndReached={onEndReached}
                  keyExtractor={(item) => String(item.id)}
                  renderItem={({ item }) =>
                    <UserAsListItem
                      {...item}
                      onRemovePress={onUnblockPress}
                      onPress={onUserPress} />}/>
      </SafeAreaView>
    );
  }
));

export default BlackListScreen;

import React, { useCallback } from 'react';
import { ImageBackground, SafeAreaView, Text, View } from 'react-native';
import { ITYPES } from '../../../../../store/store-types';

import * as screenNames from '../../../../navigation/screen-names';
import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';

const UserScreensPaymentRequiredScreen = ({ navigation }: { navigation: ITYPES['Navigation']['navigation'] }) => {
  const isMessageAttempt = navigation.getParam(`chatMessage`)
  const onGetPremiumPress = useCallback(() => {
    navigation.navigate(screenNames.GET_PREMIUM_SCREEN);
  }, []);
  const onBack = useCallback(() => {
    const goBack = navigation.getParam('goBack');
    goBack && typeof goBack === 'function' ? goBack(navigation.goBack) : navigation.goBack();
  }, []);
  return (
    <ImageBackground source={icons.rain} style={styles.backgroundImage}>
      <SafeAreaView style={{ flex: 1 }}>
        <Header title="" onBack={onBack} withoutBorder/>
        {isMessageAttempt ? (
          <View>
            <Text style={styles.label}>Спасибо, что вы с нами. Оцените энтузиазм и труд нашей команды.</Text>
            <Text style={styles.labelItem}>Оформите подписку и продолжайте общение без ограничений</Text>
          </View>
        ) : (
          <Text style={styles.label}>Доступно только для пользователей с подпиской</Text>
        )}

        <UDButton style={styles.verifyButton} onPress={onGetPremiumPress} title="Оплатить подписку"/>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default UserScreensPaymentRequiredScreen;

import {Dimensions, StyleSheet} from 'react-native';

import fontFamily from '../../../../../styles/fonts';

const isSmallScreen = Dimensions.get('window').height < 600;
const isMediumScreen = Dimensions.get('window').height < 700;

export default StyleSheet.create({
  backgroundImage: { flex: 1, width: '100%', backgroundColor: '#000'},
  label: {
    marginTop: isSmallScreen ? 110 : isMediumScreen ? 120 : 160,
    marginHorizontal: 20, color: '#fff', ...fontFamily('fontTitleBold'), fontSize: 24, textAlign: 'center'
  },
  labelItem: { marginTop: 60, marginHorizontal: 20, color: '#fff', ...fontFamily('fontTitleBold'), fontSize: 24, textAlign: 'center' },
  verifyButton: { position: 'absolute', bottom: 64, left: 40, right: 40 }
})

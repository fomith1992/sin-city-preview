import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  content: { 
    flex: 1, 
    justifyContent: 'flex-start', 
    paddingHorizontal: 35,
    paddingTop: 160
  },
  title: {
    ...fontFamily('fontTitleBold'),
    fontSize: 24,
    color: '#fff',
    textAlign: 'center',
    marginBottom: 25
  },
  subtitle: {
    ...fontFamily('fontTitleBold'),
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
    marginBottom: 25
  }
})
import React from 'react';
import { SafeAreaView, Text, View } from 'react-native';

import styles from './style';
import Header from '../../../../layout/ui/components/header';

const AldreadyVerifiedScreen = ({ navigation }: any) => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#000' }}>
      <Header title="" onBack={() => { navigation.pop() }} withoutBorder/>
      <View style={styles.content}>
        <Text style={styles.title}>Верифицикация</Text>
        <Text style={styles.subtitle}>Вы верифицированы</Text>
      </View>
    </SafeAreaView>
  );
}

export default AldreadyVerifiedScreen;
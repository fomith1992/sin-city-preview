import { StyleSheet, Dimensions } from 'react-native';

import fontFamily from '../../../../../styles/fonts';
const wH = Dimensions.get('window').height;
const wW = Dimensions.get('window').width;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    justifyContent: 'space-between'
  },
  eventPhoto: {
    ...StyleSheet.absoluteFillObject,
    height: wH * .48,
    width: '100%',
    resizeMode: 'contain',
  },
  eventInfoContainer: {
    backgroundColor: '#000',
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    paddingTop: 15,
  },
  eventInfoBlock: {
    borderBottomColor: '#ffffff33',
    borderBottomWidth: 1,
    paddingRight: 40,
    paddingLeft: 20,
    paddingVertical: 20,
  },
  title: {
    color: '#fff',
    fontSize: 19,
    ...fontFamily('fontTitleBold')
  },
  primary: {
    color: '#fff',
    fontSize: 14,
    ...fontFamily('fontBaseMedium')
  },
  secondary: {
    color: '#999999',
    fontSize: 12,
    ...fontFamily('fontBaseMedium')
  },
  caption: {
    color: '#ffbd21',
    fontSize: 14,
    ...fontFamily('fontTitleBold')
  },
  assignButtonContainer: { 
    flexDirection: 'row',
    position: 'absolute', 
    top: -85, 
    left: 20, 
    zIndex: 33
  },
  assignButton: {
    marginRight: 10,
    width: 60,
    height: 60
  },
  ownerContainer: { 
    flexDirection: 'column', 
    paddingTop: 20, 
    paddingBottom: 15, 
    minHeight: 0  
  },
  ownerText: { 
    ...fontFamily('fontTitleBold'),
    fontSize: 24, 
    color: '#fff',
  },
  activeStatusText: { 
    ...fontFamily('fontBaseMedium'),
    fontSize: 12, 
    color: '#ffbd21',
    marginTop: 8
  },
  inactiveStatusText: { 
    ...fontFamily('fontBaseMedium'),
    fontSize: 12, 
    color: '#999',
    marginTop: 8
  },
  headerTitleContainer: {
    backgroundColor: '#00000034', 
    borderRadius: 12, 
    paddingHorizontal: 15, 
    paddingVertical: 5 
  },
  photosContainer: {
    position: 'absolute', 
    height: wH * .6,
    width: wW
  },
  infoRow: { 
    flexDirection: 'row', 
    marginVertical: 5, 
    alignItems: 'center' 
  }
});
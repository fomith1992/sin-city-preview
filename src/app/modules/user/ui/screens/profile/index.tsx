import React, { useRef, useCallback, useEffect, useState} from 'react';
import { ActivityIndicator, StyleSheet, View, Text, Image, BackHandler } from 'react-native';
import { inject, observer } from 'mobx-react';
import * as _ from 'lodash';
import ActionSheet from 'react-native-action-sheet';
import { TYPES, ITYPES } from '../../../../../store/store-types';

import * as screenNames from '../../../../navigation/screen-names';

import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import { Profile } from '../../../../user/domain/interfaces/Profile';
import UDButton from '../../../../ud-ui/components/ud-button';
import PhotosViewer from '../../components/photos-viewer';
import InfoRow from '../../components/info-row';
import GenderInfoBlock from '../../components/gender-info-block';
import NotVerified from '../../components/not-verified';
import { ShownUserInformation } from '../../../domain/interfaces/ProfileManager';

import ProfileScreenPattern from '../../components/profile-screen-wrapper';

const getMainInfo = ({ userInformation }: { userInformation: any }) => {
  const { city, gender, familyStatus, hasChildren } = userInformation;
  const { purposeDating, purposeGender } = userInformation;
  return [
    { text: city, icon: icons.locationGrey},
    { text: gender, icon: icons.genderGrey},
    { text: familyStatus, icon: icons.marriageGrey},
    { text: hasChildren, icon: icons.kidsGrey},
    { text: purposeGender, icon: icons.genderGrey},
    { text: purposeDating, icon: icons.target},
  ]
}

const ScrollableContent = ({ userInformation }: { userInformation: ShownUserInformation }) => {
  const { description, profiles } = userInformation;
  return (
    <>
      <View style={styles.eventInfoBlock}>
        { getMainInfo({ userInformation }).map((item, index) => <InfoRow key={index} {...item} />) }
      </View>
      {Boolean(description) &&
        <View style={styles.eventInfoBlock}>
          <Text style={[styles.caption, { marginBottom: 12 }]}>О себе</Text>
          <Text style={[styles.primary, { lineHeight: 24 }]}>{description}</Text>
        </View>
      }
      { profiles?.map((profile: Profile, i: number, a: Profile[]) =>
          <GenderInfoBlock key={i} {...{ profile, isLast: i === a.length-1 }}/>
        )
      }
    </>
  );
}

const HeaderComponent = ({ title, onBack, onEdit }: { title: string, onBack: any, onEdit: any }) => {
  return (
    <Header titleContainerStyle={styles.headerTitleContainer}
            title={title}
            withoutBorder
            icon={icons.dots}
            onPress={onEdit}
            isProfileHeader
            onBack={onBack} />
  );
}

const StatusComponent = ({ user }: { user?: any}) => {
  const { online, lastActivity } = user;
  if (online) {
    return <Text style={styles.activeStatusText}>online</Text>;
  } else {
    return <Text style={styles.inactiveStatusText}>{lastActivity}</Text>;
  }
}

const ContentComponent = ({ user }: { user?: ShownUserInformation }) => {
  return (<>
    <View style={[styles.eventInfoBlock, styles.ownerContainer]}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        { user && user.isVerified &&
          <Image source={icons.verified} style={{ marginRight: 10 }}/>}
        <Text style={styles.ownerText}>{user && user.login}</Text>
      </View>
      {/*<StatusComponent user={user}/>*/}
    </View>
    </>
  );
}

const ActionButtons = observer(({ onMessagePress, onLikePress, iLikedIt, heFavorite, setFavourite }: { onMessagePress: any, onLikePress: Function, iLikedIt: boolean, heFavorite: boolean, setFavourite: Function }) => {
  return (
    <>
      <UDButton onPress={() => { onMessagePress() }}
                icon={icons.profileWrite}
                style={styles.assignButton}/>
      <UDButton onPress={() => { onLikePress() }}
                icon={iLikedIt ? icons.likeOnIcon : icons.profileHeart}
                style={[styles.assignButton, { backgroundColor: '#fff' }]}/>
      <UDButton onPress={() => { setFavourite() }}
                icon={heFavorite ? icons.favouriteOnIcon : icons.profileStar}
                style={[styles.assignButton, { backgroundColor: '#fff' }]}/>
    </>
  );
});

const MyProfileScreen = inject(TYPES.ProfileManager)(observer(
  ({ profileManager, navigation }: { profileManager: ITYPES['ProfileManager'], navigation: any }) => {
    const { userInformation, isCurrentUserNotVerified } = profileManager;
    const userId = useRef(profileManager.currentUserId);
    const [photoIndex, setPhotoIndex] = useState(0)
    const [showPhotos, setShowPhotos] = useState(false);
    const backHandler = useRef(null);
    useEffect(() => {
      window.fLogEventObj('profile_openUserProfile', {user_id: userId.current});
      backHandler.current = BackHandler.addEventListener('hardwareBackPress', onBack);
      return () => { backHandler.current.remove(); }
    }, []);

    const onLikePress = useCallback(() => {
      profileManager.setLike(userId.current);
      window.fLogEventObj('profile_clickUserLike', {user_id: userId.current});
    }, [userId.current, userInformation]);
    const onSetFavouritePress = useCallback(() => {
      window.fLogEventObj('profile_clickUserFavorite', {user_id: userId.current});
      profileManager.setFavourite(userId.current);
    }, [userId.current, userInformation]);
    const onPhotoLikePress = useCallback((id: number, isLiked: boolean) => {
      window.fLogEventObj('profile_clickUserPhotoLike',
        {user_id: userId.current, photo_id: id});
      profileManager.setPhotoLike(id);
    }, []);
    const onBack = useCallback(() => {
      profileManager.isCurrentUserNotVerified = false;
      const goBack = navigation.getParam('goBack');
      goBack && typeof goBack === 'function' ? goBack(navigation.goBack) : navigation.goBack();
      return true;
    }, [profileManager.isCurrentUserNotVerified, navigation]);
    const onVerifyPress = useCallback(() => {
      navigation.navigate(screenNames.VERIFICATION_FIRST_STEP_SCREEN)
    }, []);
    const onClosePhotosViewer = useCallback(() => {
      setShowPhotos(false);
    }, []);

    const onDotsPress = useCallback(() => {
       let isGoFromChat = navigation.getParam('isGoFromChat');
      const options = ['Заблокировать', 'Отмена'];
      ActionSheet.showActionSheetWithOptions({
        options: options,
        cancelButtonIndex: 1
      }, (buttonIndex) => { profileManager.doAction(buttonIndex, isGoFromChat); });
    }, [userInformation, navigation]);

    if (isCurrentUserNotVerified && false) {
        const userLogin = navigation.getParam('userLogin');
      return <NotVerified {...{ onBack, onVerifyPress, userInformation, userLogin }}/>
    } else if (_.isEmpty(userInformation) || profileManager.isLoading) {
      if (isCurrentUserNotVerified && false) { return <NotVerified {...{ onBack, onVerifyPress, userInformation }}/> }
      return <ActivityIndicator size="large" style={{ flex: 1, backgroundColor: '#000' }}/>
    }

    const { photos } = userInformation;

    if (showPhotos && !_.isEmpty(photos) && photos) {
      return <PhotosViewer
              images={photos}
              shownImageIndex={photoIndex}
              onLikePress={onPhotoLikePress}
              onClose={onClosePhotosViewer} />
    }

    return (
      <ProfileScreenPattern
        logEventScroll={() => {
          window.fLogEventObj('events_openUserDescription',
            {user_id: userId.current})
        }}
        header={<HeaderComponent title={_.isEmpty(photos) ? '' : `${photoIndex+1}/${photos && photos.length}`}
                                 onEdit={onDotsPress}
                                 onBack={onBack}/>}
        photos={photos}
        onPhotoIndexChange={setPhotoIndex}
        actionButtons={<ActionButtons onMessagePress={() => {
          profileManager.sendMessage()
          window.fLogEventObj('profile_clickMsgBtn', {user_id: userId.current});
        }}
                                      onLikePress={onLikePress}
                                      setFavourite={onSetFavouritePress}
                                      iLikedIt={userInformation.iLikedIt}
                                      heFavorite={userInformation.heFavorite}/>}
        contentComponent={userInformation ? <ContentComponent user={userInformation}/> : null}
        scrollabelContent={profileManager ? <ScrollableContent userInformation={userInformation}/> : null}
        onPhotoPress={() => {
          setShowPhotos(true);
          window.fLogEventObj('profile_openUserPhoto', {user_id: userId.current});
        }}/>
    );
  }
))

export default MyProfileScreen;

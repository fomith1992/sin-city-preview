import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  avatarTextWrap: {
    backgroundColor: 'rgba(110, 110, 110, .7)',
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center'
  },
  avatarText: {
    ...fontFamily('fontTitleBold'),
    color: '#fff',
  },
  setAvatarButton: {
    paddingHorizontal: 52.5,
  }
})
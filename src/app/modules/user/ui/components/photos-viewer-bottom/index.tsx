import React from 'react';
import { Text, View } from 'react-native';

import styles from './style';
import UDButton from '../../../../ud-ui/components/ud-button';

type props = {
  isAvatar: boolean
  onSetAvatarPressed: () => any
}

const UserComponentsPhotosViewerBottom = ({ isAvatar, onSetAvatarPressed }: props) => {
  if (isAvatar) {
    return (
      <View style={styles.avatarTextWrap}>
        <Text style={styles.avatarText}>Это Ваш Аватар</Text>
      </View>
    )
  }
  return (
    <UDButton
      style={styles.setAvatarButton}
      title="Выбрать в качестве аватара"
      onPress={onSetAvatarPressed}/>
  );
};

export default UserComponentsPhotosViewerBottom;
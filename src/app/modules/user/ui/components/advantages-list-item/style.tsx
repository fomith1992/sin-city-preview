import { StyleSheet, Dimensions } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

const wW = Dimensions.get('window').width;

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: wW * .6,
    justifyContent: 'space-between'
  },
  label: {
    ...fontFamily('fontBaseMedium'),
    textAlign: 'center',
    marginLeft: 14,
    fontSize: 14,
    color: '#fff'
  }
})
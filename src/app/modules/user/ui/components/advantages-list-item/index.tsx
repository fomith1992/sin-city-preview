import React from 'react';
import { Text, Image, View,  } from 'react-native';

import styles from './style';

const AdvantagesListItem = ({ label, icon }: { label: string, icon: number}) => {
  return (
    <View style={styles.container}>
      <Image source={icon}/>
      <Text style={styles.label}>{label}</Text>
    </View>
  );
};

export default AdvantagesListItem;
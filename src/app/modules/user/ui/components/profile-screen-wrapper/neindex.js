import React, { Component } from 'react';
import { SafeAreaView, View, Dimensions, Image, Animated, StyleSheet, Platform, TouchableOpacity } from 'react-native';
import { 
  PanGestureHandler, 
  State, 
  TapGestureHandler,
  NativeViewGestureHandler
} from 'react-native-gesture-handler';
import { getStatusBarHeight, getBottomSpace } from 'react-native-iphone-x-helper';
import * as _ from 'lodash';
import propTypes from 'prop-types';


import styles from './style';
import icons from '../../../../icons';

const wH = Dimensions.get('window').height;
const wW = Dimensions.get('window').width;
const SNAP_POINTS_FROM_TOP = [
  getStatusBarHeight([true]) + (Platform.OS === 'ios' ? 140 : 120 ), 
  wH * 0.56
];
export default class ProfileScreen extends Component {
  static propTypes = {
    photos: propTypes.array,
    onPhotoPress: propTypes.func,
    contentComponent: propTypes.node,
    scrollabelContent: propTypes.node,
    actionButtons: propTypes.node,
  }
  masterdrawer = React.createRef();
  drawer = React.createRef();
  drawerheader = React.createRef();
  imagesScroll = React.createRef();
  scroll = React.createRef();
  viewRef = React.createRef();
  constructor(props) {
    super(props);
    const START = SNAP_POINTS_FROM_TOP[0];
    const END = SNAP_POINTS_FROM_TOP[SNAP_POINTS_FROM_TOP.length - 1];

    this.state = {
      lastSnap: END,
      photoIndex: 1,
      showPhotos: false,
    };

    this._lastScrollYValue = 0;
    this._lastScrollY = new Animated.Value(0);
    this._onRegisterLastScroll = Animated.event(
      [{ nativeEvent: { contentOffset: { y: this._lastScrollY } } }],
      // {  listener : ({ nativeEvent }) => this._onHandlerStateChange({ nativeEvent }) }
      { useNativeDriver: true }
    );
    this._lastScrollY.addListener(({ value }) => {
      this._lastScrollYValue = value;
    });

    this._dragY = new Animated.Value(0);
    this._onGestureEvent = Animated.event(
      [{ nativeEvent: { translationY: this._dragY } }],
      { useNativeDriver: true }
    );

    this._reverseLastScrollY = Animated.multiply(
      new Animated.Value(-1),
      this._lastScrollY
    );

    this._translateYOffset = new Animated.Value(END);
    this._translateY = Animated.add(
      this._translateYOffset,
      Animated.add(this._dragY, this._reverseLastScrollY)
    ).interpolate({
      inputRange: [START, END],
      outputRange: [START, END],
      extrapolate: 'clamp',
    });

    this._imageBlurRaduis = this._translateY.interpolate({
      inputRange: [SNAP_POINTS_FROM_TOP[0], SNAP_POINTS_FROM_TOP[1]],
      outputRange: [10, 0]
    })
  }

  _onHeaderHandlerStateChange = ({ nativeEvent }) => {
    if (nativeEvent.oldState === State.BEGAN) {
      this._lastScrollY.setValue(0);
    }
    this._onHandlerStateChange({ nativeEvent });
  };

  _onHandlerStateChange = ({ nativeEvent }) => {
    if (nativeEvent.oldState === State.ACTIVE) {
      let { velocityY, translationY } = nativeEvent;
      translationY -= this._lastScrollYValue;
      const dragToss = 0.5;
      const endOffsetY =
        this.state.lastSnap + translationY + dragToss * velocityY;

      let destSnapPoint = SNAP_POINTS_FROM_TOP[0];
      for (let i = 0; i < SNAP_POINTS_FROM_TOP.length; i++) {
        const snapPoint = SNAP_POINTS_FROM_TOP[i];
        const distFromSnap = Math.abs(snapPoint - endOffsetY);
        if (distFromSnap < Math.abs(destSnapPoint - endOffsetY)) {
          destSnapPoint = snapPoint;
        }
      }
      this.setState({ lastSnap: destSnapPoint });
      this._translateYOffset.extractOffset();
      this._translateYOffset.setValue(translationY);
      this._translateYOffset.flattenOffset();
      this._dragY.setValue(0);
      Animated.spring(this._translateYOffset, {
        velocity: velocityY,
        tension: 12,
        friction: 50,
        toValue: destSnapPoint,
        useNativeDriver: true,
      }).start();
      
    }
  };
  
  onImageScrollEnd = ({ nativeEvent }) => {
    const photoIndex = Math.floor(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
    this.setState({ photoIndex });
    this.props.onPhotoIndexChange(photoIndex);
  }

  renderPhotos = () => {
    const { photos } = this.props;
    const imagesList = _.isEmpty(photos) ? [icons.profilePlaceholder] : photos;
    const onPhotoPress = () => { this.props.onPhotoPress(); };
    return (
      <Animated.ScrollView 
        ref={this.imagesScroll}
        onMomentumScrollEnd={this.onImageScrollEnd}
        style={[styles.photosContainer, { opacity: 1 }]} 
        horizontal 
        pagingEnabled>
        {imagesList.map((photo, key) =>
        <TouchableOpacity key={key}  
                          onPress={onPhotoPress} 
                          activeOpacity={.95}>
          <Animated.Image style={{ width: wW, height: '100%' }} 
                          resizeMode="cover" 
                          blurRadius={this._imageBlurRaduis}
                          source={typeof photo === 'number' ? photo : { uri: photo.photoUrl }}/>
        </TouchableOpacity>
      )}
      </Animated.ScrollView>
    );
  }

  renderActionButtons = () => {
    const actionButtonsContainerStyles = [
      styles.assignButtonContainer,
      { transform: [{ translateY: this._translateY }] }
    ]
    return (
      <Animated.View style={actionButtonsContainerStyles}>
        {this.props.actionButtons }
      </Animated.View>
    );
  }

  render() {
    const { lastSnap } = this.state;

    return (
      <View style={{ flex: 1, backgroundColor: '#000' }}>
        { this.renderPhotos() }
        { this.renderActionButtons() }
        <TapGestureHandler
          maxDurationMs={100}
          ref={this.masterdrawer}
          maxDeltaY={lastSnap - SNAP_POINTS_FROM_TOP[0]}>
          <SafeAreaView style={{ flex: 1,  }} pointerEvents="box-none">
            { this.props.header }
            <Animated.View
              style={[
                StyleSheet.absoluteFillObject,
                { maxHeight: wH - SNAP_POINTS_FROM_TOP[0] },
                { backgroundColor: '#770'},
                styles.eventInfoContainer,
                {
                  transform: [{ translateY: this._translateY }],
                },
              ]}>
              <PanGestureHandler
                ref={this.drawerheader}
                simultaneousHandlers={[this.scroll, this.masterdrawer]}
                shouldCancelWhenOutside={false}
                onGestureEvent={this._onGestureEvent}
                onHandlerStateChange={this._onHeaderHandlerStateChange}>
                <Animated.View> 
                  { this.props.contentComponent }
                </Animated.View>
              </PanGestureHandler>
              <PanGestureHandler
                ref={this.drawer}
                simultaneousHandlers={[this.scroll, this.masterdrawer]}
                shouldCancelWhenOutside={false}
                onGestureEvent={this._onGestureEvent}
                onHandlerStateChange={this._onHandlerStateChange}>
                <Animated.View>
                  <NativeViewGestureHandler
                    ref={this.scroll}
                    waitFor={this.masterdrawer}
                    simultaneousHandlers={this.drawer}>
                      <Animated.ScrollView 
                        bounces={false}
                        scrollEnabled={lastSnap === SNAP_POINTS_FROM_TOP[0]}
                        style={{ marginBottom: getBottomSpace(), maxHeight: wH - 100 - SNAP_POINTS_FROM_TOP[0] }}
                        onScrollBeginDrag={this._onRegisterLastScroll}
                        scrollEventThrottle={0.1}>
                        { this.props.scrollabelContent }
                      </Animated.ScrollView>
                  </NativeViewGestureHandler>
                </Animated.View>
              </PanGestureHandler>
            </Animated.View>
          </SafeAreaView>
        </TapGestureHandler>
      </View>
    );
  }
}

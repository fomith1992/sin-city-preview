import React, { useRef, useState, useCallback } from 'react';
import { ActivityIndicator, SafeAreaView, Dimensions, Animated, StyleSheet, Platform, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import {
  PanGestureHandler,
  State,
} from 'react-native-gesture-handler';
import { getStatusBarHeight, getBottomSpace } from 'react-native-iphone-x-helper';
import * as _ from 'lodash';
import { observer } from 'mobx-react';

import styles from './style';
import icons from '../../../../icons';
import UDFastImageLoader from '../../../../ud-ui/components/ud-fast-image-loader';
import UDProgressiveImage from '../../../../ud-ui/components/ud-progressive-image';

const wH = Dimensions.get('window').height;
const wW = Dimensions.get('window').width;
const SNAP_POINTS_FROM_TOP = [
  getStatusBarHeight(true) + (Platform.OS === 'ios' ? 140 : 120 ),
  wH * 0.56
];

interface PhotosProps {
  photos: string[],
  onPhotoPress: any,
  imagesScroll: any,
  onPhotoIndexChange: any,
  blurRadius: any,
}

let isScrolled = 0;

const Photos = observer(({ photos, onPhotoPress, imagesScroll, onPhotoIndexChange, blurRadius }: PhotosProps) => {
  const imagesList = _.isEmpty(photos) ? [icons.profilePlaceholder] : photos;

  const onImageScrollEnd = useCallback(({ nativeEvent }: { nativeEvent: any }) => {
    const photoIndex = Math.round(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
    onPhotoIndexChange(photoIndex);
  }, []);

  const isPlaceholder = _.isEmpty(photos);

  return (
    <Animated.ScrollView
      ref={imagesScroll}
      onScroll={onImageScrollEnd}
      style={[styles.photosContainer, { opacity: blurRadius }]}
      horizontal
      bounces={false}
      pagingEnabled>
        {imagesList.map((photo, key) => {
            return (
              <TouchableOpacity key={key}
                                onPress={onPhotoPress}
                                activeOpacity={.95}>
                { typeof photo === 'number' ?
                  <UDFastImageLoader source={photo}
                                     resizeMode={FastImage.resizeMode.cover}
                                     style={{ width: wW, height: '100%' }}/>
                  : <UDProgressiveImage photo={photo}
                                      resizeMode={FastImage.resizeMode.cover}
                                      style={{ width: wW, height: '100%' }}/>}

              </TouchableOpacity>
            )
          })}
    </Animated.ScrollView>
  );
});

const ActionButtons = ({ translateY, ActionButtonsComponent }: { translateY: Animated.Animated, ActionButtonsComponent: any }) => {
  const actionButtonsContainerStyles = [
    styles.assignButtonContainer,
    { transform: [{ translateY }] }
  ];
  return (
    <Animated.View style={actionButtonsContainerStyles}>
      { ActionButtonsComponent }
    </Animated.View>
  );
};

const UserComponentsProfileScreenWrapper = observer(({
  header,
  contentComponent,
  scrollabelContent,
  actionButtons,
  photos,
  onPhotoPress,
  onPhotoIndexChange,
  imagesScroll,
  logEventScroll
}: any) => {

  const scrolledTrigger = (inner) =>  {
    if (isScrolled > 2) return;
    if (isScrolled === 1 || (inner && isScrolled < 1)) {
     // console.log('TRIGGER_SCROLLED');
      //   window.fLogEvent('')
      if (!!logEventScroll) logEventScroll();
      if (inner) isScrolled ++;
    }
    isScrolled++;
    // console.log(isScrolled);
  };

  const masterdrawer = useRef<any>();
  const drawerheader = useRef<any>();
  const scroll = useRef<any>();
  const START = useRef(SNAP_POINTS_FROM_TOP[0]);
  const END = useRef(SNAP_POINTS_FROM_TOP[1]);
  const [scrollabelContentHeight, setScrollableContentHeight] = useState(wH - 100 - SNAP_POINTS_FROM_TOP[0]);

  const [lastSnap, setLastSnap] = useState(END.current)

  const onContentLayout = useCallback(({ nativeEvent: { layout } }: any) => {
    isScrolled = 0;
    setScrollableContentHeight(wH - (layout.height + getBottomSpace() +  10));
  }, []);

  const _lastScrollYValue = useRef(0);
  const _lastScrollY = useRef(new Animated.Value(0));

  _lastScrollY.current.addListener(({ value }) => {
    _lastScrollYValue.current = value;
    scrolledTrigger(false);
  });

  const _dragY = useRef(new Animated.Value(0));
  const _onGestureEvent = useRef(Animated.event(
    [{ nativeEvent: { translationY: _dragY.current } }],
    { useNativeDriver: true }
  ))

  const _reverseLastScrollY = useRef(Animated.multiply(
    new Animated.Value(-1),
    _lastScrollY.current
  ));

  const _translateYOffset = useRef(new Animated.Value(END.current));
  const _translateY = useRef(Animated.add(
    _translateYOffset.current,
    Animated.add(_dragY.current, _reverseLastScrollY.current)
  ).interpolate({
    inputRange: [START.current, END.current],
    outputRange: [START.current, END.current],
    extrapolate: 'clamp',
  }));

  const _imageBlurRaduis = useRef(_translateY.current.interpolate({
    inputRange: [SNAP_POINTS_FROM_TOP[0], SNAP_POINTS_FROM_TOP[1]],
    outputRange: [0.5, 1]
  }));

  const _onHeaderHandlerStateChange = useCallback(({ nativeEvent }) => {
    if (nativeEvent.oldState === State.BEGAN) {
      _lastScrollY.current.setValue(0);
    }
    _onHandlerStateChange({ nativeEvent });
  }, []);

  const _onHandlerStateChange = useCallback(({ nativeEvent }) => {
    if (nativeEvent.oldState === State.ACTIVE) {
      let { velocityY, translationY } = nativeEvent;
      translationY -= _lastScrollYValue.current;
      const dragToss = 0.15;
      const endOffsetY =
        lastSnap + translationY + dragToss * velocityY;

      let destSnapPoint = SNAP_POINTS_FROM_TOP[0];
      for (let i = 0; i < SNAP_POINTS_FROM_TOP.length; i++) {
        const snapPoint = SNAP_POINTS_FROM_TOP[i];
        const distFromSnap = Math.abs(snapPoint - endOffsetY);
        if (distFromSnap < Math.abs(destSnapPoint - endOffsetY)) {
          destSnapPoint = snapPoint;
        }
      }
      setLastSnap(destSnapPoint);
      _translateYOffset.current.extractOffset();
      _translateYOffset.current.setValue(translationY);
      _translateYOffset.current.flattenOffset();
      _dragY.current.setValue(0);
      Animated.spring(_translateYOffset.current, {
        velocity: velocityY,
        tension: 12,
        friction: 50,
        toValue: destSnapPoint,
        useNativeDriver: true,
      }).start();
    }
  }, [lastSnap, _lastScrollYValue.current, _translateYOffset.current, _dragY.current]);

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#000' }} pointerEvents="box-none">
      <Photos {...{
          photos,
          onPhotoPress: lastSnap === SNAP_POINTS_FROM_TOP[1] ? onPhotoPress : () => {},
          imagesScroll,
          blurRadius: _imageBlurRaduis.current,
          onPhotoIndexChange
        }}/>
      <ActionButtons ActionButtonsComponent={actionButtons} translateY={_translateY.current}/>
      { header }
      <Animated.View
        style={[
          StyleSheet.absoluteFillObject,
          { maxHeight: wH - SNAP_POINTS_FROM_TOP[0] },
          { backgroundColor: '#770'},
          styles.eventInfoContainer,
          {
            transform: [{ translateY: _translateY.current }],
          },
        ]}>
        <View style={styles.slideUpPanelIcon}/>
        <PanGestureHandler
          ref={drawerheader}
          simultaneousHandlers={[scroll, masterdrawer]}
          shouldCancelWhenOutside={false}
          onGestureEvent={_onGestureEvent.current}
          onHandlerStateChange={_onHeaderHandlerStateChange}>
          <Animated.View onLayout={onContentLayout}>
            { contentComponent }
          </Animated.View>
        </PanGestureHandler>
          <Animated.ScrollView
            onScrollEndDrag={() => {
              scrolledTrigger(true);
            }}
            bounces={false}
            style={{ maxHeight: scrollabelContentHeight - lastSnap }}>
            { scrollabelContent }
          </Animated.ScrollView>
      </Animated.View>
    </SafeAreaView>
  );
})
export default UserComponentsProfileScreenWrapper;

import React, { useCallback } from 'react';

import styles from './style';
import icons from '../../../../icons';
import UDButton from '../../../../ud-ui/components/ud-button';

type props = {
  onLikePress?: () => void
  onPhotoSelect?: () => void
};

const UsersComponentsPhotoActionButtons = (props: props) => {
  const { onLikePress, onPhotoSelect } = props;
  const onAddPhotoPressed = useCallback(() => { onPhotoSelect() }, [onPhotoSelect]);
  const onLikePressed = useCallback(() => { onLikePress() }, [onLikePress]);

  return (
    <>
      <UDButton
        onPress={onAddPhotoPressed}
        fontSize={16}
        title="Добавить фото"
        style={styles.assignButton}/>
      { onLikePress != null && (
        <UDButton
          onPress={onLikePressed}
          fontSize={16}
          icon={icons.likeOnIcon}
          style={[styles.assignButton, { backgroundColor: '#fff', width: 55 }]}/>
      ) }
    </>
  );
};

export default UsersComponentsPhotoActionButtons;
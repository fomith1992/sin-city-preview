import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  assignButton: {
    padding: 18,
    marginRight: 10,
    height: 55,
    alignItems: 'center',
    justifyContent: 'center'
  },
});
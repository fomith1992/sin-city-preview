import {Dimensions, StyleSheet} from 'react-native';

import fontFamily from '../../../../../styles/fonts';
const isMediumScreen = Dimensions.get('window').height < 700;
const isSmallScreen = Dimensions.get('window').height < 600;

export default StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingHorizontal: 35,
    paddingTop: 160
  },
  title: {
    ...fontFamily('fontTitleBold'),
    fontSize: 24,
    color: '#fff',
    textAlign: 'center',
    marginBottom: 25,
    marginTop: 30,
  },
  subtitle: {
    ...fontFamily('fontTitleBold'),
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
    paddingBottom: 25,
    paddingHorizontal: 16,
  },
  congratulationImage: {
   // marginTop: 130,
    marginTop: '10%',
    marginLeft: 'auto',
    marginRight: 'auto',
    height: isSmallScreen ? 200 : 300,
   // height: 100,
    resizeMode: 'contain'
  }
})

import React from 'react';
import {Text, Image, SafeAreaView, ScrollView} from 'react-native';

import styles from './style';

const AldreadyVerifiedComponent = () => {
  return (<>
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
    <Image style={styles.congratulationImage} source={require('../../../../icons/congratulations.png')}/>
    <Text style={styles.title}>Аккаунт подтвержден</Text>
    <Text style={styles.subtitle}>
      {`Поздравляем, теперь вы "Подтверждённый" участник! Спасибо, что помогаете нам сделать приложение ещё удобнее и безопаснее.`}
    </Text>
      </ScrollView>
    </SafeAreaView>
  </>);
}

export default AldreadyVerifiedComponent;

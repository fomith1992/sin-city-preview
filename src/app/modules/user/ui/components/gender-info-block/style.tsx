import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    borderBottomColor: '#ffffff33',
    borderBottomWidth: 1,
    paddingRight: 40,
    paddingLeft: 20,
    paddingVertical: 20,
  },
  caption: {
    color: '#ffbd21',
    fontSize: 14,
    ...fontFamily('fontTitleBold')
  },
})
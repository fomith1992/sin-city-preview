import React from 'react';
import { View, Text } from 'react-native';

import icons from '../../../../icons';
import styles from './style';
import InfoRow from '../info-row';
import { Profile } from '../../../../user/domain/interfaces/Profile';
import { ProfileGendersEnum } from '../../../domain/enums/Gender';
import { OrientationsEnum } from '../../../domain/enums/Orientation';
import { FigureTypesEnum } from '../../../domain/enums/FigureTypes';
import { BodyTypesEnum } from '../../../domain/enums/BodyTypes';
import { AttitudesEnum } from '../../../domain/enums/Attitudes';
import { FacialTypes } from '../../../domain/enums/FacialTypes';

const getGenderInfo = ({ gender, height, weight, orientation, typeFigure, facialHair, typePhysiques, attitudeToAlcohol, attitudeToSmoking }: Profile) => {
  return [
    { text: `${!!height && !!weight ? `${height} см / ${weight} кг.` : ''}`},
    { text: `${!height && !!weight ? `${weight} кг.` : ''}`},
    { text: `${!!height && !weight ? `${height} см.` : ''}`},
    { text: orientation ? `${OrientationsEnum[orientation]}` : 'Не указано', icon: icons.genderGrey},
    gender !== 'W' ? { text: facialHair ? `${FacialTypes[facialHair]}` : 'Не указано', icon: icons.beard} : null,
    { text: typeFigure ? `${FigureTypesEnum[typeFigure]}` : 'Не указано', icon: icons.shapeFrom},
    { text: typePhysiques ? `${BodyTypesEnum[typePhysiques]}` : 'Не указано', icon: icons.muscle},
    { text: attitudeToAlcohol ? `Отношение к алкоголю: ${AttitudesEnum[attitudeToAlcohol].toLowerCase()}` : 'Не указано', icon: icons.alcoholGrey},
    { text: attitudeToSmoking ? `Отношение к курению: ${AttitudesEnum[attitudeToSmoking].toLowerCase()}` : 'Не указано', icon: icons.smokingGrey},
  ]
}

const GenderInfoBlock = ({ profile, isLast }: { profile: Profile, isLast: boolean}) => {
  const { gender } = profile;
  const title = ProfileGendersEnum[gender];
  const containerStyles = styles.container;
  if (isLast) { containerStyles.borderBottomWidth = 0; }
  return (
    <View style={containerStyles}>
      <Text style={[styles.caption, { marginBottom: 12 }]}>{title}</Text>
      { getGenderInfo(profile).map((item, index) => <InfoRow key={index} {...item}/>)}
    </View>
  );
}

export default GenderInfoBlock;

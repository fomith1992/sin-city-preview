import React from 'react';
import LayoutComponentsHeader from "../../../../layout/ui/components/header";

import styles from "./style";
import icons from '../../../../icons';

type props = {
  photosCount: number
  shownPhotoIndex: number
  onEditPressed: () => void
  onBackPressed: () => void
  isMyProfile: boolean
};

const UsersComponentsProfileHeader = (props: props) => {
  const { photosCount, shownPhotoIndex, onEditPressed, onBackPressed, isMyProfile = false } = props;
  const title = photosCount > 0 ? `${shownPhotoIndex + 1}/${photosCount}` : '';
  return (
    <LayoutComponentsHeader
      isProfileHeader={isMyProfile}
      isMyProfile={isMyProfile}
      titleContainerStyle={styles.headerTitleContainer}
      title={title}
      withoutBorder
      icon={icons.edit}
      onPress={onEditPressed}
      onBack={onBackPressed}/>
  )
};

export default UsersComponentsProfileHeader;

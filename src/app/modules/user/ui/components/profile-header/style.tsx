import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  headerTitleContainer: {
    backgroundColor: '#00000034',
    borderRadius: 12,
    paddingHorizontal: 15,
    paddingVertical: 5
  },
});
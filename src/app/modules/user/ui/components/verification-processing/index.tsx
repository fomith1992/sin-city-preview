import React from 'react';
import { Text, View } from 'react-native'

import styles from './style';
import * as screenNames from "../../../../../modules/navigation/screen-names";
import UDButton from '../../../../ud-ui/components/ud-button';

const UserVerificationProcessingComponent = ({ navigation }) => {
    return (
        <View style={{justifyContent: 'space-between', height: '85%'}}>
          <View>
            <Text style={styles.title}>Админ уже проверяет вашу фотографию, и в течение 10 минут вы получите уведомление</Text>
            {/*<Text style={styles.subtitle}>*/}
            {/*  Мы сообщим вам о результате в течении 12 часов*/}
            {/*</Text>*/}
          </View>
          <View style={{ marginHorizontal: 25 }}>
            <UDButton title={'Перейти к аккаунтам пользователей'} onPress={() => navigation.navigate(screenNames.SEARCH_TAB)} />
          </View>
        </View>
        );
}

export default UserVerificationProcessingComponent;

import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  title: {
    ...fontFamily('fontTitleBold'),
    fontSize: 24,
    color: '#ffbd21',
    marginTop: 160,
    marginHorizontal: 20,
    textAlign: 'center'
  },
  subtitle: {
    ...fontFamily('fontTitleBold'),
    fontSize: 16,
    color: '#fff',
    marginTop: 20,
    marginHorizontal: 37,
    textAlign: 'center'
  },
  borderText: {
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    marginHorizontal: 46,
  }
})

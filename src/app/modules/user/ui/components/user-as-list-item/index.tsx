import React, { useCallback } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';

import icons from '../../../../icons';
import styles from './style';
import { BlockedUser } from '../../../domain/interfaces/BlockedUser';
import { formatLastActivity } from '../../../../ud-ui/helpers/dateFormat';

const SearchListItem = ({ id, login, lastActivity, photo, onRemovePress, onPress }: BlockedUser & { onRemovePress: any, onPress?: any }) => {
  const onItemPress = useCallback(() => { onPress && onPress(id) }, [id]);
  const onRemoveItemPress = useCallback(() => { onRemovePress(id) }, [id]);
  return (<>
    <TouchableOpacity activeOpacity={onPress ? 0 : 1} style={styles.container} onPress={onItemPress}>
      <View style={styles.leftContainer}>
        { photo && (
          <FastImage source={{ uri: photo.photoThumbnailUrl }}
                     resizeMode={FastImage.resizeMode.contain}
                     style={styles.photo}/>
        ) }
        { !photo && (
          <View style={styles.photoPlaceholderWrap}>
            <FastImage source={icons.profileListPlaceholder}
                       resizeMode={FastImage.resizeMode.contain}
                       style={styles.photoPlaceholder}/>
          </View>
        ) }
        <View style={styles.leftContent}>
          <Text style={styles.title}>{login}</Text>
          <Text style={styles.subtitle}>{`${formatLastActivity(lastActivity)}`}</Text>
        </View>
      </View>
      <View>
        <TouchableOpacity onPress={onRemoveItemPress} style={styles.iconContainer}>
          <Image source={icons.cancelSelected} />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  </>);
};

export default SearchListItem;
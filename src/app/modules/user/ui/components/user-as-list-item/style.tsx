import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 80,
    borderBottomColor: '#ffffff22',
    borderTopColor: '#ffffff22',
    borderWidth: 1,
    paddingLeft: 20,
  },
  leftContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rightContainer: {
    flexDirection: 'row'
  },
  leftContent: {
    flexDirection: 'column',
    marginLeft: 14,
    justifyContent: 'space-between'
  },
  title: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 14,
    color: '#fff'
  },
  subtitle: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 12,
    color: '#999999'
  },
  notVerifiedContainer: {
    height: 73,
    backgroundColor: '#ffbd21',
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  notVerifiedText: {
    ...fontFamily('fontTitleBold'),
    fontSize: 14,
    color: '#000',
    marginLeft: 15,
    marginRight: 70
  },
  photo: {
    width: 43,
    height: 43,
    borderRadius: 50
  },
  iconContainer: { 
    paddingLeft: 40,
    paddingHorizontal: 20, 
    paddingVertical: 7.5,
    height: 80, 
    justifyContent: 'center' 
  },

  photoPlaceholderWrap: {
    width: 43,
    height: 43,
    borderRadius: 50,
    overflow: 'hidden',
    backgroundColor: '#2D2D2D',
    alignItems: 'center',
    justifyContent: 'center'
  },
  photoPlaceholder: {
    width: 30,
    height: 22,
  },
})
import React from 'react';
import { ImageBackground, SafeAreaView, Text } from 'react-native';

import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import { ShownUserInformation } from '../../../domain/interfaces/ProfileManager';

const NotVerified = ({ onBack, onVerifyPress, userInformation, userLogin }: { onBack: Function, onVerifyPress: Function, userInformation: ShownUserInformation, userLogin: string }) => {
  return (
    <ImageBackground source={icons.rain} style={styles.backgroundImage}>
      <SafeAreaView style={{ flex: 1 }}>
        <Header title="" onBack={onBack} withoutBorder/>
        <Text style={styles.label}>{`Для просмотра ${userInformation?.login || userLogin || 'этого пользователя'} вам нужно получить статус "Подтверждённый". Это не сложно.`}</Text>
        <UDButton style={styles.verifyButton} onPress={onVerifyPress} title="Стать Подтверждённым"/>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default NotVerified;

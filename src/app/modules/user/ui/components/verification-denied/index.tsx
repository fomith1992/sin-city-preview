import React from 'react';
import {Text, ScrollView, View, Image} from 'react-native';

import styles from './style';
import UDButton from '../../../../ud-ui/components/ud-button';

const UserVerificationDeniedScreen = ({
  onTryAgainPress,
  reasonText,
}: {
  onTryAgainPress: any;
  reasonText: string;
}) => {
  return (
    <>
      <Image
        style={styles.image}
        source={require('./img/verification-denied.png')}
      />
      <Text style={styles.title}>Аккаунт не подтвержден</Text>
      <Text style={styles.subtitle}>
        Что-то пошло не так? Не расстраивайтесь. Наш админ вам поможет
      </Text>
      <View>
        <ScrollView contentContainerStyle={{flexGrow: 1}} bounces={false}>
          {Boolean(reasonText) && (
            <Text style={styles.subtitle}>{`Причина: ${reasonText}`}</Text>
          )}
        </ScrollView>
      </View>
      <View style={styles.button}>
        <UDButton
          title="Мне нужна помощь админа"
         // style={styles.button}
          onPress={onTryAgainPress}
        />
      </View>
    </>
  );
};
//1966
export default UserVerificationDeniedScreen;

import { StyleSheet, Dimensions } from 'react-native';

const isMediumScreen = Dimensions.get('window').height < 700;
const isSmallScreen = Dimensions.get('window').height < 600;
import fontFamily from '../../../../../styles/fonts';
import {resizeMode} from "react-native-fast-image";

export default StyleSheet.create({
  title: {
    ...fontFamily('fontTitleBold'),
    fontSize: isSmallScreen ? 20 : 24,
   // color: '#e84747',
    color: '#fff',

    marginHorizontal: 20,
    textAlign: 'center'
  },
  subtitle: {
    ...fontFamily('fontTitleBold'),
    fontSize: isSmallScreen ? 14 : 16,
    color: '#fff',
    marginTop: 20,
    marginHorizontal: 37,
    textAlign: 'center'
  },
  button: {
    paddingHorizontal: 40,
    marginBottom: 50,
    marginTop: isSmallScreen ? 15 : isMediumScreen ? 25 : 30,
  },
  image: {
    marginTop: isMediumScreen ? isSmallScreen ? 40 : 40 : 100,
    height: isSmallScreen ? 200 : 300,
    resizeMode: 'contain',
    marginBottom: 15,
   // marginLeft: 25,
     marginRight: 'auto',
     marginLeft: 'auto'
  },
})

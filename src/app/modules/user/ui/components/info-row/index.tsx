import React from 'react';
import { View, Text, Image } from 'react-native';

import styles from './style';

const InfoRow = ({ text, icon }: { text: string, icon?: number}) => {
  if (!text) { return null; }
  return (
    <View style={styles.infoRow}>
      { icon && <Image style={{ marginRight: 8 }} source={icon}/>}
      <Text style={styles.primary}>{text}</Text>
    </View>
  )
}

export default InfoRow;

import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  infoRow: { 
    flexDirection: 'row', 
    marginVertical: 5, 
    alignItems: 'center' 
  },
  primary: {
    color: '#fff',
    fontSize: 14,
    ...fontFamily('fontBaseMedium')
  },
})
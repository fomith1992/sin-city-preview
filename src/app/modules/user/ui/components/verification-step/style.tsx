import {Dimensions, StyleSheet} from 'react-native';

import fontFamily from '../../../../../styles/fonts';

const isMediumScreen = Dimensions.get('window').height < 700;

export default StyleSheet.create({
  container: {
   // maxWidth: '80%',
    alignSelf: 'center',
    flexDirection: 'row',
    marginBottom: 45,
    alignItems: 'center',
  },
  indexContainer: {
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,

    borderColor: 'rgba(255,189,33,0.42)',
    borderWidth: 1,
    borderRadius: 50,
  },
  indexText: {
    ...fontFamily('fontTitleBold'),
    fontSize: 11,
    lineHeight: 14,
    color: '#fff',

  },
  labelContainer: {
    flex: 1,
    marginRight: 15,
  },
  text: {
    textAlign: 'center',
    ...fontFamily('fontBaseMedium'),
    //fontSize: isMediumScreen ? 12 : 14,
    fontSize:  14,
    fontWeight: 'normal',
    lineHeight: 20,
    flexWrap: 'wrap',
    color: '#fff'
  },
  imageStyle: {
  //  marginTop: 5,
    marginRight: 15,
    marginLeft: 10,
    width: 17.5,
    resizeMode: 'contain',
  }
})

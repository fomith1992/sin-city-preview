import React from 'react';
import {Text, View, ViewStyle, TextStyle, Image} from 'react-native';

import styles from './style';
import fontFamily from '../../../../../styles/fonts';

interface props {
  index: number,
  label: string,
  style?: ViewStyle,
  labelStyle?: ViewStyle
  textStyle?: TextStyle
}

const VerificationStep = ({ index, label, style = {}, labelStyle = {}, textStyle = {},
                            isCentered = false, isBold = false, boldText, check = false}: props , ) => {
  return (
    <View style={[styles.container, style, isCentered ? ({ maxWidth: '80%'}) : null]}>
      <View style={{}}/>
        {check ? (
            <Image style={styles.imageStyle} source={require('../../screens/verification-confirmation/img/check.png')}/>
        ) : (
            <View style={[styles.indexContainer]}>
                <Text style={styles.indexText}>{index}</Text>
            </View>
        )}

      <View style={[styles.labelContainer, labelStyle]}>
        {isBold ? (
          <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          <Text style={[ styles.text, textStyle, {
            fontWeight: 'bold',
          }]}>
            {boldText}
            <Text style={[styles.text, textStyle]}>{ ' ' + label}</Text>
          </Text>

          </View>
        ) : (
          <Text style={[styles.text, textStyle]}>{label}</Text>
        )}

      </View>
      {isCentered ? (
        <View style={{marginRight: 15}}/>
      ) : null}

    </View>
  );
}

export default VerificationStep;

import React from 'react';
import { SafeAreaView, View, Text } from 'react-native';

import styles from './style';
import Header from '../../../../layout/ui/components/header';

const Locked = ({ onBack }: any) => {
  return (
    <SafeAreaView style={styles.container}>
      <Header withoutBorder title="" onBack={onBack}/>
      <View style={styles.content}>
        <Text style={styles.text}>Пользователь заблокировал вас</Text>
      </View>
    </SafeAreaView>
  );
}

export default Locked;
import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';


export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000'
  },
  content: {
    flex: 1,
    marginTop: 160,
    alignItems: 'center'
  },
  text: {
    color: '#fff',
    fontSize: 24,
    paddingHorizontal: 30,
    textAlign: 'center',
    ...fontFamily('fontTitleBold')
  }
})
import { StyleSheet, Dimensions } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

const wW = Dimensions.get('window').width;

export default StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
   // alignItems: 'center',
    backgroundColor: '#474747'
  },
  titleContainer: {
    backgroundColor: '#00000034',
    borderRadius: 12,
    paddingHorizontal: 15,
    paddingVertical: 5
  },
  bottomContainer: {
   // flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingBottom: 30,
  },
  likeButton: {
    backgroundColor: '#fff',
    borderRadius: 20,
    padding: 16,
    marginTop: 20,
    width: 53,
    height: 53,
    alignItems: 'center',
    justifyContent: 'center'
  },
  carouselContainer: {
    maxHeight: wW,
    marginTop: 30
  },
  carouselItem: {
    width: '100%',
    height: '100%'
  },
  likesCount: {
    marginTop: 12,
    fontSize: 16,
    color: '#fff',
    ...fontFamily('fontTitleBold'),
    marginBottom: 30,
  },
  setAvatarButton: {
    paddingHorizontal: 52.5,
  }
})

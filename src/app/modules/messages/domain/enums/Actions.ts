export const actions = [
  { label: 'Удалить чат для меня', value: 'REMOVE_FOR_ME' },
  { label: 'Удалить чат для всех', value: 'REMOVE_FOR_ALL' },
  { label: 'Заблокировать', value: 'BLOCK' },
]

export const favouriteActions = {
  on: { label: 'Добавить пользователя в избранное', value: 'ADD_FAVOURITE' },
  off: { label: 'Удалить пользователя из избранного', value: 'DELETE_FAVOURITE' },
}
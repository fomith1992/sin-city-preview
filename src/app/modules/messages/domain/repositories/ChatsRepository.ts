import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import chatsResource from "../resources/ChatsResource";

class ChatsRepository extends BaseRepository {
  constructor() {
    super(chatsResource)
  }

  public sendTextMessage(chatId: number, text: string) {
    return this.resource(chatId, 'add-message').create({ text })
      .then((response: any) => this.processResponse(response));
  }
  public sendPhotoMessage(chatId: number, data: any) {
   // console.log('PHOTO_DATA', data['_parts'][0][1]);
    console.log('PHOTO_DATA', data);
    return this.resource(chatId, 'add-message').create(data, { contentType: 'form-data' })
      .then((response: any) => {
       // console.log('RESPONSE', response);
        console.log('RESPONSE');
        this.processResponse(response);
        window.sendMessage(
          JSON.stringify(
            {
              // "type": "chat_message",
              "type": "chat_message",
              "action": 'photo_sent',
              "message": {
                "chat": chatId,
                // "createdAt": "2020-02-27T15:15:47.719731Z"
              }
            }
          )
        );
      })
      .catch((e) => {
        console.log('CATCH', e, 'BAOBAN');
      })
  }
  public removeChatFormMe(chatId: number) {
    return this.resource(chatId, 'remove-yourself')
      .delete().then((response: any) => this.processResponse(response));
  }
}

const chatsRepository = new ChatsRepository();

export default chatsRepository;

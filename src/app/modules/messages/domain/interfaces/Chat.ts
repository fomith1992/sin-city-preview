import { Recipient } from './Recipient';

export interface Chat {
  id:	number
  recipient: Recipient
  lastActivity:	string
  lastMessage:	string
  createdAt:	string
}
export interface Recipient {
  id:	number
  login:	string
  photo?: {
    id:	number
    photoUrl:	string
    photoThumbnailUrl:	string
  }
  lastActivity:	string
  online:	string
  heFavorite: boolean
}
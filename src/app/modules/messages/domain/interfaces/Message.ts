export interface Message {
  id: number,
  chat: number,
  sender: number,
  text: string,
  photoUrl: string,
  photoThumbnailUrl: string,
  createdAt: string,
  is_read: boolean,
}

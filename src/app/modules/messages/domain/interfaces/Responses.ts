import { ChatsListItem } from './Chat';
import { Message } from './Message';

export interface ChatResponse {
  chat: ChatsListItem
  count: number,
  next: string | null,
  previous: string | null,
  results: Message[]
}

export interface ChatsResponse {
  chat: ChatsListItem
  count: number,
  next: string | null,
  previous: string | null,
  results: ChatsListItem[]
}

export interface AddMessageResponse {
  createdAt: string, 
  id: number, 
  photo: string, 
  text: string
}
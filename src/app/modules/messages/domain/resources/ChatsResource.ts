import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../../modules/core/infrastructure/httpResource";

class ChatsResource extends BaseRestResource {
    constructor() {
        super(httpResource, 'chats', {});
    }

    public currentChatResource(chatId: number) {
        return this.child(chatId);
    }
}

const chatsResource = new ChatsResource();

export default chatsResource;
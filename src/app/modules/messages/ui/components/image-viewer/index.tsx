import React, { useState, useCallback } from 'react';
import { Modal, TouchableOpacity, SafeAreaView, View } from 'react-native';

import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import UDFastImageLoader from "../../../../ud-ui/components/ud-fast-image-loader";

const MessageComponentsImageViewer = ({ style, children, photo }: any) => {
  const [isVisible, setIsVisible] = useState(false);
  const onClosePress = useCallback(() => { setIsVisible(false); }, []);
  const onOpenPress = useCallback(() => { setIsVisible(true); }, []);

  const onRequestClose = useCallback(() => {
    if (isVisible) {
      setIsVisible(false);
    }
  }, [isVisible]);

  if (!isVisible) { 
    return (
      <TouchableOpacity style={style} 
                        onPress={onOpenPress}>
        <UDFastImageLoader source={{ uri: photo.photoThumbnailUrl }} 
                           style={styles.image} 
                           resizeMode={'cover'}/>
        {children}
      </TouchableOpacity>
    ); 
  }

  return (
    <Modal visible={isVisible} onRequestClose={onRequestClose}>
      <View style={styles.container}>
        <SafeAreaView style={{ flex: 1 }}>
          <Header title='' icon={icons.navCancel} onPress={onClosePress} withoutBorder/>
          <TouchableOpacity activeOpacity={1} 
                            onPress={onClosePress} 
                            style={styles.touchableImage}>
            <UDFastImageLoader source={{ uri: photo.photoUrl }} 
                               style={styles.image} 
                               resizeMode={'contain'}/>
          </TouchableOpacity>
        </SafeAreaView>
      </View>
    </Modal>
  );
};

export default MessageComponentsImageViewer;
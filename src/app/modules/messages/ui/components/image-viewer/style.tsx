import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  image: { width: '100%', height: '100%' },
  touchableImage: { flex: 1, marginBottom: 50 },
  container: { backgroundColor: '#000', flex: 1 }
});
import React, { memo } from 'react';
import {View, Text, Image} from 'react-native';

import styles from './style';
import ImageViewer from '../../components/image-viewer';
import icons from '../../../../icons';

interface props {
  photoUrl: string,
  createdAt: string,
  photoThumbnailUrl: string
}

const DialogPhoto = memo(({ photoUrl, photoThumbnailUrl, createdAt, isRead, isMy }: props) => {
  return (
    <ImageViewer style={styles.messagePhoto}
                 photo={{ photoUrl, photoThumbnailUrl }}>
      <View style={styles.photoDate}>
        <Text style={[styles.date, { color: '#fff', marginTop: 0 }]}>{createdAt}</Text>
        {!isRead && isMy? (

          <Image
            source={icons.check_icon}
            style={styles.unreadCounterIsZeroImage}
          />
        ) : ( isRead && isMy? (
            <Image
              source={icons.double_check_icon}
              style={styles.unreadCounterIsPositiveImage}
            />
          ) : null

        )}
      </View>
    </ImageViewer>
  );
});

export default DialogPhoto;

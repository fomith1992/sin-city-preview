import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  unreadCounterIsZeroImage: {
    tintColor: 'rgba(255,255,255,0.68)',
    //  rotation: 2,
    transform: [{ rotate: '5deg'}],
    backgroundColor: 'transparent',
    width: 18,
    height: 18,
    marginRight: 2,
    marginLeft: 5,
    // paddingBottom: 10,
    // alignSelf: 'flex-end',
    // marginBottom: 15,
    // marginHorizontal: 20,
    //  resizeMode: "contain",
  },
  unreadCounterIsPositiveImage: {
    // tintColor: 'rgba(255,255,255,0.61)',
    // backgroundColor: 'transparent',
    // width: 18,
    // height: 18,
    // marginRight: 5,
    tintColor: 'rgba(255,255,255,0.68)',
    //  rotation: 2,
    // transform: [{ rotate: '2deg'}],
    backgroundColor: 'transparent',
    width: 18,
    height: 18,
    marginRight: 2,
    marginLeft: 6,
    // alignSelf: 'flex-end',
    // marginBottom: 15,
    // marginHorizontal: 20,
    //  resizeMode: "contain",
  },
  messagePhoto: {
    width: '60%',
    height: 160,
    borderRadius: 16,
    overflow: 'hidden',
  },
  date: {
    marginTop: 6,
    ...fontFamily('fontBaseRegular'),
    fontSize: 12,
    color: '#00000064',
    textAlign: 'right'
  },
  photoDate: {
    flexDirection: 'row',
    position: 'absolute',
    paddingHorizontal: 7,
    paddingVertical: 3,
    backgroundColor: '#00000064',
    borderRadius: 10,
    right: 5,
    bottom: 12
  }
})

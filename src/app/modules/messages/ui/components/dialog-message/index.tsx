import React, { memo } from 'react';
import {View, Text, Image} from 'react-native';

import styles from './style';
import icons from '../../../../icons';

const DialogMessage = memo(({ text, isMy, createdAt, isRead}: { isMy: boolean, text: string,
  createdAt: string, isRead: boolean }) => {
  return (
    <View style={styles.messageTrickWrap}>
      <View style={[styles.message, { backgroundColor: isMy ? '#ffbd21' : '#3e3e3e' }]}>
        <Text style={[styles.messageText, { color: isMy ? '#000' : '#fff' }]}>{text}</Text>
        <View style={{
          flexDirection: 'row-reverse',
          justifyContent: 'flex-start',
          alignItems: 'flex-end',
        }}>

          {!isRead && isMy? (

            <Image
              source={icons.check_icon}
              style={styles.unreadCounterIsZeroImage}
            />
          ) : ( isRead && isMy? (
              <Image
                source={icons.double_check_icon}
                style={styles.unreadCounterIsPositiveImage}
              />
            ) : null

          )}
        <Text style={[styles.date, { color: isMy ? '#00000064' : '#ffffff64'}]}>{createdAt}</Text>
        </View>
        {/*{*/}
        {/*  !isRead ? (*/}
        {/*    <Text>*/}
        {/*      Dab*/}
        {/*    </Text>*/}
        {/*  ) : null*/}
        {/*}*/}

      </View>
    </View>
  );
});

export default DialogMessage;

import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  unreadCounterIsZeroImage: {
    tintColor: 'rgba(255,255,255,0.61)',
  //  rotation: 2,
    transform: [{ rotate: '5deg'}],
    backgroundColor: 'transparent',
    width: 18,
    height: 18,
    marginRight: 2,
    marginLeft: 5,
   // paddingBottom: 10,
    // alignSelf: 'flex-end',
    // marginBottom: 15,
    // marginHorizontal: 20,
    //  resizeMode: "contain",
  },
  unreadCounterIsPositiveImage: {
    // tintColor: 'rgba(255,255,255,0.61)',
    // backgroundColor: 'transparent',
    // width: 18,
    // height: 18,
    // marginRight: 5,
    tintColor: 'rgba(255,255,255,0.61)',
    //  rotation: 2,
   // transform: [{ rotate: '2deg'}],
    backgroundColor: 'transparent',
    width: 18,
    height: 18,
    marginRight: 2,
    marginLeft: 6,
    // alignSelf: 'flex-end',
    // marginBottom: 15,
    // marginHorizontal: 20,
    //  resizeMode: "contain",
  },
  message: {
    paddingBottom: 7,
    paddingTop: 8,
    paddingLeft: 14,
    paddingRight: 11,
    borderRadius: 20,
  },
  messageText: {
    ...fontFamily('fontBaseRegular'),
    fontSize: 14,
    color: '#000'
  },
  date: {
  //  marginTop: 6,
    ...fontFamily('fontBaseRegular'),
    fontSize: 12,
    color: '#00000064',
    textAlign: 'right'
  },
  messageTrickWrap: {

    // overflow: 'hidden',
    width: '60%'
  },
});

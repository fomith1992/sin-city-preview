import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import FastImage from 'react-native-fast-image';

import icons from '../../../../icons';
import styles from './style';
import { truncateString } from '../../../../ud-ui/helpers/stringDuration';
import { Recipient } from "../../../domain/interfaces/Recipient";

const DialogsListItem = ({ lastMessage, onPress, recipient, unreadCounter }: { lastMessage: string, onPress: any,
  recipient: Recipient, unreadCounter: number }) => {
  //const photo = React.useRef(recipient.photo ? { uri: recipient.photo.photoThumbnailUrl } : icons.profilePlaceholder);
  const photo = recipient.photo ? { uri: recipient.photo.photoThumbnailUrl } : icons.profilePlaceholder;
  return (<>
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.fixOverlay} />
      <FastImage source={photo}
                 style={[styles.photo]}
                 resizeMode={FastImage.resizeMode.contain}
      />
      <View style={styles.infoContainer}>
        <Text style={styles.primary}>{recipient.login}</Text>
        <View style={{flexDirection: 'row'}}>
        <Text
          style={styles.secondary}
          numberOfLines={1}
        >{truncateString(lastMessage, { max: 35 })}</Text>
          <View style={styles.unreadCounterIsNegativeTextCont}>
            {unreadCounter === 0 ? (
              <Image
                source={icons.double_check_icon}
                style={styles.unreadCounterIsZeroImage}
              />
            ) : null}
            {unreadCounter === -1 ? (
             // <Text style={styles.unreadCounterIsNegativeText}>
             //   d
             // </Text>
              <Image
                source={icons.check_icon}
                style={styles.unreadCounterIsNegativeImage}
              />
            ) : null}
          </View>
        </View>
      </View>
      {unreadCounter > 0 ? (
        <View style={styles.unreadCounterIsPositive}>
          <View style={styles.unreadCounterIsPositiveTextCont} >
            <Text style={styles.unreadCounterIsPositiveText}>
              {unreadCounter}
            </Text>
          </View>

        </View>
      ) : null}

    </TouchableOpacity>
  </>);
};

export default DialogsListItem;

import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

const avatarSize = 50;

export default StyleSheet.create({
  unreadCounterIsZeroImage: {
    tintColor: '#ffa700',
    backgroundColor: 'transparent',
    width: 19,
    height: 19,
    marginLeft: 10,


  //  resizeMode: "contain",
  },
  unreadCounterIsPositiveImage: {
    tintColor: '#ffa700',
    backgroundColor: 'transparent',
    width: 19,
    height: 19,
    marginLeft: 10,
    //  resizeMode: "contain",
  },
  unreadCounterIsNegativeImage: {
    transform: [{ rotate: '5deg'}],
    tintColor: '#ffa700',
    backgroundColor: 'transparent',
    width: 19,
    height: 19,
    marginLeft: 10,
    //  resizeMode: "contain",
  },
  unreadCounterIsNegativeTextCont: {
  //  backgroundColor: 'white',
  },
  unreadCounterIsNegativeText: {

  },
  unreadCounterIsPositive: {
   // flex: 1,
   // alignSelf: 'stretch',
    flexDirection: 'row',
   // width: 10,
    height: '100%',
   // backgroundColor: 'white',
    alignItems: 'center',
    paddingRight: '5%',
    justifyContent: 'flex-end',
  },
  unreadCounterIsPositiveText: {
  //  justifySelf: 'center',
    ...fontFamily('fontTitleBold'),
    lineHeight: 16,
    fontSize: 13,
   // color: '#ffffff',
    color: '#000000',

  },
  unreadCounterIsPositiveTextCont: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 23,
    height: 23,
   // padding: 5,
    backgroundColor: '#ffa700',
    borderRadius: 50,
  },
  container: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderBottomColor: '#ffffff33',
    borderBottomWidth: 1
  },
  // Android <7 fix (not around avatar)
  fixOverlay: {
    position: 'absolute',
    zIndex: 20,
    width: avatarSize,
    height: avatarSize,
    borderRadius: avatarSize / 2,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'transparent'
  },
  photo: {
    width: avatarSize,
    height: avatarSize,
    borderRadius: avatarSize / 2,
    backgroundColor: '#000'
  },
  infoContainer: {
    flex: 1,
    marginLeft: 14,
    paddingVertical: 3,
    justifyContent: 'space-between'
  },
  primary: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 14,
    color: '#fff'
  },
  secondary: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 12,
    color: '#999',
    maxWidth: '80%',
  //  backgroundColor: 'red',
  }
})

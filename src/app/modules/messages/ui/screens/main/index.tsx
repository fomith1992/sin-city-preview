import React, { useCallback } from 'react';
import { FlatList, View, SafeAreaView, Text, Alert } from 'react-native';
import { inject, observer } from 'mobx-react';

import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import DialogsListItem from '../../components/dialogs-list-item';
import ChatStore from '../../../store/ChatStore';
import FastImage from "react-native-fast-image";
import { useFocusEffect } from "react-navigation-hooks";
import * as screenNames from "../../../../navigation/screen-names";

const NotFoundImg = require('./img/pic_messages_empty.png');

const NotFoundMessages = () => {
  return (
    <View style={{ marginTop: 20, flex: 1, justifyContent: 'center' }}>
      <Text style={styles.notFoundText}>У вас еще нет сообщений</Text>
      <FastImage source={NotFoundImg}
                 style={{ height: '70%' }}
                 resizeMode='contain'
      />
    </View>
  );
};

const MessagesMainScreen = inject('chatsStore')(observer(
  ({ chatsStore, navigation }: { chatsStore: ChatStore, navigation: any }) => {
    window.refreshChatList = () => {
      onRefreshPress();
    }
    const onRefreshPress = useCallback(() => {
      chatsStore.refreshChats();
    }, []);

    const onScroll = useCallback(({ nativeEvent }) => {
      const contentHeight = nativeEvent.contentSize.height - nativeEvent.layoutMeasurement.height;
     // console.log('onScroll', nativeEvent.contentOffset.y, contentHeight)
      if (nativeEvent.contentOffset.y + 0.5 >= contentHeight) {
       // console.log('shouldPaginate')
        chatsStore.loadChats();
      }
    }, []);
// обработка нажатия на иконку профиля
    const onProfilePressed = useCallback(() => {
        window.fLogEventObj('main_clickItem', {item_name: 'myprofile'});
        navigation.navigate(screenNames.MY_PROFILE_SCREEN); //EVENTS_TAB
    }, []);

    const onScreenFocus = useCallback(() => {
      //  console.log('Main_onScreenFocus');
      chatsStore.refreshChats();
    }, []);

    const onDialogPressed = useCallback((recipient, dialogId, unreadCounter) => {
      chatsStore.openChat(recipient, dialogId, unreadCounter);
      window.fLogEventObj('openChat', {chat_id: dialogId});
    }, []);

    const renderDialogItem = ({ item }: any) => {
      return (
        <DialogsListItem
          lastMessage={item.lastMessage}
          recipient={item.recipient}
          unreadCounter={item.unreadCounter}
          onPress={() => onDialogPressed(item.recipient, item.id, item.unreadCounter > 0)}
          {...item}
        />
      )
    };
    // обработка нажатия на иконку последних уведомлений
    const onNotificationPressed = useCallback(() => {
        window.fLogEventObj('menu_clickItem', {item_name: 'notifications'});
        navigation.navigate(screenNames.NOTIFICATIONS_SCREEN);
    }, []);

    useFocusEffect(onScreenFocus);

    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#000' }}>
        <Header leftIcon={icons.notificationIcon}
                onPress={() => onProfilePressed()}
                icon={icons.profileMenu}
                onBack={() => onNotificationPressed()}
                title="Сообщения"
                leftIconSecond={icons.refresh}
                onBackSecond={() => {
                    onRefreshPress();
                    window.fLogEventObj('chat_clickRefresh', {});
                }}
        />
        <FlatList data={chatsStore.chats}
                  contentContainerStyle={{ flexGrow: 1 }}
                  bounces={false}
                  onScroll={onScroll}
                  ListEmptyComponent={chatsStore.loadingChats ? null : <NotFoundMessages/>}
                  keyExtractor={(item) => String(item.id)}
                  renderItem={renderDialogItem}/>
      </SafeAreaView>
    );
  }
));
// window.dialogsMainOnScreenFocus = () => {
//   const MessagesMainScree = inject('chatsStore')(observer(
//     ({ chatsStore }: { chatsStore: ChatStore }) => {
//       window.refreshChatList = () => {
//         onRefreshPress();
//       };
//       const onRefreshPress = useCallback(() => {
//         chatsStore.refreshChats();
//       }, []);
//
//       const onScroll = useCallback(({ nativeEvent }) => {
//         const contentHeight = nativeEvent.contentSize.height - nativeEvent.layoutMeasurement.height;
//         if (nativeEvent.contentOffset.y === contentHeight) {
//           chatsStore.loadChats();
//         }
//       }, []);
//
//       const onScreenFocus = useCallback(() => {
//         chatsStore.refreshChats();
//       }, []);
//
//       const onDialogPressed = useCallback((recipient, dialogId, unreadCounter) => {
//         Alert.alert('onDialogPress');
//         chatsStore.openChat(recipient, dialogId, unreadCounter);
//       }, []);
//
//       const renderDialogItem = ({ item }: any) => {
//         return (
//           <DialogsListItem
//             lastMessage={item.lastMessage}
//             recipient={item.recipient}
//             unreadCounter={item.unreadCounter}
//             onPress={() => {
//               onDialogPressed(item.recipient, item.chatId, item.unreadCounter);
//               Alert.alert('PRESSED');
//             }}
//             {...item}
//           />
//         )
//       };
//
//       useFocusEffect(onScreenFocus);
//
//       return (
//         <SafeAreaView style={{ flex: 1, backgroundColor: '#000' }}>
//           <Header leftIcon={icons.refresh}
//                   onBack={onRefreshPress}
//                   title="Сообщения"/>
//           <FlatList data={chatsStore.chats}
//                     contentContainerStyle={{ flexGrow: 1 }}
//                     bounces={false}
//                     onScroll={onScroll}
//                     ListEmptyComponent={chatsStore.loadingChats ? null : <NotFoundMessages/>}
//                     keyExtractor={(item) => String(item.id)}
//                     renderItem={renderDialogItem}/>
//         </SafeAreaView>
//       );
//     }
//   ));
// }

export default MessagesMainScreen;

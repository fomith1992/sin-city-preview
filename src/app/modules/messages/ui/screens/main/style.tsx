import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start' 
  },
  notFoundText: {
    marginTop: 20,
    textAlign: 'center',
    ...fontFamily('fontBaseMedium'),
    fontSize: 14,
    color: '#fff',
    marginBottom: 40,
  }
})
import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  unreadCounterIsZeroImage: {
    tintColor: '#ffa700',
    backgroundColor: 'transparent',
    width: 19,
    height: 19,
    marginLeft: 10,
    // alignSelf: 'flex-end',
    // marginBottom: 15,
    // marginHorizontal: 20,
    //  resizeMode: "contain",
  },
  unreadCounterIsPositiveImage: {
    tintColor: '#ffa700',
    backgroundColor: 'transparent',
    width: 19,
    height: 19,
    marginLeft: 10,
    //  resizeMode: "contain",
  },
  safeArea: {
    flex: 1,
    backgroundColor: '#000'
  },
  container: {
    flex: 1,
    backgroundColor: '#252525',
    paddingHorizontal: 20,
  },
  messagesLoadingIndicator: {
    backgroundColor: '#252525',
    height: 30,
  },
  messageContainer: {
    minHeight: 50,
    marginVertical: 5
  },
  message: {
    padding: 12
  },
  messagePhoto: {
    width: '60%',
    height: 160,
    borderRadius: 16,
    overflow: 'hidden',
  },
  bottomContainer: {
    flexDirection: 'row',
    paddingTop: 6,
    paddingBottom: 10,
    paddingHorizontal: 5,
    alignItems: 'center',
    maxHeight: '35%',
  },
  bottomTextInput: {
    flex: 1,
    backgroundColor: '#fff',
    borderRadius: 17,
    paddingVertical: 11,
    paddingHorizontal: 12,
   // maxHeight: 150,
    maxHeight: '100%',
    alignItems: 'flex-start',
    ...fontFamily('fontBaseMedium'),
    color: '#000'
  },
  sectionHeader: {
    textAlign: 'center',
    marginVertical: 10,
    ...fontFamily('fontBaseMedium'),
    color: '#fff'
  }
})

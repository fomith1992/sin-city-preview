import React, {useRef, useState, memo, useCallback, useEffect} from 'react';
import {
  SectionList,
  ActivityIndicator,
  KeyboardAvoidingView,
  SafeAreaView,
  View,
  Image,
  TextInput,
  Text,
  TouchableOpacity,
  Platform,
  Alert,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import ActionSheet from 'react-native-action-sheet';
import {useIsFocused} from 'react-navigation-hooks';

import styles from './style';
import icons from '../../../../icons';
import ChatsStore from '../../../store/ChatStore';
import Header from '../../../../layout/ui/components/header';
import {actions, favouriteActions} from '../../../domain/enums/Actions';
import DialogPhoto from '../../components/dialog-photo';
import DialogMessage from '../../components/dialog-message';

interface messageProps {
  text: string,
  photoUrl: string,
  isMy: boolean,
  createdAt: string
  photoThumbnailUrl: string,
  is_read: boolean,
}

const MessageComponent = memo(({text, photoUrl, photoThumbnailUrl, isMy, createdAt, is_read}: messageProps) => {
  //console.log('item', text, is_read);
  return (
    <View style={[styles.messageContainer, {flexDirection: isMy ? 'row-reverse' : 'row'}]}>

      {Boolean(text) && (
        <DialogMessage
          isRead={is_read || !isMy}
          // isRead={isRead}
          text={text}
          isMy={isMy}
          createdAt={createdAt}/>
      )}
      {Boolean(photoUrl) && (
        <DialogPhoto
          isMy={isMy}
          isRead={is_read || !isMy}
          photoUrl={photoUrl}
          photoThumbnailUrl={photoThumbnailUrl}
          createdAt={createdAt}/>
      )}


    </View>
  );
});

{/*<View style={{*/}
{/*  backgroundColor: 'rgb(152,152,152)',*/}
{/*  // backgroundColor: 'rgba(255,189,33,0.8)',*/}
{/*  width: 9,*/}
{/*  height: 9,*/}
{/*  alignSelf: 'flex-end',*/}
{/*  marginBottom: 15,*/}
{/*  marginHorizontal: 20,*/}
{/*  //borderRadius: '50',*/}
{/*  borderRadius: 50,*/}
{/*}}/>*/}

const DialogScreen = inject('chatsStore')(observer(
  ({chatsStore, navigation}: {chatsStore: ChatsStore, navigation: any}) => {
    const isFocused = useIsFocused();
    const [currentMessage, setCurrentMessage] = useState('');
    const handleScroll = (event: any) => {
      const contentOffsetY = event.nativeEvent.contentOffset.y;
      const contentHeight = event.nativeEvent.contentSize.height - event.nativeEvent.layoutMeasurement.height;
      if (contentOffsetY === contentHeight) {
        chatsStore.loadMessages();
 //       console.log('scrollHandler');
      }
    };
    useEffect(() => {
      chatsStore.setDialogScreenState(isFocused);
      const messageAutofill = navigation.getParam('messageAutofill');
      if (!!messageAutofill) {
        setCurrentMessage(messageAutofill)
      }

      //  Alert.alert("useEffect");
      // window.sendMessage(
      //   JSON.stringify(
      //     {
      //       // "type": "chat_message",
      //       'type': 'chat_message',
      //       'action': 'messages_read',
      //       'message': {
      //         'chat': chatsStore.chatId,
      //         // "createdAt": "2020-02-27T15:15:47.719731Z"
      //       },
      //     },
      //   ),
      // );
    }, [isFocused]);

    const onSendPress = () => {
      // console.log('onSendPress');
      if (!chatsStore.isPayed && !chatsStore.withAdmin) {
          chatsStore.openPaymentError();
          return;
      }
      if (currentMessage.match(/\S/g)) {
        window.fLogEventObj('chat_sendMessage', {chat_id: chatsStore.chatId});
        chatsStore.sendMessage({text: currentMessage});
        setCurrentMessage('');
      }
    };

    window.onGetPaymentError = () => {
        chatsStore.openPaymentError();
    }

    window.onGetPress = (action, message) => {
       // console.log('window.onGetPress', action, message)
      // Alert.alert('onGetPress');
      //Alert.alert(message);
      //  console.log(message);
      // console.log("onGetPress");
    //    console.log("onGetPress", action, message);
      if (action === 'message_sent') {
         // console.log('ifAction message_sent')
        if (message['chat'] === chatsStore.chatId && message['sender'] !== chatsStore.userId && chatsStore.isDialogScreenFocused && isFocused) {
          //  Alert.alert('чат прочитан');
          window.sendMessage(
            JSON.stringify(
              {
                // "type": "chat_message",
                'type': 'chat_message',
                'action': 'messages_read',
                'message': {
                  'chat': chatsStore.chatId,
                  // "createdAt": "2020-02-27T15:15:47.719731Z"
                },
              },
            ),
          );
        }
        chatsStore.getMessage({
          text: message['text'],
          sender: message['sender'],
          photo: message['photo'],
          photoThumbnailUrl: message['photoThumbnailUrl'],
          photoUrl: message['photoUrl'],
          chat: message['chat'],
          isRead: message['is_read'],
        });
      } else if (action === 'photo_sent' && message['chat'] === chatsStore.chatId) {
        // chatsStore.clearMessages();
        chatsStore.loadMessages(1);
        if (message['chat'] === chatsStore.chatId && message['sender'] !== chatsStore.userId && chatsStore.isDialogScreenFocused) {
          //  Alert.alert('чат прочитан');
          window.sendMessage(
            JSON.stringify(
              {
                // "type": "chat_message",
                'type': 'chat_message',
                'action': 'messages_read',
                'message': {
                  'chat': chatsStore.chatId,
                  // "createdAt": "2020-02-27T15:15:47.719731Z"
                },
              },
            ),
          );
        }
      } else if (action === 'messages_read' && message['chat'] === chatsStore.chatId) {
  //      console.log('onMessageRead chat = ', chatsStore.chatId);
        //   Alert.alert('shuld reload');
        chatsStore.loadMessages(1);
      }


    };

    const onInputFieldFocus = () => {
        if (!chatsStore.isPayed && !chatsStore.withAdmin) {
            chatsStore.openPaymentError();
        }
    };

    const onActionsShowPress = useCallback(() => {
      const options = [chatsStore.recipientInfo.heFavorite ? favouriteActions.off.label : favouriteActions.on.label, ...actions.map(action => action.label)];
      Platform.OS === 'ios' && options.push('Отмена');
      ActionSheet.showActionSheetWithOptions({
        title: Platform.OS === 'ios' ? 'Дополнительно' : '',
        cancelButtonIndex: 4,
        options,
      }, (buttonIndex) => {
        chatsStore.doAction(buttonIndex);
      });
    }, [chatsStore.recipientInfo.heFavorite]);

    const choicePhoto = useCallback(() => {
        if (!chatsStore.isPayed && !chatsStore.withAdmin) {
            chatsStore.openPaymentError()
        } else {
            chatsStore.choicePhoto();
        }
    }, []);

    const messageKeyExtractor = useRef((item: any) => String(item.id));

    return (
      <>
        <KeyboardAvoidingView style={{flex: 1}} enabled={Platform.OS === 'ios'} behavior='padding'>
          <SafeAreaView style={styles.safeArea}>
            <Header title={chatsStore.recipientInfo.login}
                   // subtitle={chatsStore.recipientInfo.online ? 'online' : chatsStore.recipientInfo.lastActivity}
                    subtitleColor={chatsStore.recipientInfo.online ? '#ffbd21' : '#999'}
                    height={50}
                    onTitlePress={() => {
                      chatsStore.openRecipient();
                      window.fLogEventObj('chat_clickProfile',
                        {user_id: chatsStore.recipient.id});
                    }}
                    titleContainerStyle={{alignItems: 'center'}}
                    icon={icons.dots}
                    onPress={onActionsShowPress}
                    onBack={() => {
                      navigation.pop();
                    }}/>
            {chatsStore.loadingMessages && <ActivityIndicator style={styles.messagesLoadingIndicator}/>}
            <SectionList sections={chatsStore.messagesList}
                         bounces={false}
                         onScroll={handleScroll}
                         style={styles.container}
                         contentContainerStyle={{paddingVertical: 10}}
                         keyExtractor={messageKeyExtractor.current}
                         inverted
                         renderSectionFooter={({section: {title}}) =>
                           <Text style={styles.sectionHeader}>{title}</Text>}
                         renderItem={({item}) => <MessageComponent {...item}/>}/>
            <View style={styles.bottomContainer}>
              <TouchableOpacity activeOpacity={.8}
                                style={{padding: 15}}
                                onPress={choicePhoto}>
                <Image source={icons.navPlus}/>
              </TouchableOpacity>
              <TextInput value={currentMessage}
                // returnKeyType="send"
                         multiline={true}
                         numberOfLines={1}
                         onFocus={() => {
                             onInputFieldFocus();
                         }}
                // onSubmitEditing={onSendPress}
                         onChangeText={setCurrentMessage}
                         style={styles.bottomTextInput}
                         placeholder="Сообщение…"/>
              <TouchableOpacity activeOpacity={.8}
                                style={{padding: 15}}
                                onPress={onSendPress}
                //onPress={onGetPress}
                // onPress={() => {chatsStore.loadMessages(1)}}

              >
                <Image source={icons.send}/>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </KeyboardAvoidingView>
      </>
    );
  },
));

export default DialogScreen;

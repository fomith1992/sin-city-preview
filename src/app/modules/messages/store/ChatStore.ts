import { formatLastActivity } from './../../ud-ui/helpers/dateFormat';
import firebase from 'react-native-firebase';
import { showMessage } from 'react-native-flash-message';
import { Recipient } from './../domain/interfaces/Recipient';
import { action, observable, computed } from 'mobx';
import { utc } from 'moment';

const uuidv1 = require('uuid/v1');
import * as _ from 'lodash';

import * as screenNames from '../../navigation/screen-names';
import {Alert} from 'react-native';
import chatsRepository from '../domain/repositories/ChatsRepository';
import Uploader from '../../upload/Uploader';
import { Chat } from '../domain/interfaces/Chat';
import { Message } from '../domain/interfaces/Message';
import { ChatsResponse } from '../domain/interfaces/Responses';
import userRepository from "../../user/domain/repositories/UserRepository";
import { BaseStore } from "../../../store/BaseStore";
import { AuthUserStore } from "../../auth/store/auth-user";
import { TYPES, ITYPES } from "../../../store/store-types";
import UserSearchStore from '../../search/store/UsersSearchStore';
import {showModalAndTakePhoto} from "../../core/domain/services/MediaTakingService";
import {wsConnect} from '../../../utils/webSocketConnect';

type ExtendedMessage = Message & { isMy: boolean };
export default class ChatsStore extends BaseStore {
  public chatId: number = -1;
  public userId: number | null = -1;
  private recipientId: number = -1;
  public isDialogScreenFocused: boolean = false;
  public withAdmin: boolean = false;
  public isPayed: boolean = false;
  @observable recipient: Recipient = { login: '', lastActivity: '', id: -1, online: '', heFavorite: false };
  @observable chats: Chat[] = [];
  @observable messageRecords: Record<number, Message> = {};
  @observable loadingMessages: boolean = false;
  @observable loadingChats: boolean = false;
  private messagesPage: number = 1;
  private chatsPage: number = 1;

  private onMessage: any;

  private navigation: ITYPES['Navigation'];
  private userSearchStore: UserSearchStore;
  private authUser: AuthUserStore;
  private layout: ITYPES['Layout'];
  private profileManager: ITYPES['ProfileManager'];
  private messageAutofill: any;

  storeInit() {
    this.authUser = this.root.resolve(TYPES.AuthUser);
    this.userSearchStore = this.root.usersSearchStore;
    this.navigation = this.root.navigationStore;
    this.layout = this.root.resolve(TYPES.Layout);
    this.profileManager = this.root.resolve(TYPES.ProfileManager);
    if (!this.onMessage) {
      this.onMessage = firebase.messaging().onMessage(this.messageListener.bind(this));
    }
  }

  @computed get messages(): Message[] {
    const messageList = Object.values(this.messageRecords);
    const sortedMessageList = messageList.sort((a, b) => Date.parse(b.createdAt) - Date.parse(a.createdAt));
   // console.log(sortedMessageList);
    return sortedMessageList;
  }

  public setDialogScreenState(value: boolean) {
    this.isDialogScreenFocused = value;
  }

  set messages(messages: Message[]) {
    messages.forEach(message => {
      this.messageRecords[message.id] = message;
     // console.log('')
    });
  }

  @action
  appendMessage(message: Message) {
    this.messageRecords[message.id] = message;
    this.messageRecords = {...this.messageRecords};
  }

  @action
  clearMessages() {
    this.messageRecords = {};
  }

  openRecipient() {
    this.profileManager.openProfile(this.recipientId, { isGoFromChat: true, userLogin: this.recipient.login });
  }

  // todo show name and date after navigating from search screen
  @computed get recipientInfo(): Recipient {
    const chatsListItem = this.chats.find(chat => chat.recipient.id === this.recipientId);
   // console.log('thi.Chats', this.chats, chatsListItem)
      if (!!chatsListItem) {
          this.withAdmin = chatsListItem.withAdmin;
      }
    const recipient = chatsListItem ? chatsListItem.recipient : this.recipient;
    const lastActivity = formatLastActivity(recipient.lastActivity);
    return Object.assign({}, recipient, { lastActivity });
  }

  public choicePhoto(){
    this.root.layoutStore.showOverflowLoader(false);
    showModalAndTakePhoto(true)
      .then((photo) => {
        this.sendMessage({ photo })
        window.fLogEventObj('chat_sendMessage', {chat_id: this.chatId});
      })
      .catch((err) => {})
      .finally(()=> this.root.layoutStore.hideOverflowLoader())
  }

  @action
  private handleChatRemovePromise(promise: Promise<any>) {
    const chatIndex = this.chats.findIndex((chat: Chat) => chat.id === this.chatId);
    return promise
      .then(() => {
        this.chats = this.chats.filter((chat, index) => index !== chatIndex);
        this.root.navigationStore.popToTop();
      }).catch(() => {
    })
      .finally(() => {
        this.root.layoutStore.hideOverflowLoader()
      })
  }

  @action
  async doAction(action: number) {
    this.root.layoutStore.showOverflowLoader();
    const chatIndex = this.chats.findIndex((chat: Chat) => chat.id === this.chatId);
    switch (Number(action)) {
      case 0: {
        const action = this.recipientInfo.heFavorite ? 'delete-favorite' : 'add-favorite';
        window.fLogEvent('chat_clickFavotiteBtn', {user_id: this.recipient.id});
        userRepository.setFavourite(this.recipientId, action)
          .then(() => {
            if (this.recipientInfo.heFavorite) {

              showMessage({ message: 'Вы удалили пользователя из списка избранных', type: 'info' })
            } else {

              showMessage({ message: 'Вы добавили пользователя в список избранных', type: 'success' })
            }
            this.root.users[this.recipientId] = {
              ...this.root.users[this.recipientId],
              heFavorite: !this.recipientInfo.heFavorite
            };
            this.chats = this.chats.map((chat, index) => {
              if (index === chatIndex) {
                chat.recipient.heFavorite = !chat.recipient.heFavorite
              }
              return chat;
            });
          }).catch(() => {
        })
          .finally(() => {
            this.root.layoutStore.hideOverflowLoader()
          })
        break;
      }
      case 1: {
        this.handleChatRemovePromise(chatsRepository.removeChatFormMe(this.chatId))
        window.fLogEvent('chat_clickDeleteMyChat', {chat_id: this.chatId});
        break;
      }
      case 2: {
        this.handleChatRemovePromise(chatsRepository.delete(this.chats[chatIndex]));
        window.fLogEvent('chat_clickDeleteAllChat', {chat_id: this.chatId});
        break;
      }
      case 3: {
        await this.handleChatRemovePromise(userRepository.blockUser(this.recipientId));
        window.fLogEvent('chat_clickBlockUser', {user_id: this.recipient.id});
        delete this.root.users[this.recipientId];
        break;
      }
      default: {
        this.root.layoutStore.hideOverflowLoader();
      }
    }
  }

  @action
  refreshChats() {
    const hasAlreadyDialogs = this.chats && this.chats.length > 0;
    const showOverflowLoader = !hasAlreadyDialogs;
    this.chatsPage = 1;
    if (showOverflowLoader) {
      this.chats = [];
    }

    if (this.chatsPage) {
      this.loadAndReturnChats(showOverflowLoader)
        .then(({ results, next, unreadMessagesCounter }: ChatsResponse) => {
          this.chats = results;
         // console.log('this.chats', this.chats, unreadMessagesCounter)
         // const unreadCounterSum = unreadMessagesCounter;
          // results.forEach((object) => {
          //   if (object.unreadCounter > 0) {
          //     unreadCounterSum += object.unreadCounter;
          //   }
          // });

          // let unreadCounterSum = () => {
          //   lc
          //   results.map((e) => {
          //   //  return 4;
          //     stas = stas + 5;
          //   });
          //   return stas;
         // console.log('RESULTS', results);
         // console.log('unreadCounterSum', unreadCounterSum);
          window.setTNUnreadAmount(unreadMessagesCounter);
          this.chatsPage = Boolean(next) ? this.chatsPage + 1 : 0;
        });
    }
  }

  @action
  loadChats(showOverflowLoader: boolean = true) {
    if (this.chatsPage) {
      this.loadAndReturnChats(showOverflowLoader)
        .then(({ results, next }: ChatsResponse) => {
          this.chats = [...this.chats, ...results];
          this.chatsPage = Boolean(next) ? this.chatsPage + 1 : 0;
        });
    }
  }

  @computed get messagesList(): { title: string, data: (Message & { isMy: boolean })[] }[] {
    const messages: Record<string, { title: string, data: ExtendedMessage[] }> = {} ;
   // console.log("messages", messages);
    this.messages.forEach((message) => {
      const messageDate = utc(message.createdAt).format('ll');
    //  console.log('LAO_MESSAGE', message);
      message = Object.assign({}, message, {
        isMy: message.sender === this.userId,
        createdAt: utc(message.createdAt).local().format('HH:mm'),
        is_read: message.isRead,
      });

      if (messages[messageDate]) {
        messages[messageDate].data.push(message as any);
      } else {
        Object.assign(messages, {
          [messageDate]: {
            data: [message]
          }
        });
      }
    });
    return Object.keys(messages).map((title) => {
    //  console.log('DATAMESSAGE', title, messages[title].data, '               ');
      return {
        title,
        data: messages[title].data
      }
    });
  }

  public handleMessageResponse = (promise: Promise<Message>) => {
    promise
      .then((message: Message) => {
      })
      .catch(() => {
      })
  };

  @action
  sendMessage = ({ photo, text }: { text?: string, photo?: any }) => {
   // console.log('sendMessage');
    const id = uuidv1();
    const message = {
      id,
      chat: this.chatId,
      sender: this.userId,
      createdAt: new Date().toISOString(),
      text: '',
      photoUrl: '',
      photoThumbnailUrl: ''
    };
    if (text) {
     // this.appendMessage({ ...message, text });
      window.sendMessage(
        JSON.stringify(
          {
           // "type": "chat_message",
            "type": "chat_message",
            "action": 'message_sent',
            "message": {
              "chat": this.chatId,
              "sender": this.userId,
              "text": text,
              "photo": null,
              "photoUrl": null,
              "photoThumbnailUrl": null,
             // "createdAt": "2020-02-27T15:15:47.719731Z"
            }
          }
        )
      );
      // chatsRepository.sendTextMessage(this.chatId, text)
      //   .then(() => {
      //   })
      //   .catch(() => {
      //     this.messages = this.messages.filter((message) => message.id !== id);
      //   })
    }
    // else if (photo) {
    //   window.sendMessage(
    //     JSON.stringify(
    //       {
    //         // "type": "chat_message",
    //         "type": "chat_message",
    //         "message": {
    //           "chat": this.chatId,
    //           "sender": this.userId,
    //           "text": '',
    //           "photo": photo,
    //           "photoUrl": photo.uri,
    //           "photoThumbnailUrl": photo.uri,
    //           // "createdAt": "2020-02-27T15:15:47.719731Z"
    //         }
    //       }
    //     )
    //   );
    // }

    else if (photo) {
      this.appendMessage({
        ...message,
        photoUrl: photo.uri,
        photoThumbnailUrl: photo.uri,
      });
      chatsRepository.sendPhotoMessage(this.chatId, Uploader.prepareFormData(photo))
        .then(() => {
        })
        .catch(() => {
          this.messages = this.messages.filter((message) => message.id !== id);
        })
    }

  };

  // window.toggleMessageSend = () => {
  //
  // }
  // window.passToken = (token) => {
  //   wsConnect(token);
  // };
  @action
  getMessage = ({ photo, text, sender, photoThumbnailUrl, photoUrl, chat,  isRead}: { text?: string,
    photo?: any, sender?: string, isRead?: boolean,
    photoThumbnailUrl?: any, photoUrl?: any, chat?: string, }) => {

   // Alert.alert(jac);
   // console.log('getMessage', chat, this.chatId);
    const id = uuidv1();
    const message = {
      id,
      chat: chat,
     // sender: this.userId,
      sender: sender,
      createdAt: new Date().toISOString(),
      is_read: false,
      text: '',
      photoUrl: '',
      photoThumbnailUrl: ''
    };
    if (text && chat.toString() === this.chatId.toString()) {
      this.appendMessage({ ...message, text });
    } else if (photo) {
      this.appendMessage({
        ...message,
        photoUrl: photo.uri,
        photoThumbnailUrl: photo.uri,
      });
      console.log('URLI', photo.uri);

    }
  };

  @action
  loadMessages(page = null) {
    if (!!page) {
      if ( !this.loadingMessages) {
  //      console.log('loadMessages PAGE = ', page);
       // this.loadingMessages = true;
        return chatsRepository.loadById(this.chatId, { page: page })
          .then(({ chat, results, next }: any) => {
            this.clearMessages();
            this.recipient = chat.recipient;
            this.recipientId = chat.recipient.id;
            this.messages = [...results];
           // this.messagesPage = next ? this.messagesPage + 1 : 0;
          })
          .finally(() => {
           // this.loadingMessages = false;
          })
      } else { return new Promise((res, rej) => {
        if (this.messagesPage) {
      //    console.log('!!this.messagesPage');
        }
        if (!this.loadingMessages) {
      //    console.log('!this.loadingMessages');
        }
      //  console.log('loadingMessages REJECT');
        rej();
      })}
    } else {
      if (this.messagesPage && !this.loadingMessages) {
        this.loadingMessages = true;
        return chatsRepository.loadById(this.chatId, { page: this.messagesPage })
          .then(({ chat, results, next }: any) => {
           // console.log('chatsRepository.loadById', chat, results, next);
            this.recipient = chat.recipient;
            this.recipientId = chat.recipient.id;
            this.messages = [...results];
          //  console.log("currentPage", this.messagesPage);
            this.messagesPage = next ? this.messagesPage + 1 : 0;
          //  console.log("NEXT ", next, 'currentPage ', this.messagesPage);
          })
          .finally(() => {
            this.loadingMessages = false;
          })
      } else { return new Promise((res, rej) => rej())}
    }

  }

  private loadAndReturnChats(showOverflowLoader: boolean = true) {
    this.loadingChats = true;
    showOverflowLoader && this.root.layoutStore.showOverflowLoader();
    return chatsRepository.load({ page: this.chatsPage })
      .catch(() => {
      })
      .finally(() => {
        showOverflowLoader && this.root.layoutStore.hideOverflowLoader();
        this.loadingChats = false;
      })
  }

  @action
  private messageListener = (message: any) => {
    const { data } = message;
    if (data.typeEvent === 'BLACKLIST_ADD') {
      if (data.userId == this.recipientId && this.isDialogScreenFocused) {
        this.navigation.popToTop();
        window.fLogEvent('app_errorScreen',
          'Пользователь заблокировал вас.');
        showMessage({ message: 'Пользователь заблокировал вас.', type: 'danger' });
      }
    } else if (data.typeEvent === 'MESSAGE' && data.chat == this.chatId) {
      // this.appendMessage({
      //   ...data,
      //   createdAt: data.created_at,
      //   photoUrl: data.photo_url,
      //   photoThumbnailUrl: data.photo_thumbnail_url
      // })
    }
  };

  @action
  public createChat(recipient: number, messageAutofill: string = null) {
    this.messageAutofill = messageAutofill;
    this.root.layoutStore.showOverflowLoader();
    chatsRepository.create({ recipient })
      .then(({ id, recipient, withAdmin }: { id: number, recipient: Recipient, withAdmin: any }) => {
        console.log('chatsRepository.create withAdmin', withAdmin);
        if (withAdmin !== null ) {this.withAdmin = withAdmin} else {console.log('withAdmin NULL')};
        this.openChat(recipient, id, messageAutofill);
      })
      .catch((error: any) => {
        console.log('chatsRepository.create ERROR_CODE', error);
        this.messageAutofill = null;
        if (error.code === '1337') {
          this.navigation.navigate(screenNames.GET_PREMIUM_SCREEN);
          // this.navigation.navigate(screenNames.USERS_SCREENS_PAYMENT_REQUIRED_SCREEN, {chatMessage: true});
        }

      })
      .finally(() => {
        this.root.layoutStore.hideOverflowLoader();
      })
  }

  @action
  public openPaymentError() {
    this.navigation.navigate(screenNames.GET_PREMIUM_SCREEN);
    // this.navigation.navigate(screenNames.USERS_SCREENS_PAYMENT_REQUIRED_SCREEN, {chatMessage: true});
  }

  @action
  public openChat(recipient: Recipient, chatId: number, unreadCounter: boolean) {
    const recipentValue = recipient;
    this.layout.showOverflowLoader();
    this.messagesPage = 1;
    this.clearMessages();
    this.userId = this.authUser.userId;
    const isPayed = this.authUser.currentUserStore.user.isPayed;
    this.isPayed = isPayed;
    this.recipientId = recipient.id;
    this.recipient = recipient;
    this.chatId = chatId;
    //console.log('openChat', recipentValue, this.recipient, this.recipientId, this.recipientInfo)
    //console.log('UNREADCounter', unreadCounter, chatId);
    this.loadMessages()
      .then(() => {
       // console.log('recipentValue', recipentValue, typeof recipentValue, this.messageAutofill)
        if (typeof recipentValue === "string") {
          setTimeout(() => {
           // console.log('refreshChts')
            this.refreshChats();
          }, 1000);
         // console.log('isString')
         // this.root.navigationStore.pop();
       //   this.root.navigationStore.popToTop();

        }
        this.root.navigationStore.navigate(screenNames.MESSAGES_TAB);
        // if (!!this.messageAutofill) {
        //   this.root.navigationStore.navigate(screenNames.MESSAGES_DIALOG_SCREEN, {messageAutofill: this.messageAutofill});
        // } else {
        //   setTimeout(() => {
        //     this.root.navigationStore.navigate(screenNames.MESSAGES_DIALOG_SCREEN, {messageAutofill: this.messageAutofill});
        //   }, 10)
        // }
        this.root.navigationStore.navigate(screenNames.MESSAGES_DIALOG_SCREEN, {messageAutofill: this.messageAutofill});
        if (unreadCounter) {
          window.sendMessage(
            JSON.stringify(
              {
                // "type": "chat_message",
                'type': 'chat_message',
                'action': 'messages_read',
                'message': {
                  'chat': Number(chatId),
                  // "createdAt": "2020-02-27T15:15:47.719731Z"
                },
              },
            ),
          );
        }

      })
      .catch((e) => {
          console.log('catch', e)
      })
      .finally(() => {
          this.layout.hideOverflowLoader();
          this.messageAutofill = null;
      })
  }
}

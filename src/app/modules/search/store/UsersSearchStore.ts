import { observable } from 'mobx';

import SearchStore from './SearchStore'
import { UsersListItem } from "../domain/interface/UsersListItem";

export default class UsersSearchStore extends SearchStore {
  @observable.shallow result: UsersListItem[] = [];
  @observable currentUser: UsersListItem | null = null;
}
import { ITYPES } from './../../../store/store-types';
import { action, observable, computed, toJS } from 'mobx';

import SearchStore from './SearchStore';
import coordsRepository from '../domain/repositories/CoordinatesRepository';
import { Coordinates } from '../domain/interface/Coordinates';
import userRepository from "../../user/domain/repositories/UserRepository";
import { TYPES } from "../../../store/store-types";
import GeolocationStore from "../../core/store/geolocation";
import { Filter } from './../domain/interface/Filter';
import { FilterFactory } from '../domain/factories/FilterFactory';
import * as screenNames from '../../navigation/screen-names';

export default class CoordinatesSearchStore extends SearchStore {
  @observable isIShownOnMap: boolean = true;
  @observable intention: 'PARTY' | 'SEX' | null = null;
  public shouldNotNavigateToCurrentPosition = false;
  private cancelDataLoading: any;

  @observable filters: Filter = FilterFactory.create();
  @observable temporaryFilter: Filter = FilterFactory.create();
  protected nextPage: number = 1;
  @observable nextPageLoading: boolean = false;
  @observable isRefreshing: boolean = false;

  private geolocationStore: GeolocationStore;
  private currentUserStore: ITYPES['CurrentUserStore'];

  storeInit() {
    super.storeInit();
    this.geolocationStore = this.root.resolve(TYPES.GeolocationStore);
    this.currentUserStore = this.root.resolve(TYPES.CurrentUserStore);
  }

  private get userId() {
    return this.authStore.userId;
  }

  @computed get usersCoordinates(): Coordinates[] {
    return this.resultList;
  }

  @action loadShownOnMap() {
    this.handleShownPromisse(userRepository.getMapVisibility(this.userId));
  }

  @action search(shouldCleanResult: boolean) {
    this.nextPage = 1;
    this.resultList = shouldCleanResult ? [] : this.resultList;
    this.loadNextPage();
  }

  @action
  private putResults(value: Coordinates[]) {
    const results = [...this.resultList, ...value];
    results.forEach((coord) => {
      results.forEach((anotherCoord) => {
        if (coord.latitude === anotherCoord.latitude && coord.longitude === anotherCoord.longitude && coord.id !== anotherCoord.id) {
          results[coord.id].latitude = results[coord.id].latitude + 0.0001;
          results[coord.id].longitude = results[coord.id].longitude + 0.0001;
        }
      })
    });
    this.resultList = [...results];

  }


  @action loadNextPage() {
    if (this.isIShownOnMap && this.nextPage && !this.nextPageLoading) {
      this.nextPageLoading = true;
      const { promise, cancel } = coordsRepository.load(this.getPreparedFilters());
      this.cancelDataLoading = cancel;
      promise
        .then((response: any) => {
          if (response) {
            this.nextPage = response.next ? this.nextPage + 1 : 0;
            this.putResults(response.results);
          }
        })
        .catch((err: any) => { console.log(err) })
        .finally(() => {
          this.nextPageLoading = false;
          this.isRefreshing = false;
        })
    }
  }

  protected cancelNextPageLoading() {
    this.nextPage = 1;
    this.cancelDataLoading && this.cancelDataLoading();
    this.nextPageLoading = false;
    this.isRefreshing = false;
  }

  @action openProfile(id: number) {
    // subscription (defeatured)
    if (this.currentUserStore.user?.isPayed || true) {
      this.profileManager.openProfile(id, { goBack: this.customGoBack });
    } else {
      this.navigationStore.navigate(screenNames.USERS_SCREENS_PAYMENT_REQUIRED_SCREEN, {
        goBack: this.customGoBack
      });
    }

    //this.profileManager.openProfile(id, { goBack: this.customGoBack });
  }

  public get customGoBack() {
    return (goBack: Function) => {
      this.shouldNotNavigateToCurrentPosition = true;
      goBack();
    };
  }

  @action setShownOnMap(value: boolean) {
    this.isIShownOnMap = !this.isIShownOnMap;
    if (this.isIShownOnMap) {
      window.fLogEventObj('map_enableDisplay',
        {map_mode: this.intention === 'MEETING' ? 'dating' : 'sex'});
      this.geolocationStore.startPositionWatching();
    }
    else {
      window.fLogEventObj('map_disableDisplay',
        {map_mode: this.intention === 'MEETING' ? 'dating' : 'sex'});
      this.geolocationStore.stopPositionWatching();
    }
    userRepository.saveMapVisibility(this.userId, value)
      .then((response: any) => {
        this.isIShownOnMap = response.showMe;
        if (value) { this.search(true); }
      })
      .catch(() => {
        this.isIShownOnMap = !this.isIShownOnMap;
      })
  }

  @action
  setLocalIntention(intention: 'PARTY' | 'SEX') {
    this.intention = intention;
    this.filters.intention = intention;
  }

  @action loadIntention() {
    userRepository.getIntention(this.userId)
      .then((response: any) => {
        this.setLocalIntention(response.intention);
      })
      .catch(() => {
      })
  }

  @action setIntention(intention: string) {
    window.fLogEventObj('map_changeMode',
      {map_mode: intention === 'MEETING' ? 'dating' : 'sex'});
    userRepository.saveIntention(this.userId, intention)
      .then(async (response: any) => {
        this.cancelNextPageLoading();
        this.setLocalIntention(response.intention);
        this.search(true);
      })
      .catch((e) => {
        console.log(e)
      })
  }

  protected getPreparedFilters() {
    const filter = Object.assign({}, this.filters, { page: this.nextPage });
    const genders = filter.genders.slice() as any;
    if (genders.length === 1 ) { filter.is_couple = false; }
    if (genders.length === 2 ) { filter.is_couple = true; }
    genders.forEach((gender: any, index: number) => {
      let postfix = '';
      if (Boolean(index)) { postfix = String(index); }
      Object.keys(gender).forEach((key) => {
        Object.assign(filter, { [key + postfix]: gender[key] })
      })
    })
    !filter.online && delete filter.online;
    delete filter.genders;
    return filter;
  }

  private handleShownPromisse(promisse: Promise<{ showMe: boolean }>) {
    promisse
      .then((response: any) => {
        this.isIShownOnMap = response.showMe;
      })
      .catch(() => {
        this.isIShownOnMap = !this.isIShownOnMap;
      })
  }
}

import { TYPES, ITYPES } from './../../../store/store-types';
import { observable, action, computed, observe } from "mobx";
import * as _ from 'lodash';

import { Profile } from './../../user/domain/interfaces/Profile';
import { User } from './../../user/domain/interfaces/User';
import { Filter } from './../domain/interface/Filter';
import { FilterFactory } from '../domain/factories/FilterFactory';
import { BaseStore } from "../../../store/BaseStore";
import userRepository from "../../user/domain/repositories/UserRepository";

const everyNItemsNotAvailable = 19;

export default class SearchStore extends BaseStore {
  @observable filters: Filter = FilterFactory.create();
  @observable temporaryFilter: Filter = FilterFactory.create();
  protected nextPage: number = 1;
  @observable notAvailable: number | null = null;
  @observable isLoading: boolean = false;
  @observable nextPageLoading: boolean = false;
  @observable searchMessage: string = "";
  @observable private shownResultIds: number[] = [];
  protected repository: any = userRepository;
  @observable isRefreshing: boolean = false;

  protected authStore: ITYPES['AuthUser'];
  protected navigationStore: ITYPES['Navigation'];
  protected layout: ITYPES['Layout'];
  private chatsStore: ITYPES['ChatsStore'];
  protected users: Record<number, User>
  protected profileManager: ITYPES['ProfileManager'];

  storeInit() {
    this.navigationStore = this.root.resolve(TYPES.Navigation);
    this.layout = this.root.resolve(TYPES.Layout);
    this.chatsStore = this.root.resolve(TYPES.ChatsStore);
    this.authStore = this.root.resolve(TYPES.AuthUser);
    this.profileManager = this.root.resolve(TYPES.ProfileManager);
    this.users = this.root.users;
  }

  @computed
  public get resultList(): any[] {
    return this.shownResultIds.map((id) => this.root.users[id]).filter(Boolean);
  }

  public set resultList(result: any[]) {
    this.shownResultIds = [];
    result.forEach((result) => {
      this.shownResultIds.push(result.id);
      this.users[result.id] = {...this.users[result.id], ...result};
    });
    this.shownResultIds = _.uniq(this.shownResultIds);
  }

  @action removeUserById(userId: number) {
    delete this.users[userId];
  }

  @computed get searchResults(): any[] {
    const searchResult: any[] = [];
    if (this.notAvailable != null && this.notAvailable > 0) {
      this.resultList.forEach((result: any, index: number) => {
        searchResult.push(result);
        if (index !== 0 && index % everyNItemsNotAvailable === 0) {
          searchResult.push({
            id: `info_${index}`,
            notAvailable: this.notAvailable
          });
        }
      });
      if (this.resultList.length < everyNItemsNotAvailable) {
        searchResult.push({
          id: `info_${1}`,
          notAvailable: this.notAvailable
        });
      }
      return searchResult;
    } else { return this.resultList; }

  }

  @action
  public prepareFilters() {
    this.temporaryFilter = { ...this.filters };
  }

  @action
  public dissmissFilterChanges() {
    this.temporaryFilter = { ...FilterFactory.create() };
  }

  public onRefresh() {
    this.nextPage = 1;
    this.resultList = [];
    this.isRefreshing = true;
    this.nextPageLoading = false;
    this.search(true);
  }

  public applyFilterChanges() {
    this.nextPage = 1;
    this.filters = Object.assign({}, this.temporaryFilter);
    this.search(true);
    this.navigationStore.goBack(null);
  }

  public clearFilters() {
    this.filters = Object.assign({}, this.temporaryFilter);
  }

  public setFilterGender(gender: Profile['gender'][]) {
    this.temporaryFilter.genders = [];
    if (gender.join('') === 'nope') { return; }
    gender.forEach((gender) => {
      this.temporaryFilter.genders.push(FilterFactory.createGender(gender));
    });
  }

  public get getFilterGender(): string {
    const { genders } = this.temporaryFilter;
    if (!_.isEmpty(genders)) {
      return genders.map((item) => item.gender).join('');
    } else { return ''; }
  }

  @action loadNextPage() {
    if (this.nextPage && !this.nextPageLoading) {
      this.nextPageLoading = true;
      this.repository.load(this.getPreparedFilters())
        .then((response: any) => {
          this.nextPage = response.next ? this.nextPage + 1 : 0;
          this.notAvailable = response.notAvailable;
          this.resultList = [...this.resultList, ...response.results];
        })
        .catch((err: any) => { console.log(err) })
        .finally(() => {
          this.isLoading = false;
          this.nextPageLoading = false;
          this.isRefreshing = false;
        })
    }
  }

  protected getPreparedFilters() {
    const filter = Object.assign({}, this.filters, { page: this.nextPage });
    const genders = filter.genders.slice() as any;
    if (genders.length === 1 ) { filter.is_couple = false; }
    if (genders.length === 2 ) { filter.is_couple = true; }
    genders.forEach((gender: any, index: number) => {
      let postfix = '';
      if (Boolean(index)) { postfix = String(index); }
      Object.keys(gender).forEach((key) => {
        Object.assign(filter, { [key + postfix]: gender[key] })
      })
    })
    !filter.online && delete filter.online;
    delete filter.genders;
    const purpose_dating = filter.purpose_dating;
    if (purpose_dating.length > 1) {
      var purDateString = purpose_dating[0];
      purpose_dating.map((item, index) => {
        if (index > 0) {
          purDateString = purDateString + '&' + 'purpose_datings' + '=' + item;
        }
      })
      Object.assign(filter, { purpose_datings: decodeURI(purDateString), purpose_dating: null });
    }
//key=value&key=value1

    return filter;
  }

  @action search(showLoader: boolean = true) {
    this.nextPage = 1;
    this.resultList = [];
    this.isLoading = showLoader;
    this.loadNextPage();
  }

  @action
  public async setLike(id: number) {
    await this.profileManager.setLike(id);
  }

  @action
  public async setFavourite(id: number) {
    await this.profileManager.setFavourite(id);
  }

  @action
  public openProfile(id: number) {
    this.profileManager.openProfile(id);
  }
}

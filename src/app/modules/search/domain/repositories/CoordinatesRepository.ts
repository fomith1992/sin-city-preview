import { GeolocationResponse } from '@react-native-community/geolocation';
import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import coordsResource from "../resources/CoordinatesResource";
import makeCancellablePromise from 'make-cancellable-promise';

class CoordsRepository extends BaseRepository {
  constructor() {
    super(coordsResource);
  }
  public updateCoordinate({ latitude, longitude }: GeolocationResponse['coords']) {
    this.resource('change')
      .update({ latitude, longitude })
      .then((res: any) => this.processResponse(res));
  }

  public load(params?: {}) {
    return makeCancellablePromise(super.load(params));
  }
}

const coordsRepository = new CoordsRepository();

export default coordsRepository;

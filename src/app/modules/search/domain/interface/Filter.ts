export interface Filter {
  coordinates?: number[],
  login?: string
  is_couple?: boolean
  has_children?: boolean
  family_status?: 'MARRIED' | 'UNMARRIED' | 'CIVIL_MARRIAGE'
  city?: number
  intention?: 'PARTY' | 'SEX'
  online?: boolean
  purpose_dating: 'SWING_OPEN' | 'SWING_CLOSED' | 'SWING_SOFT' | 'GROUP_SEX' | [],

  genders: GenderFilter[]
}

export interface GenderFilter {
  gender: 'M' | 'W'
  age: number[]
  height: number[]
  weight: number[]
  type_figure?:	'TRIANGLE'| 'INVERTED TRIANGLE'| 'HOURGLASS'| 'RECTANGLE'| 'CIRCLE'| 'TRAPEZE'| 'OVAL'
  type_physiques?: 'ECTOMORPH' | 'MESOMORPH' | 'ENDOMORPH'
  facial_hair?: 'NOT' | 'BRISTLES' | 'BEARD' | 'MUSTACHE' | 'MUSTACHE BEARD'
  orientation?: 'BI' | 'HETERO' | 'HOMO'
  attitude_to_smoking?: 'POSITIVE' | 'NEUTRAL' | 'NEGATIVE'
  attitude_to_alcohol?: 'POSITIVE' | 'NEUTRAL' | 'NEGATIVE'
}

export interface Coordinates {
  id:	number
  latitude:	number
  longitude: number
  photo: {
    id:	number
    photoUrl:	string
    photoThumbnailUrl:	string
  }
}

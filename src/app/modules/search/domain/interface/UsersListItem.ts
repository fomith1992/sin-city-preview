export interface UsersListItem {
  id: number
  login: string
  photo?:	{
    id: number
    photoUrl: string
    photoThumbnailUrl: string
  }
  profiles: {
    id: number
    age: string
    gender: 'M' | 'W'
  }
  iLikedIt: boolean
  heLikedMe: boolean
  heFavorite: boolean
  lastActivity: string
  online: string
  isVerified: boolean
  purposeGender: any
}

import { Filter, GenderFilter } from './../interface/Filter';

export class FilterFactory {
  static create(): Filter {
    return {
      coordinates: [],
      genders: [],
      purpose_dating: [],
      family_status: null,
      city: null,
      intention: null,
      online: false,
      has_children: null,
    }
  }

  static createGender(gender: 'M' | 'W'): GenderFilter {
    return {
      gender,
      age: [18, 80],
      height: [115, 245],
      weight: [30, 180],
      type_figure: undefined,
      type_physiques: undefined,
      facial_hair: undefined,
      orientation: undefined,
      attitude_to_smoking: undefined,
      attitude_to_alcohol: undefined,
    }
  }
}

import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../../modules/core/infrastructure/httpResource";

class CoordinatesResource extends BaseRestResource {
  constructor() {
    super(httpResource, 'coordinates');
  }

  loadUsersByCoordinates(coords: { latitude: number, longitude: number }) {
    return this.get(coords).then((res: any) => res.json());
  }
}

const coordinatesResource = new CoordinatesResource();

export default coordinatesResource;
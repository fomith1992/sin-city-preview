import userRepository from "../../../user/domain/repositories/UserRepository";

export default class UserActions {
  private userId: number;
  constructor(userId: number) { 
    this.userId = userId;
  }

  updateILikedIt(isLiked: boolean) {
    const action = isLiked ? 'unlike' : 'like';
    return userRepository.setLike(this.userId, action);
  }

  updateHeFavorite(isFavourite: boolean) {
    const action = isFavourite ? 'delete-favorite' : 'add-favorite';
    return userRepository.setFavourite(this.userId, action);
  }
}
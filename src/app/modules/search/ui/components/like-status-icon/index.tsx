import React from 'react';
import FastImage from 'react-native-fast-image';

import icons from '../../../../icons';

const LikeStatusIcon = ({ iLiked, heLiked }: { iLiked: boolean, heLiked: boolean }) => {
  const style = React.useRef({ width: 19, height: 17 });
  if (iLiked) {
    if (heLiked) {
      return <FastImage source={icons.likeMutuallyIcon} style={style.current}/>
    } else {
      return <FastImage source={icons.likeOnIcon} style={style.current}/>
    }

  } else {
    return <FastImage source={icons.likeOffIcon} style={style.current}/>

  }
}

export default LikeStatusIcon;
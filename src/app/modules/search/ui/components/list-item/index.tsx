import React, {memo} from 'react';
import {View, Text, Image, TouchableOpacity, Switch} from 'react-native';
import {observer} from 'mobx-react';
import FastImage from 'react-native-fast-image';

import icons from '../../../../icons';
import styles from './style';
import {formatLastActivity} from '../../../../ud-ui/helpers/dateFormat';
import FavouriteStatusIcon from '../favourite-status-icon';
import LikeStatusIcon from '../like-status-icon';
import {pluralize} from '../../../../ud-ui/helpers/pluralize';

interface props {
  notAvailable?: number;
  login?: string;
  photo?: {
    id: number;
    photoUrl: string;
    photoThumbnailUrl: string;
  };
  lastActivity?: string;
  iLikedIt?: boolean;
  heLikedMe?: boolean;
  heFavorite?: boolean;
  onLikePress?: any;
  onFavouritePress?: any;
  purposeGender?: any;
  onPress?: any;
  createdAt?: string;
  online?: boolean | string;
  errorText?: boolean;
}
// получение String гендера из двух профилей
const getProfileGender = (profiles) => {
  if (profiles.length === 1) {
    return profiles[0].gender
  } else if (profiles.length === 2) {
    return profiles[0].gender + profiles[1].gender
  } else {
    return 'MW'
  }
};
// выбор иконки, соответствующей профилю
const getGenderIcon = (profiles) => {
    const gender = getProfileGender(profiles);
    switch(gender) {
        case "MW":
            return icons.mw;
            break;
        case "WM":
            return icons.mw;
            break;
        case "M":
            return icons.m;
            break;
        case "W":
            return icons.w;
            break;
        case "MM":
            return icons.mm;
            break;
        case "WW":
            return icons.ww;
            break;
    }
}

const SearchListItem = memo(
  observer(
    ({
      notAvailable,
      createdAt,
      heFavorite,
      heLikedMe,
      iLikedIt,
      lastActivity,
      login,
      online,
      photo,
      onPress,
      onLikePress,
      onFavouritePress,
      errorText,
      isVerified,
      purposeGender,
        profiles,
    }: props) => {
      const gender = getProfileGender(profiles);
      if (notAvailable) {
        const pluralizeWord = pluralize(notAvailable, [
          'ваших гостей недоступны',
          'вашего гостя недоступно',
          'ваших гостей недоступны',
        ]);
        const pluralizeAnketWord = pluralize(notAvailable, [
          'анкетам',
          'анкете',
          'анкетам',
        ]);
        //`Пройдите верификацию и получите доступ ещё к ${notAvailable} ${pluralizeAnketWord}`

        return (
          <TouchableOpacity
            onPress={onPress}
            style={styles.notVerifiedContainer}>
            <FastImage
              source={icons.diamondIcon}
              style={{width: 42, height: 42}}
              resizeMode={FastImage.resizeMode.contain}
            />

            <Text style={styles.notVerifiedText}>
              {errorText
                ? ` Более ${notAvailable} ${pluralizeWord} без активной подписки`
                : 'Получите статус "Подтверждённый" и просматривайте в два раза больше пользователей'}
            </Text>
          </TouchableOpacity>
        );
      }

      return (
        <>
          <TouchableOpacity style={styles.container} onPress={onPress}>
            <View style={styles.leftContainer}>
              <View style={styles.avatarWrapCont}>
                {isVerified && (
                  <Image source={icons.verified} style={styles.verifiedIcon} />
                )}
                <View style={styles.avatarWrap}>
                  {photo && (
                    <FastImage
                      source={{uri: photo.photoThumbnailUrl}}
                      resizeMode={FastImage.resizeMode.contain}
                      style={styles.photo}
                    />
                  )}
                  {!photo && (
                    <View style={styles.photoPlaceholderWrap}>
                      <FastImage
                        source={icons.profileListPlaceholder}
                        resizeMode={FastImage.resizeMode.contain}
                        style={styles.photoPlaceholder}
                      />
                    </View>
                  )}
                </View>
              </View>
              <View style={styles.leftContent}>
                <Text style={styles.title} numberOfLines={1}>
                  {login}
                </Text>
                {/*<Text style={online ? styles.activeSubtitle : styles.inactiveSubtitle}>*/}
                {/*  {online ? 'online' : formatLastActivity(lastActivity, {smallFormat: true})}*/}
                {/*</Text>*/}
              </View>
            </View>
            <View style={styles.rightContainer}>
                <TouchableOpacity
                    style={styles.iconContainer}>
                <FastImage source={getGenderIcon(profiles)}
                           style={gender.length !== 1 ? styles.genderIcon : gender[0] === 'W' ? styles.genderIconSingle : styles.genderIconSingleMan }
                />
                </TouchableOpacity>
              <TouchableOpacity
                onPress={onFavouritePress}
                style={styles.iconContainer}>
                <FavouriteStatusIcon isActive={heFavorite} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={onLikePress}
                style={[styles.iconContainer, {paddingRight: 22.5}]}>
                <LikeStatusIcon iLiked={iLikedIt} heLiked={heLikedMe} />
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        </>
      );
    },
  ),
);

export default SearchListItem;

import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 80,
    borderBottomColor: '#ffffff22',
    borderWidth: 1,
    paddingLeft: 20,
  },
  genderIcon: {
    height: 19,
    width: 19,
   // color: '#ffffff',
    tintColor: '#ff2c48',
  },
  genderIconSingle: {
    height: 22,
    width: 22,
    // color: '#ffffff',
    tintColor: '#ff2c48',
  },
  genderIconSingleMan: {
    height: 23,
    width: 23,
    marginRight: -1,
    // color: '#ffffff',
    tintColor: '#ff2c48',
  },
  leftContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightContainer: {
    flexDirection: 'row'
  },
  leftContent: {
    flexDirection: 'column',
    marginLeft: 14,
    justifyContent: 'space-between',
    maxWidth: 210
  },
  title: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 14,
    color: '#fff'
  },
  inactiveSubtitle: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 12,
    color: '#999999',
  },
  activeSubtitle: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 12,
    color: '#ffbd21',
  },
  notVerifiedContainer: {
    height: 73,
    backgroundColor: '#ffbd21',
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  notVerifiedText: {
    ...fontFamily('fontTitleBold'),
    fontSize: 14,
    color: '#000',
    marginLeft: 15,
    marginRight: 70,
  },
  photo: {
    width: 43,
    height: 43,
  },
  avatarWrapCont: {

  },
  verifiedIcon: {
    position: 'absolute',
    right: -1,
    bottom: -1,
    zIndex: 1,
  },
  avatarWrap: {
    width: 43,
    height: 43,
    borderRadius: 50,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  photoPlaceholderWrap: {
    width: 43,
    height: 43,
    backgroundColor: '#2D2D2D',
    alignItems: 'center',
    justifyContent: 'center'
  },
  photoPlaceholder: {
    width: 30,
    height: 22,
  },
  iconContainer: {
    paddingHorizontal: 7.5,
    height: 80,
    justifyContent: 'center'
  }
})

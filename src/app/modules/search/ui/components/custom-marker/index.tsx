import React, { memo, useRef, useCallback } from 'react';

import { Coordinates } from '../../../domain/interface/Coordinates';

import { Marker } from 'react-native-maps';
import SearchComponentsMapMarkerView from "../map-marker-view";

const SearchComponentsCustomMarker = memo(({ coordinate, onPress }: { coordinate: Coordinates, onPress: any, isVerified: Boolean }) => {
  const markerRef = useRef(null);
  //console.log('item_isVerified', isVerified)
  const onLoad = useCallback(() => {
    if (markerRef.current) {
      markerRef.current.redraw();
    }
  }, [markerRef.current]);
  return (
    <Marker
      ref={ref => markerRef.current = ref}
      identifier={String(coordinate.id)}
      key={coordinate.id}
      coordinate={coordinate}
      tracksViewChanges={false}
      tracksInfoWindowChanges={false}
      onPress={() => onPress(coordinate)}>
      <SearchComponentsMapMarkerView onLoad={onLoad} {...coordinate}/>
    </Marker>
  );
})

export default SearchComponentsCustomMarker;

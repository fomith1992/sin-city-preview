import React from 'react';
import { View, Text } from 'react-native';
import FastImage from 'react-native-fast-image';

import styles from './style'
const emptySearchImg = require('./img/search_empty.png');
const NotFound = () => {
  return (
    <View style={{ marginTop: 20, flex: 1, justifyContent: 'center' }}>
      <Text style={styles.text}>Поиск не дал результатов</Text>
      <FastImage source={emptySearchImg}
                 style={{ height: '70%' }}
                 resizeMode='contain'/>
    </View>
  );
};
export default NotFound;
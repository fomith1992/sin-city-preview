import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  text: {
    ...fontFamily('fontBaseMedium'),
    fontSize: 14,
    color: '#fff',
    marginBottom: 10,
    textAlign: 'center'
  }
})
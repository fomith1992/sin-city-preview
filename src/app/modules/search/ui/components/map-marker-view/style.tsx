import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    borderColor: '#ffbd21',
    backgroundColor: '#252525',
    borderWidth: 2,
    width: 57,
    height: 57,
    borderRadius: 57,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center'
  },
  photo: {
    width: 57,
    height: 57,
    aspectRatio: 1
  },
  verifiedIcon: {
    position: 'absolute',
    right: 1.4,
    bottom: 0.7,
    zIndex: 1,
    height: 18,
    width: 18
  },
})

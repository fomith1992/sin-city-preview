import React, { memo } from 'react';
import {Image, View} from 'react-native';
import FastImage from 'react-native-fast-image';

import styles from './style';
import { Coordinates } from '../../../domain/interface/Coordinates';
import icons from "../../../../icons";

interface props {
  photo: Coordinates['photo'],
  onLoad: any,
  isVerified: boolean
}

const SearchComponentsMapMarkerView = memo(({ onLoad, photo, isVerified }: props) => {
  const source = photo ? { uri: photo.photoThumbnailUrl } : icons.profileListPlaceholder;
  return (
    <View>
      {isVerified &&
        <Image source={icons.verified} style={styles.verifiedIcon}/>
      }
        <View style={styles.container}>

          <FastImage source={source}
                     style={styles.photo}
                     onLoad={onLoad}
                     resizeMode={photo ? 'cover' : 'center'}/>
        </View>
    </View>
  );
});

export default SearchComponentsMapMarkerView;

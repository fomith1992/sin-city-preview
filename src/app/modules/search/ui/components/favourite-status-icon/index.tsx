import React from 'react';
import FastImage from 'react-native-fast-image';

import icons from '../../../../icons';

const FavouriteStatusIcon = ({ isActive }: { isActive: boolean }) => {
  const style = React.useRef({ width: 19, height: 17 });
  if (isActive) {
    return <FastImage source={icons.favouriteOnIcon} style={style.current}/>;
  } else {
    return <FastImage source={icons.favouriteOffIcon} style={style.current}/>
  }
}

export default FavouriteStatusIcon;
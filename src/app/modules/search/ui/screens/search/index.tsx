import React, {useEffect, useCallback} from 'react';
import {
  ActivityIndicator,
  View,
  SafeAreaView,
  TextInput,
  RefreshControl,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import {Observer, inject, observer} from 'mobx-react';
import * as _ from 'lodash';
import {withNavigationFocus} from 'react-navigation';
import {Settings, AppEventsLogger} from 'react-native-fbsdk-next';

import * as screenNames from '../../../../navigation/screen-names';

import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import SearchListItem from '../../components/list-item';
import NotFound from '../../components/not-found';
import UsersSearchStore from '../../../store/UsersSearchStore';
import {CurrentUserStore} from '../../../../auth/store/current-user';
import {UsersListItem} from '../../../domain/interface/UsersListItem';
import FastImage from 'react-native-fast-image';
//import chatsRepository from '../domain/repositories/ChatsRepository';
import chatsRepository from '../../../../messages/domain/repositories/ChatsRepository';
import {ChatsResponse} from '../../../../messages/domain/interfaces/Responses';
import rootStore from '../../../../../rootStore';

const searchIcon = require('./icons/search.png');
const resetCrossIcon = require('./icons/cross-icon.png');
const LoadingIndicator = ({visible}: {visible: boolean}) => {
  return visible ? <ActivityIndicator style={{paddingVertical: 20}} /> : <></>;
};
var isFirstLoadInit = true;
window.searchScreenChangeIsFirstLoadInit = (value) => {
  isFirstLoadInit = value;
};
interface props {
  currentUserStore: CurrentUserStore;
  usersSearchStore: UsersSearchStore;
  navigation: any;
  isFocused: boolean;
}
function loadAndReturnChats() {
  return chatsRepository.load({page: 1}).catch(() => {});
}
window.searchScreenChatLoad = () => {
  loadAndReturnChats().then(({results, next, unreadMessagesCounter}: ChatsResponse) => {
   // console.log('search_unreadounter', unreadMessagesCounter)
    // let unreadCounterSum = 0;
    // results.forEach((object) => {
    //   if (object.unreadCounter > 0) {
    //     unreadCounterSum += object.unreadCounter;
    //   }
    // });

    // let unreadCounterSum = () => {
    //   lc
    //   results.map((e) => {
    //   //  return 4;
    //     stas = stas + 5;
    //   });
    //   return stas;
    // console.log('RESULTS', results);
    // console.log('unreadCounterSum', unreadCounterSum);
    window.setTNUnreadAmount(unreadMessagesCounter);
  });
};
const SearchScreen = ({
  currentUserStore,
  usersSearchStore,
  navigation,
  isFocused,
}: props) => {
  useEffect(() => {
    if (!window.getUserExists()) {
      // navigation.navigate();
      navigation.navigate(screenNames.MESSAGES_TAB);
      window.setUserExists(true);
    }


    let {phone} = currentUserStore.user;
   // let {phone} = {phone: '+7(927)9801998'};
    phone = '8' + phone.replace(/[^\d]/g, '').slice(1, phone.length);
    const birthday = !!currentUserStore.user && !!currentUserStore.user.profiles && !!currentUserStore.user.profiles[0] && !!currentUserStore.user.profiles[0].birthday ? currentUserStore.user.profiles[0].birthday.replace(/[^\d]/g, '') : null;
    const gender = !!currentUserStore.user && !!currentUserStore.user.profiles && !!currentUserStore.user.profiles[0] ? currentUserStore.user.profiles[0].gender : null;
    const userData = {
      // email: 'testEmail@abo.ba',
      // firstName: 'Ignat',
      // lastName: 'Tester',
      phone: phone,
      dateOfBirth: !!gender && gender.length === 1 ? birthday : null,
      gender: !!gender && gender.length === 1 ? gender.toLowerCase() : null,
      // city: 'Murmansk',
      // state: '',
      // zip: null,
      // country: 'ru',
    };
    AppEventsLogger.setUserData(userData);

    isFocused && usersSearchStore.search();
    if (isFirstLoadInit) {
      isFirstLoadInit = false;
      // Alert.alert('isFirstInit = true');
      // console.log('DDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
      loadAndReturnChats().then(({results, next, unreadMessagesCounter}: ChatsResponse) => {
        // let unreadCounterSum = 0;
        // results.forEach((object) => {
        //   if (object.unreadCounter > 0) {
        //     unreadCounterSum += object.unreadCounter;
        //   }
        // });
        window.setTNUnreadAmount(unreadMessagesCounter);
      });
    } else {
      // Alert.alert('isFirstInit = false');
    }
    // const results =() => {
    //   return chatsRepository.load({ page: 1 })
    //     .catch(() => {
    //     })
    // };
    // console.log(results);
  }, []);
  const onFilterPress = useCallback(() => {
    window.fLogEventObj('search_openFilter', {});
    navigation.navigate(screenNames.FILTER_MODAL, {store: 'usersSearchStore'});
  }, []);

  const onProfilePress = useCallback((item) => {
    window.fLogEventObj('search_clickProfile', {userID: item.id});
    // if (item.notAvailable) {
    //   navigation.navigate(screenNames.VERIFICATION_FIRST_STEP_SCREEN);
    // } else {
    //   usersSearchStore.openProfile(item.id);
    // }
    usersSearchStore.openProfile(item.id);
  }, []);
  // обработка нажатия на иконку профиля
  const onProfilePressed = useCallback(() => {
    window.fLogEventObj('search_clickItem', {item_name: 'myprofile'});
    navigation.navigate(screenNames.MY_PROFILE_SCREEN); //EVENTS_TAB
  }, []);
  const onSearchInputPress = useCallback((text) => {
    usersSearchStore.filters.login = text;
  }, []);
  const onSearchPress = useCallback(() => {
    usersSearchStore.search();
    window.fLogEventObj('search_clickSearchBtn', {
      searchQuery: usersSearchStore.filters.login,
    });
  }, []);
  const onResetInputPress = useCallback(() => {
    usersSearchStore.filters.login = null;
    usersSearchStore.search();
  }, [])
  const onLikePress = (user: UsersListItem) => {
    usersSearchStore.setLike(user.id);
    window.fLogEventObj('search_clickLikeBtn', {userId: user.id});
  };
  const onFavouritePress = (user: UsersListItem) => {
    usersSearchStore.setFavourite(user.id);
    window.fLogEventObj('search_clickFavoriteBtn', {userId: user.id});
  };
  //обработка нажатия на иконку последних уведомлений
  const onNotificationPressed = useCallback(() => {
    window.fLogEventObj('menu_clickItem', {item_name: 'notifications'});
    navigation.navigate(screenNames.NOTIFICATIONS_SCREEN);
  }, []);
  //обработка нажатия на иконку событий
  const onEventsPressed = useCallback(() => {
    navigation.navigate(screenNames.EVENTS_TAB);
  }, []);

  const onRefresh = useCallback(() => {
    usersSearchStore.onRefresh();
  }, []);

  const renderListItem = ({item}: {item: UsersListItem}) => {
    return (
      <Observer>
        {() => (
          <SearchListItem
            {...item}
            onPress={() => onProfilePress(item)}
            onLikePress={onLikePress.bind(null, item)}
            onFavouritePress={onFavouritePress.bind(null, item)}
          />
        )}
      </Observer>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <Header
        title="Поиск"
        onPress={() => onProfilePressed()}
        icon={icons.profileMenu}
        onRightIconSecondPress={() => onFilterPress()}
        rightIconSecond={icons.filter}
        leftIcon={icons.notificationIcon}
        onBack={() => onNotificationPressed()}
        leftIconSecond={icons.eventsIcon}
        onBackSecond={() => onEventsPressed()}
      />
      <View style={{height: 50}}>
        <View style={styles.inputCont}>
        <TextInput
          value={usersSearchStore.filters.login}
          onChangeText={(text) => onSearchInputPress(text)}
          style={styles.input}
          onSubmitEditing={onSearchPress}
          returnKeyType="search"
          placeholder="Введите никнейм"
          placeholderTextColor="#999999"

        />
        {!!usersSearchStore.filters.login && (
            <TouchableOpacity onPress={onResetInputPress} style={styles.resetCrossIcon}>
              <View style={styles.resetCont}>
                <FastImage
                    source={resetCrossIcon}
                    style={{width: 10, height: 10}}
                    resizeMode={FastImage.resizeMode.contain}
                    tintColor={'#999999'}
                />
              </View>
            </TouchableOpacity>
        )}

        </View>
        <TouchableOpacity onPress={onSearchPress} style={styles.search}>
          <FastImage
            source={searchIcon}
            style={{width: 20, height: 20}}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>
      </View>
      {usersSearchStore.isLoading ? (
        <ActivityIndicator style={{flex: 1}} size="large" />
      ) : (
        <FlatList
          contentContainerStyle={{flexGrow: 1}}
          data={usersSearchStore.searchResults}
          refreshControl={
            <RefreshControl
              refreshing={usersSearchStore.isRefreshing}
              onRefresh={onRefresh}
            />
          }
          renderItem={renderListItem}
          keyExtractor={(item) => String(item.id)}
          onEndReachedThreshold={0.5}
          onEndReached={() => {
            usersSearchStore.loadNextPage();
          }}
          ListFooterComponent={
            <LoadingIndicator visible={usersSearchStore.nextPageLoading} />
          }
          ListEmptyComponent={<NotFound />}
        />
      )}
    </SafeAreaView>
  );
};

export default withNavigationFocus(
  inject('usersSearchStore', 'currentUserStore')(observer(SearchScreen)),
);

import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    backgroundColor: '#000'
  },
  input: {
    flex: 1,
    display: 'flex',
    height: 50,
    backgroundColor: '#3e3e3e',
    paddingLeft: 20,
  //  paddingRight: -20,
    ...fontFamily('fontBaseMedium'),
    fontSize: 16,
    color: '#999999',
  },
  inputCont: {
    backgroundColor: '#3e3e3e',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
   // color: '#999999'
  },
  resetCont: {
    backgroundColor: 'rgba(0,0,0,0.99)',
    padding: 6,
    borderRadius: 50,
  },
  resetCrossIcon: {
    position: 'absolute',
    justifyContent: 'center',
    right: 45,
    top: 0,
    bottom: 14,
    paddingHorizontal: 20,
   // marginLeft: 30,
    height: '100%'
  },
  search: {
    position: 'absolute',
    justifyContent: 'center',
    right: 0,
    top: 0,
    bottom: 14,
    paddingHorizontal: 20,
    height: '100%'
  }
})

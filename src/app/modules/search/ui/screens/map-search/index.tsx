import React, { useCallback, useRef, useState } from 'react';
import {
  ActivityIndicator,
  SafeAreaView,
  View,
  Text,
  Switch,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import MapView from "react-native-map-clustering";
import SwitchSelector from "react-native-switch-selector";
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx';
import * as _ from 'lodash';
import { showMessage } from 'react-native-flash-message';

import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import CoordinatesSearchStore from '../../../store/CoordinatesSearchStore';
import SearchComponentsCustomMarker from '../../components/custom-marker';
import { useFocusEffect, useIsFocused } from "react-navigation-hooks";
import { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import useInterval from '../../../../ud-ui/hooks/use-interval';
import { isEmpty } from 'lodash';

const locationIcon = require('./icons/icon_map_my_location.png');
const stubImage = require('./img/stub-image.png');

import mapStyle from "./map-style.json";
import FastImage from "react-native-fast-image";
import {ITYPES, TYPES} from "../../../../../store/store-types";
import GeolocationStore from "../../../../core/store/geolocation";
import * as screenNames from "../../../../navigation/screen-names";
const { width, height } = Dimensions.get('window');
const screenRatio = width / height;

const ONE_METER_IN_RAD = .00003919625725;
const INITIAL_METERS_SCALE = 300;
const DEFAULT_MOSCOW_COORDS = {
  latitude: 55.751244,
  longitude: 37.618423,
  latitudeDelta: ONE_METER_IN_RAD * INITIAL_METERS_SCALE,
  longitudeDelta: ONE_METER_IN_RAD * INITIAL_METERS_SCALE * screenRatio
};

const intentions = [
  { label: "Свидание", value: 'MEETING' },
  { label: "Секс", value: 'SEX' }
];

type props = {
  navigation: any,
  coordinatesSearchStore: CoordinatesSearchStore,
  geolocationStore: GeolocationStore,
  profileManager: ITYPES['ProfileManager']
};

function getDefaultInitialRegion(userLocation: any) {
  if (!isEmpty(userLocation)) {
    return {
      ...DEFAULT_MOSCOW_COORDS,
      latitude: userLocation.latitude,
      longitude: userLocation.longitude
    };
  }
  return DEFAULT_MOSCOW_COORDS;
}

const SearchMapScreen = inject('coordinatesSearchStore', TYPES.GeolocationStore, TYPES.ProfileManager)(observer(
  ({ navigation, coordinatesSearchStore, geolocationStore, profileManager }: props) => {
    const [initialRegion, setInitialRegion] = useState<any>(getDefaultInitialRegion(geolocationStore.location));
    const isFocused = useIsFocused();
    const mapRef = useRef(null);
    const mapRefTimer = useRef(null);
    const [isCurrentRegionSelected, setCurrentRegionSelectedState] = useState(false);
    const [delay, setDelay] = useState(null);
    const [isCurrentLocationLoading, setCurrentLocationLoading] = useState(false);

    useInterval(() => {
      coordinatesSearchStore.loadNextPage();
    }, delay);

    window.goToPushUser = () => {
        const { userCoordinates } = profileManager;
        console.log('goToPushUser userCoords', userCoordinates, mapRef)
        if (userCoordinates !== null && mapRef !== null && mapRef.current !== null) {
            console.log('userCoordinates notNull', userCoordinates)
            const region = {
                latitudeDelta: ONE_METER_IN_RAD * INITIAL_METERS_SCALE,
                longitudeDelta: ONE_METER_IN_RAD * INITIAL_METERS_SCALE * screenRatio,
                ...userCoordinates
            };
            (mapRef as any).current.animateToRegion(region, 100);
          //  (mapRef as any).current.animateToRegion(DEFAULT_MOSCOW_COORDS, 100);
            profileManager.userCoordinates = null;
        } else {
            console.log('userCoordinates NULL', userCoordinates)
        }
    }

    const onScreenFocus = useCallback(() => {
        console.log('onMapScreenOpen')
       // const latitude = navigation.getParam('latitude');
       // console.log('latitude', latitude)
       //  setTimeout(() => {
       //      goToPushUser();
       //  }, 600)
      if (coordinatesSearchStore.shouldNotNavigateToCurrentPosition) { coordinatesSearchStore.shouldNotNavigateToCurrentPosition = false; }
      else { navigateToCurrentPosition() }
      setDelay(3000);
      return () => {
        setDelay(null);
      };
    }, []);

    const navigateToCurrentPosition = useCallback(() => {
      coordinatesSearchStore.loadShownOnMap();
      coordinatesSearchStore.loadIntention();
      tryLoadMapInfo();
    }, [isCurrentRegionSelected]);

    const tryLoadMapInfo = useCallback(() => {
      if (mapRef.current) {
        if (mapRefTimer.current != null) {
          clearInterval(mapRefTimer.current);
        }
        showCurrentPositionAndUsers();
      } else if (mapRefTimer.current == null) {
        mapRefTimer.current = setInterval(() => tryLoadMapInfo(), 300);
      }
      return () => {
        if (mapRefTimer.current != null) {
          clearInterval(mapRefTimer.current);
        }
      };
    }, []);

    const showCurrentPositionAndUsers = useCallback(() => {
      showCurrentPosition(true);
    }, [mapRef])

    const showCurrentPosition = useCallback((isInitial?: boolean) => {
      if (isCurrentRegionSelected || isCurrentLocationLoading) {
        return;
      }
      setCurrentLocationLoading(true);
      geolocationStore.updateCoordinates().then(() => {
        const coords = geolocationStore.location;
        coordinatesSearchStore.filters.coordinates = [coords.latitude, coords.longitude];
        if (isInitial) { coordinatesSearchStore.search(false); }
        if (initialRegion == null) {
          const region = {
            latitudeDelta: ONE_METER_IN_RAD * INITIAL_METERS_SCALE,
            longitudeDelta: ONE_METER_IN_RAD * INITIAL_METERS_SCALE * screenRatio,
            ...coords
          };
          setInitialRegion(region);
        }
        console.log('initialRegion', initialRegion)
        const scale = {
          latitudeDelta: ONE_METER_IN_RAD * INITIAL_METERS_SCALE,
          longitudeDelta: ONE_METER_IN_RAD * INITIAL_METERS_SCALE * screenRatio
        };

        if (mapRef.current) {
          (mapRef as any).current.animateToRegion(Object.assign({}, coords, scale), 100);
          setCurrentRegionSelectedState(true);
        } else {
          // isFocused && showMessage({ message: `ERROR #0002. Экземпляр карты не доступен.`, type: "info" });
        }
      }).catch((error) => {
        window.fLogEvent('app_errorScreen',
          'ERROR #0003. Не удалось определить местоположение');
        showMessage({ message: `ERROR #0003. Не удалось определить местоположение. ${error.message}`, type: "info" });
      }).finally(() => { setCurrentLocationLoading(false); });
    }, [mapRef, isCurrentRegionSelected]);

    useFocusEffect(onScreenFocus);

    const onMarkerPressed = useCallback((item) => {
      window.fLogEventObj('map_clickProfile', {user_id: item.id})
      coordinatesSearchStore.openProfile(item.id);
    }, []);

    const onFilterPressed = useCallback(() => {
      navigation.navigate('FILTER_MODAL', {
        store: 'coordinatesSearchStore',
        goBack: coordinatesSearchStore.customGoBack
      });
    }, []);
// обработка нажатия на иконку профиля
    const onProfilePressed = useCallback(() => {
        window.fLogEventObj('search_clickItem', {item_name: 'myprofile'});
        navigation.navigate(screenNames.MY_PROFILE_SCREEN); //EVENTS_TAB
    }, []);

    const onVisibilityChange = useCallback((value) => {

      coordinatesSearchStore.setShownOnMap(value);
      setDelay(value ? 3000 : null);
    }, [delay, setDelay]);
    // обработка нажатия на иконку уведомлений
    const onNotificationPressed = useCallback(() => {
        window.fLogEventObj('menu_clickItem', {item_name: 'notifications'});
        navigation.navigate(screenNames.NOTIFICATIONS_SCREEN);
    }, []);
    // обработка нажатия на иконку на события
    const onEventsPressed = useCallback(() => {
        navigation.navigate(screenNames.EVENTS_TAB);
    }, []);

    const onIntentionChange = useCallback((intention: string) => {
      coordinatesSearchStore.setIntention(intention);
    }, []);

    const onMapRef = (ref: any) => {
      mapRef.current = ref;
    };

    if (!coordinatesSearchStore.intention) {
      return <ActivityIndicator size="large"/>
    }

    return (
      <SafeAreaView style={styles.container}>
        <Header title="Поиск на карте"
                onPress={onProfilePressed}
                icon={icons.profileMenu}
                onRightIconSecondPress={() => onFilterPressed()}
                rightIconSecond={icons.filter}
                leftIcon={icons.notificationIcon}
                onBack={() => onNotificationPressed()}
                leftIconSecond={icons.eventsIcon}
                onBackSecond={() => onEventsPressed()}
        />
        <View
            style={{
                marginHorizontal: 20,
                marginTop: 15
            }}
        >
            <Text style={styles.searchLabel}>*Другие пользователи видят ваше местоположение в радиусе 500 м. от реального положения.</Text>
        </View>
        <View style={styles.switchContainer}>
          <Text style={styles.switchLabel}>Показывать меня на карте</Text>
          <Switch thumbColor="#fff"
                  value={coordinatesSearchStore.isIShownOnMap}
                  ios_backgroundColor="#333"
                  trackColor={{ true: "#ffbd21", false: "#333" }}
                  onValueChange={onVisibilityChange}/>
        </View>
        {!coordinatesSearchStore.isIShownOnMap &&
        <View style={{ flexGrow: 1, backgroundColor: '#333', paddingHorizontal: 50 }}>
          <Text style={[styles.notShownLabel, styles.topText]}> Вы скрыли свое местоположение и не видите других участников</Text>
          <View style={{alignItems: 'center'}}>
            <FastImage source={stubImage} style={{ width: 260, height: 136}}/>
          </View>
          <Text style={[styles.notShownLabel, styles.bottomText]}>Чтобы воспользоваться поиском на карте, необходимо включить свою гео позицию
            и выбрать намерение</Text>
        </View>}
        {coordinatesSearchStore.isIShownOnMap && <>
          <SwitchSelector
            options={intentions}
            initial={intentions.findIndex(intention => coordinatesSearchStore.intention === intention.value)}
            textStyle={styles.switchSelectorText}
            selectedTextStyle={styles.switchSelectorSelectedText}
            style={styles.switchSelector}
            buttonColor="#ffbd21"
            disableValueChangeOnPress={true}
            onPress={onIntentionChange}/>
          <View style={styles.mapContainer}>
            <MapView
              customMapStyle={mapStyle}
              animationEnabled={false}
              showsTraffic={false}
              showsIndoors={false}
              showsIndoorLevelPicker={false}
              mapRef={onMapRef}
              provider={PROVIDER_GOOGLE}
              clusterColor={'#ffbd21'}
              clusterTextColor={'#252525'}
              radius={50}
              onRegionChangeComplete={() => { setCurrentRegionSelectedState(false); }}
              initialRegion={initialRegion}
              style={styles.map}>
              { !isEmpty(geolocationStore.location) && (
                <Marker cluster={false}
                        coordinate={geolocationStore.location}>
                  <FastImage source={locationIcon} style={{ width: 22, height: 22 }}/>
                </Marker>
              ) }
              { coordinatesSearchStore.usersCoordinates.map((item) =>
                <SearchComponentsCustomMarker
                  key={item.id}
                  coordinate={toJS(item)}
                  onPress={onMarkerPressed}
                  isVerified={item.isVerified}
                />

              )}
            </MapView>
          </View>
          <TouchableOpacity
            style={styles.currentPositionButton}
            onPress={() => {
               // goToPushUser();
                showCurrentPosition(false);
                }}>
            <Image source={locationIcon}/>
          </TouchableOpacity>
        </>}
      </SafeAreaView>
    );
  }
));

export default SearchMapScreen;

import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000'
  },
  notShownLabel: {
    fontSize: 14,
    color: '#fff',
    ...fontFamily('fontBaseMedium'),
    lineHeight: 20,
    textAlign: 'center'
  },
  topText: {
    marginBottom: 60,
    marginTop: 40
  },
  bottomText: {
    marginTop: 60
  },
  switchContainer: {
    flexDirection: 'row',
    height: 50,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20
  },
  switchLabel: {
    color: '#fff',
    fontSize: 15,
    ...fontFamily('fontBaseMedium')
  },
  searchLabel: {
    color: '#fff',
    fontSize: 12,
    ...fontFamily('fontBaseMedium')
  },
  switchSelector: {
    marginVertical: 15,
    marginHorizontal: 65
  },
  switchSelectorText: {
    fontSize: 14,
    ...fontFamily('fontTitleBold'),
    color: '#000'
  },
  switchSelectorSelectedText: {
    fontSize: 14,
    ...fontFamily('fontTitleBold'),
    color: '#000'
  },
  mapContainer: {
    width: '100%',
    flex: 1,
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    backgroundColor: '#220',
    overflow: 'hidden'
  },
  map: {
    width: '100%',
    flex: 1
  },
  currentPositionButton: {
    position: 'absolute',
    bottom: 30,
    right: 20,
    backgroundColor: '#fff',
    borderRadius: 16,
    width: 48,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center'
  },
  coordLoadingText: {
    position: 'absolute',
    bottom: 8,
    right: 5,
    color: '#ffbd21'
  },

  markerContainer: {
    borderColor: '#ffbd21',
    backgroundColor: '#000',
    borderWidth: 2,
    width: 57,
    height: 57,
    borderRadius: 40,
    overflow: 'hidden'
  },
  markerPhoto: {
    flex: 1,
    width: 57,
    height: 57,
    borderRadius: 40
  }

})

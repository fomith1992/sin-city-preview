import {Image, Text, View} from 'react-native';
import styles from '../messages/ui/screens/main/style';
import FastImage from 'react-native-fast-image';
import React from 'react';
import {NavigationEvents} from "react-navigation";
var unreadAmount = 0;
import ee from '../../utils/EventEmitter';
import fontFamily from '../../styles/fonts';


window.setTNUnreadAmount = (e) => {
  unreadAmount = e;
  ee.emitEvent("refreshIconState");
 // console.log('EMITEd', unreadAmount);
};
class iconComp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      unreadAmount: 0,
    };
  };
  componentWillFocus() {
    ee.addListener('refreshIconState', () => {
      this.setState({unreadAmount: unreadAmount});
    });

  }
  // setTNUnreadAmount = (e) => {
  //   this.setState({unreadAmount: e});
  // };

  render() {
    const {icon,isMessagesTab, isFocused} = this.props;
    const {unreadAmount} = this.state;
    return (
      <View
      //   style={unreadAmount > 0  && isMessagesTab && !isFocused ? {
      //   backgroundColor: 'red',
      // } : null}
      >
        { unreadAmount > 0  && isMessagesTab  ? (
          <View style={{
            zIndex: 10,
            position: 'absolute',
            right: -5,
            top: -4,
            minWidth: 15,
            height: 15,
            borderRadius: 50,
            backgroundColor: '#e84747',
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 5,
          }}>
            <Text style={{
                //  justifySelf: 'center',
                ...fontFamily('fontTitleBold'),
                lineHeight: 13,
                fontSize: 10,
                // color: '#ffffff',
                color: '#ffffff',
            }}>
              {unreadAmount}
            </Text>
          </View>
        ) : null}



        <NavigationEvents
          onWillFocus={() => this.componentWillFocus()}
        //  onDidFocus={() => this.componentDidFocus()}
       //   onWillBlur={payload => console.log('will blur', payload)}
        //  onDidBlur={() => this.componentDidBlur()}
        />
        <Image source={icon}/>
      </View>
    );
  };
}
export default iconComp;
// export class iconComp = (icon) => {
//   return (
//
//   );
// };


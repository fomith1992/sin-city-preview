import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AuthStack from "../../auth/navigation";
import AppStack from "./app";

const Main = createSwitchNavigator(
  {
    Auth: AuthStack,
    App: AppStack
  },
  {
    initialRouteName: 'Auth',
  }
);

export default createAppContainer(createStackNavigator(
  {
    Main,
  },
  {
    mode: 'card',
    headerMode: 'none',
  }
));
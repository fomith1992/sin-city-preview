import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import TabsNavigator from './tab-navigator';

import * as screenNames from "../screen-names";
import CreateEventScreen from '../../events/ui/screens/new-event';
import EventScreen from '../../events/ui/screens/event';
import DialogScreen from '../../messages/ui/screens/dialog';
import ProfileScreen from '../../user/ui/screens/profile';
import PhotoLikesScreen from '../../user/ui/screens/photo-likes';
import NotificationsSettingsScreen from '../../notifications/ui/screens/settings';
import UserScreensVerificationRequired from '../../user/ui/screens/verification-required';
import UserScreensVerificationFirstStep from '../../user/ui/screens/verification-first-step';
import UserScreensVerificationSecondStep from '../../user/ui/screens/verification-second-step';
import VerificationConfirmationScreen from '../../user/ui/screens/verification-confirmation';
import NotificationsScreen from '../../notifications/ui/screens/index';
import ConnectionsScreen from '../../user/ui/screens/connections';
import FavouritesScreen from '../../user/ui/screens/favourites';
import GetPremiumScreen from '../../user/ui/screens/premium';
import SupportScreen from '../../menu/ui/screens/support';
import BlackListScreen from '../../user/ui/screens/black-list';
import EditProfileScreen from '../../user/ui/screens/edit-profile';
import EventsScreen from '../../events/ui/screens/index';
import EventComponentNotVerified from '../../events/ui/components/not-verified';
import RemoveMyProfileScreen from '../../user/ui/screens/remove-profile';
import MyAccountScreensIndex from "../../my-account/ui/screens/index";
import PdfViewer from "../../ud-ui/screens/pdf-View";
import MyAccountComponentsAvatarRemoveConfirmation from '../../my-account/ui/components/avatar-remove-confirmation';
import UserScreensPaymentRequiredScreen from '../../user/ui/screens/payment-required';
import UserScreensInfoFieldsRequired from '../../user/ui/screens/info-fields-required';

export default createStackNavigator({
  [screenNames.TABS_FLOW]: TabsNavigator,

  [screenNames.CREATE_EVENT_TAB]: { screen: CreateEventScreen },
  [screenNames.EVENT_SCREEN]: { screen: EventScreen },
  [screenNames.EVENT_NOT_VERIFIED]: { screen: EventComponentNotVerified },
  [screenNames.EVENTS_TAB]: { screen: EventsScreen },

  [screenNames.MESSAGES_DIALOG_SCREEN]: { screen: DialogScreen },

  [screenNames.PROFILE_SCREEN]: { screen: ProfileScreen },
  [screenNames.REMOVE_PROFILE_SCREEN]: { screen: RemoveMyProfileScreen },
  [screenNames.USERS_SCREENS_PAYMENT_REQUIRED_SCREEN]: { screen: UserScreensPaymentRequiredScreen },
  [screenNames.USERS_SCREENS_INFO_FIELDS_REQUIRED_SCREEN]: { screen: UserScreensInfoFieldsRequired },
  [screenNames.EDIT_PROFILE_SCREEN]: {
    screen: EditProfileScreen,
    navigationOptions: () => ({
      gestureResponseDistance: {
        horizontal: -1,
        vertical: -1,
      },
    }),
  },
  [screenNames.PROFILE_PHOTO_LIKES_LIST_SCREEN]: { screen: PhotoLikesScreen },
  //[screenNames.CONNECTIONS_SCREEN]: { screen: ConnectionsScreen },
  [screenNames.FAVOURITES_SCREEN]: { screen: FavouritesScreen },

  [screenNames.MENU_NOTIFICATIONS_SETTINGS]: { screen: NotificationsSettingsScreen },
  [screenNames.SUPPORT_SCREEN]: { screen: SupportScreen },
  [screenNames.NOTIFICATIONS_SCREEN]: { screen: NotificationsScreen },

  [screenNames.PROFILE_PHOTO_LIKES_LIST_SCREEN]: { screen: PhotoLikesScreen },
  //[screenNames.CONNECTIONS_SCREEN]: { screen: ConnectionsScreen },
  [screenNames.FAVOURITES_SCREEN]: { screen: FavouritesScreen },
  [screenNames.VERIFICATION_CONFIRMATION_SCREEN]: { screen: VerificationConfirmationScreen },
  [screenNames.VERIFICATION_FIRST_STEP_SCREEN]: { screen: UserScreensVerificationFirstStep},
  [screenNames.VERIFICATION_SECOND_STEP_SCREEN]: { screen: UserScreensVerificationSecondStep},
  [screenNames.BLACK_LIST_SCREEN]: { screen: BlackListScreen },
  [screenNames.GET_PREMIUM_SCREEN]: { screen: GetPremiumScreen },
  [screenNames.VERIFICATION_REQUIRED]: { screen: UserScreensVerificationRequired },
  [screenNames.AVATAR_REMOVE_CONFIRMATION]: { screen: MyAccountComponentsAvatarRemoveConfirmation },

  [screenNames.MY_PROFILE_SCREEN]: { screen: MyAccountScreensIndex },
  [screenNames.PDF_VIEWER]: { screen: PdfViewer },
},{
  initialRouteName: screenNames.TABS_FLOW,
  mode: 'card',
  headerMode: 'none',

});

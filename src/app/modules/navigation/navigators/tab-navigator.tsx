import React from 'react';
import { Image, View, Alert } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';

import * as screenNames from '../screen-names';
import SearchScreen from '../../search/ui/screens/search';
import SearchMapScreen from '../../search/ui/screens/map-search';
import MessagesScreen from '../../messages/ui/screens/main';
import EventsScreen from '../../events/ui/screens/index';
import MenuScreen from '../../menu/ui/screens/index';
import icons from './icons';
import FilterModal from '../../layout/ui/screens/filter';
import IconComp from '../iconComp';
import ConnectionsScreen from "../../user/ui/screens/connections";



const searchStack = createStackNavigator({
  [screenNames.SEARCH_TAB]: { screen: SearchScreen },
  ['FILTER_MODAL']: { screen: FilterModal },
}, {
  mode: 'modal',
  headerMode: 'none',
});

const mapSearchStack = createStackNavigator({
  [screenNames.LOCATION_TAB]: { screen: SearchMapScreen },
  ['FILTER_MODAL']: { screen: FilterModal },
}, {
  mode: 'modal',
  headerMode: 'none',
});

export default (createBottomTabNavigator as any)({
  [screenNames.SEARCH_TAB]: searchStack,
  [screenNames.LOCATION_TAB]: mapSearchStack,
  [screenNames.MESSAGES_TAB]: { screen: MessagesScreen },
  [screenNames.CONNECTIONS_SCREEN]: {screen: ConnectionsScreen},
  //[screenNames.EVENTS_TAB]: { screen: EventsScreen },
  [screenNames.MENU_TAB]: { screen: MenuScreen },
}, {
  initialRouteName: screenNames.SEARCH_TAB,
  //initialRouteName: screenNames.MESSAGES_TAB,
  tabBarOptions: {
    style: {
      height: 70,
      backgroundColor: '#000',
      borderTopColor: '#000'
    },
    showLabel: false,
    tabStyle: {
      paddingTop: 35,
      paddingBottom: 25,
      marginHorizontal: 15,
    },
    keyboardHidesTabBar: false,
  },
  resetOnBlur: false,

  defaultNavigationOptions: ({ navigation }: any) => ({

    tabBarIcon: ({ focused }: any) => {
      const { routeName } = navigation.state;
      let icon;
      if (routeName === screenNames.SEARCH_TAB) {
        icon = focused ? icons.firstOn : icons.firstOff;
      } else if (routeName === screenNames.LOCATION_TAB) {
        icon = focused ? icons.secondOn : icons.secondOff;
      } else if (routeName === screenNames.EVENTS_TAB) {
        icon = focused ? icons.thirdOn : icons.thirdOff;
      } else if (routeName === screenNames.MESSAGES_TAB) {
        icon = focused ? icons.fourthOn : icons.fourthOff;
      } else if (routeName === screenNames.MENU_TAB) {
        icon = focused ? icons.fifthOn : icons.fifthOff;
      } else if (routeName === screenNames.CONNECTIONS_SCREEN) {
        icon = focused ? icons.sixOn : icons.sixOff;
      }

      // if (!(routeName === screenNames.MESSAGES_TAB && unreadAmount > 0) ) {
      //   return <Image source={icon}/>
      // } else {
      //   return <View style={{
      //     backgroundColor: 'red',
      //   }}>
      //     <Image source={icon}/>
      //   </View>
      // }
     // return <Image source={icon}/>
     // return iconComp(icon)
      return (
        <IconComp
         icon={icon}
         isMessagesTab={routeName === screenNames.MESSAGES_TAB}
         isFocused={focused}
        />
      )
    }
  }),
})

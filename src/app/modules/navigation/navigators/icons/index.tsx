export default {
  firstOn: require('./icon_tab_1_on.png'),
  firstOff: require('./icon_tab_1_off.png'),
  secondOn: require('./icon_tab_2_on.png'),
  secondOff: require('./icon_tab_2_off.png'),
  thirdOn: require('./icon_tab_3_on.png'),
  thirdOff: require('./icon_tab_3_off.png'),
  fourthOn: require('./icon_tab_4_on.png'),
  fourthOff: require('./icon_tab_4_off.png'),
  fifthOn: require('./icon_tab_5_on.png'),
  fifthOff: require('./icon_tab_5_off.png'),
  sixOn: require('./icon_tab_6_on.png'),
  sixOff: require('./icon_tab_6_off.png')
}

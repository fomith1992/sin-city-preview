import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  eventInfoBlock: {
    borderBottomColor: '#ffffff33',
    borderBottomWidth: 1,
    paddingRight: 40,
    paddingLeft: 20,
    paddingVertical: 20
  },
  ownerContainer: {
    flexDirection: 'column',
    paddingTop: 20,
    paddingBottom: 15,
    minHeight: 0
  },
  ownerText: {
    ...fontFamily('fontTitleBold'),
    fontSize: 24,
    color: '#fff',
  }
});
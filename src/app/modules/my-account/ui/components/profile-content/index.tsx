import React  from 'react';
import { Image, Text, View } from "react-native";

import { User } from "../../../../user/domain/interfaces/User";
import styles from "./style";
import icons from "../../../../icons";

const MyAccountComponentsProfileContent = ({ user }: { user?: User }) => {
  return (
    <View style={[styles.eventInfoBlock, styles.ownerContainer]}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        {user && user.isVerified && <Image source={icons.verified} style={{ marginRight: 10 }}/>}
        <Text style={styles.ownerText}>{user && user.login}</Text>
      </View>
    </View>
  );
};

export default MyAccountComponentsProfileContent;
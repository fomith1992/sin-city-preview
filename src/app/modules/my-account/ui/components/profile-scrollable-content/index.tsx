import React  from 'react';
import { View, Text } from 'react-native';
import styles from "./style";
import { Profile } from "../../../../user/domain/interfaces/Profile";
import icons from "../../../../icons";
import * as moment from "moment";
import UserComponentsInfoRow from "../../../../user/ui/components/info-row";
import UserComponentsGenderInfoBlock from "../../../../user/ui/components/gender-info-block";
import { User } from "../../../../user/domain/interfaces/User";
import { GendersEnum } from "../../../../user/domain/enums/Gender";
import { FamilyStatusEnum } from "../../../../user/domain/enums/FamilyStatus";
import { PurposeDating } from "../../../../user/domain/enums/PurposeDating";
import { PurposeGenderEnum } from "../../../../user/domain/enums/PurposeGender";

type props = {
  user: User
  profiles: Profile[]
};

const MyAccountComponentsProfileScrollableContent = ({ user, profiles }: props) => {
  const { familyStatus, hasChildren, description } = user;
  const { purposeDating, purposeGender, interestingAgeMin, interestingAgeMax } = user;
  const city: any = user.city;

  const gender = profiles.map((profile: Profile) => {
    return `${GendersEnum[profile.gender]} (${moment.utc().diff(moment.utc(profile.birthday), 'years')})`
  }).join(' & ');

  const infoRows = [
    { id: 1, text: city && city.title ? `г. ${city.title}` : `Не указано`, icon: icons.locationGrey },
    { id: 2, text: gender, icon: icons.genderGrey },
    { id: 3, text: familyStatus ? FamilyStatusEnum[familyStatus] : 'Не указано', icon: icons.marriageGrey },
    { id: 4, text: hasChildren != null ? hasChildren ? 'Дети есть' : 'Детей нет' : 'Не указано', icon: icons.kidsGrey },
    { id: 5, text: purposeGender ? `${PurposeGenderEnum[profiles.length === 1 ? purposeGender : 'p' + purposeGender]} (${interestingAgeMin} - ${interestingAgeMax})` : '', icon: icons.genderGrey },
    { id: 6, text: purposeDating ? PurposeDating[purposeDating] : 'Не указано', icon: icons.target }
  ];

  const showDescription = description != null && description !== '';
  return (
    <>
      <View style={styles.eventInfoBlock}>
        {infoRows.map((item: any, index: number) => {
          return (
            <UserComponentsInfoRow
              text={item.text}
              icon={item.icon}
              key={item.id}/>
          );
        })}
      </View>
      {showDescription &&
      <View style={styles.eventInfoBlock}>
        <Text style={[styles.caption, { marginBottom: 12 }]}>О себе</Text>
        <Text style={[styles.primary, { lineHeight: 24 }]}>{description}</Text>
      </View>}
      {profiles.map((profile: Profile, index: number, profiles: Profile[]) => {
        const isLast = profiles.length - 1 === index;
        return (
          <UserComponentsGenderInfoBlock
            key={profile.id}
            profile={profile}
            isLast={isLast}/>
        );
      })}
    </>
  );
};

export default MyAccountComponentsProfileScrollableContent;

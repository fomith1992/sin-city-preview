import {Dimensions, StyleSheet} from 'react-native';

import fontFamily from '../../../../../styles/fonts';

const isMediumScreen = Dimensions.get('window').height < 700;
const isSmallScreen = Dimensions.get('window').height < 600;

export default StyleSheet.create({
  backgroundImage: {
    backgroundColor: '#000',
    width: '100%',
    height: '100%'
  },
  safeArea: {
    zIndex: 2,
    flex: 1,
    width: '100%'
  },
  backButton: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  remove: {
    color: '#e84747',
    fontSize: 16,
    ...fontFamily('fontTitleBold')
  },
  header: {
    height: 70,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  removeContainer: {
    height: 50,
    paddingHorizontal: 10,
    justifyContent: 'center'
  },
  description: {
    color: '#fff',
    fontSize: isSmallScreen ? 18 : 24,
    ...fontFamily('fontTitleBold'),
    textAlign: 'center',
    marginTop: 40,
    marginHorizontal: 20
  },
  secondaryDescription: {
    color: '#fff',
    fontSize: isSmallScreen ? 16 : 19,
    lineHeight: 19,
    ...fontFamily('fontTitleBold'),
    textAlign: 'center',
    marginTop: 30,
    marginHorizontal: 20
  },
  cancelButton: {
    marginTop: isSmallScreen ? 51 : 91,
    marginHorizontal: 10,
  },
  applyButton: {
    marginTop: 10,
    marginHorizontal: 10,
    marginBottom: 30,
  },
  changePhotoImage: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: isSmallScreen ? 0 : 25,
    height: isSmallScreen ? 130 : 150,
    resizeMode: 'contain',

  }
})

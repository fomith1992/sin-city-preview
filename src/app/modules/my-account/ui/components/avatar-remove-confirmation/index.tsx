import React, { useCallback } from 'react';
import { ImageBackground, View, SafeAreaView, Image, Text, TouchableOpacity, ScrollView } from "react-native";
import { inject } from 'mobx-react';

import styles from './style';
import icons from '../../../../icons';
import UDButton from '../../../../ud-ui/components/ud-button';

import { CurrentUserStore } from "../../../../auth/store/current-user";

interface props {
  navigation: any,
  currentUserStore: CurrentUserStore
}

const RemoveAvatarScreen = inject('currentUserStore')(({ navigation, currentUserStore }: props) => {
  const { state } = navigation;
  const onPress = () => {
    window.fLogEventObj('myprofile_clickDeletePhotoApply', {});

    if (state.params.event === 'SET') {
      currentUserStore.setPhotoAsAvatarById(state.params.photoIndex);
    } else {
      currentUserStore.deleteAvatar();
    }

    navigation.goBack();
  };
  const cancel = useCallback(() => { navigation.goBack(); }, []);

  return (
    <ImageBackground source={icons.rain} style={styles.backgroundImage}>
      <SafeAreaView style={styles.safeArea}>
        <ScrollView>
          <View style={styles.header}>
            <TouchableOpacity onPress={cancel}
                              style={styles.backButton}>
              <Image source={icons.navBack} resizeMode='cover'/>
            </TouchableOpacity>
          </View>
          <Image source={icons.changeMainPhoto} style={styles.changePhotoImage}/>
          <Text style={styles.description}>{state.params.event === 'SET' ?
              'После замены аватара требуется повторно подтвердить аккаунт'
              : `При удалении аватара вы потеряете статус "Подтвержденный".`}</Text>
          <Text style={styles.secondaryDescription}>{`Продолжить?`}</Text>
          <UDButton style={styles.cancelButton} title="Нет" onPress={cancel}/>
          <UDButton style={styles.applyButton} title="Да" onPress={onPress} isApply/>
        </ScrollView>
      </SafeAreaView>
    </ImageBackground>
  );
})

export default RemoveAvatarScreen;

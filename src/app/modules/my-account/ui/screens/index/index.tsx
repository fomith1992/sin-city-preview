import React, { useState, useEffect, useCallback, useRef } from 'react';
import { ActivityIndicator, Dimensions } from 'react-native';
import { inject, observer } from 'mobx-react';

import UserComponentsPhotosViewer from '../../../../user/ui/components/photos-viewer';

import {ITYPES, TYPES} from "../../../../../store/store-types";
import { CurrentUserStore } from "../../../../auth/store/current-user";
import UserComponentsProfileScreenWrapper from "../../../../user/ui/components/profile-screen-wrapper";
import UsersComponentsProfileHeader from "../../../../user/ui/components/profile-header";
import UsersComponentsPhotoActionButtons from "../../../../user/ui/components/photo-action-buttons";
import MyAccountComponentsProfileContent from "../../components/profile-content";
import MyAccountComponentsProfileScrollableContent from "../../components/profile-scrollable-content";
import { Photo } from "../../../../upload/domain/interfaces/Photo";
import * as screenNames from "../../../../navigation/screen-names";
import Navigation from "react-navigation-mobx-helpers/dist";
import { EditUserStore } from "../../../../user/store/edit-user";
import LayoutStore, { ModalMessage } from '../../../../layout/store/LayoutStore';
import {verificationStatus} from "../../../../user/domain/enums/VerificationStatus";

const wW = Dimensions.get('window').width;

type props = {
  currentUserStore: CurrentUserStore,
  editUserStore: EditUserStore,
  navigationStore: Navigation,
  layoutStore: LayoutStore,
  verificationStore: ITYPES['VerificationStore']
};

const MyAccountScreensIndex =
  inject(TYPES.CurrentUserStore, TYPES.Navigation, TYPES.EditUserStore, TYPES.Layout, TYPES.VerificationStore)
  (observer(({ currentUserStore, navigationStore, editUserStore, layoutStore, verificationStore }: props) => {
    const imagesScrollRef = useRef(null);
      const { user, profiles, isProfileSyncing, profileCarouselPhotos, profileInfo, shownPhotoIndex } = currentUserStore;

      const [isPhotosMode, setPhotosMode] = useState(false);

      useEffect(() => {
        currentUserStore.syncCurrentUser();
        verificationStore.checkVerification();
      }, []);

      const onPhotoIndexChange = useCallback((index) => {
        currentUserStore.shownPhotoIndex = index;
      }, [shownPhotoIndex]);

      const onLikePhotoPressed = useCallback((photoId: number) => {
        window.fLogEventObj('myprofile_clickLikeUsers', {});
        navigationStore.navigate(screenNames.PROFILE_PHOTO_LIKES_LIST_SCREEN, { photoIndex: photoId });
      }, []);

      const onSetAvatarPressed = useCallback((photoId: number) => {
        window.fLogEventObj('myprofile_clickSetAvatar', {});
        if (verificationStore.verificationStatus === verificationStatus.ready && false) {
          navigationStore.navigate(screenNames.AVATAR_REMOVE_CONFIRMATION, { photoIndex: photoId, event: 'SET' });
        } else {
          currentUserStore.setPhotoAsAvatarById(photoId)
        }

      }, []);

      const onClosePhotoViewerPressed = useCallback((imageIndex) => {
        // setTimeout(() => {
        //   imagesScrollRef.current?.getNode().scrollTo({
        //     x: wW * imageIndex,
        //     animated: false
        //   });
        // }, 0);
        setPhotosMode(false);
      }, []);

      const onPhotoPressed = useCallback(() => {
        window.fLogEventObj('myprofile_openPhoto', {});
        profileCarouselPhotos.length > 0 && setPhotosMode(true);
      }, [profileCarouselPhotos]);

      const onDeletePhotoPressed = useCallback((photo: Photo) => {
        window.fLogEventObj('myprofile_clickDeletePhoto', {});
        if (photo.isAvatar && verificationStore.verificationStatus === verificationStatus.ready && false) {
          navigationStore.navigate(screenNames.AVATAR_REMOVE_CONFIRMATION, { photoId: photo.id, event: 'DELETE' });
        } else {
          currentUserStore.deletePhoto(photo);
        }
      }, []);

      const onEditAccountPressed = useCallback(() => {
        window.fLogEventObj('myprofile_clickEditProfile', {});
        editUserStore.editCurrentAccount();
      }, []);

      const onBackPressed = useCallback(() => {
        navigationStore.pop();
      }, []);

      const onShowWhoLikedMePressed = useCallback(() => {
        navigationStore.navigate(screenNames.CONNECTIONS_SCREEN);
      }, []);

      const onPhotoSelected = useCallback(() => {
        window.fLogEventObj('myprofile_clickAddPhoto', {});
        currentUserStore.choicePhoto();
      }, []);

      if (isProfileSyncing || !user || !profiles || !profileInfo) {
        return (
          <ActivityIndicator
            size="large"
            style={{ flex: 1, backgroundColor: '#000' }}/>
        )
      }

      if (isPhotosMode && profileCarouselPhotos.length) {
        return <UserComponentsPhotosViewer
                images={profileCarouselPhotos}
                onLikePress={onLikePhotoPressed}
                onSetAvatarPress={onSetAvatarPressed}
                onClose={onClosePhotoViewerPressed}
                shownImageIndex={shownPhotoIndex}
                onDelete={onDeletePhotoPressed}/>
      }

      return (
        <UserComponentsProfileScreenWrapper
          header={<UsersComponentsProfileHeader
            photosCount={profileCarouselPhotos.length}
            shownPhotoIndex={shownPhotoIndex}
            onBackPressed={onBackPressed}
            isMyProfile={true}
            onEditPressed={onEditAccountPressed}/>}
          photos={profileCarouselPhotos}
          onPhotoIndexChange={onPhotoIndexChange}
          actionButtons={<UsersComponentsPhotoActionButtons
            onPhotoSelect={onPhotoSelected}/>}
          contentComponent={<MyAccountComponentsProfileContent user={user}/>}
          imagesScroll={imagesScrollRef}
          scrollabelContent={
            <MyAccountComponentsProfileScrollableContent
              user={user}
              profiles={profiles}/>
          }
          onPhotoPress={onPhotoPressed}/>
      );
    }
  ));

export default MyAccountScreensIndex;

import { EventsResponse } from './../domain/interfaces/Response';
import { EventCategoryNames } from './../domain/interfaces/EventCategoryNames';
import { EventCategory } from './../domain/interfaces/EventCategory';
import { EventFilterEnum } from './../domain/enums/Filters';
import { CurrentUserStore } from './../../auth/store/current-user';
import NavigationStore from 'react-navigation-mobx-helpers';
import { TYPES, ITYPES } from '../../../store/store-types';
import { BaseStore } from './../../../store/BaseStore';
import { showMessage } from 'react-native-flash-message';
import eventRepository from '../domain/repositories/EventRepository'
import { computed, action, observable } from 'mobx';
import { EventFactory } from '../domain/factories/EventFactory';
import { EventCategoryFactory } from '../domain/factories/EventCategoryFactory';
import Uploader from '../../upload/Uploader';
import { Event } from '../domain/interfaces/Event'
import { ImagePickerResponse } from 'react-native-image-picker';
import * as screenNames from '../../navigation/screen-names';
import UsersSearchStore from '../../search/store/UsersSearchStore';
import LayoutStore from '../../layout/store/LayoutStore';
import NewMeetingEventValidator from "../domain/validators/NewMeetingEventValidator";
import NewPartyMeetingEventValidator from "../domain/validators/NewPartyEventValidator";
import moment, { utc } from 'moment';
import userRepository from "../../user/domain/repositories/UserRepository";
import * as _ from 'lodash';
import {showModalAndTakePhoto} from "../../core/domain/services/MediaTakingService";

const eventCategories: EventCategoryNames[] = ['ALL', 'CREATED_BY_ME', 'MY_PARTICIPATION', 'EXPIRED'];

const errorMessages = require('./error-messages.json');

export class EventStore extends BaseStore {
  private photo: ImagePickerResponse;

  @observable filterKey: EventCategoryNames = 'ALL';
  @observable eventsCategories: Record<EventCategoryNames, EventCategory> = null;

  @observable event: Event = EventFactory.create();
  @observable isEditing: boolean = false;
  private eventsWasLoadedById: number = -1;

  private navigation: NavigationStore;
  private searchStore: UsersSearchStore;
  private currentUser: CurrentUserStore;
  private layout: LayoutStore;
  public authUser: ITYPES['AuthUser'];
  private profileManager: ITYPES['ProfileManager'];

  storeInit() {
    this.navigation = this.root.resolve(TYPES.Navigation);
    this.searchStore = this.root.resolve('usersSearchStore')
    this.currentUser = this.root.resolve(TYPES.CurrentUserStore);
    this.layout = this.root.resolve(TYPES.Layout);
    this.authUser = this.root.resolve(TYPES.AuthUser);
    this.eventsCategories = EventCategoryFactory.create(this.authUser.userId);
    this.profileManager = this.root.resolve(TYPES.ProfileManager);
  }

  @computed get isMyEvent() {
    return this.event.author?.id === this.authUser.userId;
  }

  @computed get canEdit(): boolean {
    const date = this.event.date != null;
    if (date != null) {
      const isBefore = moment.utc(this.event.date).isAfter(moment.utc());
      return this.isMyEvent && isBefore;
    }
    return false;
  }

  @computed get iCanAssign() {
    const id = this.authUser.userId;
    const isMyEvent = this.event.author && this.event.author.id === id;
    const isParty = this.event.type === 'PARTY';
    const amIAlreadyAssigned = this.event.participants.findIndex((participant) => participant.id === id) !== -1;
    const isNotExpired = moment(this.event.date).isAfter() && this.filterKey !== 'EXPIRED';
    return !isMyEvent && isParty && !amIAlreadyAssigned && isNotExpired;
  }

  @computed public get getEventList(): Event[] {
    return this.eventsCategories[this.filterKey]?.results;
  }

  @computed
  public get isRefreshing() {
    return this.eventsCategories[this.filterKey]?.isLoading;
  }

  @computed get isUserCanCreateParty() {
   // return this.currentUser.user.isVerified && this.currentUser.user.isPayed;
    return true;
  }

  @computed get isUserVerified() {
    return this.currentUser.user.isVerified;
  }

  @computed get isUserPayed() {
    return this.currentUser.user.isPayed;
  }

  public substractTimezone(date: Date) {
    return moment(date).add(new Date().getTimezoneOffset(), 'm').toDate()
  }

  public removeTimezone(date: Date): moment.Moment {
    return moment(date).utcOffset(-new Date().getTimezoneOffset(), false);
  }

  @computed
  private get amICanNotEditMyParty() {
    const isParty = this.event.type === 'PARTY';
    const isMyEvent = this.event.author.id === this.authUser.userId;
    return isParty && isMyEvent && !this.isUserVerified;
  }

  @action
  doAction(action: 'Редактировать' | 'Удалить') {
    if (action === 'Редактировать') {
      if (this.amICanNotEditMyParty) {
        window.fLogEvent('app_errorScreen', 'Необходима верификация.');
        showMessage({ message: 'Необходима верификация.', type: 'danger' });
        this.navigation.navigate(screenNames.VERIFICATION_REQUIRED);
      } else {
        this.event.date = this.substractTimezone(this.event.date);
        this.isEditing = true;
        this.navigation.navigate(screenNames.CREATE_EVENT_TAB);
      }
    }
    else if (action === 'Удалить') {
      this.remove();
    }
  }

  private userAccess(id: number){
    this.profileManager.openProfile(id);
  }

  public openAuthorProfile() {
    if (this.event.author) {
      this.userAccess(this.event.author.id);
    }
  }

  public openUserProfile(userId: number) {
    if (userId === this.authUser.userId) {
      return this.navigation.navigate(screenNames.MY_PROFILE_SCREEN);
    }
    this.userAccess(userId);
  }

  public choicePhoto(){
    this.layout.showOverflowLoader(false);
     return showModalAndTakePhoto()
      .then((response: any) => {
        this.event.photo = {
          photoUrl: response.uri,
          photoThumbnailUrl: response.uri
        };
        this.setPhoto(response);
        return response;
      })
      .catch(() => {})
      .finally(() => this.layout.hideOverflowLoader())
  }

  @action
  public async openCreateEvent() {
    const user = await this.currentUser.syncCurrentUserInfo();
    //defeature
    if (user?.isPayed || true) {
      this.photo = null;
      this.event = EventFactory.create();
      this.isEditing = false;
      this.navigation.navigate(screenNames.CREATE_EVENT_TAB);
    } else {
      this.navigation.navigate(screenNames.USERS_SCREENS_PAYMENT_REQUIRED_SCREEN);
    }
    //
    // this.photo = null;
    // this.event = EventFactory.create();
    // this.isEditing = false;
    // this.navigation.navigate(screenNames.CREATE_EVENT_TAB);
  }

  @action
  public openEvent(eventId: number, type: Event['type']) {
    window.fLogEventObj('events_openEvent', {event_id: eventId});
    if (type === 'MEETING' && !(this.isUserVerified || this.isUserPayed) && false) {
      this.navigation.navigate(screenNames.VERIFICATION_REQUIRED);
    } else {
      this.layout.showOverflowLoader();
      this.isEditing = false;
      eventRepository.loadById(eventId)
        .then((response: any) => {
          this.event = response;
          this.navigation.navigate(screenNames.EVENT_SCREEN)
        })
        .finally(() => {
          this.layout.hideOverflowLoader();
        })
    }
  }

  @action
  public assign = () => {
    this.layout.showOverflowLoader();
    eventRepository.assignToEvent(this.event.id as number)
      .then((res: any) => {
      //  window.fLogEvent('app_errorScreen', 'Успешно.');
        showMessage({ message: 'Успешно', type: 'success' });
        this.event.participants.push(this.currentUser.user as any );
        this.event.maxCountParticipants = Number(this.event.maxCountParticipants) + 1;
      })
      .catch((e: any) => {
        console.log(e);
      })
      .finally(() => {
        this.layout.hideOverflowLoader();
      })
  }


  @action
  public remove = () => {
    this.layout.showOverflowLoader();
    eventRepository.delete(this.event)
      .then((res: any) => {
        (Object.keys(this.eventsCategories) as EventCategoryNames[]).forEach((key) => {
          this.eventsCategories[key].results = this.eventsCategories[key].results.filter(event => event.id !== this.event.id)
        });
        this.navigation.pop();
      })
      .catch((e: any) => {
        console.log(e);
      })
      .finally(() => {
        this.layout.hideOverflowLoader();
      });
  };

  @action
  public setPhoto(photo: ImagePickerResponse) {
    this.photo = photo;
    this.event.photo = photo as any;
  }

  @action
  public setFilterIndex(index: number) {
    this.filterKey = Object.keys(EventFilterEnum)[index] as any;
  }

  @action
  loadAllEvents() {
    if (this.eventsWasLoadedById !== this.authUser.userId) {
      this.eventsCategories.CREATED_BY_ME.options.author = this.authUser.userId;
      this.eventsCategories = EventCategoryFactory.create(this.authUser.userId)
      eventCategories.forEach((type) => {
        this.loadEventByType(type);
      });
      this.eventsWasLoadedById = this.authUser.userId;
    }
  }

  @action
  public onEndReached() {
    this.loadEventByType(this.filterKey);
  }

  @action
  public onRefresh() {
    this.refreshEventByType(this.filterKey);
  }

  @action
  private async loadEventByType(type: EventCategoryNames, options : {isRefresh: boolean} = {isRefresh: false}) {
    const event = this.eventsCategories[type];
    if (event.options.page && !event.isLoading) {
      event.isLoading = true;
      try {
        const { results, next }: EventsResponse = await eventRepository.load(event.options);
        event.options.page = Boolean(next) ? event.options.page + 1 : 0;
        const comparator = (a: any, b: any) => a.id === b.id;
        event.results = _.uniqWith([...options.isRefresh ? [] : event.results, ...results], comparator);
      } catch(e) {
      } finally {
        event.isLoading = false;
      }
    }
  }

  @action
  private refreshEventByType(type: EventCategoryNames) {
    const event = this.eventsCategories[type];
    event.options.page = 1;
    this.loadEventByType(type, {isRefresh: true});
    event.isLoading = false;
  }

  @action
  updateEvent() {
    if (this.errors) {
      window.fLogEvent('app_errorScreen', this.errors.join('\n'));
      showMessage({ message: this.errors.join('\n'), type: 'danger', duration: 6000 });
    } else {
      this.layout.showOverflowLoader();

      const formdata = this.prepareFormDate();
      const object = this.prepareObject();
      window.fLogEventObj('addEvent_saveForm',
        {author_id: this.authUser.userId, fields: object});
      eventRepository.saveEvent(formdata)
        .then(this.handleUpdateResponse)
        .catch((e) => this.handleUpdateError(e))
        .finally(() => { this.layout.hideOverflowLoader(); })
    }
  }

  @action
  private handleUpdateResponse = (event: Event & { photoUrl: string }) => {
    if (!event.photo?.photoUrl && event.photoUrl) {
      Object.assign(event, {
        photo: {
          photoUrl: event.photoUrl,
          photoThumbnailUrl: event.photoUrl
        }
      });
    };

    event.date = new Date(Date.parse(event.date as any));

    const createdEvents = this.eventsCategories?.CREATED_BY_ME;

    this.event = {...EventFactory.create(), ...event};

    if (this.event.id && createdEvents) {
      const eventIndex = createdEvents.results.findIndex(event => event.id === this.event.id);
      if (eventIndex > -1) {
        createdEvents.results[eventIndex] = event;
        createdEvents.results = [...createdEvents.results]
      } else {
        createdEvents.results = [...createdEvents.results, event];
      }
    } else {
      createdEvents.results = [...createdEvents.results, event];
    }
    this.photo = null;
    this.navigation.pop();
  };

  private handleUpdateError = (e: any) => {
    console.log('e', e);
    let message = '';
    Object.keys(e).forEach((key) => {
      message += `${key}: ${e[key][0]}\n`;
    });
    window.fLogEvent('app_errorScreen', message);
    showMessage({ message: message, type: 'danger', duration: 6000 });
  };

  private prepareFormDate(): FormData {
    const event = { ...this.event };

    const formdata = this.photo ? Uploader.prepareFormData(this.photo) : new FormData();
    (Object.keys(event) as (keyof Event)[]).forEach((key) => {
      let value = event[key];
      if (key === 'date') {
        value = utc(new Date(event.date).valueOf() - new Date().getTimezoneOffset() * 60000).toISOString()
      } else if (key === 'photo') {
        if (event.photo?.photoUrl) { return; }
        else { value = ''; }
      } else if (key === 'city' && typeof event.city !== 'number' && event.city?.id) {
        value = event.city.id;
      } else if (typeof event[key] !== 'string') {
        value = JSON.stringify(event[key])
      }
      formdata.append(key, value);

    });
    return formdata;
  }

private prepareObject(): FormData {
  const event = { ...this.event };

  const formdata = this.photo ? Uploader.prepareFormData(this.photo) : new FormData();
 // let obj = [];
  let obj = {};
  (Object.keys(event) as (keyof Event)[]).forEach((key) => {
    let value = event[key];
    if (key === 'date') {
      value = utc(new Date(event.date).valueOf() - new Date().getTimezoneOffset() * 60000).toISOString()
    } else if (key === 'photo') {
      if (event.photo?.photoUrl) { return; }
      else { value = ''; }
    } else if (key === 'city' && typeof event.city !== 'number' && event.city?.id) {
      value = event.city.id;
    } else if (typeof event[key] !== 'string') {
      value = JSON.stringify(event[key])
    }
    formdata.append(key, value);
  //  obj[obj.length] = {key, value};
  //  obj.push({d: 2});
    if (key === 'type' || key === 'typeMeeting' || key === 'date'
      || key === 'title' || key === 'city' || key === 'address' || key === 'description') {
      obj = {
        ...obj,
        [key]: value,
      }
    }

  });
  return obj;
}

  @computed
  public get errors(): any {
    let validator = this.event.type === 'MEETING' ? NewMeetingEventValidator : NewPartyMeetingEventValidator;
    const errors = validator.validate(this.event, errorMessages);
    if (errors) {
      return Object.keys(errors).reduce((result, key) => {
        result.push(...errors[key]);
        return result;
      }, []);
    }
  }
}

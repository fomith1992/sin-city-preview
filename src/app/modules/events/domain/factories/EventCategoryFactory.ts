import { EventCategoryNames } from './../interfaces/EventCategoryNames';
import { EventCategory } from './../interfaces/EventCategory';

export class EventCategoryFactory {
  constructor() {}
  public static create(author: number): Record<EventCategoryNames, EventCategory> {
    const defaultCategoryOption: Record<EventCategoryNames, EventCategory['options']> = {
      'ALL': { page: 1 }, 
      'CREATED_BY_ME': { author, page: 1 }, 
      'MY_PARTICIPATION': { my_participation: true, page: 1 },
      'EXPIRED': { expired: true, page: 1 }
    };
    return {
      'ALL': EventCategoryFactory.createDefaultCategory(defaultCategoryOption.ALL), 
      'CREATED_BY_ME': EventCategoryFactory.createDefaultCategory(defaultCategoryOption.CREATED_BY_ME), 
      'MY_PARTICIPATION': EventCategoryFactory.createDefaultCategory(defaultCategoryOption.MY_PARTICIPATION),
      'EXPIRED': EventCategoryFactory.createDefaultCategory(defaultCategoryOption.EXPIRED),
    };
  }

  private static createDefaultCategory(options: EventCategory['options']): EventCategory{
    return {
      isLoading: false,
      options,
      results: []
    }
  }
}
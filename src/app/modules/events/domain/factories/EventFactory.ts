import { Event } from '../interfaces/Event'

export class EventFactory {
  constructor() {}
  public static create():Event {
    return {
      title: '',
      description: '',
      date: new Date(),
      address: '',
      city: undefined,
      type: 'MEETING',
      isParticipant: true,
      participants: [],
      typeMeeting: 'MEETING',
      displayDate: '',
    };
  }
}
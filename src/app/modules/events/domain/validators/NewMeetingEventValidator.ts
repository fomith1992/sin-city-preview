import { Validator } from "../../../validation/Validator";
import { Validators } from "../../../validation/Validators";

class NewEventValidator extends Validator {
  protected schema = {
    title: [Validators.required, Validators.maxLength(35)],
    description: [Validators.required, Validators.maxLength(1000)],
    date: [Validators.required],
    city: [Validators.required],
    type: [Validators.required],
    photo: [Validators.required]
  };
}

const newEventValidator = new NewEventValidator();
export default newEventValidator;
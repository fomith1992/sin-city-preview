import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../core/infrastructure/httpResource";

class EventResource extends BaseRestResource {
    constructor() {
        super(httpResource, 'events', {});
    }
}

const eventResource = new EventResource();

export default eventResource
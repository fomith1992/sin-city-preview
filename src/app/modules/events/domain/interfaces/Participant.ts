import { Photo } from './Photo';

export interface IParticipant {
  id: number,
  login: string,
  photo?: Photo,
  profiles: Profile[],
  lastActivity: string,
  online: boolean
}

interface Profile {
  id: number,
  age: number,
  gender: "W" | "M"
}

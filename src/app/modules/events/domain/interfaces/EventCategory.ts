import { Event } from './Event';

export type EventCategory = {
  options: {
    author?: number
    my_participation?: boolean
    page: number
    expired?: boolean
  }
  isLoading: boolean
  results: Event[]
}
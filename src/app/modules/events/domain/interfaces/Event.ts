import { Photo } from './Photo';
import { IParticipant } from './Participant';

export interface Event {
  id?: number
  title?: string
  description?: string
  date: Date
  displayDate: string
  address?:string
  city?: {
    title: string
    id: number
  } | number
  type: 'PARTY' | 'MEETING'
  typeMeeting?: 'MEETING' | 'SEX'
  maxCountParticipants?: number|string,
  photo?: Photo
  author?: IParticipant
  isParticipant: boolean,
  participants: IParticipant[]
}
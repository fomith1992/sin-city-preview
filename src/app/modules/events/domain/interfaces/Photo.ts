export interface Photo {
  photoUrl: string
  photoThumbnailUrl: string
}
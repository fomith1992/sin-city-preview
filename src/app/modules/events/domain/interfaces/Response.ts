import { Event } from './Event';

export interface EventsResponse {
  results: Event[],
  next: string
}
export const EventFilterEnum = {
  'ALL': 'Все события',
  'CREATED_BY_ME': 'Созданные мной',
  'MY_PARTICIPATION': 'Мое участие',
  'EXPIRED': 'Завершенные события'
}
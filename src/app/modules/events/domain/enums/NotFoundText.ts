export const NotFoundText = {
  'ALL': 'События не найдено',
  'CREATED_BY_ME': 'Вы еще не создали ни одного события',
  'MY_PARTICIPATION': 'Вы еще не участвуете ни в одном событии',
  'EXPIRED': 'События не найдены'
};
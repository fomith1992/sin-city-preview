import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import eventResource from "../resources/EventResource";

class EventRepository extends BaseRepository {
  constructor() {
    super(eventResource);
  }

  public assignToEvent(eventId: number) {
    return this.handleResponse(this.resource(eventId, 'participate').create());
  }

  private getFormdataFieldValue(data: any, key: string): any {
    return data.getParts().find((item: any) => item.fieldName === key);
  }

  public saveEvent(data: any) {
    const id = this.getFormdataFieldValue(data, 'id');
    if (id) {
      return this.handleResponse(this.resource(id.string).update(data, { contentType: 'form-data' }));
    } else {
      return this.handleResponse(this.resource().create(data, { contentType: 'form-data' }));
    }
  }

  private handleResponse(promise: Promise<any>): Promise<any> {
    return promise.then((res: any) => this.processResponse(res));
  }
}

const eventRepository = new EventRepository();

export default eventRepository;
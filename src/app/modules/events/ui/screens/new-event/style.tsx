import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  safeArea: {
    flex: 1,
    width: '100%',
    backgroundColor: '#000',
    paddingBottom: 40 
  },
  scrollView: {
    paddingTop: 15,
    paddingLeft: 20 
  },
  photoContainer: {
    width: '100%',
    height: 160,
    paddingRight: 20,
    marginTop: 10 
  },
  photoDelete: {
    position: "absolute",
    zIndex: 2,
    top: 20,
    right: 40
  },
  photo: {
    width: '100%',
    height: '100%',
    borderRadius: 20,
    flexDirection: 'row'
  },
  descriptionContainer: {
    marginTop: 13,
    paddingBottom: 15,
    flexDirection: 'row' 
  },
  primaryText: {
    color: '#fff',
    fontSize: 16,
    ...fontFamily('fontBaseMedium')
  },
  pickerInput: {
    color: '#fff',
    fontSize: 16,
    ...fontFamily('fontBaseMedium')
  },
  marker: {
    height: 19,
    width: 19,
    backgroundColor: '#fff'
  },
  viewContainer: { 
    flex: 1, 
    justifyContent: 'center',
    paddingLeft: 7
  },
  description: { 
    flex: 1, 
    marginLeft: 12, 
    marginRight: 20, 
    fontSize: 14, 
    lineHeight: 24,
    maxHeight: 227
  },
  photoAdd: {
    flex: 1,
    marginTop: 10,
    paddingVertical: 12,
    backgroundColor: '#252525',
    borderRadius: 18,
    alignItems: 'center',
    marginRight: 20
  },
  saveButtonContainer: {
    paddingVertical: 30,
    paddingHorizontal: 40,
    borderTopColor: '#333',
    borderTopWidth: 1
  },
  multilineTextInputInvalid: {
    color: '#ff2020'
  },
  multilineTextInputCounterInvalid: {
    color: '#ff2020'
  },
  multilineTextInputCounter: {
    position: 'absolute',
    bottom: -26,
    right: 20,
    color: '#999',
    fontSize: 9,
    textAlign: 'right',
    marginRight: 40,
    marginBottom: 13
  },
})
import React, { useCallback, useState, useRef } from 'react';
import {
  SafeAreaView,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  Platform,
  Keyboard
} from 'react-native';
import { inject, observer } from 'mobx-react';
import RNPickerSelect from 'react-native-picker-select';
import  moment, { utc } from 'moment';
import { ImagePickerResponse } from 'react-native-image-picker';
import { showMessage } from 'react-native-flash-message';

import icons from '../../../../icons';
import styles from './style';
import pickerStyles from '../../../../../styles/picker';
import Header from '../../../../layout/ui/components/header';
import ListItem from '../../../../auth/ui/components/profile-list-item';
import UDButton from '../../../../ud-ui/components/ud-button';
import UDDatetimePicker from '../../../../ud-ui/components/ud-datetime-picker';
import { City } from '../../../../dictionaries/domain/interfaces/City';
import { TYPES, ITYPES } from '../../../../../store/store-types';
import { useFocusEffect } from "react-navigation-hooks";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const eventTypes = [
  { label: 'Встреча', value: 'MEETING' },
  { label: 'Вечеринка', value: 'PARTY' },
];

const meetingTypes = [
  { label: 'Свидание', value: 'MEETING' },
  { label: 'Секс', value: 'SEX' },
];

interface props {
  navigation: any
  eventStore: ITYPES['EventStore']
  dictionariesStore: ITYPES['Dictionaries']
}

const CreateEventScreen = inject(TYPES.EventStore, TYPES.Dictionaries)(observer(
  ({ navigation, eventStore, dictionariesStore }: props) => {
    const { event } = eventStore;

    const [photo, setPhoto] = useState();
    const cities = useRef(dictionariesStore.cities);
    const [isDatetimePickerVisible, setDatetimePickerVisibility] = useState(false);
    React.useEffect(() => {
      window.fLogEventObj('addevent_openForm', {author_id: eventStore.authUser.userId});
      if (event.photo) {
        setPhoto({ uri: event.photo.photoUrl });
      }
    }, []);

    const removePhotoPressed = useCallback(() => {
      setPhoto(null);
      eventStore.setPhoto(null);
    }, []);

    const choicePhoto = useCallback(() => {
      eventStore.choicePhoto()
        .then(res => {  setPhoto(res);});
    }, []);

    const onDatetimePickerConfirm = useCallback((date) => {
      setDatetimePickerVisibility(false);
      if (Platform.OS === 'android' && moment(date).isBefore(moment())) {
        window.fLogEvent('app_errorScreen',
          'Нельзя выбрать прошедшее время');
        showMessage({ message: 'Нельзя выбрать прошедшее время', type: 'danger' });
      } else {
        Object.assign(event, { date });
      }
    }, []);

    const onSaveEventPressed = useCallback(() => {
      eventStore.updateEvent();
    }, []);

    const onDescriptionChange = useCallback((description) => {
      Object.assign(event, { description });
    }, []);

    const onAddressChange = useCallback((address) => {
      Object.assign(event, { address });
    }, []);

    const onCityChange = useCallback((city) => {
      Object.assign(event, { city });
    }, []);

    const onMeetingTypeChange = useCallback((typeMeeting) => {
      Object.assign(event, { typeMeeting });
    }, []);

    const onTypeChange = useCallback((type) => {
      Object.assign(event, { type });
    }, []);

    const onTitleChange = useCallback((title) => {
      if (title.length <= 35) {
        Object.assign(event, { title });
      }
    }, []);

    const onMaxCountChange = useCallback((maxCountParticipants) => {
      Object.assign(event, { maxCountParticipants });
    }, []);

    const onBackPressed = useCallback(() => {
      navigation.pop();
    }, []);

    const displayedDate = eventStore.removeTimezone(event.date).format('DD.MM.YYYY HH:mm');

    const onScreenFocus = useCallback(() => {
      return () => Keyboard.dismiss()
    }, []);

    useFocusEffect(onScreenFocus);

    const descriptionCounterStyles = [styles.multilineTextInputCounter] as any;
    const descriptionStyles = [styles.primaryText, styles.description] as any;

    if (event.description.length > 1000) {
      descriptionCounterStyles.push(styles.multilineTextInputCounterInvalid);
      descriptionStyles.push(styles.multilineTextInputInvalid);
    }

    return (<SafeAreaView style={styles.safeArea}>
      <Header title="Событие" onBack={onBackPressed}/>
      <KeyboardAwareScrollView
        keyboardOpeningTime={1}
        extraScrollHeight={20}
        enableResetScrollToCoords={false}
        keyboardShouldPersistTaps="never"
        contentContainerStyle={styles.scrollView}
        bounces={false}>
        <ListItem title="Тип события">
          <Image source={icons.eventType}/>
          <RNPickerSelect
            disabled={eventStore.isEditing}
            placeholder={{}}
            value={event.type}
            style={pickerStyles}
            onValueChange={onTypeChange}
            items={eventStore.isUserCanCreateParty ? eventTypes : [eventTypes[0]]}/>
        </ListItem>
        {event.type === 'MEETING' &&
        <ListItem title="Тип встречи">
          <Image source={icons.eventType}/>
          <RNPickerSelect
            placeholder={{}}
            value={event.typeMeeting}
            style={pickerStyles}
            onValueChange={onMeetingTypeChange}
            items={meetingTypes}/>
        </ListItem>
        }
        <ListItem title="Дата и время встречи">
          <Image source={icons.date}/>
          <TouchableOpacity style={{ flex: 1, marginLeft: 10 }}
                            onPress={() => {
                              setDatetimePickerVisibility(true);
                            }}>
            <Text style={styles.primaryText}>{displayedDate}</Text>
          </TouchableOpacity>
          <UDDatetimePicker isVisible={isDatetimePickerVisible}
                            onConfirm={onDatetimePickerConfirm}
                            date={event.date}
                            minimumDate={new Date()}
                            mode='datetime'
                            onCancel={() => {
                              setDatetimePickerVisibility(false);
                            }}/>
        </ListItem>
        <ListItem title="Название">
          <TextInput
            value={event.title}
            style={[styles.primaryText, { flex: 1 }]}
            onChangeText={onTitleChange}/>
        </ListItem>
          {event.type === 'PARTY' && <ListItem title="Количество участников">
          <Image source={icons.maxCountParticipants}/>
          <TextInput
            keyboardType="numeric"
            value={String(event.maxCountParticipants || '')}
            style={[styles.primaryText, { flex: 1, marginLeft: 10 }]}
            onChangeText={onMaxCountChange}/>
        </ListItem>}
        <ListItem title="Город">
          <Image source={icons.location}/>
          <RNPickerSelect
            placeholder={{ label: 'Выбор города...' }}
            value={typeof event.city === 'number' ? event.city : event.city?.id}
            style={pickerStyles}
            onValueChange={onCityChange}
            items={cities.current.map((city: City) => ({ label: city.title, value: city.id }))}/>
        </ListItem>
        <ListItem title="Адрес">
          <Image source={icons.location}/>
          <TextInput
            value={event.address}
            style={[styles.primaryText, { flex: 1, marginLeft: 10 }]}
            onChangeText={onAddressChange}/>
        </ListItem>
        <ListItem title="Добавить фотографию" withoutBorder>
          {!Boolean(photo) && <TouchableOpacity style={styles.photoAdd} onPress={choicePhoto}>
            <Image source={icons.eventAddPhoto}/>
          </TouchableOpacity>}
          {Boolean(photo) &&
          <View style={styles.photoContainer}>
            <TouchableOpacity style={styles.photoDelete}
                              onPress={removePhotoPressed}>
              <Image source={icons.deletePhoto} style={{}}/>
            </TouchableOpacity>
            <Image
              source={{ uri: photo.uri }}
              style={styles.photo}
              resizeMode="cover"/>
          </View>}
        </ListItem>

        <ListItem withoutBorder={true} title="Описание">
          <View style={{ alignSelf: 'flex-start', marginTop: 13 }}>
            <Image source={icons.info} style={{ marginRight: 12 }}/>
          </View>
          <View style={{ flexDirection: 'row', paddingRight: 30 }}>
            <TextInput value={event.description}
                       multiline
                       onChangeText={onDescriptionChange}
                       style={descriptionStyles}/>
            <Text style={descriptionCounterStyles}>
              {`${ event.description ? event.description.length : 0}/1000`}
            </Text>
          </View>
        </ListItem>
        <View style={styles.saveButtonContainer}>
          <UDButton title="Сохранить" onPress={onSaveEventPressed}/>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>);
  }));

export default CreateEventScreen;

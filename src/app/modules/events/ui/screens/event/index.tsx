import React, { useCallback } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import { inject, observer } from 'mobx-react';
import ActionSheet from 'react-native-action-sheet';

import icons from '../../../../icons';
import styles from './style';
import UDButton from '../../../../ud-ui/components/ud-button';
import Header from '../../../../layout/ui/components/header';
import AnimatedPattern from '../../components/animated-view-pattern';
import { Event } from '../../../domain/interfaces/Event';
import Participant from '../../components/participant';
import { IParticipant } from '../../../domain/interfaces/Participant';
import { EventType } from '../../../domain/enums/EventType';
import { getProfileInfoString } from '../../../../ud-ui/helpers/genderInfo';

import { EventStore } from '../../../store/EventStore';

const Buttons = ({ onAssignPress }: any) => {
  return (
    <UDButton onPress={onAssignPress}
              icon={icons.takePart}
              title="Участвую"
              style={styles.assignButton}/>
  );
};

const ContentComponent = (props: Event & { onPress?: any, isMyEvent: boolean }) => {
  const { isMyEvent, type, author, onPress } = props;
  return (<>
    {(author && !isMyEvent) &&
    <View style={[styles.eventInfoBlock, styles.ownerContainer]}>
      <TouchableOpacity onPress={onPress}
                        style={[styles.membersListItem, { borderBottomWidth: 0, paddingVertical: 0 }]}>
        <Image source={author.photo ? { uri: author.photo.photoThumbnailUrl } : icons.profilePlaceholder}
               style={styles.avatar}/>
        <View style={styles.infoContainer}>
          <Text style={styles.primary}>{`${author.login}, ${!!author.profiles ? getProfileInfoString(author.profiles) : ''}`}</Text>
          <Text style={styles.secondary}>
            { type === 'MEETING' ? 'автор встречи' : 'автор вечеринки' }
          </Text>
        </View>
      </TouchableOpacity>
    </View>}
    {isMyEvent && <EventInfoBlock {...props} />}
  </>);
};

const EventInfoBlock = ({ type, title, city, address, typeMeeting, displayDate, maxCountParticipants }: Event) => {
  return (
    <View style={styles.eventInfoBlock}>
      <Text style={styles.title}>{title}</Text>
      <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
        <Image style={{ marginRight: 8 }} source={icons.dateGrey}/>
        <Text style={styles.primary}>{displayDate}</Text>
      </View>
      <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
        <Image style={{ marginRight: 8 }} source={icons.locationGrey}/>
        <Text style={styles.primary}>{`г. ${(city as any).title}, ${address}`}</Text>
      </View>
        { type === "PARTY" &&
            <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
                <Image style={{ marginRight: 8 }} source={icons.maxCountParticipantsGray} />
                <Text style={styles.primary}>Участников: {maxCountParticipants}</Text>
            </View>
        }
        {type === 'MEETING' &&
        <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
            <Image style={{ marginRight: 8 }} source={icons.meetingType} />
            <Text style={styles.primary}>
                { typeMeeting === 'MEETING' ? 'Свидание' : 'Секс' }
            </Text>
        </View>}
    </View>
  );
}

const DescriptionBlock = ({ description }: Event) => {
  return (
    <View style={styles.eventInfoBlock}>
      <Text style={[styles.caption, { marginBottom: 12 }]}>Описание</Text>
      <Text style={[styles.primary, { lineHeight: 24 }]}>{description}</Text>
    </View>
  );
}

const ParticipantsBlock = ({ participants, type, onPress }: Event & { participants: IParticipant[], onPress: any }) => {
  return (
    <View style={{marginHorizontal: 20}}>
      <Text
          style={[styles.caption, {marginBottom: 12, marginTop: 15}]}>{`Участники (${participants.length})`}</Text>
      {participants.map((participant, index) =>
          <Participant key={index} onPress={onPress} {...participant} />
      )}
    </View>
  );
};

const ScrollabelContent = (props: Event & { onPress: (id: number) => void, isMyEvent: boolean }) => {
  const { type, isMyEvent } = props;
  return (<>
    {!isMyEvent && <EventInfoBlock {...props} />}
    <DescriptionBlock {...props} />
    { type === "PARTY" && <ParticipantsBlock {...props}/>}
  </>);
}

const EventsComponentEvent = inject('eventStore')(observer(
  ({ navigation, eventStore }: { eventStore: EventStore, navigation: any }) => {

    const onBackPress = React.useCallback(() => {
      navigation.pop();
    }, []);

    const onAssignPress = React.useCallback(() => {
      eventStore.assign();
      window.fLogEventObj('events_clickRunEvent',
        {event_id: eventStore.event.id, event_type: eventStore.event.type});
    }, []);

    const onParticipantPress = React.useCallback((userId) => {
      window.fLogEventObj('events_clickMember', {member_id: userId});
      eventStore.openUserProfile(userId);
    }, []);
    const onAuthorPress = React.useCallback(() => {
      window.fLogEventObj('events_clickAuthor', {event_author_id: eventStore.event.author.id});
      eventStore.openAuthorProfile();
    }, []);

    const onActionPressed = useCallback(() => {
      const options: any[] = ['Редактировать', 'Отмена'];
      ActionSheet.showActionSheetWithOptions({
        options,
        destructiveButtonIndex: 0,
        cancelButtonIndex: 1,
      }, (buttonIndex) => {
        eventStore.doAction(options[buttonIndex]);
      })
    }, []);

    return (
      <AnimatedPattern
        logEventScroll={() => {
          window.fLogEventObj('events_eventDetail',
            {event_id: eventStore.event.id, event_type: eventStore.event.type})
        }}
        photo={eventStore.event.photo && eventStore.event.photo.photoUrl}
        participantsCount={eventStore.event.participants?.length || 0}
        header={<Header title={EventType[eventStore.event.type]}
                        withoutBorder
                        onBack={onBackPress}
                        icon={icons.dots}
                        onPress={eventStore.canEdit ? onActionPressed : null}/>}
        actionButtons={eventStore.iCanAssign ? <Buttons onAssignPress={onAssignPress}/> : null}
        scrollabelContent={
          <ScrollabelContent
          {...eventStore.event}
          isMyEvent={eventStore.isMyEvent}
          onPress={onParticipantPress}/>}
        contentComponent={
          <ContentComponent
            isMyEvent={eventStore.isMyEvent}
            onPress={onAuthorPress}
            {...eventStore.event}/>}
        />
    );
  }
));

export default EventsComponentEvent;

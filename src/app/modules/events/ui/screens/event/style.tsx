import { StyleSheet, Dimensions } from 'react-native';

import fontFamily from '../../../../../styles/fonts';
const wH = Dimensions.get('window').height;

export default StyleSheet.create({
  avatar: { 
    width: 50, 
    height: 50, 
    borderRadius: 50 
  },
  infoContainer: { 
    marginLeft: 14, 
    justifyContent: 'space-between',
    paddingVertical: 3 
  },
  container: {
    flex: 1,
    backgroundColor: '#000',
    justifyContent: 'space-between'
  },
  eventPhoto: {
    ...StyleSheet.absoluteFillObject,
    height: wH * .48,
    width: '100%',
    resizeMode: 'contain',
  },
  eventInfoContainer: {
    minHeight: wH * .5,
    backgroundColor: '#000',
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    paddingTop: 15,
  },
  eventInfoBlock: {
    borderBottomColor: '#ffffff33',
    borderBottomWidth: 1,
    paddingRight: 40,
    paddingLeft: 20,
    paddingVertical: 20,
    minHeight: 100,
  },
  title: {
    color: '#fff',
    fontSize: 19,
    ...fontFamily('fontTitleBold')
  },
  primary: {
    color: '#fff',
    fontSize: 14,
    ...fontFamily('fontBaseMedium')
  },
  secondary: {
    color: '#999999',
    fontSize: 12,
    ...fontFamily('fontBaseMedium')
  },
  caption: {
    color: '#ffbd21',
    fontSize: 14,
    ...fontFamily('fontTitleBold')
  },
  membersListItem: { 
    flexDirection: 'row', 
    paddingVertical: 15, 
    borderBottomColor: '#ffffff33', 
    borderBottomWidth: 1 
  },
  assignButton: { 
    padding: 15 
  },
  ownerContainer: { 
    flexDirection: 'row', 
    paddingVertical: 0, 
    paddingBottom: 10, 
    minHeight: 0  
  }
});
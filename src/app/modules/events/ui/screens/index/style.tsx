import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#000'
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  eventContainer: {
    maxHeight: 40,
    marginVertical: 20
  },
  eventType: {
    borderRadius: 12,
    marginRight: 12,
    paddingHorizontal: 20,
    paddingVertical: 10,
    justifyContent: 'center',
  },
  notFoundText: {
    color: '#fff',
    fontSize: 14,
    lineHeight: 22,
    textAlign: 'center',
    ...fontFamily('fontBaseMedium'),
    marginBottom: 10
  }
})
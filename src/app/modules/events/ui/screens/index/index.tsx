import React, { useEffect, useCallback } from 'react';
import { FlatList, RefreshControl, View, SafeAreaView, Text, ScrollView } from 'react-native';
import { inject, observer } from 'mobx-react';
import { TYPES } from '../../../../../store/store-types';
import { useIsFocused } from 'react-navigation-hooks';

import icons from '../../../../icons';
import styles from './style';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import EventsItem from '../../components/events-item';
import { Event } from '../../../domain/interfaces/Event';
import { EventFilterEnum } from '../../../domain/enums/Filters';
import { NotFoundText } from '../../../domain/enums/NotFoundText'
import { Viewport } from '@skele/components';
import { EventStore } from '../../../store/EventStore';
import FastImage from "react-native-fast-image";
import { NavigationScreenProp, NavigationState } from "react-navigation";

const notFound = require('./img/picEventsEmpty.png');

const NotFound = ({ text }: { text: string }) => {
  return (
    <View style={{ flex: 1, justifyContent: 'center' }}>
      <Text style={styles.notFoundText}>{text}</Text>
      <FastImage source={notFound}
                 style={{ height: '70%' }}
                 resizeMode='contain'/>
    </View>
  );
};


interface props {
  navigation: NavigationScreenProp<NavigationState>,
  eventStore: EventStore
}

const EventsMainScreen = inject(TYPES.EventStore)(observer(
  ({navigation, eventStore }: props) => {
    const isFocused = useIsFocused();
    useEffect(() => { isFocused && eventStore.loadAllEvents(); }, [isFocused]);
    const onFilterChange = useCallback((filterIndex: number) => {
      const eventsArray = ['all_events', 'created_me', 'i_member', 'events_end'];
      window.fLogEventObj('events_openList', {events_list: eventsArray[filterIndex]});
      eventStore.setFilterIndex(filterIndex);
    }, []);
    const goBack = useCallback(() => {
      navigation.goBack();
    }, []);
    const onBackPressed = useCallback(() => {
      goBack();
    }, []);
    const onCreateEvent = useCallback(() => {
      eventStore.openCreateEvent();
      window.fLogEventObj('events_clickAddEvent', {});
      }, []);
    const onEventPress = useCallback((eventId, type) => { eventStore.openEvent(eventId, type); }, []);
    const ViewportAwareView = Viewport.Aware(View);
    const renderItem = ({ item }: { item: Event }) => (<ViewportAwareView
      onViewportEnter={() => window.fLogEventObj('events_hitEvent', {event_list: eventStore.filterKey, event_id: item.id})}
    ><EventsItem {...item} onPress={onEventPress}/></ViewportAwareView>);
    const onEndReached = useCallback(() => { eventStore.onEndReached(); }, []);
    const onRefresh = useCallback(() => { eventStore.onRefresh(); }, []);

    return (

      <SafeAreaView style={styles.safeArea}>
        <Header title="События"
                icon={icons.navPlus}
                onPress={onCreateEvent}
                onBack={onBackPressed}
        />
        <ScrollView horizontal
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 20 }}
                    style={styles.eventContainer}
                    showsHorizontalScrollIndicator={false}>
          {Object.values(EventFilterEnum).map((filter, index) => {
            const isNotSelected = index !== Object.keys(EventFilterEnum).indexOf(eventStore.filterKey);
            return <UDButton
                      key={index}
                      title={filter}
                      onPress={() => { onFilterChange(index); }}
                      style={[styles.eventType, isNotSelected ? { backgroundColor: '#fff' } : null]}/>
          }

          )}
        </ScrollView>
        <View style={styles.container}>
          <Viewport.Tracker>
          <FlatList data={eventStore.getEventList}
                    windowSize={50}
                    refreshControl={
                      <RefreshControl refreshing={eventStore.isRefreshing}
                                      onRefresh={onRefresh} />
                    }
                    contentContainerStyle={{ flexGrow: 1 }}
                    onEndReached={onEndReached}
                    ListEmptyComponent={eventStore.isRefreshing ? null : <NotFound text={NotFoundText[eventStore.filterKey]}/>}
                    keyExtractor={(item) => `${item.id}`}
                    style={{ width: '100%', flex: 1 }}
                    renderItem={renderItem}/>
          </Viewport.Tracker>
        </View>
      </SafeAreaView>

    );
  }
))

export default EventsMainScreen;

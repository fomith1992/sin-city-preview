import React, { memo, useCallback } from 'react';
import { TouchableOpacity, Image, Text, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import moment from 'moment';

import icons from '../../../../icons';
import styles from './style';
import { IParticipant } from '../../../domain/interfaces/Participant';
import { getProfileInfoString } from '../../../../ud-ui/helpers/genderInfo';

const EventsComponentParticipant = memo(({ id, login, photo, profiles, lastActivity, onPress, online }: IParticipant & { onPress: any }) => {
  const onParticipantPress = useCallback(() => { onPress(id) }, [id]);
  console.log('profiles', profiles)
  return (
    <TouchableOpacity onPress={onParticipantPress} style={styles.container}>
      <FastImage source={photo ? { uri: photo.photoThumbnailUrl } : icons.profilePlaceholder}
                 style={styles.avatar}/>
      <View style={styles.infoContainer}>
        <Text style={styles.primary} ellipsizeMode='tail' numberOfLines={1}>{`${login}, ${!!profiles ? getProfileInfoString(profiles) : ''}` }</Text>
        <Text style={[styles.secondary, online && {color: '#ffbd21'}]}>
          { online ? 'online' : moment.utc(lastActivity).fromNow()}
        </Text>
      </View>
    </TouchableOpacity>
  );
})
export default EventsComponentParticipant;

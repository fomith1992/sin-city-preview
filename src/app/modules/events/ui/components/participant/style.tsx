import { StyleSheet, Dimensions } from 'react-native';

import fontFamily from '../../../../../styles/fonts';
const wW = Dimensions.get('window').width;

export default StyleSheet.create({
  avatar: { 
    width: 50, 
    height: 50, 
    borderRadius: 50 
  },
  primary: {
    maxWidth: wW * .65,
    color: '#fff',
    fontSize: 14,
    ...fontFamily('fontBaseMedium')
  },
  secondary: {
    color: '#999999',
    fontSize: 12,
    ...fontFamily('fontBaseMedium')
  },
  infoContainer: { 
    marginLeft: 14, 
    justifyContent: 'space-between',
    paddingVertical: 3 
  },
  container: { 
    flexDirection: 'row', 
    paddingVertical: 15, 
    borderBottomColor: '#ffffff33', 
    borderBottomWidth: 1
  }
});
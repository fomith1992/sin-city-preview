import React, { useCallback } from 'react';
import { ImageBackground, SafeAreaView, Text } from 'react-native';

import * as screenNames from '../../../../navigation/screen-names';
import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';

const EventsComponentNotVerified = ({ navigation }: { navigation: any }) => {
  const onBack = useCallback(() => { navigation.pop(); }, []);
  const onVerifyPress = useCallback(() => { navigation.navigate(screenNames.VERIFICATION_FIRST_STEP_SCREEN) }, []);
  return (
    <ImageBackground source={icons.rain} style={styles.backgroundImage}>
      <SafeAreaView style={{ flex: 1 }}>
        <Header title="" onBack={onBack} withoutBorder/>
        <Text style={styles.label}>Участвовать в мероприятиях могут только верицифированные пользователи</Text>
        <UDButton style={styles.verifyButton} onPress={onVerifyPress} title="Верифицироваться сейчас"/>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default EventsComponentNotVerified;
import React, { memo } from 'react';
import { ActivityIndicator, View, Text, Image, TouchableOpacity } from 'react-native';

import styles from './style';
import icons from '../../../../icons';
import { Event } from '../../../domain/interfaces/Event';
import { EventType } from '../../../domain/enums/EventType';
import { getProfileInfoString } from '../../../../ud-ui/helpers/genderInfo';
import UDFastImageLoader from '../../../../ud-ui/components/ud-fast-image-loader';
import { Viewport } from '@skele/components'

const BackgroundImage = memo((props: any) => {
  if (props.photo) {
    return <UDFastImageLoader source={{ uri: props.photo.photoThumbnailUrl }} {...props} />
  } else {
    return <Image source={icons.eventPhotoPlaceholder} resizeMode={props.resizeMode} style={{ width: '100%', height: '100%' }} />
  }
});

const EventsItem = memo(({ id, title, author, type, date, photo, onPress, displayDate }: Event & { onPress: any }) => {
  return (

    <TouchableOpacity style={styles.container} onPress={() => { onPress(id, type); }}>
      <View style={styles.backgroundImageContainer}>
        <BackgroundImage photo={photo}
                         indicatorStyle={{ top: -180 }}
                         style={styles.backgroundImage}
                         resizeMode="cover"/>
        <ActivityIndicator />
      </View>

      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text style={[styles.date, styles.textShadow]}>{displayDate}</Text>
        {author &&
          <View style={{ flexDirection: 'row' }}>
            <Image source={icons.username} style={{ marginRight: 4 }}/>
            <Text style={[styles.composition, styles.textShadow]}>{getProfileInfoString(author.profiles)}</Text>
          </View>}
      </View>
      <View>
        <Text style={[styles.type, styles.textShadow]}>{EventType[type]}</Text>
        <Text style={[styles.title, styles.textShadow]}>{title}</Text>
      </View>
    </TouchableOpacity>

  );
});

export default EventsItem;

import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    backgroundColor: '#455',
    borderRadius: 20,
    overflow: 'hidden',
    maxHeight: 160,
    marginBottom: 12,
    marginHorizontal: 20,
    paddingVertical: 15,
    paddingHorizontal: 20,
    minHeight: 140,
    justifyContent: 'space-between' 
  },
  backgroundImageContainer: {
    flexDirection: 'row', 
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    zIndex: 0,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  backgroundImage: { 
    position: 'relative', 
    width: '100%', 
    aspectRatio: 1 
  },
  date: {
    color: '#ffbd21',
    fontSize: 12,
    ...fontFamily('fontBaseMedium')
  },
  composition: {
    color: '#fff',
    fontSize: 12,
    ...fontFamily('fontTitleBold')
  },
  type: {
    color: '#fff',
    fontSize: 12,
    ...fontFamily('fontBaseMedium'),
    marginBottom: 10
  },
  title: {
    color: '#fff',
    fontSize: 19,
    ...fontFamily('fontTitleBold'),
    width: '75%',
  },
  textShadow: {
    shadowColor: '#000',
    shadowOffset: { 
      width: 0, 
      height: 0 
    },
    shadowOpacity: 1,
    shadowRadius: 3
  }
})
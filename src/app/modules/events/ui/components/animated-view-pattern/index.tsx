import React, { useState, useCallback } from 'react';
import { SafeAreaView, View, Dimensions, Animated, StyleSheet, Platform } from 'react-native';
import 'react-native-gesture-handler';
import {
  PanGestureHandler,
  State,
  TapGestureHandler,
  NativeViewGestureHandler
} from 'react-native-gesture-handler';
import { getStatusBarHeight, getBottomSpace } from 'react-native-iphone-x-helper';
import * as _ from 'lodash';

import styles from './style';
import icons from '../../../../icons';
import UDFastImageLoader from "../../../../ud-ui/components/ud-fast-image-loader";

interface props {
  header: any
  contentComponent: any
  scrollabelContent: any
  actionButtons: any
  photo?: string
  participantsCount: number
}

const wH = Dimensions.get('window').height;
const wW = Dimensions.get('window').width;
const SNAP_POINTS_FROM_TOP = [
  getStatusBarHeight(true) + (Platform.OS === 'ios' ? 140 : 120 ),
  wH * 0.53
];
let isScrolled = 0;

const ActionButtons = ({ translateY, ActionButtonsComponent }: { translateY: Animated.Animated, ActionButtonsComponent: any }) => {
  const actionButtonsContainerStyles = [
    styles.assignButtonContainer,
    { transform: [{ translateY }] }
  ]
  return (
    <Animated.View style={actionButtonsContainerStyles}>
      { ActionButtonsComponent }
    </Animated.View>
  );
}

const ProfilePattern = ({ header, contentComponent, scrollabelContent, actionButtons, photo, participantsCount, logEventScroll }: props) => {
  const scrolledTrigger = (inner) =>  {
    if (isScrolled > 2) return;
    if (isScrolled === 1 || (inner && isScrolled < 1)) {
      //  console.log('TRIGGER_SCROLLED');
      //   window.fLogEvent('')
         if (!!logEventScroll) logEventScroll();
      if (inner) isScrolled ++;
    }
    isScrolled++;
    // console.log(isScrolled);
  };
  const masterdrawer = React.useRef<any>();
  const drawerheader = React.useRef<any>();
  const scroll = React.useRef<any>();

  const START = React.useRef(SNAP_POINTS_FROM_TOP[0]);
  const END = React.useRef(SNAP_POINTS_FROM_TOP[1]);

  const [lastSnap, setLastSnap] = React.useState(END.current);
  const [scrollabelContentHeight, setScrollableContentHeight] = useState(wH - 100 - SNAP_POINTS_FROM_TOP[0]);

  const onContentLayout = useCallback(({ nativeEvent: { layout } }: any) => {
    isScrolled = 0;
    setScrollableContentHeight(wH - (layout.height + getBottomSpace() +  20));
  }, []);

  const _lastScrollYValue = React.useRef(0);
  const _lastScrollY = React.useRef(new Animated.Value(0));

  _lastScrollY.current.addListener(({ value }) => {
    _lastScrollYValue.current = value;
    scrolledTrigger(false);
  });

  const _dragY = React.useRef(new Animated.Value(0));
  const _onGestureEvent = React.useRef(Animated.event(
    [{ nativeEvent: { translationY: _dragY.current } }],
    { useNativeDriver: true }
  ))

  const _reverseLastScrollY = React.useRef(Animated.multiply(
    new Animated.Value(-1),
    _lastScrollY.current
  ));

  const _translateYOffset = React.useRef(new Animated.Value(END.current));
  const _translateY = React.useRef(Animated.add(
    _translateYOffset.current,
    Animated.add(_dragY.current, _reverseLastScrollY.current)
  ).interpolate({
    inputRange: [START.current, END.current],
    outputRange: [START.current, END.current],
    extrapolate: 'clamp',
  }));

  const _onHeaderHandlerStateChange = React.useCallback(({ nativeEvent }) => {
    if (nativeEvent.oldState === State.BEGAN) {
      _lastScrollY.current.setValue(0);
    }
    _onHandlerStateChange({ nativeEvent });
  }, []);

  const _onHandlerStateChange = React.useCallback(({ nativeEvent }) => {
    if (nativeEvent.oldState === State.ACTIVE) {
      let { velocityY, translationY } = nativeEvent;
      translationY -= _lastScrollYValue.current;
      const dragToss = 0.15;
      const endOffsetY =
        lastSnap + translationY + dragToss * velocityY;

      let destSnapPoint = SNAP_POINTS_FROM_TOP[0];
      for (let i = 0; i < SNAP_POINTS_FROM_TOP.length; i++) {
        const snapPoint = SNAP_POINTS_FROM_TOP[i];
        const distFromSnap = Math.abs(snapPoint - endOffsetY);
        if (distFromSnap < Math.abs(destSnapPoint - endOffsetY)) {
          destSnapPoint = snapPoint;
        }
      }
      setLastSnap(destSnapPoint);
      _translateYOffset.current.extractOffset();
      _translateYOffset.current.setValue(translationY);
      _translateYOffset.current.flattenOffset();
      _dragY.current.setValue(0);
      Animated.spring(_translateYOffset.current, {
        velocity: velocityY,
        tension: 12,
        friction: 50,
        toValue: destSnapPoint,
        useNativeDriver: true,
      }).start();
    }
  }, [lastSnap, _lastScrollYValue.current, _translateYOffset.current, _dragY.current]);

  const androidMargin = Platform.OS === 'android' ? 25 : 0;
  return (
    <TapGestureHandler
      maxDurationMs={100}
      ref={masterdrawer}
      maxDeltaY={lastSnap - SNAP_POINTS_FROM_TOP[0]}>
      <SafeAreaView style={{ flex: 1, backgroundColor: '#000' }} pointerEvents="box-none">
        <View style={styles.photosContainer}>
          <UDFastImageLoader
            style={{ width: wW, height: '100%' }}
            source={photo ? { uri: photo } : icons.eventPhotoPlaceholder}
            resizeMoe="cover"/>
        </View>
        <ActionButtons ActionButtonsComponent={actionButtons} translateY={_translateY.current}/>
        { header }
        <Animated.View
          style={[
            StyleSheet.absoluteFillObject,
            { maxHeight: wH - SNAP_POINTS_FROM_TOP[0] },
            { backgroundColor: '#770'},
            styles.eventInfoContainer,
            {
              transform: [{ translateY: _translateY.current }],
            },
          ]}>
          <View style={styles.slideUpPanelIcon}/>
          <PanGestureHandler
            ref={drawerheader}
            simultaneousHandlers={[scroll, masterdrawer]}
            shouldCancelWhenOutside={false}
            onGestureEvent={_onGestureEvent.current}
            onHandlerStateChange={_onHeaderHandlerStateChange}>
            <Animated.View onLayout={onContentLayout}>
              { contentComponent }
            </Animated.View>
          </PanGestureHandler>
          <Animated.ScrollView
            onScrollEndDrag={() => {
              scrolledTrigger(true);
            }}
            bounces={false}
            style={{ maxHeight: scrollabelContentHeight - lastSnap - androidMargin }}
            contentContainer={{ flexGrow: 1 }}
            >
            { scrollabelContent }
          </Animated.ScrollView>
        </Animated.View>
      </SafeAreaView>
    </TapGestureHandler>
  );
}
export default ProfilePattern;

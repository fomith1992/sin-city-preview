import { showMessage } from 'react-native-flash-message';

import { BaseHttpResource } from "../../../infrastructure/BaseHttpResource";
import httpClient from "./httpClient";

const type = 'danger';
// todo extract from config
// const BASE_URL = 'https://dating.dev.getgain.ru/api/v0';
//export const BASE_URL = 'https://scinfoback.ru/api/v0';

//export const BASE_URL = 'https://sc-dating.truemachine.ru/api/v0';
export const BASE_URL = 'https://sc-dating.truemachine.ru/api/v0';

//export const BASE_URL = 'https://sc-dating-preprod.truemachine.ru/api/v0';

const defaultOptions = {
  timeOffset: true,
  responseType: 'json',
  accessType: 'json',
  contentType: 'json',
  handleError: (e: any) => {
    console.log('HANDLE ERROR ------', e)
    let resultMessage = 'Неизвестная ошибка';
    if (e.status === 500) {
      resultMessage = 'Неизвестная ошибка';
      // todo закомментил для демонстрации релиза
      // showMessage({ message: resultMessage, type: 'warning', duration: 5000 });
      throw resultMessage;
    } else if (isFuncExists(e, 'json')) {
      const promise = e.json();
      if (isFuncExists(promise, 'then')) {
        return promise.then((body: any) => {
          if (body != null) {
            resultMessage = body;
            const { message, detail } = body;
            if ((message || detail) && detail !== "None") {
              console.log('showMess', detail)
              window.fLogEvent('app_errorScreen', message || detail);
              showMessage({
                message: message || detail,
                type,
              });
            }
            throw { message: resultMessage, status: e.status };
          }
          // todo handle
        });
      } else {
        // todo handle
      }
    } else if (isFuncExists(e, 'text')) {
      return e.text().then((message: string) => {
        resultMessage = message;
        window.fLogEvent('app_errorScreen', message);
        console.log('2d IF ____________________________');
        showMessage({ message, type });
        throw resultMessage;
      });
    } else  {
      window.fLogEvent('app_errorScreen', resultMessage);
      console.log('3d_fpOS')
      showMessage({ message: resultMessage, type: 'warning', duration: 6000 });
      throw resultMessage;
    }
  }
};

const httpResource = new BaseHttpResource(BASE_URL, httpClient, defaultOptions as any);

export default httpResource;

// helpers

export function isFuncExists(target: any, funcName: string) {
  return target != null && target[funcName] != null && typeof target[funcName] === 'function';
}

function handleErrorAsJsonPromise(promise: any) {
  return promise.then((body: any) => {
    if (body != null) {
      const { message, detail } = body;
      if (message || detail) {
        window.fLogEvent('app_errorScreen',
          message || detail);
        console.log('er_hnad_2');
        showMessage({
          message: message || detail,
          type,
        });
      }
    } else {
      throw new Error('json error body is empty');
    }
  })
}

function handleErrorAsTextPromise(promise: any) {
  return promise.then((message: string) => {
    if (message != null && message !== '') {
      window.fLogEvent('app_errorScreen',
        message);
      showMessage({ message, type });
      console.log('er_hnad_3');
    } else {
      throw new Error('text error body is empty');
    }
  })
}

function handleUnknownError(e: any): void {
  window.fLogEvent('app_errorScreen',
    'Извините, неизвестная ошибка');
  showMessage({ message: 'Извините, неизвестная ошибка', type: 'info', duration: 6000 })
}

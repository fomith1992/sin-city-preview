import { FetchHttpClient } from "../../../infrastructure/FetchHttpClient";

const httpClient = new FetchHttpClient();

export default httpClient;
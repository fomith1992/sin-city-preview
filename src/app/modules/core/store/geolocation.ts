import { Platform } from 'react-native';
import { BaseStore } from "../../../store/BaseStore";
import { action, computed, observable, toJS } from "mobx";
import coordRepository from "../../search/domain/repositories/CoordinatesRepository";
import Geolocation, { GeoError, GeoOptions, PositionError } from 'react-native-geolocation-service';
import PermissionsServices from "../domain/services/PermissionsService";
import { crashlytics } from 'react-native-firebase'
import { isEmpty } from 'lodash';

export default class GeolocationStore extends BaseStore {
  @observable currentLocation: { latitude: number, longitude: number };

  private watchId: number | null = null;
  private options: GeoOptions = {
    timeout: 2000,
    enableHighAccuracy: Platform.OS === 'ios' ? false : true,
  };

  public get geolocationOptions(): GeoOptions {
    return this.options;
  }
  private randomizeFH() {
    const offset = (Math.random()*500)/111120;
    let signInt;
    if (Math.random() > 0.5) {
      signInt = 1;
    } else {
      signInt = -1;
    }
  //  console.log('RANDOOOOOOOOOOOM ----------', signInt * offset);
      return signInt * offset;
  }
  @action
  private onSuccess(coords: any) {
  //  console.log('geoOnSUCCESS'. coords);
    coords.latitude += this.randomizeFH();
    coords.longitude += this.randomizeFH();
    if (!isEmpty(coords)) {
      coordRepository.updateCoordinate(coords);
      this.currentLocation = coords;
    }
  }

  @computed
  public get location(): any {
  //  this.currentLocation.latitude += 10;
    return {...toJS(this.currentLocation)};
  }

  private onError(error: GeoError) {
    !__DEV__ && crashlytics().recordError(error.code, error.message);
  }

  public async startPositionWatching() {
  //  console.log('startWatching');
    try {
      await PermissionsServices.checkAndRequestLocationPermission();
      this.getInitialCoordinates();
      this.watchId = Geolocation.watchPosition(
        ({ coords }) => this.onSuccess(coords),
        this.onError,
        this.options);
    } catch(e) {}
  }

  public stopPositionWatching() {
   // console.log('stopWatching');
    if (typeof this.watchId === 'number') {
      Geolocation.clearWatch(this.watchId);
    }
  }

  private sleep(ms: number): Promise<any> {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  @action
  public updateCoordinates(): Promise<any> {
   // console.log('UPDATE_COORDS', this.currentLocation);
    return new Promise(async (resolve, reject) => {
      try {
        while(!this.currentLocation) {
          await this.sleep(500);
        }
        resolve(this.currentLocation);
      } catch(error) {
        reject(error);
      }
    });
  }

  private getInitialCoordinates() {
    Geolocation.getCurrentPosition(({ coords }) => this.onSuccess(coords), this.onError, this.options);
  }
}

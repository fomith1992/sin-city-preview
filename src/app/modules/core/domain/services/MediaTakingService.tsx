import { Platform } from 'react-native';
import ImagePicker, { ImagePickerResponse } from 'react-native-image-picker';
import ImageCropPicker from 'react-native-image-crop-picker';

const imagePickerOptions = {
  title: 'Выберите фото',
  takePhotoButtonTitle: 'Сделать фото',
  chooseFromLibraryButtonTitle: 'Выбрать фото из галереи...',
  mediaType: 'photo',
  cameraType: 'front',
  cancelButtonTitle: 'Отменить',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
} as { cameraType: 'back' | 'front' | undefined};

interface response {
  uri: string
  fileName: string
  type: string
}

export const showModalAndTakePhoto = (withCrop: boolean = true): Promise<ImagePickerResponse|response> => {
  return new Promise((resolve, reject) => {
    console.log('showModalAndTakePhoto_return Prom')
    ImagePicker.showImagePicker(imagePickerOptions, async (response: ImagePickerResponse) => {
      console.log('imagePicker_RESPONSE', response);
      if (!response.didCancel) {
        if (response.error) { reject(response.error); }
        else {
          if (withCrop) {
            try {
              const smallerSide = response.width > response.height ? response.height : response.width;
              // const dimensions = Platform.OS === 'ios' ? {
              //   width: response.width,
              //   height: response.height,
              // } : {
              //   width: smallerSide === response.width && response.width !== response.height ? response.height : response.width,
              //   height: smallerSide === response.width && response.width !== response.height ? response.width : response.height,
              // };
              const dimensions = {
                width: smallerSide,
                height: smallerSide
              };
              console.log('smallerSide', smallerSide, response, smallerSide === response.width && response.width !== response.height)
              const croppedImage = await ImageCropPicker.openCropper({
                cropperCancelText: 'Отмена',
                cropperChooseText: 'Выбрать',
                cropperToolbarTitle: '',
                path: response.uri,
                enableRotationGesture: false,
                  ...dimensions
              });
              const data = {
                uri: croppedImage.path,
                fileName: croppedImage.path.split('/').pop(),
                type: croppedImage.mime
              };
              resolve(data);
            } catch(e) {
              reject(e);
            }
          }
          resolve(response as any);
        }
      } else {
        reject(response);
      }
    });
  })
};

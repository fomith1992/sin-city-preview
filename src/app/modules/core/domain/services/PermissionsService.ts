import { Alert, Platform, Linking } from 'react-native';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import { showMessage } from 'react-native-flash-message';

class PermissionService {
  private isChecking: boolean = false;
  private UNAVAILABLE_MESSAGE = 'Служба геопозиции не доступна на вашем устройстве';
  private OPEN_SETTINGS_MESSAGE = 'Разрешите определение вашей геопозиции в системных настройках приложения';
  private REQUEST_ERROR_MESSAGE = 'Ошибка получения доступа к геолокации';
  constructor() {}

  public isOccupied() {
    return this.isChecking;
  }

  public checkAndRequestLocationPermission(): Promise<any> {
    this.isChecking = true;
    return new Promise(async (resolve, reject) => {
      let requestedPermissions = Platform.select({
        android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
      });

      let result;
      try {
        result = await check(requestedPermissions);
      } catch(e) {}
      // if (Platform.OS === 'android' && result === RESULTS.UNAVAILABLE) {
      //   this.showErrorMessage(this.UNAVAILABLE_MESSAGE);
      //   requestedPermissions = Platform.select({
      //     android: PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION,
      //     ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
      //   }) as any;
      //   try {
      //     result = await check(requestedPermissions);
      //   } catch(e) {}
      // }

      if (result === RESULTS.UNAVAILABLE) {
        this.showErrorMessage(this.UNAVAILABLE_MESSAGE);
        reject();
      }

      switch (result) {
        case RESULTS.DENIED:
          try {
            await request(requestedPermissions)
            resolve();
          } catch(e) {
            this.openSettings();
            reject();
          }
          break;
        case RESULTS.GRANTED:
          resolve();
          break;
        case RESULTS.BLOCKED:
          this.openSettings();
          reject();
          break;
        default:
          reject();
          break;
        this.showErrorMessage(this.REQUEST_ERROR_MESSAGE);
      }
    }).finally(() => { this.isChecking = false; });
  }
  private openSettings() {
    Alert.alert(
      'Настройка разрешений',
      this.OPEN_SETTINGS_MESSAGE,
      [
        { text: 'Перейти к настройкам', onPress: () => { Linking.openSettings().catch(() => {}) } },
        { text: 'Отмена', onPress: () => {} },
      ]
    );
  }
  private showErrorMessage(message: string) {
    window.fLogEvent('app_errorScreen',
      message);
    showMessage({
      message,
      type: 'danger'
    })
  }
}

const permissionService = new PermissionService();
export default permissionService;

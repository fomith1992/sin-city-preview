export default {
  manBodyTypes: [
    { label: '', value: 'ECTOMORPH', image: require('./mans/ectomorph.png') },
    { label: '', value: 'MESOMORPH', image: require('./mans/mesomorph.png') },
    { label: '', value: 'ENDOMORPH', image: require('./mans/endomorph.png') },
  ],
  womanBodyTypes: [
    { label: '', value: 'ECTOMORPH', image: require('./womans/ectomorph.png') },
    { label: '', value: 'MESOMORPH', image: require('./womans/mesomorph.png') },
    { label: '', value: 'ENDOMORPH', image: require('./womans/endomorph.png') },
  ]
}
const facialTypes = [
  {
    value: 'NOT',
    label: '',
    image: require('./icon_face_shaved.png'),
  },
  {
    value: 'BRISTLES',
    label: '',
    image: require('./icon_man_bristle.png'),
  },
  {
    value: 'MUSTACHE',
    label: '',
    image: require('./icon_man_moustache.png'),
  },
  {
    value: 'BEARD',
    label: '',
    image: require('./icon_man_beard.png'),
  },
  {
    value: 'MUSTACHE BEARD',
    label: '',
    image: require('./icon_man_beard_moustache.png'),
  },
];

export default facialTypes as FacialType[];

export interface FacialType {
  label: string,
  image: number,
  value: string
}
import React from 'react';
import FastImage from 'react-native-fast-image';
import icons from "./index";

type props = any;

export function Icon({ key, ...otherProps }: props) {
  const iconSource = (icons as any)[key];
  return (
    <FastImage source={iconSource} {...otherProps} />
  );
}
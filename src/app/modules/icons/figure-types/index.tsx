export default {
  manFigureTypeImages: 
  [
    { label: 'Перевернутый треугольник',
      value: 'INVERTED TRIANGLE',
      images: {
        on: require('./mans/icon_man_triangle_on.png'),
        off: require('./mans/icon_man_triangle_off.png')
      }
    },
    { label: 'Прямоугольный',
      value: 'RECTANGLE',
      images: {
        on: require('./mans/icon_man_rectangle_on.png'),
        off: require('./mans/icon_man_rectangle_off.png')
      }
    },
    { label: 'Круглый', 
      value: 'CIRCLE',
      images: {
        on: require('./mans/icon_man_round_on.png'),
        off: require('./mans/icon_man_round_off.png')
      } 
    },
  ],
  womanFigureTypeImages: [
    { label: 'Песочные часы',
      value: 'HOURGLASS',
      images: {
        on: require('./womans/icon_woman_time_on.png'),
        off: require('./womans/icon_woman_time_off.png')
      }
    },
    { label: 'Груша',
      value: 'TRIANGLE',
      images: {
        on: require('./womans/icon_woman_trev_on.png'),
        off: require('./womans/icon_woman_trev_off.png')
      }
    },
    { label: 'Перевернутый треугольник',
      value: 'INVERTED TRIANGLE',
      images: {
        on: require('./womans/icon_woman_tr_on.png'),
        off: require('./womans/icon_woman_tr_off.png')
      }
    },
    { label: 'Прямоугольный',
      value: 'RECTANGLE',
      images: {
        on: require('./womans/icon_woman_rectangle_on.png'),
        off: require('./womans/icon_woman_rectangle_off.png')
      }
    },
    { label: 'Яблоко', 
      value: 'OVAL',
      images: {
        on: require('./womans/icon_woman_round_on.png'),
        off: require('./womans/icon_woman_round_off.png')
      } 
    },
  ]
}
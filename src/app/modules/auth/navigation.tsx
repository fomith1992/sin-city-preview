import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import * as screenNames from "../navigation/screen-names";

import AuthScreensProfileFirstStep from './ui/screens/profile-first-step';
import ProfileInfoScreenSecondStep from './ui/screens/profile-second-step';
import ProfileInfoScreenThirdStep from './ui/screens/profile-third-step';
import AuthScreensAuthChecking from "./ui/screens/auth-checking";
import AuthScreensPhone from "./ui/screens/phone";
import AuthScreensCode from "./ui/screens/code";
import LayoutScreensWelcome from "../layout/ui/screens/welcome";
import PdfViewer from "../ud-ui/screens/pdf-View";

export default createStackNavigator({
  [screenNames.WELCOME]: { screen: LayoutScreensWelcome },
  [screenNames.AUTH_CHECKING]: { screen: AuthScreensAuthChecking },
  [screenNames.PHONE_INPUT]: { screen: AuthScreensPhone },
  [screenNames.CODE_INPUT]: { screen: AuthScreensCode },
  [screenNames.PDF_VIEWER]: { screen: PdfViewer },
  [screenNames.PROFILE_FISRT_STEP_SCREEN]: {
    screen: AuthScreensProfileFirstStep,
    navigationOptions: () => ({
      gestureResponseDistance: {
        horizontal: -1,
        vertical: -1,
      },
    }),
  },
  [screenNames.PROFILE_SECOND_STEP_SCREEN]: {
    screen: ProfileInfoScreenSecondStep,
    navigationOptions: () => ({
      gestureResponseDistance: {
        horizontal: -1,
        vertical: -1,
      },
    }),
  },
  [screenNames.PROFILE_THIRD_STEP_SCREEN]: {
    screen: ProfileInfoScreenThirdStep,
    navigationOptions: () => ({
      gestureResponseDistance: {
        horizontal: -1,
        vertical: -1,
      },
    }),
  },
}, {
  initialRouteName: screenNames.AUTH_CHECKING,
  // mode: 'card',
  headerMode: "none"
});

import { BaseStore } from "../../../store/BaseStore";
import { action, observable, toJS, computed } from "mobx";
import { TYPES } from "../../../store/store-types";
import NavigationStore from 'react-navigation-mobx-helpers';
import { User } from "../../user/domain/interfaces/User";
import { Profile } from "../../user/domain/interfaces/Profile";
import * as screenName from "../../navigation/screen-names";
import { ProfileFactory } from "../../user/domain/factories/ProfileFactory";
import { showMessage } from "react-native-flash-message";
import { UserFactory } from "../../user/domain/factories/UserFactory";
import { showModalAndTakePhoto } from "../../core/domain/services/MediaTakingService";
import { ImagePickerResponse } from "react-native-image-picker";
import LayoutStore from "../../layout/store/LayoutStore";
import { CurrentUserStore } from "./current-user";
import userRepository from "../../user/domain/repositories/UserRepository";
import { AuthUserStore } from "./auth-user";
import avatarRepository from "../../user/domain/repositories/AvatarRepository";
import userRegistrationValidator from '../domain/validators/UserRegistrationValidator';
import profileRegistrationValidator from '../domain/validators/ProfileRegistrationValidation';
import moment from 'moment';

const errorMessages = require('./error-messages.json');
import notification from '../../notifications/domain/services/Notifications';
import {Linking, Platform} from "react-native";

export class AuthRegisterStore extends BaseStore {
  @observable user: User | null = UserFactory.createEmpty();
  @observable profiles: Profile[] | null;
  @observable photo: ImagePickerResponse | null = null;
  @observable gender: string | null = null;
  @observable phone: string;
  @observable isPhotoLoading: boolean = false;

  // ui flags
  @observable showPhotoRequiredMessage: boolean = false;

  private navigation: NavigationStore;
  private layout: LayoutStore;
  private currentUserStore: CurrentUserStore;
  private authUser: AuthUserStore;

  storeInit() {
    this.navigation = this.root.resolve(TYPES.Navigation);
    this.layout = this.root.resolve(TYPES.Layout);
    this.currentUserStore = this.root.resolve(TYPES.CurrentUserStore);
    this.authUser = this.root.resolve(TYPES.AuthUser);
  }

  public nextFromUserScreen(): void {
  //  console.log('nextFromUserScreen')
    if (this.userErrors) {
      console.log('errors', this.userErrors)
      window.fLogEvent('app_errorScreen', this.userErrors.join('\n'));
      window.fLogEventObj('registration_submitError',
        {screenName: 'reg_screen_1', typeError: 'danger', fieldError: this.userErrors.join('\n')});
      showMessage({ message: this.userErrors.join('\n'), type: 'danger' });
    }
    else {
  //    const profiles = this.gender.split('')
   //     .map((gender) => ProfileFactory.create({ gender }));
      // if (this.user.city?.id != null) {
      //   this.user.city = this.user.city.id;
      // }
      //  delete this.user.city;
    //  this.user.city = 3;
      this.user.city = null;
      this.layout.showOverflowLoader();
      userRepository.update(this.authUser.userId, this.user)
        .then((user: User) => {
         // console.log('update_then', user)
          this.user = user;
         // this.profiles = profiles;
          this.nextFromPhotoScreen();
        //  this.navigation.navigate(screenName.PROFILE_SECOND_STEP_SCREEN);
        })
        .catch((e) => {
          console.log('update_CATCH', e);
          this.layout.hideOverflowLoader()
        })
       // .finally(() => this.layout.hideOverflowLoader());
    }
  }

  public nextFromProfilesScreen(): void {
    if (this.profileErrors) {
      window.fLogEvent('app_errorScreen', this.profileErrors.join('\n'));
      window.fLogEventObj('registration_submitError',
        {screenName: 'reg_screen_2', typeError: 'danger', fieldError: this.profileErrors.join('\n')});
      showMessage({ message: this.profileErrors.join('\n'), type: 'danger' });
    } else {
      this.navigation.navigate(screenName.PROFILE_THIRD_STEP_SCREEN);
    }
  }

  public nextFromPhotoScreen(): void {
  //  console.log('nextFromPhoto')
    this.layout.showOverflowLoader();
    const user = {
      ...toJS(this.user),
      profiles: toJS(this.profiles)
    };
    user.city = null;
    // if (typeof user.city === 'object') {
    //   user.city = user.city.id;
    // }
  //  console.log('userToSend', user)
    if (this.photo) {
    //  console.log('if this.photo')
      window.fLogEvent('registration_startLoading');
     // this.layout.showOverflowLoader();
      avatarRepository.uploadAndSaveAvatar(this.photo)
          .then((photo) => {
       //     console.log('avatarRepository.uploadAndSaveAvatar_then', photo)

            window.fLogEvent('registration_endLoading');
            notification.init(this.root);
            this.user = user;
            this.profiles = [...user.profiles];
            this.currentUserStore.user = {
              ...(this.user as User),
              photo,
              /**
               * hotfix
               * номер видимо не приходит с бэка при сохранении,
               * поэтому записываем его в модель пользователя на этапе регистрации
               */
              phone: this.phone
            };
            this.clearStore();
            window.fLogEvent('registration_regSuccess');
            this.navigation.navigate(screenName.TABS_FLOW);
          })
          .catch((e) => {
            console.log('avatarRepository.uploadAndSaveAvatar_ERROR', e)
            window.fLogEventObj('registration_errorLoading', e);
          })
          .finally(() => this.layout.hideOverflowLoader());

      userRepository.update(this.authUser.userId, user)
          .then((user: User) => {
            notification.init(this.root);
            this.user = user;
            this.profiles = [...user.profiles];
            this.currentUserStore.user = {
              ...(this.user as User),
              phone: this.phone
            };
            this.clearStore();
            window.setUserExists(false);
            window.fLogEvent('registration_regSuccess', 'success');
            this.navigation.navigate(screenName.TABS_FLOW);
          })
          .catch((e) => {
          })
          .finally(() => this.layout.hideOverflowLoader());
      // userRepository.update(this.authUser.userId, user)
      //     .then((user) => {
      //       console.log('userRepository.update_then', user)
      //
      //     })
      //     .catch((e) => {
      //       console.log('userRepository.update_ERROR', e)
      //       window.fLogEventObj('registration_errorLoading', e);
      //     })
      //     .finally(() => {
      //       this.layout.hideOverflowLoader()
      //     })

      // Promise.all([
      //   userRepository.update(this.authUser.userId, user),
      //   avatarRepository.uploadAndSaveAvatar(this.photo)
      // ]).then(([user, photo]) => {
      //   console.log('uploadAndSaveAvatar_then', user, photo)
      //   window.fLogEvent('registration_endLoading');
      //   notification.init(this.root);
      //   this.user = user;
      //   this.profiles = [...user.profiles];
      //   this.currentUserStore.user = {
      //     ...(this.user as User),
      //     photo,
      //     /**
      //      * hotfix
      //      * номер видимо не приходит с бэка при сохранении,
      //      * поэтому записываем его в модель пользователя на этапе регистрации
      //      */
      //     phone: this.phone
      //   };
      //   this.clearStore();
      //   window.fLogEvent('registration_regSuccess');
      //   this.navigation.navigate(screenName.TABS_FLOW);
      // })
      //   .catch((e) => {
      //     window.fLogEventObj('registration_errorLoading', e);
      //     console.log('uploadAndSaveAvatar_CATCH', e)
      //     // todo закомментил для демонстрации релиза
      //     // showMessage({
      //     //   message: 'Неизвестная ошибка',
      //     //   type: 'warning',
      //     // });
      //   })
      //   .finally(() => this.layout.hideOverflowLoader());
    } else {
      userRepository.update(this.authUser.userId, user)
        .then((user: User) => {
          notification.init(this.root);
          this.user = user;
          this.profiles = [...user.profiles];
          this.currentUserStore.user = {
            ...(this.user as User),
            /**
             * hotfix
             * номер видимо не приходит с бэка при сохранении,
             * поэтому записываем его в модель пользователя на этапе регистрации
             */
            phone: this.phone
          };
          this.clearStore();
          window.setUserExists(false);
          window.fLogEvent('registration_regSuccess', 'success');
          this.navigation.navigate(screenName.TABS_FLOW);
        })
        .catch((e) => {
            console.log('update_ERROR', e)
          // todo закомментил для демонстрации релиза
          // showMessage({
          //   message: 'Неизвестная ошибка',
          //   type: 'warning',
          // });
        })
        .finally(() => this.layout.hideOverflowLoader());
    }
  }

  @action
  public selectPhoto(): void {
    this.isPhotoLoading = true;
    this.showPhotoRequiredMessage = false;
    this.layout.showOverflowLoader(false);
    showModalAndTakePhoto(true)
      .then((response: any) => {
        this.photo = response;
        this.showPhotoRequiredMessage = false;
      })
      .catch((error) => {
        this.showPhotoRequiredMessage = true;
        if(!error.didCancel) {
          window.fLogEvent('app_errorScreen',
            'Возникла ошибка при загрузке фото, попробуйте снова');
          showMessage({
            message: 'Возникла ошибка при загрузке фото, попробуйте снова',
            type: 'danger',
          });
        }
      })
      .finally(() => {
        this.isPhotoLoading = false;
        this.layout.hideOverflowLoader();
      });
  }

  @action
  public clearPhoto(): void {
    this.showPhotoRequiredMessage = true;
    this.photo = null;
  }

  @action
  public changeUser(changes: any): void {
    this.user = { ...this.user, ...changes };
  }
  // открытие условий использования приложения
  @action
  public openUsingPolicy(): void {
    Platform.OS === 'android'
        ? this.navigation.navigate('PDF_VIEWER', {url:
              {
                uri: 'https://scinfo.ru/eula.pdf',
                cache: true
              }
        })
        : Linking.openURL('https://scinfo.ru/eula.pdf').then(() => {})
  }

  @action
  public changeGender(value: any): void {
  //  console.log('changeGender', value)
    if (value) {
      this.gender = value;
      const profiles = value.split('')
          .map((gender) => ProfileFactory.create({ gender }));
   //   console.log('changeGender_profiles', profiles);
      this.profiles = profiles;
    }

  }

  private clearStore(): void {
    this.profiles = null;
    this.user = UserFactory.createEmpty();
    this.photo = null;
    this.gender = null;
  }

  @computed
  public get userErrors(): any {
    const birthdays = !!this.profiles ? this.profiles.length > 1 ? {
        birthday1: this.profiles[0].birthday,
        birthday2: this.profiles[1].birthday,
    } : {
          birthday1: this.profiles[0].birthday,
          birthday2: 'dr',
      } : {
        birthday1: 'dr',
        birthday2: 'dr',
    };
    const errors = userRegistrationValidator.validate({
      ...this.user,
      ...birthdays,
      gender: this.gender,
    }, errorMessages);
    if (errors) {
      return Object.keys(errors).reduce((result, key) => {
        result.push(...errors[key]);
        return result;
      }, []);
    }
  }

  @computed
  public get profileErrors(): any {
    let result: any[] = [];
    this.profiles.forEach((profile, index) => {
      const errors = profileRegistrationValidator.validate(profile, errorMessages);
      if (errors) {
      //  result.push(`Профиль ${index + 1}`)
        const errorsList = Object.keys(errors).reduce((res, key) => {
          res.push(...errors[key]);
          return res;
        }, []);
        result.push(...errorsList);
      }
    })
    return result.length ? result : null;
  }

}

import { action, observable, toJS } from "mobx";
import LayoutStore from "../../layout/store/LayoutStore";
import authenticationRepository from "../domain/repositories/AuthRepository";
import * as screenNames from "../../navigation/screen-names";
import NavigationStore from 'react-navigation-mobx-helpers';
import { BaseStore } from "../../../store/BaseStore";
import { TYPES, ITYPES } from "../../../store/store-types";
import { AuthUserStore } from "./auth-user";
import { AuthCodeSuccessResponse } from "../domain/interfaces/AuthCodeSuccessResponse";
import notification from '../../notifications/domain/services/Notifications';
import { AuthRegisterStore } from "./auth-register";

export class AuthEnterStore extends BaseStore {
  @observable phone = '';
  @observable code = '';
  @observable repeatCodeTime: number = 0;

  private timer: any;
  private layout: LayoutStore;
  private authUser: AuthUserStore;
  private navigation: NavigationStore;
  private registration: AuthRegisterStore;
  private currentUserStore: ITYPES['CurrentUserStore'];
  private geolocationStore: ITYPES['GeolocationStore'];

  storeInit() {
    this.layout = this.root.resolve(TYPES.Layout);
    this.authUser = this.root.resolve(TYPES.AuthUser);
    this.navigation = this.root.resolve(TYPES.Navigation);
    this.registration = this.root.resolve(TYPES.AuthRegister);
    this.currentUserStore = this.root.resolve(TYPES.CurrentUserStore);
    this.geolocationStore = this.root.resolve(TYPES.GeolocationStore);
  }

  @action.bound
  public requestCode(phone: string) {
    this.phone = phone;
    this.layout.showOverflowLoader();
    authenticationRepository.getCode(phone)
      .then(() => this.onCodeRequestSuccess())
      .catch((e: any) => { /* nothing */ })
      .finally(() => this.layout.hideOverflowLoader());
  }

  @action.bound
  public requestCodeAgain() {
    this.requestCode(this.phone);
  }

  @action.bound
  public verifyCode(code: string): void {

    this.layout.showOverflowLoader();
    const data = { phone: this.phone, sms: code };

    authenticationRepository.verifyCode(data)
      .then((response: AuthCodeSuccessResponse) => {
        window.fLogEvent('onboarding_clickEnterBtn', 'success', 'response');
        Object.assign(response, { user: { phone: this.phone } });
        this.authUser.authorizeUser(response)
          .then(() => this.authRedirect(response))
          .catch((e) => this.authRedirect(response))
          .finally(() => notification.init(this.root));
        this.registration.phone = this.phone;
        this.resetObservables();
      })
      .catch(() => {
        window.fLogEvent('onboarding_clickEnterBtn', 'fail', 'response');
      })
      .finally(() => this.layout.hideOverflowLoader());
  }

  private onCodeRequestSuccess(): void {
    this.timer && clearInterval(this.timer);
    this.repeatCodeTime = 60;
    this.startTimer();
    this.navigation.navigate(screenNames.CODE_INPUT);
  }

  private resetObservables(): void {
    this.phone = '';
    this.code = '';
  }

  private startTimer() {
    this.timer = setInterval(() => {
      if (this.repeatCodeTime) {
        this.repeatCodeTime--;
      }
      else {
        this.timer && clearInterval(this.timer);
        this.repeatCodeTime = 0;
      }
    }, 1000);
  }

  private authRedirect(response: AuthCodeSuccessResponse): void {
    const { userExist, showMe, isVerified, isPayed } = response;
    this.currentUserStore.user = userExist ? { ...response.user, isVerified, isPayed } : null;
    showMe && this.geolocationStore.startPositionWatching();
    const routeName = userExist ? screenNames.TABS_FLOW : screenNames.PROFILE_FISRT_STEP_SCREEN;
    this.navigation.navigate({ routeName });
  }
}

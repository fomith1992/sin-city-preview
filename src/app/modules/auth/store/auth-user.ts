import { BaseStore } from "../../../store/BaseStore";
import {Alert} from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";
import httpResource from "../../core/infrastructure/httpResource";
import { NavigationActions, StackActions } from "react-navigation";
import * as screenNames from "../../navigation/screen-names";
import authenticationRepository from "../domain/repositories/AuthRepository";
import { observable } from "mobx";
import { AuthCodeSuccessResponse } from "../domain/interfaces/AuthCodeSuccessResponse";
import { TYPES, ITYPES } from "../../../store/store-types";
import NavigationStore from 'react-navigation-mobx-helpers';
import notification from '../../notifications/domain/services/Notifications';
import GeolocationStore from "../../core/store/geolocation";
import CoordinatesSearchStore from "../../search/store/CoordinatesSearchStore";
import UsersSearchStore from "../../search/store/UsersSearchStore";

export class AuthUserStore extends BaseStore {
  private TOKEN_STORAGE_KEY: string = '@Token';
  private USER_ID_STORAGE_KEY: string = '@UserId';

  @observable userId: number;
  @observable token: string;

  private currentUserStore: ITYPES['CurrentUserStore']
  private navigation: NavigationStore;
  private geolocationStore: GeolocationStore;
  private purchasesStores: ITYPES['PurchasesStore'];
  private coordinatesSearchStore: CoordinatesSearchStore;
  private usersSearchStore: UsersSearchStore;

  storeInit() {
    this.currentUserStore = this.root.resolve(TYPES.CurrentUserStore);
    this.navigation = this.root.resolve(TYPES.Navigation);
    this.geolocationStore = this.root.resolve(TYPES.GeolocationStore);
    this.purchasesStores = this.root.resolve(TYPES.PurchasesStore);
    this.usersSearchStore = this.root.resolve('usersSearchStore');
    this.coordinatesSearchStore = this.root.resolve('coordinatesSearchStore');
  }

  public authorizeUser(response: AuthCodeSuccessResponse): Promise<AuthCodeSuccessResponse> {
    this.storeAuthorizeInMemory(response);
    return this.storeAuthorizeInStorage(response);
  }

  public checkAuthorization(): void {
    AsyncStorage.multiGet([this.TOKEN_STORAGE_KEY, this.USER_ID_STORAGE_KEY])
      .then(([[k1, token], [k2, userId]]) => {
        if (token && userId) {
          this.storeAuthorizeInMemory({ userId, token } as any);
          authenticationRepository.verifyToken()
            .then(({ userExist, user }: AuthCodeSuccessResponse) => {
              notification.init(this.root);
              this.currentUserStore.user = user;
              this.redirectAfterCheck({ userExist });
              user.showMe && this.geolocationStore.startPositionWatching();
              this.purchasesStores.tryUpdatePurchase();
            })
            .catch((e: any) => {
              this.clearAuthorizeInMemory();
              this.redirectAfterCheck({ userExist: false });
            });
        } else {
          this.redirectAfterCheck({ userExist: false });
        }
      })
      .catch((e) => this.redirectAfterCheck({ userExist: false }))
  }

  public logout(removeToken: boolean = true) {
    if (removeToken) {
      notification.destroyFCMToken();
    }
    AsyncStorage.multiRemove([this.TOKEN_STORAGE_KEY, this.USER_ID_STORAGE_KEY])
      .then(() => {
        this.clearAuthorizeInMemory();
        const resetAction = StackActions.reset({
          index: 0,
          key: 'Auth',
          actions: [NavigationActions.navigate({ routeName: screenNames.AUTH_CHECKING })],
        });
        this.navigation.dispatch(resetAction);

        // clear stores
        this.coordinatesSearchStore.clearFilters();
        this.usersSearchStore.clearFilters();
      })
  }

  private storeAuthorizeInMemory({ token, userId }: { token: string, userId: number }) {
    httpResource.setHeaders({ Authorization: `Token ${token}` });
    this.userId = Number(userId);
    this.token = token;
    window.passToken(token);
  }

  private clearAuthorizeInMemory():void {
    httpResource.clearHeaders();
    this.userId = null;
    this.token = null;
    this.currentUserStore.user = null;
    this.currentUserStore.profiles = null;
    this.currentUserStore.photos = null;
  }

  private storeAuthorizeInStorage(response: AuthCodeSuccessResponse) {
    const { token, userId } = response;
    window.passToken(token);
    return AsyncStorage.multiSet([
      [this.TOKEN_STORAGE_KEY, token],
      [this.USER_ID_STORAGE_KEY, String(userId)],
    ]).then(() => response);
  }

  private redirectAfterCheck({ userExist }: { userExist: boolean }): void {
    // Alert.alert('USER', JSON.stringify(userExist));
    // if (userExist) {
    //   window.setUserExists(true);
    // } else {
    //   window.setUserExists(false);
    // }
    const routeName = userExist ? screenNames.TABS_FLOW : screenNames.WELCOME;
    const resetAction = StackActions.reset({
      index: 0,
      key: userExist ? 'App' : 'Auth',
      actions: [
        NavigationActions.navigate({ routeName })
      ],
    });
    this.navigation.dispatch(resetAction);
  }

}

import { ITYPES } from './../../../store/store-types';
import { Platform } from 'react-native';
import { LayoutStore } from './../../layout/store/LayoutStore';
import { CommunicationType } from './../../menu/domain/interfaces/CommunicationType';
import SupportService from './../../menu/domain/services/SupportService';
import { BaseStore } from "../../../store/BaseStore";
import { User } from "../../user/domain/interfaces/User";
import { action, computed, observable, toJS } from "mobx";
import uploader from "../../upload/Uploader";
import { AuthUserStore } from "./auth-user";
import { TYPES } from "../../../store/store-types";
import { Profile } from "../../user/domain/interfaces/Profile";
import { Response } from "../../../infrastructure/interfaces/Response";
import photosRepository from "../../upload/domain/repositories/PhotosRepository";
import { Photo } from "../../upload/domain/interfaces/Photo";
import { showModalAndTakePhoto } from "../../core/domain/services/MediaTakingService";
import { showMessage } from "react-native-flash-message";
import Navigation from "react-navigation-mobx-helpers";
import userRepository from "../../user/domain/repositories/UserRepository";
import avatarRepository from "../../user/domain/repositories/AvatarRepository";
import { ImagePickerResponse } from "react-native-image-picker";
import { getUniqueId, getBuildNumber } from 'react-native-device-info';

export class CurrentUserStore extends BaseStore {
  @observable user: User;
  @observable profiles: Profile[];
  @observable photos: Photo[];

  // state flags
  @observable isSyncingInfo: boolean = false;
  @observable syncInfoError: Error | null = null;
  @observable isSyncingPhotos: boolean = false;
  @observable syncPhotosError: Error | null = null;
  @observable isAvatarSaving: boolean = false;
  @observable isUpdateAvatar: boolean = false;

  // todo move to MyAccountStore
  @observable tempUser: User;
  @observable tempProfiles: Profile[];
  @observable shownPhotoIndex: number = 0;

  private authUser: AuthUserStore;
  private navigation: Navigation;
  private layout: LayoutStore;
  private geolocation: ITYPES['GeolocationStore'];

  storeInit() {
    this.authUser = this.root.resolve(TYPES.AuthUser);
    this.navigation = this.root.resolve(TYPES.Navigation);
    this.geolocation = this.root.resolve(TYPES.GeolocationStore);
    this.layout = this.root.resolve(TYPES.Layout);
  }

  @action
  public setUserVerification(value: boolean) {
    if (this.user) {
      this.user.isVerified = value;
    }
  }

  @action
  public setUserPayment(value: boolean) {
    if (this.user) {
      this.user.isPayed = value;
    }
  }

  @action
  public uploadNewAvatar(pickedPhoto: ImagePickerResponse): Promise<any> {
    this.isAvatarSaving = true;
    return avatarRepository.uploadAndSaveAvatar(pickedPhoto)
      .then((photo: Photo) => {
        Object.assign(this.user, { photo });
        this.photos = [photo, ...this.photos];
        return photo
      })
      .finally(() => this.isAvatarSaving = false);
  }

  public setPhotoAsAvatar(photo: Photo): Promise<any> {
    return this.setPhotoAsAvatarById(photo.id);
  }

  @action
  public setPhotoAsAvatarById(photoId: number): Promise<Photo> {
    this.isAvatarSaving = true;

    // if (this.isUpdateAvatar) {
      return avatarRepository.setAvatarByPhotoId(photoId)
          .then((photo: Photo) => {
            this.photos = this.photos.map((p) => Object.assign(p, { isAvatar: p.id === photo.id }));
            Object.assign(this.user, { photo });
            return photo;
          })
          .finally(() => this.isAvatarSaving = false);
    // }

    // this.layout.showModalMessage('При замене аватара вы потеряете статус Подлинного, пока админ не проверит вас еще раз. Мы делаем все для безопасности вашей и других участников.', true);
    this.isUpdateAvatar = true;
  }

  @action
  public uploadNewPhoto(photo: any) {
    this.layout.showOverflowLoader(true);
    return uploader.upload(photo)
      .then((savedPhoto: any) => {

        this.photos = [...this.photos, { ...savedPhoto, countLikes: 0 }];
        this.layout.hideOverflowLoader();
        window.fLogEventObj('myprofile_successAddPhoto', {});
        showMessage({ message: 'Фото загружено', type: 'success' });
        this.syncCurrentUser();
      })
      .catch(() => {
        window.fLogEventObj('myprofile_failAddPhoto', {});
      })
      .finally(() => {
        this.layout.hideOverflowLoader();
      })
  }

  @action
  public deletePhoto(photo: Photo): Promise<any> {
    return photosRepository.delete(photo)
      .then(() => {
        const deletedPhotoIndex = this.photos.findIndex((p: Photo) => p.id === photo.id);
        this.photos = this.photos.filter((p: Photo) => p.id !== photo.id);
        if (this.shownPhotoIndex === deletedPhotoIndex) {
          if (this.shownPhotoIndex > 0 && this.profileCarouselPhotos) {
            this.shownPhotoIndex--;
          }
        }
      });
  }

  @action
  public deleteAvatar(): Promise<any> {
    const avatar = this.photos.find(photo => photo.isAvatar);
    return photosRepository.delete(avatar)
      .then(() => {
        const deletedPhotoIndex = this.photos.findIndex((p: Photo) => p.id === avatar.id);
        this.photos = this.photos.filter((p: Photo) => p.id !== avatar.id);
        if (this.shownPhotoIndex === deletedPhotoIndex) {
          if (this.shownPhotoIndex > 0 && this.profileCarouselPhotos) {
            this.shownPhotoIndex--;
          }
        }
      });
  }

  @action
  public removeMyProfile(): void {
    userRepository.removeMyProfile(this.authUser.userId)
      .then(() => {
        this.geolocation.stopPositionWatching();
        this.authUser.logout(false);
      });
  }

  public syncCurrentUser(): void {
    this.syncCurrentUserInfo();
    this.syncCurrentUserPhotos();
  }

  @action
  public syncCurrentUserInfo(): Promise<User> {
    this.isSyncingInfo = true;
    return userRepository.loadById(this.authUser.userId)
      .then((user: User) => {
        this.user = user;
        this.profiles = user.profiles;
        return this.user;
      })
      .finally(() => this.isSyncingInfo = false);
  }

  @action
  public syncCurrentUserPhotos(): Promise<Photo[]> {
    this.isSyncingPhotos = true;
    return photosRepository.load()
      .then((response: Response<Photo>) => {
      //  console.log('syncCurrentUserPhotos_response', response)
        this.photos = response.results;
        this.shownPhotoIndex = 0;
        return response.results;
      }).finally(() => this.isSyncingPhotos = false);
  }

  @computed
  public get isProfileSyncing(): boolean {
    return this.isSyncingPhotos || this.isSyncingInfo;
  }

  // todo move to MyProfileStore
  @action
  public choicePhoto() {
    this.layout.showOverflowLoader(false);
    showModalAndTakePhoto(true)
      .then((response) => this.uploadNewPhoto(response))
      .catch((e) => { this.layout.hideOverflowLoader(); })
      .finally(() => { });
  }

  @computed
  public get avatar(): Photo | null {
    if (this.user && this.user.photo) {
      return this.user.photo;
    }
    return null;
  }

  @computed
  public get profileCarouselPhotos(): Photo[] {
    return this.photos ? this.photos : [];
  }

  @computed
  public get profileInfo(): any {
    const user = (this.user || {}) as User;
    const profiles = this.profiles || [];
    const gender = profiles.map((p) => p.gender).join('');
    return {
      login: user.login,
      city: user.city,
      familyStatus: user.familyStatus,
      interestingAgeMin: user.interestingAgeMin,
      interestingAgeMax: user.interestingAgeMax,
      description: user.description,
      profiles,
      gender
    }
  }

  sendFeedback(message: string, type: CommunicationType) {
    this.layout.showOverflowLoader();
    if (message && message.length && message.length > 200) {
      window.fLogEvent('app_errorScreen', 'Максимум 200 символов');
      window.fLogEventObj('feedback_clickSendBtn', {status_feedback: 'error'});
      showMessage({ message: 'Максимум 200 символов', type: 'danger' });
    } else {
      const hiddenData = {
        id: this.authUser.userId,
        phone: this.user.phone,
        buildNumber: getBuildNumber(),
        deviceId: getUniqueId(),
        platform: Platform.OS,
      };
      SupportService.sendMessage(message, type)
        .then(() => {
          window.fLogEventObj('feedback_clickSendBtn', {status_feedback: 'success'});
          this.navigation.pop();
          // this.layout.showModalMessage('Сообщение успешно отправлено.');
        })
        .catch(() => {
          window.fLogEventObj('feedback_clickSendBtn', {status_feedback: 'error'});
        })
        .finally(() => { this.layout.hideOverflowLoader(); })
    }
  }
}

import { User } from './../../../user/domain/interfaces/User';
export interface AuthCodeSuccessResponse {
  token: string
  userExist: boolean
  userId: number
  user: User
  showMe: boolean
  isVerified: boolean
  isPayed: boolean
}
import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import authenticationResource from "../resources/AuthResource";

class AuthenticationRepository extends BaseRepository {
  constructor() {
    super(authenticationResource)
  }

  getCode(phone: string): Promise<any> {
    return this.resource('getcode')
      .create({ phone }).then((res: any) => this.processResponse(res));
  }

  verifyCode({ phone, sms }: { phone: string, sms: string }): Promise<any> {
    return this.resource('verifycode')
      .create({ phone, sms }).then((res: any) => this.processResponse(res));
  }

  verifyToken(): Promise<any> {
    return this.resource('checktoken')
      .get().then((res: any) => this.processResponse(res));
  }
}

const authenticationRepository = new AuthenticationRepository();

export default authenticationRepository;

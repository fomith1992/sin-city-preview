import { Validator } from "../../../validation/Validator";
import { Validators } from "../../../validation/Validators";

class ProfileRegistrationValidator extends Validator {
  protected schema = {
    birthday: [Validators.required],
  };
}

const profileRegistrationValidator = new ProfileRegistrationValidator();
export default profileRegistrationValidator;
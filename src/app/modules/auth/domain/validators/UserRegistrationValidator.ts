import { Validator } from "../../../validation/Validator";
import { Validators } from "../../../validation/Validators";

class UserRegistrationValidator extends Validator {
  protected schema = {
    login: [
      Validators.required,
      Validators.minLength(3),
      Validators.excludeNonWordCharacters,
      Validators.maxLength(20)
    ],
    description: [Validators.maxLength(200)],
    gender: [Validators.required],
    birthday1: [Validators.required],
    birthday2: [Validators.required],
   // city: [Validators.required],
   /// purposeDating: [Validators.required],
    purposeGender: [Validators.required]
  };
}

const userRegistrationValidator = new UserRegistrationValidator();
export default userRegistrationValidator;

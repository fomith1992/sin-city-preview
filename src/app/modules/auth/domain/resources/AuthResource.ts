import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../../modules/core/infrastructure/httpResource";
import { BaseHttpResource } from "../../../../infrastructure/BaseHttpResource";

class AuthenticationResource extends BaseRestResource {
    constructor(resource: BaseHttpResource) {
        super(resource, 'login', null);
    }
}

const authenticationResource = new AuthenticationResource(httpResource);

export default authenticationResource;
import { useEffect, useState } from "react";
import icons from "../../../../icons";

const manPartTypes = {
  figureTypes: icons.figureTypes.manFigureTypeImages,
  bodyTypes: icons.bodyTypes.manBodyTypes,
  facialTypes: icons.manFacialHairs
};

const womanPartTypes = {
  figureTypes: icons.figureTypes.womanFigureTypeImages,
  bodyTypes: icons.bodyTypes.womanBodyTypes,
  facialTypes: [] as any[]
};

export function useGenderPartTypes(profile: any) {
  const [genderPartTypes, setGenderPartTypes] = useState<any>({
    figureTypes: [],
    bodyTypes: [],
    facialTypes: []
  });

  useEffect(() => {
    switch(profile.gender){
      case "M":
        setGenderPartTypes(manPartTypes);
        break;
      case 'W':
        setGenderPartTypes(womanPartTypes);
        break;
    }
  }, [profile]);

  return genderPartTypes;
}
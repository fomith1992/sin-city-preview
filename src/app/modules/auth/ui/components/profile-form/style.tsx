import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export const activeColor = '#ffbd21';
export const inactiveColor = '#ffffff33';

export default StyleSheet.create({
  container: {
    backgroundColor: '#000',
    paddingLeft: 20,
    paddingBottom: 20,
    paddingTop: 15,
    borderBottomColor: inactiveColor,
    borderBottomWidth: 1,
  },
  listItemContent: {
    color: '#fff',
    letterSpacing: -1,
    fontSize: 16,
    marginLeft: 12,
    ...fontFamily('fontBaseMedium')
  },
  bodyTypeContainer: {
    borderRadius: 23,
    width: 60,
    height: 60,
    marginRight: 8,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  marker: {
    height: 19,
    width: 19,
    backgroundColor: '#fff'
  },
  viewContainer: { 
    flex: 1, 
    justifyContent: 'center'
  }
});


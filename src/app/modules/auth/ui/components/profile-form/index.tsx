import React, { useState, useCallback } from 'react';
import { Text, Image, Dimensions, TouchableOpacity, ScrollView, View } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { observer } from "mobx-react";
import moment from 'moment';

import { Profile } from '../../../../user/domain/interfaces/Profile';
import styles, { activeColor, inactiveColor } from './style';
import pickerStyles from '../../../../../styles/picker';
import icons from '../../../../icons';
import ListItem from '../../components/profile-list-item';
import { Attitudes } from '../../../../user/domain/enums/Attitudes';
import { Orientations } from '../../../../user/domain/enums/Orientation';
import UDDateTimePicker from '../../../../ud-ui/components/ud-datetime-picker';
import { useGenderPartTypes } from "./custom-hooks";
import UDIconButton from "../../../../ud-ui/components/ud-icon-button";
import { showMessage } from "react-native-flash-message";

const wW = Dimensions.get('screen').width;
const maximumMomentDate = moment()
  .subtract(18, 'years');
const maximumDate = moment()
  .subtract(18, 'years')
  .toDate();
const minimumDate = moment.utc("01-01-1900", "DD-MM-YYYY").toDate();

const AuthComponentsProfileForm = observer(({ isRegister = false, profile }: { isRegister: any, profile: Profile }) => {
  const { figureTypes, bodyTypes, facialTypes } = useGenderPartTypes(profile);
  const [isDatetimePickerVisible, setDatetimePickerVisibility] = useState(false);
  const hideDatetimePicker = useCallback(() => {
    setDatetimePickerVisibility(false);
  }, []);

  const onBirthdaySelect = useCallback((newDate: any) => {
    setDatetimePickerVisibility(false);
    const newDateWithResetTime = moment.utc(newDate)
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    if (newDateWithResetTime.isAfter(maximumMomentDate)) {
      window.fLogEvent('app_errorScreen',
        'Вам должно быть от 18 лет');
      showMessage({ message: 'Вам должно быть от 18 лет', type: 'warning' });
    } else {
      const birthday = moment(newDate).format('YYYY-MM-DD');
      Object.assign(profile, { birthday });
    }
  }, [profile]);

  const onFigurePressed = useCallback((index) => {
    Object.assign(profile, { typeFigure: figureTypes[index].value });
  }, [figureTypes]);

  const onBodyPressed = useCallback((index) => {
    Object.assign(profile, { typePhysiques: bodyTypes[index].value });
  }, [bodyTypes]);

  const onFacialPressed = useCallback((index) => {
    Object.assign(profile, { facialHair: facialTypes[index].value });
  }, [facialTypes]);

  const onOrientationChange = useCallback((orientation) => {
    Object.assign(profile, { orientation });
  }, []);

  const onAlcoholChange = useCallback((attitudeToAlcohol) => {
    Object.assign(profile, { attitudeToAlcohol });
  }, []);

  const onSmokingChange = useCallback((attitudeToSmoking) => {
    Object.assign(profile, { attitudeToSmoking });
  }, []);

  const onDateTimePickerTapped = useCallback(() => {
    setDatetimePickerVisibility(true);
  }, []);

  const dateValue = profile.birthday ? new Date(profile.birthday) : maximumDate;

  return (
    <>
      <ListItem title="ДАТА РОЖДЕНИЯ*">
        <Image source={icons.date}/>
        <TouchableOpacity onPress={onDateTimePickerTapped}
                          style={{ flex: 1, paddingVertical: 10 }}>
          <Text style={[styles.listItemContent, { letterSpacing: 0.5 }]}>
            {Boolean(profile.birthday) ? moment.utc(profile.birthday).format('DD.MM.YYYY') : 'ДД.ММ.ГГГГ'}
          </Text>
        </TouchableOpacity>
        <UDDateTimePicker
          mode={'date'}
          maximumDate={maximumDate}
          minimumDate={minimumDate}
          isVisible={isDatetimePickerVisible}
          date={dateValue}
          onConfirm={onBirthdaySelect}
          onCancel={hideDatetimePicker}/>
      </ListItem>
      {isRegister ? null : (
          <>
              <ListItem title="РОСТ" postTitle={`${!!profile.height ? profile.height : ''} см`} withoutBorder>
                  <MultiSlider
                      values={[profile.height]}
                      sliderLength={wW - 61}
                      min={115}
                      max={245}
                      step={1}
                      snapped
                      selectedStyle={{ backgroundColor: activeColor }}
                      unselectedStyle={{ backgroundColor: inactiveColor }}
                      markerStyle={styles.marker}
                      containerStyle={{ paddingTop: 15, paddingHorizontal: 10 }}
                      onValuesChange={(height: number[]) => {
                          Object.assign(profile, { height: height[0] });
                      }}/>
              </ListItem>
              <ListItem title="ВЕС" postTitle={`${!!profile.weight ? profile.weight : ''} кг`} withoutBorder>
                  <MultiSlider
                      values={[profile.weight]}
                      sliderLength={wW - 61}
                      min={30}
                      max={180}
                      step={1}
                      snapped
                      selectedStyle={{ backgroundColor: activeColor }}
                      unselectedStyle={{ backgroundColor: inactiveColor }}
                      markerStyle={styles.marker}
                      containerStyle={{ paddingTop: 0, paddingHorizontal: 10 }}
                      onValuesChange={(weight: number[]) => {
                          Object.assign(profile, { weight: weight[0] });
                      }}/>
              </ListItem>
              <ListItem title="ТИП ФИГУРЫ" withoutBorder>
                  <ScrollView horizontal={true}
                              showsHorizontalScrollIndicator={false}
                              style={{ paddingTop: 20, flexDirection: 'row' }}>
                      {figureTypes.map((figureType: any, index: any) => {
                          const active = profile.typeFigure === figureType.value;
                          const image = active ? figureType.images.off : figureType.images.on;
                          return (
                              <UDIconButton
                                  index={index}
                                  image={image}
                                  key={figureType.value}
                                  onPress={onFigurePressed}
                                  active={active}/>
                          );
                      })}
                  </ScrollView>
              </ListItem>
              <ListItem title="ТИП ТЕЛОСЛОЖЕНИЯ" withoutBorder>
                  <ScrollView
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                      style={{ paddingTop: 20, paddingBottom: 15, flexDirection: 'row' }}>
                      {bodyTypes.map((bodyType: any, index: any) => {
                          const image = bodyType.image;
                          const active = bodyType.value === profile.typePhysiques;
                          return (
                              <UDIconButton
                                  image={image}
                                  key={bodyType.value}
                                  index={index}
                                  width={67}
                                  height={60}
                                  onPress={onBodyPressed}
                                  active={active}/>
                          )
                      })}
                  </ScrollView>
              </ListItem>
              {Boolean(facialTypes.length) &&
              <ListItem title="Растительность на лице" withoutBorder>
                <ScrollView horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            style={{ paddingTop: 20, paddingBottom: 15, flexDirection: 'row' }}>
                    {facialTypes.map((facialType: any, index: any) => {
                        const image = facialType.image;
                        const active = profile.facialHair === facialType.value;
                        return (
                            <UDIconButton
                                active={active}
                                onPress={onFacialPressed}
                                image={image}
                                index={index}
                                width={67}
                                height={60}
                                key={facialType.value}/>
                        )
                    })}
                </ScrollView>
              </ListItem>}
              <ListItem title="ОРИЕНТАЦИЯ">
                  <Image source={icons.gender}/>
                  <RNPickerSelect
                      placeholder={{label: 'Выберите...'}}
                      doneText="Готово"

                      value={profile.orientation}
                      style={pickerStyles}
                      onValueChange={onOrientationChange}
                      items={Orientations}/>
              </ListItem>
              <ListItem title="ОТНОШЕНИЕ К АЛКОГОЛЮ">
                  <Image source={icons.alcohol}/>
                  <RNPickerSelect
                      placeholder={{label: 'Выберите...'}}
                      doneText="Готово"

                      value={profile.attitudeToAlcohol}
                      style={pickerStyles}
                      onValueChange={onAlcoholChange}
                      items={Attitudes}/>
              </ListItem>
              <ListItem withoutBorder={true} title="ОТНОШЕНИЕ К КУРЕНИЮ">
                  <Image source={icons.smoking}/>
                  <RNPickerSelect
                      placeholder={{label: 'Выберите...'}}
                      doneText="Готово"

                      value={profile.attitudeToSmoking}
                      style={pickerStyles}
                      onValueChange={onSmokingChange}
                      items={Attitudes}/>
              </ListItem>
          </>
      )}

    </>
  );
});

export default AuthComponentsProfileForm;

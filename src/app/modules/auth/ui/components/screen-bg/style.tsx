import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  backgroundImage: {
    position: 'absolute',
    backgroundColor: '#000',
    width: '100%',
    height: '100%' 
  }
})
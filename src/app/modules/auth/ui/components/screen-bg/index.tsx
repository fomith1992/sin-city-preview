import React from 'react';
import { ImageBackground } from 'react-native';

import styles from './style';
const backgroundImage = require('./img/pic_registration.png');

const AuthComponentsScreenBg = () => {
  return (<ImageBackground source={backgroundImage} style={styles.backgroundImage}/>);
};

export default AuthComponentsScreenBg;
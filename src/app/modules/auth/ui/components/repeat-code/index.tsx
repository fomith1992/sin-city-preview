import React from 'react';
import {
  TouchableOpacity,
  Text,
} from 'react-native';

import styles from './style';

const AuthComponentsRepeatCode = ({ timer, onSendAgainPress }: any) => {
  if (timer > 0) {
    return (
      <Text style={styles.repeatCode}>
          {'Повторно смс-сообщение можно \nотправить через'}
        <Text style={styles.repeatTimer}>{` ${timer} сек`}</Text>.
      </Text>
    );
  } else {
    return (
      <TouchableOpacity onPress={onSendAgainPress}>
        <Text style={{ ...styles.repeatCode, color: 'rgb(255, 189, 33)' }}>Отправить сообщение повторно</Text>
      </TouchableOpacity>
    );
  }
};

export default AuthComponentsRepeatCode;

import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  repeatTimer: { 
    color: 'rgb(255, 189, 33)', 
    fontSize: 12,
    ...fontFamily('fontBaseMedium'),
  },
  repeatCode: {
    height: 40,
    marginBottom: 30,
    lineHeight: 20,
    paddingHorizontal: 6,
    ...fontFamily('fontBaseMedium'),
    color: '#999999',
    fontSize: 12,
    textAlign: 'center'
  },
})
import React from 'react';
import {
  TouchableOpacity,
  View
} from 'react-native';
import FastImage from 'react-native-fast-image';
import propTypes from 'prop-types';

const navBackIcon = require('./img/icon_nav_back.png');

import { StyleSheet } from 'react-native';

const AuthComponentsBackBtn = ({ onPress }: any) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress}
                        style={styles.touchable}>
        <FastImage style={styles.icon} source={navBackIcon}/>
      </TouchableOpacity>
    </View>
  );
};

(AuthComponentsBackBtn as any).propTypes = {
  onPress: propTypes.func
};

(AuthComponentsBackBtn as any).defaultProps = {
  onPress: () => {}
};


const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  touchable: {
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 11,
    height: 19
  }
});

export default AuthComponentsBackBtn;
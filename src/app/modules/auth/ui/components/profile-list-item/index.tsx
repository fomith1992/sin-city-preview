import React from 'react';
import { View, Text } from 'react-native';

import styles from './style';

type props = {
  children: any
  title: string
  withoutBorder?: boolean
  postTitle?: string
}

const ListItem = ({ children, title, withoutBorder, postTitle }: props) => {
  const style = [styles.listItem, { borderBottomWidth: withoutBorder ? 0 : 1 }] as any[];
  return (
    <View style={style}>
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.listItemTitle}>{title}</Text>
        <Text style={[styles.listItemPostTitle, { marginLeft: 15 }]}>{postTitle}</Text>
      </View>
      <View style={styles.listItemContentContainer}>
        {children}
      </View>
    </View>
  );
};

export default ListItem;
import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export const activeColor = '#ffbd21';
export const inactiveColor = '#ffffff33';

export default StyleSheet.create({
  container: {
    backgroundColor: '#000',
    paddingLeft: 20,
    paddingBottom: 20,
    paddingTop: 15,
    borderBottomColor: inactiveColor,
    borderBottomWidth: 1,

  },
  listItem: {
    borderBottomColor: inactiveColor,
    paddingLeft: 2,
    marginBottom: 25,
  },
  listItemTitle: {
    color: '#999999',
    fontSize: 12,
    ...fontFamily('fontBaseMedium')
  },
  listItemPostTitle: {
    color: '#fff',
    fontSize: 12,
    ...fontFamily('fontBaseMedium')
  },
  listItemContentContainer: {
    minHeight: 50,
    alignItems: 'center',
    flexDirection: 'row',
  },
  multilineTextInput: {
    ...fontFamily('fontBaseMedium'),
    color: '#fff',
    height: 84,
    fontSize: 14,
    lineHeight: 24,
    paddingVertical: 0,
    marginRight: 40,
    marginBottom: 6,
    marginTop: 13
  },
  multilineTextInputCounter: {
    ...fontFamily('fontBaseMedium'),
    color: '#999',
    fontSize: 9,
    textAlign: 'right',
    marginRight: 40,
    marginBottom: 13
  },
  listItemContent: {
    color: '#fff',
    fontSize: 16,
    marginLeft: 12,
    ...fontFamily('fontBaseMedium')
  },
  bodyTypeContainer: {
    borderRadius: 23,
    width: 67,
    height: 60,
    marginRight: 8,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  marker: {
    height: 19,
    width: 19,
    backgroundColor: '#fff'
  },
  profilePhotoContainer: {
    backgroundColor: '#252525',
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center'
  },
  profilePhotoText: { 
    color: '#fff',
    fontSize: 16,
    position: 'absolute',
    bottom: 30,
    ...fontFamily('fontBaseMedium'),
  },
  headerContainer: {
    position: 'relative',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000'
  },
  viewContainer: { 
    flex: 1, 
    justifyContent: 'center'
  }
});


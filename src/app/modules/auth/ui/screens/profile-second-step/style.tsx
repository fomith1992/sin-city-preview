import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export const inactiveColor = '#ffffff33';

export default StyleSheet.create({
  container: {
    backgroundColor: '#000',
    paddingLeft: 20,
    paddingBottom: 20,
    paddingTop: 15,
    borderBottomColor: inactiveColor,
    borderBottomWidth: 1,
  },
  listItemContent: {
    color: '#fff',
    fontSize: 16,
    marginLeft: 12,
    ...fontFamily('fontBaseMedium')
  }
});


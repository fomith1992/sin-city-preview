import React, { useCallback } from 'react';
import { View, Text, ScrollView, SafeAreaView, KeyboardAvoidingView, Platform, Keyboard } from 'react-native';
import { inject, observer } from "mobx-react";

import styles from './style';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import { TYPES } from "../../../../../store/store-types";
import { AuthRegisterStore } from "../../../store/auth-register";
import DictionariesStore from "../../../../dictionaries/store/DictionariesStore";
import AuthComponentsProfileForm from "../../components/profile-form";
import Navigation from "react-navigation-mobx-helpers";
import { useFocusEffect } from "react-navigation-hooks";

type props = {
  authRegisterStore: AuthRegisterStore,
  dictionariesStore: DictionariesStore,
  navigationStore: Navigation
};

export const AuthScreensProfileSecondStep = inject(TYPES.AuthRegister, TYPES.Dictionaries, TYPES.Navigation)
(observer(({ authRegisterStore, dictionariesStore, navigationStore }: props) => {
  const onBackPressed = useCallback(() => navigationStore.pop(), [navigationStore]);
  const onNextPressed = useCallback(() => {
    authRegisterStore.nextFromProfilesScreen();
    window.fLogEvent('registration_submitForm', 'reg_screen_2')
  }, []);
  const profiles = authRegisterStore.profiles || [];
  const keyboardB = Platform.OS === 'ios' ? "padding" : undefined;

  const onScreenFocus = useCallback(() => {
    return () => Keyboard.dismiss();

  }, []);

  useFocusEffect(onScreenFocus);

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#000', overflow: 'visible' }}>
      <Header title="Регистрация" onBack={onBackPressed}/>
      <KeyboardAvoidingView behavior={keyboardB}
                            style={{ flex: 1, backgroundColor: '#000', overflow: 'visible' }}>
        <ScrollView bounces={false}
                    keyboardDismissMode={'on-drag'}
                    style={[styles.container, { flex: 1, maxHeight: 650 }]}>
          {profiles.map((profile, index) =>
            <View key={index}>
              {profiles.length === 2 &&
              <View style={{ height: 30, alignItems: 'flex-end', paddingRight: 20 }}>
                <Text style={styles.listItemContent}>{profile.gender === 'M' ? 'Мужчина' : 'Женщина'}</Text>
              </View>
              }
              <AuthComponentsProfileForm
                profile={profile}/>
            </View>
          )}
        </ScrollView>
        <UDButton onPress={onNextPressed}
                  title="Продолжить"
                  style={{ marginHorizontal: 40, marginVertical: 15 }}/>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}));

export default AuthScreensProfileSecondStep;

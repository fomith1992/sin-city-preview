import React, { useCallback, useRef, useState } from 'react';
import { inject, observer } from "mobx-react";
import {
  SafeAreaView,
  Text,
  TextInput,
  KeyboardAvoidingView,
  Platform, Keyboard, TouchableWithoutFeedback, View
} from 'react-native';
import IMask from 'imask';

import styles from './style';
import fontFamily from '../../../../../styles/fonts';
import UDButton from '../../../../ud-ui/components/ud-button';
import AuthComponentsScreenBg from "../../components/screen-bg";
import { AuthEnterStore } from "../../../store/auth-enter";
import AuthComponentsRepeatCode from "../../components/repeat-code";
import { TYPES } from "../../../../../store/store-types";
import AuthComponentsBackBtn from "../../components/back-btn";
import { useFocusEffect } from "react-navigation-hooks";

const codeMask = IMask.createMask({
  mask: '0000'
});

type props = {
  authEnterStore: AuthEnterStore,
  navigation: any
}

const AuthScreensCode = inject(TYPES.AuthEnter)
(observer(({ authEnterStore, navigation }: props) => {
  const [code, setCode] = useState('');
  const onSendAgainPressed = useCallback((event) => authEnterStore.requestCodeAgain(), []);
  const onNextPressed = useCallback((event) => {

    authEnterStore.verifyCode(code);
    Keyboard.dismiss();
  }, [code]);
  const onBackPressed = useCallback(() => navigation.pop(), []);

  const inputRef = useRef(null);
  const onScreenFocus = useCallback(() => {
    window.setUserExists(true);
    if (inputRef) {
      setTimeout(() => {
        inputRef.current.focus();
      }, 500);
    }
    // return () => {
    //   Keyboard.dismiss();
    // }
  }, []);

  const onCodeChanged = useCallback((code) => {
    setCode(codeMask.resolve(code))
  }, []);

  useFocusEffect(onScreenFocus);

  return (
    <>
      <AuthComponentsScreenBg/>
      <KeyboardAvoidingView style={{ flex: 1 }}
                            keyboardVerticalOffset={10}
                            enabled={true}
                            behavior={Platform.OS === "ios" ? 'padding' : 'height'}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>

          <SafeAreaView style={[styles.container]}>
            <AuthComponentsBackBtn onPress={onBackPressed} />
            <View style={styles.content}>
              <Text style={styles.title}>Регистрация</Text>
              <Text style={styles.subtitle}>КОД ПОДТВЕРЖДЕНИЯ</Text>
              <TextInput
                ref={inputRef}
                keyboardType="numeric"
                style={styles.input}
                onChangeText={onCodeChanged}
                value={code}/>
              <AuthComponentsRepeatCode
                timer={authEnterStore.repeatCodeTime}
                onSendAgainPress={onSendAgainPressed}/>
              <UDButton title="Войти"
                        disabled={code.length < 4}
                        onPress={onNextPressed}
                        style={[styles.button]}/>
            </View>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </>
  );
}));

export default AuthScreensCode;

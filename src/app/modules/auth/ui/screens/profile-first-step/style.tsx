import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export const activeColor = '#ffbd21';
export const inactiveColor = '#ffffff33';

export default StyleSheet.create({
  container: {
    backgroundColor: '#000',
    paddingLeft: 20,
    paddingBottom: 20,
    paddingTop: 15,
    borderBottomColor: inactiveColor,
    borderBottomWidth: 1,
  },
  policyAcceptCont: {
   // backgroundColor: 'red',
    display: 'flex',
    height: 60,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  policyAcceptTextCont: {
    marginLeft: 20,
  },
  policyAcceptDescription: {
    ...fontFamily('fontBaseRegular'),
    color: '#fff',
    fontSize: 12,
  },
  policyAcceptLink: {
    ...fontFamily('fontTitleMedium'),
    color: '#ffbd21',

  },
  policyAcceptCheckbox: {
    width: 24,
    height: 24,
    marginLeft: 20,
    borderRadius: 4,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkMark: {
    width: 15,
    height: 12,
  },
  multilineTextInput: {
    color: '#fff',
    flex: 1,
    minHeight: 70,
    fontSize: 14,
    lineHeight: 24,
    paddingVertical: 0,
    marginRight: 40,
    marginBottom: 26,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: 10,
    textAlignVertical: 'top',
    ...fontFamily('fontBaseRegular')
  },
  multilineTextInputInvalid: {
    color: '#ff2020'
  },
  multilineTextInputCounterInvalid: {
    color: '#ff2020'
  },
  multilineTextInputCounter: {
    position: 'absolute',
    bottom: 0,
    right: 20,
    color: '#999',
    fontSize: 9,
    textAlign: 'right',
    marginRight: 40,
    marginBottom: 13
  },
  pickerInput: {
    color: '#fff',
    fontSize: 16,
    ...fontFamily('fontBaseMedium')
  },
  keyboard: {
    flex: 1,
    backgroundColor: '#000',
    overflow: 'visible'
  },
  profilePhotoContainer: {
    backgroundColor: '#252525',
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    alignSelf: 'center',
    marginBottom: 20,
    marginLeft: -20,
  },
  profilePhotoText: {
    color: '#FFBD21',
    fontSize: 16,
    // backgroundColor: '#aae',
    textAlign: 'center',
    paddingTop: 10,
    // position: 'absolute',
    // bottom: 30,
    ...fontFamily('fontBaseMedium'),
  },
  iconStyle: {
    resizeMode: 'contain',
    height: 46
  },
    listItemContent: {
        color: '#fff',
        fontSize: 16,
        marginLeft: 12,
        ...fontFamily('fontBaseMedium')
    }
});


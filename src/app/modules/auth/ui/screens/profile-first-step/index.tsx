import React, {useCallback, useRef, useState} from 'react';
import {
    TextInput,
    View,
    Text,
    Image,
    Dimensions,
    SafeAreaView,
    TouchableOpacity,
    Platform, Keyboard, BackHandler, Linking, ActivityIndicator
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { inject, observer } from "mobx-react";
import { City } from '../../../../dictionaries/domain/interfaces/City';
import { FamilyStatus } from '../../../../user/domain/enums/FamilyStatus';
import { GendersList } from '../../../../user/domain/enums/Gender';

import icons from '../../../../icons';
import pickerStyles from '../../../../../styles/picker';
import styles from './style';
import ListItem from '../../components/profile-list-item';
import LayoutComponentsHeader from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import UDSlider from '../../../../ud-ui/components/ud-slider';
import { TYPES } from "../../../../../store/store-types";
import { AuthRegisterStore } from "../../../store/auth-register";
import DictionariesStore from "../../../../dictionaries/store/DictionariesStore";
import { useFocusEffect } from "react-navigation-hooks";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { PurposeDatingForPicker } from '../../../../user/domain/enums/PurposeDating';
import { PurposeGenderForPicker } from '../../../../user/domain/enums/PurposeGender';
import fontFamily from "../../../../../styles/fonts";
import * as screenNames from "../../../../navigation/screen-names";
import Navigation from "react-navigation-mobx-helpers/dist";
import AuthComponentsProfileForm from "../../components/profile-form";

const wW = Dimensions.get('screen').width;
const sliderWidth = wW - 61;

const childrenOptions = [
  { label: 'Нет', value: false },
  { label: 'Есть', value: true },
];

type props = {
  authRegisterStore: AuthRegisterStore,
  dictionariesStore: DictionariesStore,
    navigationStore: Navigation;
};

const AuthScreensProfileFirstStep = inject(TYPES.AuthRegister, TYPES.Dictionaries, TYPES.Navigation)
(observer(({ authRegisterStore, dictionariesStore, navigationStore }: props) => {
    const { user, gender, photo, isPhotoLoading } = authRegisterStore;
    const { cities } = dictionariesStore;
    const descriptionInputRef = useRef(null);
    const [ifPolicyAccepted, setIfPolicyAccepted] = useState(false);
    const profiles = authRegisterStore.profiles || [];

    const onTakePhotoPressed = useCallback(
        () => authRegisterStore.selectPhoto(),
        [],
    );

    const onLoginChange = useCallback((login) => {
      authRegisterStore.changeUser({ login });
    }, []);

    const onCityChange = useCallback((city) => {
      authRegisterStore.changeUser({ city });
    }, []);

    const onGenderChange = useCallback((value) => {
      authRegisterStore.changeGender(value);
      onPurposeGenderChange(null);
    }, []);

    const onFamilyStatusChange = useCallback((familyStatus) => {
      authRegisterStore.changeUser({ familyStatus });
    }, []);

    const onInterestedYearsValuesChange = useCallback(([interestingAgeMin, interestingAgeMax]) => {
      authRegisterStore.changeUser({ interestingAgeMin, interestingAgeMax });
    }, []);

    const onDescriptionTextChange = useCallback((description) => {
      authRegisterStore.changeUser({ description });
    }, []);

    const onPurposeDatingChange = useCallback((purposeDating) => {
      authRegisterStore.changeUser({ purposeDating });
    }, []);

    const onPurposeGenderChange = useCallback((purposeGender) => {
      authRegisterStore.changeUser({ purposeGender });
    }, []);

    const onChildrenChange = useCallback((hasChildren) => {
      authRegisterStore.changeUser({ hasChildren });
    }, []);

    const onBackPressed = useCallback(() => {
      BackHandler.exitApp();
      return true;
    }, []);

    const onScreenFocus = useCallback(() => {
      window.fLogEvent('registration_viewScreen', 'reg_screen_1')
      window.setUserExists(false);
      BackHandler.addEventListener('hardwareBackPress', onBackPressed);

      return () => {
        Keyboard.dismiss();
        BackHandler.removeEventListener('hardwareBackPress', onBackPressed);
      }
    }, []);

    useFocusEffect(onScreenFocus);

    const onNextPressed = useCallback((event) => {
      window.fLogEvent('registration_submitForm', 'reg_screen_1')
      authRegisterStore.nextFromUserScreen();
    }, []);

    const descriptionCounterStyles = [styles.multilineTextInputCounter] as any;
    const descriptionStyles = [styles.multilineTextInput] as any;

    // if (user.description.length > 200) {
    //   descriptionCounterStyles.push(styles.multilineTextInputCounterInvalid);
    //   descriptionStyles.push(styles.multilineTextInputInvalid);
    // }
    // определение мн/ед лица
    const iLookFor = () => {
        if (!!gender && gender.length > 1) {
            return 'Ищем'
        } else {
            return 'Ищу'
        }
    }
    // обработка нажатия на услолвия использования
    const onPolicyPress = () => {
        authRegisterStore.openUsingPolicy();
    }
  //  const selectedCity = typeof user.city === 'object' ? user.city.id : user.city;

    if (user) {
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#000', overflow: 'visible' }}>
          <LayoutComponentsHeader title="Регистрация"/>
          <KeyboardAwareScrollView
            keyboardShouldPersistTaps="never"
            enableResetScrollToCoords={false}
            contentContainerStyle={styles.container}
            keyboardOpeningTime={1}
            bounces={false}
            extraScrollHeight={100}>
              <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={onTakePhotoPressed}
                  style={[
                      {width: wW - 120, height: wW - 120},
                      styles.profilePhotoContainer,
                  ]}>
                  {!Boolean(photo) && (
                      <>
                          {isPhotoLoading ? (
                              <ActivityIndicator size="large" color="#ffbd21" />
                          ) : (
                              <Image source={icons.plus} style={styles.iconStyle} />
                          )}
                          <Text style={styles.profilePhotoText}>Загрузить фото</Text>
                      </>
                  )}
                  {photo != null && (
                      <Image
                          source={{uri: photo.uri}}
                          style={{width: '100%', height: '100%', borderRadius: 18}}
                      />
                  )}
              </TouchableOpacity>
            <ListItem title="ВВЕДИТЕ ВАШ НИКНЕЙМ*">
              <Image source={icons.username}/>
              <TextInput style={[styles.pickerInput, { paddingRight: 20, marginLeft: 12, flex: 1,
                                ...fontFamily( user.login ?  'fontBaseMedium': 'fontBaseRegular')}]}
                         value={user.login}
                         placeholderTextColor="#fff"
                         onChangeText={onLoginChange}/>
            </ListItem>
            {/*<ListItem title="ГОРОД*">*/}
            {/*  <Image source={icons.location}/>*/}
            {/*  <RNPickerSelect*/}
            {/*    doneText="Готово"*/}
            {/*    placeholder={{ label: 'Выбор города...' }}*/}
            {/*    value={selectedCity}*/}
            {/*    style={pickerStyles}*/}
            {/*    onValueChange={onCityChange}*/}
            {/*    items={cities.map((city: City) => ({ label: city.title, value: city.id }))}/>*/}
            {/*</ListItem>*/}
            <ListItem title="КТО ВЫ*">
              <Image source={icons.gender}/>
              <RNPickerSelect
                doneText="Готово"
                placeholder={{ label: 'Кто вы...' }}
                value={gender}
                style={pickerStyles}
                onValueChange={onGenderChange}
                items={GendersList}/>
            </ListItem>
              {profiles.map((profile, index) =>
                  <View key={index}>
                      {profiles.length === 2 &&
                      <View style={{ height: 30, alignItems: 'flex-end', paddingRight: 20 }}>
                        <Text style={styles.listItemContent}>{profile.gender === 'M' ? 'Мужчина' : 'Женщина'}</Text>
                      </View>
                      }
                      <AuthComponentsProfileForm
                          isRegister
                          profile={profile}/>
                  </View>
              )}
              <ListItem title={iLookFor().toUpperCase() + ' *'}>
                  <Image source={icons.gender}/>
                  <RNPickerSelect
                      placeholder={{ label: `${iLookFor()}...` }}
                      value={user.purposeGender}
                      style={pickerStyles}
                      onValueChange={onPurposeGenderChange}
                      items={PurposeGenderForPicker} />
              </ListItem>

            {/*<ListItem title="СЕМЕЙНОЕ ПОЛОЖЕНИЕ">*/}
            {/*  <Image source={icons.marriage}/>*/}
            {/*  <RNPickerSelect*/}
            {/*    doneText="Готово"*/}
            {/*    placeholder={{ label: 'Выбор семейного положения...' }}*/}
            {/*    value={user.familyStatus}*/}
            {/*    style={pickerStyles}*/}
            {/*    onValueChange={onFamilyStatusChange}*/}
            {/*    items={FamilyStatus}/>*/}
            {/*</ListItem>*/}
            {/*<ListItem title="ДЕТИ">*/}
            {/*  <Image source={icons.kids}/>*/}
            {/*  <RNPickerSelect*/}
            {/*    placeholder={{ label: 'Наличие детей...' }}*/}
            {/*    value={user.hasChildren}*/}
            {/*    style={pickerStyles}*/}
            {/*    onValueChange={onChildrenChange}*/}
            {/*    items={childrenOptions} />*/}
            {/*</ListItem>*/}
            {/*<TouchableOpacity activeOpacity={1}*/}
            {/*                  onPress={() => { descriptionInputRef.current.focus(); }}>*/}
            {/*  <ListItem title="РАССКАЖИТЕ О СЕБЕ">*/}
            {/*    <View style={{ alignSelf: 'flex-start', marginTop: 13 }}>*/}
            {/*      <Image source={icons.info} style={{ marginRight: 12 }}/>*/}
            {/*    </View>*/}
            {/*    <View style={{ flexDirection: 'row' }}>*/}
            {/*      <TextInput*/}
            {/*        ref={(input) => { descriptionInputRef.current = input; }}*/}
            {/*        multiline={true}*/}
            {/*        value={user.description}*/}
            {/*        onChangeText={onDescriptionTextChange}*/}
            {/*        placeholder="О себе"*/}
            {/*        style={descriptionStyles}/>*/}
            {/*      <Text style={descriptionCounterStyles}>*/}
            {/*        {`${ user.description ? user.description.length : 0}/200`}*/}
            {/*      </Text>*/}
            {/*    </View>*/}
            {/*  </ListItem>*/}
            {/*</TouchableOpacity>*/}
            {/*<ListItem title="ЦЕЛЬ ЗНАКОМСТВА *">*/}
            {/*  <Image source={icons.targetWhite}/>*/}
            {/*  <RNPickerSelect*/}
            {/*    placeholder={{ label: 'Цель знакомства...' }}*/}
            {/*    value={user.purposeDating}*/}
            {/*    style={pickerStyles}*/}
            {/*    onValueChange={onPurposeDatingChange}*/}
            {/*    items={PurposeDatingForPicker} />*/}
            {/*</ListItem>*/}
            {/*<ListItem title="ИНТЕРЕСУЮЩИЙ ВОЗРАСТ"*/}
            {/*          postTitle={`от ${user.interestingAgeMin} до ${user.interestingAgeMax}`}*/}
            {/*          withoutBorder={true}>*/}
            {/*  <UDSlider*/}
            {/*    values={[user.interestingAgeMin, user.interestingAgeMax]}*/}
            {/*    onValuesChange={onInterestedYearsValuesChange}*/}
            {/*    sliderLength={sliderWidth}*/}
            {/*    min={18}*/}
            {/*    max={80}/>*/}
            {/*</ListItem>*/}
              <View style={styles.policyAcceptCont}>
                <TouchableOpacity style={styles.policyAcceptCheckbox}
                                  activeOpacity={0.8}
                    onPress={() => {
                        setIfPolicyAccepted(!ifPolicyAccepted)
                    }}
                >
                    {ifPolicyAccepted && (
                        <Image style={styles.checkMark} source={require('../../../../icons/checkmark.png')}/>
                    )}
                </TouchableOpacity>
                  <View style={styles.policyAcceptTextCont}>
                      <Text style={styles.policyAcceptDescription}>
                          Я подтверждаю согласие&nbsp;
                          <Text style={styles.policyAcceptLink}
                            onPress={onPolicyPress}
                          >
                              с условиями использования
                          </Text>
                          &nbsp;приложения
                      </Text>
                  </View>
              </View>
            <UDButton onPress={onNextPressed}
                      title="Продолжить"
                      style={{ marginHorizontal: 40, marginVertical: 15 }}
                   //   disabled={!ifPolicyAccepted || !photo}
                      disabled={!ifPolicyAccepted}
            />
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )
    }

    return null;
  }
));

export default AuthScreensProfileFirstStep;

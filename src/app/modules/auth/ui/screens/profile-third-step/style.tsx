import {Dimensions, StyleSheet, Platform} from 'react-native';


const isMediumScreen = Dimensions.get('window').height < 700;
const isSmallScreen = Dimensions.get('window').height < 600;
import fontFamily from '../../../../../styles/fonts';

export const activeColor = '#ffbd21';
export const inactiveColor = '#ffffff33';

export default StyleSheet.create({
  container: {
    backgroundColor: '#000',
  //  paddingLeft: 20,
    paddingBottom: 20,
    paddingTop: 25,

   // borderBottomColor: inactiveColor,
   // borderBottomWidth: 1,
  },
  containerCont: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  profilePhotoContainer: {
    backgroundColor: '#252525',
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  profilePhotoText: {
    color: '#FFBD21',
    fontSize: 16,
   // backgroundColor: '#aae',
    textAlign: 'center',
    paddingTop: 10,
    // position: 'absolute',
    // bottom: 30,
    ...fontFamily('fontBaseMedium'),
  },
  iconStyle: {
    resizeMode: 'contain',
    height: 46
  },
  cancelContainer: {
    flexDirection: 'row',
    paddingVertical: 24,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 20,
  },
  cancelText: {
    color: '#fff',
    marginLeft: 8,
    ...fontFamily('fontBaseMedium'),
    fontSize: 16
  },
  needPhotoMessage: {
    color: '#e84747',
    ...fontFamily('fontTitleBold'),
    fontSize: 12,
    textAlign: 'center',
    marginTop: 10
  },
  descriptCont: {
    marginTop: 30,
    marginHorizontal: 20,
    borderWidth: 0.5,
    borderColor: Platform.OS === 'ios' ? 'rgb(135,135,135)' : 'rgb(74,74,74)',
    borderRadius: 18,
    paddingTop: 20,
    paddingBottom: 20,
    paddingHorizontal: 16,
    flexDirection: 'row',
  },
  descriptTextCont: {
    flexDirection: 'column',
    marginLeft: 16,
  },
  iconUploadPhotoDesc: {
    // resizeMode: 'contain',
    maxHeight: 24,
    maxWidth: 24,
  },
  text: {
    textAlign: 'left',
  //  display: 'flex',
   // alignSelf: 'center',
   // marginTop: isMediumScreen ? 15 : 20,
  //  marginRight: 20,
   // marginHorizontal: 10,
    ...fontFamily('fontBaseMedium'),
    fontSize: isMediumScreen ? 12 : 14,
    lineHeight: 20,
   // flexWrap: 'wrap',
    color: '#fff'
  },
  secondText: {
    marginTop: 10,
    textAlign: 'left',
    ...fontFamily('fontBaseMedium'),
    fontSize: isMediumScreen ? 11 : 12,
    lineHeight: 14.6,
    // flexWrap: 'wrap',
    color: '#8C8C8C'
  },
});


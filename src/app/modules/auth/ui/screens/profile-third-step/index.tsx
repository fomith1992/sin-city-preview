import React, {useCallback, useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';

import styles from './style';
import icons from '../../../../icons';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';

import {TYPES} from '../../../../../store/store-types';
import {AuthRegisterStore} from '../../../store/auth-register';
import Navigation from 'react-navigation-mobx-helpers/dist';
import {showMessage} from 'react-native-flash-message';
import {useFocusEffect} from 'react-navigation-hooks';

const wW = Dimensions.get('screen').width;

type props = {
  authRegisterStore: AuthRegisterStore;
  navigationStore: Navigation;
};

const AuthScreensProfileThirdStep = inject(
  TYPES.AuthRegister,
  TYPES.Navigation,
)(
  observer(({authRegisterStore, navigationStore}: props) => {
    const {photo, isPhotoLoading} = authRegisterStore;
    const onTakePhotoPressed = useCallback(
      () => authRegisterStore.selectPhoto(),
      [],
    );
    const onClearPhotoPressed = useCallback(
      () => authRegisterStore.clearPhoto(),
      [],
    );
    const onBackPressed = useCallback(() => navigationStore.pop(), []);
    const onNextPressed = useCallback(() => {
      authRegisterStore.nextFromPhotoScreen();
      window.fLogEvent('registration_submitForm', 'reg_screen_3');
    }, []);
    const keyboardB = Platform.OS === 'ios' ? 'padding' : undefined;

    const onScreenFocus = useCallback(() => {
      window.fLogEvent('registration_viewScreen', 'reg_screen_3');
    }, []);

    useFocusEffect(onScreenFocus);

    return (
      <SafeAreaView
        style={{flex: 1, backgroundColor: '#000', overflow: 'visible'}}>
        <Header title="Регистрация" onBack={onBackPressed} />
        <KeyboardAvoidingView
          behavior={keyboardB}
          style={{flex: 1, backgroundColor: '#000', overflow: 'visible'}}>
          <ScrollView
            bounces={false}
            keyboardDismissMode={'on-drag'}
            style={[styles.container, {flex: 1, maxHeight: 650}]}
            contentContainerStyle={styles.containerCont}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={onTakePhotoPressed}
              style={[
                {width: wW - 120, height: wW - 120},
                styles.profilePhotoContainer,
              ]}>
              {!Boolean(photo) && (
                <>
                  {isPhotoLoading ? (
                    <ActivityIndicator size="large" color="#ffbd21" />
                  ) : (
                    <Image source={icons.plus} style={styles.iconStyle} />
                  )}
                  <Text style={styles.profilePhotoText}>Загрузить фото</Text>
                </>
              )}
              {photo != null && (
                <Image
                  source={{uri: photo.uri}}
                  style={{width: '100%', height: '100%', borderRadius: 18}}
                />
              )}
            </TouchableOpacity>
            {/*{Boolean(photo) &&*/}
            {/*<TouchableOpacity*/}
            {/*  onPress={onClearPhotoPressed}*/}
            {/*  style={styles.cancelContainer}>*/}
            {/*  <Image source={icons.cancelSelected}/>*/}
            {/*  <Text style={styles.cancelText}>Изменить</Text>*/}
            {/*</TouchableOpacity>}*/}
            <View style={styles.descriptCont}>
              <Image
                source={icons.upload_photo_desc}
                style={styles.iconUploadPhotoDesc}
              />
              <View style={styles.descriptTextCont}>
                <Text style={styles.text}>
                  Чтобы продолжить, загрузите свое настоящее фото, тогда вы
                  сможете подтвердить свой статус.
                </Text>
                <Text style={styles.secondText}>
                  С подтвержденными
                  участниками охотнее знакомятся, а также вы получите доступ к
                  таким же реальным аккаунтам.
                </Text>
              </View>
            </View>
          </ScrollView>

          <UDButton
           // title={!Boolean(photo) ? 'Загрузить фото' : "Продолжить"}
            title={"Продолжить"}
           // onPress={!Boolean(photo) ? onTakePhotoPressed : onNextPressed}
            onPress={onNextPressed}
            style={{marginHorizontal: 40, marginVertical: 15}}
            disabled={isPhotoLoading}
          />
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }),
);

export default AuthScreensProfileThirdStep;

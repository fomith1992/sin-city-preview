import React, { useEffect } from 'react';
import { inject, observer } from "mobx-react";
import { ActivityIndicator } from 'react-native';
import AuthComponentsScreenBg from "../../components/screen-bg";
import SplashScreen from "react-native-splash-screen";
import { TYPES } from "../../../../../store/store-types";
import { AuthUserStore } from "../../../store/auth-user";

type props = {
  authUserStore: AuthUserStore
}

const AuthScreensAuthChecking = inject(TYPES.AuthUser)
(observer(({ authUserStore }: props) => {
  useEffect(() => {
    authUserStore.checkAuthorization();
    SplashScreen.hide();
  }, []);
  return (
    <>
      <AuthComponentsScreenBg/>
      <ActivityIndicator size="large" style={{ flex: 1 }} color="#ffbd21"/>
    </>
  );
}));

export default AuthScreensAuthChecking;
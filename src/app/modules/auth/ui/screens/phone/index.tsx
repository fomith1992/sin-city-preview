import React, { useCallback, useState } from 'react';
import { inject, observer } from "mobx-react";
import {
  SafeAreaView,
  Text,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import IMask from 'imask';

import styles from './style';
import fontFamily from '../../../../../styles/fonts';
import UDButton from '../../../../ud-ui/components/ud-button';
import AuthComponentsScreenBg from "../../components/screen-bg";
import { AuthEnterStore } from "../../../store/auth-enter";
import { TYPES } from "../../../../../store/store-types";
import LayoutStore from "../../../../layout/store/LayoutStore";
import { useFocusEffect } from "react-navigation-hooks";
import * as screenNames from '../../../../navigation/screen-names';

const phoneMask = IMask.createMask({
  mask: '+{7} (000) 000-00-00'
});

type props = {
  authEnterStore: AuthEnterStore;
  layoutStore: LayoutStore;
}

const AuthScreensPhone = inject(TYPES.AuthEnter, TYPES.Layout)
(observer(({ authEnterStore, layoutStore }: props) => {
  const [phone, setPhone] = useState('+7');
  const [isPhoneSentOnce, setPhoneSentOnce] = useState(false);
  const onButtonPressed = useCallback((event) => {
    window.fLogEvent('onboarding_clickGetCode');
    setPhoneSentOnce(true);
    Keyboard.dismiss();
    authEnterStore.requestCode(phone);
    layoutStore.showOverflowLoader();
  }, [phone]);

  const onScreenFocus = useCallback(() => {
    return () => Keyboard.dismiss()
  }, []);

  useFocusEffect(onScreenFocus);

  const onPhoneChanged = useCallback((phoneNumber) => setPhone(phoneMask.resolve(phoneNumber)), []);

  return (
    <>
      <AuthComponentsScreenBg/>
      <KeyboardAvoidingView style={{ flex: 1 }}
                            keyboardVerticalOffset={10}
                            enabled={true}
                            behavior={Platform.OS === "ios" ? 'padding' : 'height'}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <SafeAreaView style={[styles.container]}>
            <Text style={[fontFamily('fontTitleBold'), styles.title]}>Регистрация</Text>
            <Text style={[fontFamily('fontBaseMedium'), styles.subtitle]}>ВАШ НОМЕР ТЕЛЕФОНА</Text>
            <TextInput
              autoFocus={true}
              keyboardType="numeric"
              style={[fontFamily('fontBaseMedium'), styles.input]}
              onChangeText={onPhoneChanged}
              value={phone}/>
            <UDButton
              title="Получить код"
              isAnimated={true}
              disabled={phone.length < 18}
              onPress={onButtonPressed}
              isHidden={!isPhoneSentOnce && phone.length < 18}
              style={styles.button}/>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </>
  );
}));

export default AuthScreensPhone;

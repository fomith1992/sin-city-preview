import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'center',
    marginHorizontal: 40,
  },
  title: {
    color: '#fff',
    fontSize: 24,
    marginBottom: 30
  },
  subtitle: {
    ...fontFamily('fontBaseMedium'),
    color: '#999999',
    fontSize: 12,
    marginBottom: 10,
    textAlign: 'center'
  },
  repeatCode: {
    height: 40,
    marginBottom: 30,
    lineHeight: 20,
    paddingHorizontal: 6,
    ...fontFamily('fontBaseMedium'),
    color: '#999999',
    fontSize: 12,
    textAlign: 'center'
  },
  input: {
    marginBottom: 90,
    height: 49,
    borderColor: 'rgb(151, 151, 151)',
    borderWidth: 1,
    ...fontFamily('fontBaseMedium'),
    color: '#000',
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderRadius: 22,
    textAlign: 'center',
    fontSize: 16,
    letterSpacing: 0
  },
  button: {
    alignSelf: 'stretch',
    // marginBottom: 100
  }
})
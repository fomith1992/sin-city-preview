import { StyleSheet } from 'react-native';

export const activeColor = '#ffbd21';
export const inactiveColor = '#ffffff33';

export default StyleSheet.create({
  marker: {
    height: 19,
    width: 19,
    backgroundColor: '#fff'
  },
})
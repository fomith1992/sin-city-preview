import React from 'react';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

import styles, { activeColor, inactiveColor } from './style';

interface props {
  values: number[]
  onValuesChange: any
  sliderLength: number
  min: number
  max: number
}

const UDSlider = ({ values, onValuesChange, sliderLength, min, max }: props) => {
  return (
    <MultiSlider
          values={values}
          sliderLength={sliderLength}
          min={min}
          max={max}
          step={1}
          snapped
          selectedStyle={{ backgroundColor: activeColor }}
          unselectedStyle={{ backgroundColor: inactiveColor}}
          markerStyle={styles.marker}
          containerStyle={{ paddingTop: 25, paddingHorizontal: 10 }}
          onValuesChange={onValuesChange}/>
  );
};

export default UDSlider;
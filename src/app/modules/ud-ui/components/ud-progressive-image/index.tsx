import React, { useState, useRef } from 'react';
import { ActivityIndicator, Animated, View, ImageResizeMode, StyleSheet } from 'react-native';

import { Photo } from '../../../upload/domain/interfaces/Photo';
import FastImage from 'react-native-fast-image';

const AnimatedFastImage = Animated.createAnimatedComponent(FastImage);

const UDProgressiveImage = ({ photo, resizeMode, style }: { photo: Photo, resizeMode: ImageResizeMode, style: any }) => {
  const thumbnailOpacity = useRef(new Animated.Value(0));
  const [thumbnailLoaded, setThumbnailLoaded] = useState(false);
  const [imageLoaded, setImageLoaded] = useState(false);
  const onLoad = () => {
    setImageLoaded(true);
    Animated.timing(thumbnailOpacity.current, {
      toValue: 0,
      duration: 300
    }).start();
  }

  const onThumbnailLoad = () => {
    setThumbnailLoaded(true);
    Animated.timing(thumbnailOpacity.current, {
      toValue: 1,
      duration: 300
    }).start();
  }

  return (<View style={style}>
    { !(thumbnailLoaded || imageLoaded) && <ActivityIndicator size='large' style={{ ...StyleSheet.absoluteFillObject, zIndex: 122 }}/>}
    <AnimatedFastImage style={[{ opacity: thumbnailOpacity.current, position: 'absolute' }, style]}
                    onLoad={onThumbnailLoad}
                    source={{ uri: photo.photoThumbnailUrl }} 
                    resizeMode={resizeMode}/>
    <AnimatedFastImage onLoad={onLoad} 
                    source={{ uri: photo.photoUrl }}
                    resizeMode={resizeMode}
                    style={[style]}/>
  </View>);
};

export default UDProgressiveImage;
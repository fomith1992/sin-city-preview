import React from 'react';
import { Appearance } from 'react-native-appearance';
import DateTimePicker from 'react-native-modal-datetime-picker';

type Mode = "date" | "time" | "datetime";

type props = { 
  mode?: Mode, 
  isVisible: boolean, 
  date: Date 
  onConfirm: any
  onCancel: any
  minimumDate?: any
  maximumDate?: any
}

const UDDateTimePicker = ({ mode = 'date', isVisible, date, onConfirm, onCancel, minimumDate, maximumDate }: props) => {
  const colorScheme = React.useRef(Appearance.getColorScheme());
  return (
    <DateTimePicker
      isVisible={isVisible}
      mode={mode}
      maximumDate={maximumDate}
      minimumDate={minimumDate}
      is24Hour={true}
      locale='ru_RU'
      headerTextIOS="Выберите"
      confirmTextIOS="Установить"
      cancelTextIOS="Отмена"
      isDarkModeEnabled={colorScheme.current === 'dark'}
      date={date} 
      onConfirm={onConfirm}
      onCancel={onCancel}/>
  );
};

export default UDDateTimePicker;
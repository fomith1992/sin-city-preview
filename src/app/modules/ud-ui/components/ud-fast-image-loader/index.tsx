import React, { useState } from 'react';
import { ActivityIndicator, View, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';

const UDFastImageLoader = (props: any) => {
  const [loading, setLoading] = useState(false);
  const indicatorStyles = props.indicatorStyle ? props.indicatorStyle : {};
  return (
    <View style={{ width: props.width, height: props.height, position: 'relative' }}>
      { loading && (
        <ActivityIndicator size="large" color={'#fff'} style={[styles.indicator, indicatorStyles]} />
      ) }
      <FastImage {...props}
                 onLoadStart={() => setLoading(true)}
                 onLoadEnd={() => setLoading(false)}>
      </FastImage>
    </View>
  )
};

export default UDFastImageLoader;

const styles = StyleSheet.create({
  indicator: {
    position: 'absolute',
    zIndex: 1,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
});
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  touchable: {
    backgroundColor: 'rgb(255, 189, 33)',
    paddingVertical: 15,
    borderRadius: 22,
    alignItems: 'center',
    justifyContent: 'center'
  },
  touchableDisabled: {
    backgroundColor: '#555555'
  },
  apply: {
    backgroundColor: 'transparent',
    borderColor: 'rgb(255, 189, 33)',
    borderWidth: 2,
  }
})

import React, { ReactNode } from 'react';
import { TouchableOpacity, Text, View, Animated, Image } from 'react-native';
import PropTypes from "prop-types";
import { useAnimation } from 'react-native-animation-hooks';

import fontFamily from '../../../../styles/fonts';

interface UDButtonProps {
  onPress: any
  title?: string
  style?: any
  icon?: number
  fontSize?: number
  disabled?: boolean
  isHidden?: boolean,
  isAnimated?: boolean,
  isApply?: boolean
}

import styles from "./style";

const AnimatedButton = Animated.createAnimatedComponent(TouchableOpacity);

const UDButton =  React.memo(({ onPress, title, style, icon, fontSize, disabled, isHidden, isAnimated, isApply }: UDButtonProps) => {
  let opacity;
  if (isAnimated) {
    opacity = useAnimation({
      type: 'timing',
      initialValue: 0,
      toValue: isHidden ? 0 : 1,
      duration: 180,
      useNativeDriver: true,
    });
  } else {
    opacity = 1;
  }

  if (isHidden) {
    return <View style={{ height: 47 }}/>;
  }

  const btnStyles = [{ flexDirection: icon ? 'row' : 'column', opacity: opacity }, styles.touchable, style];

  if (disabled) {
    btnStyles.push(styles.touchableDisabled);
  }
  if (isApply) {
    btnStyles.push(styles.apply);
  }

  return (
    <AnimatedButton
      onPress={onPress}
      activeOpacity={0.8}
      disabled={disabled}
      style={btnStyles}>
      {icon && <Image style={{ marginRight: Boolean(title) ? 6 : 0 }} source={icon}/>}
      <Text style={[{ textAlign: 'center', ...fontFamily('fontTitleBold'), ...{ fontSize }}, isApply && {color: 'rgb(255, 189, 33)'}]}>{title}</Text>
    </AnimatedButton>
  );
});

(UDButton as any).propTypes = {
  onPress: PropTypes.func,
  title: PropTypes.string,
  style: PropTypes.any,
  icon: PropTypes.any,
  fontSize: PropTypes.number,
  disabled: PropTypes.bool,
  isHidden: PropTypes.bool,
  isAnimated: PropTypes.bool
};

export default UDButton;

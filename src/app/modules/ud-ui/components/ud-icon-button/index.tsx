import React, { useCallback } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';

import styles, { activeColor } from './style';

type props = {
  index: number
  image: any
  onPress: Function
  active: boolean
  width?: number
  height?: number
};

const UDIconButton = ({ index, image, onPress, active, width, height }: props) => {
  const backgroundColor = active ? activeColor : '#fff';
  const onPressed = useCallback(() => onPress(index), [onPress, index]);
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPressed}
      style={[{ backgroundColor }, styles.touchable]}>
      { width != null && height != null && (
        <FastImage
          source={image}
          style={{ width, height }}
          resizeMode={FastImage.resizeMode.stretch}/>
      ) }
      { (width == null || height == null) && (
        <Image
          source={image}
          resizeMode="stretch"/>
      ) }
    </TouchableOpacity>
  );
};

export default UDIconButton;

import { StyleSheet } from 'react-native';

export const activeColor = '#ffbd21';
export const inactiveColor = '#ffffff33';

export default StyleSheet.create({
  touchable: {
    overflow: 'hidden',
    borderRadius: 12,
    width: 60,
    height: 60,
    marginRight: 8,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
});


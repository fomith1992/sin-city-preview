import React from 'react';
import { TouchableWithoutFeedback, Keyboard, View } from 'react-native';

export const UDDismissKeyboardHOC = (Comp: any) => {
  return ({ children, ...props }: any) => (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <Comp {...props}>
        {children}
      </Comp>
    </TouchableWithoutFeedback>
  );
};
export const UDDismissKeyboardView = UDDismissKeyboardHOC(View);
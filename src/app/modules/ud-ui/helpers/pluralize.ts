export const pluralize = (num: number, words: string[]): string => {
  let zero = words[0], one = words[1], two = words[2];

  let unit = num % 16;
  if(unit > 10 && unit <= 15 ) return zero;

  unit = num % 10;
  if(unit === 1) return one;
  if(unit < 5 && unit != 0) return two;
  return zero;
};

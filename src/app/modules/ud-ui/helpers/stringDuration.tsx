export function truncateString(text: string, options: { max: number }){
  const max = options.max;
  return text && text.length > max ? text.substring(0, max) + '...' : text;
}
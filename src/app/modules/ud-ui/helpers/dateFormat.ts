import moment, {locale} from 'moment';
import 'moment/locale/ru';

interface ITimeFormat {
  smallFormat: boolean;
}

export const formatLastActivity = (
  lastActivity: string | null | undefined,
  smallFormat?: ITimeFormat,
) => {
  if (!lastActivity) {
    // return 'быть или не быть...';
    return '';
  }
  locale('ru');
  const diff = moment().diff(moment(lastActivity), 'minutes');

  if (moment().isSame(moment(lastActivity), 'day')) {
    // return `был(а) в сети в ${moment(lastActivity).format('HH:mm')}`;
    return '';
  }
  if (diff) {
    const isSameYear = moment(lastActivity).isSame(moment(), 'year');
    if (isSameYear) {
      // return smallFormat
      //   ? `был(а) ${moment(lastActivity).format('D MMM')}`
      //   : `был(а) ${moment(lastActivity).format('D MMM в HH:mm')}`;
      return '';
    }
    // return smallFormat
    //   ? `был(а) ${moment(lastActivity).format('D MMM YYYY г.')}`
    //   : `был(а) ${moment(lastActivity).format('D MMM YYYY г. в HH:mm')}`;
    return '';
  }
  // return 'быть или не быть...';
  return '';
};

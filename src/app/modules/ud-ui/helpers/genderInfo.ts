import { GenderFirstLetter } from '../../user/domain/enums/Gender';

export const getProfileInfoString = (profiles: any) => profiles.map(({ gender, age }: any) => `${GenderFirstLetter[gender]}(${age})`).join(' & ');

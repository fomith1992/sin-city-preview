import IMask from 'imask';

const phoneMask = IMask.createMask({
  mask: '+{7} 000 000 00 00'
});

export const beautifyPhoneNumber = (number: string): string => {
  if (typeof number === "string") {
    return phoneMask.resolve(number);
  } else {
    return ''
  }
};
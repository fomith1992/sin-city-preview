import React, { useCallback } from 'react';
import { View } from 'react-native';
import styles from './style';
import Pdf from 'react-native-pdf';
import Header from "../../../layout/ui/components/header";

type props = {
  navigation: any
}

const PdfViewer = ({ navigation }: props) => {
    const onBackPressed = useCallback(() => {
      navigation.goBack();
    }, []);

    return (
      <View style={styles.container}>
        <View style={styles.headerBg}>
          <Header title={""} onBack={onBackPressed}/>
        </View>
        <Pdf
          source={navigation.getParam('url')}
          style={styles.pdf}
          activityIndicator={true}
        />
      </View>
  )
};

export default PdfViewer;

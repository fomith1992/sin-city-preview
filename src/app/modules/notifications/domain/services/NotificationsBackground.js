import Notifications from './Notifications';

export default async (message) => {
    if (!!Notifications.displayMessage) {
        console.log('if Not.DisMes_pos')
        await Notifications.displayMessage(message);
    } else {
        console.log('if Not.DisMes_neg')
    }
}

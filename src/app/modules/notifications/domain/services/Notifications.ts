import { Device } from './../interfaces/Device';
import firebase from 'react-native-firebase';
import devicesRepository from '../repositories/DevicesRepository';
import { getUniqueId } from 'react-native-device-info';
import { Alert, Linking, Platform } from 'react-native';
import * as _ from 'lodash';
import AsyncStorage from "@react-native-community/async-storage";
import * as screenNames from '../../../navigation/screen-names';

import RootStore from '../../../../rootStore';
import navigation from "../../../auth/navigation";
import {MESSAGES_TAB} from "../../../navigation/screen-names";

const isIOS = Platform.OS === 'ios';
const PERMISSIONS_REQUESTED_ST_KEY = 'push_requested';

const NOTIFICATION_EVENTS = {
  MESSAGE: 'MESSAGE',
  LIKE: 'LIKE',
  FAVORITE_ADD_PHOTO: 'FAVORITE_ADD_PHOTO',
  FAVORITE_IS_ONLINE: 'FAVORITE_IS_ONLINE',
  FAVORITE_CHANGE_AVATAR: 'FAVORITE_CHANGE_AVATAR',
  FAVORITE_CREATE_EVENT: 'FAVORITE_CREATE_EVENT',
  FAVORITE_MARK_ON_MAP: 'FAVORITE_MARK_ON_MAP',
  VERIFIED_OK: 'VERIFIED_OK',
  VERIFIED_NO_OK: 'VERIFIED_NO_OK',
  PAYMENT_NO_OK: 'PAYMENT_NO_OK',
  PAYMENT_OK: 'PAYMENT_OK',
};

class Notifications {
  private onTokenRefresh: any;
  private onNotificationDisplayed: any;
  private onNotificationOpened: any;
  private onNotification: any;
  private onMessage: any;
  private device: any;
  private rootStore: RootStore | null = null;
  public token: string = '';

  init(rootStore: RootStore) {
    // firebase.analytics().logEvent("MyTestEventXiaoMao");
    // Alert.alert('Sent');
   // console.log('Notifications_init')
    this.rootStore = rootStore;
    this.hasPermissions()
      .then((enabled) => {
      //  console.log("hasPermissions", enabled)
      //  firebase.analytics().logEvent()
        if (enabled) {
          this.register();
        } else {
          this.refresh();
        }
      });
  }

  hasPermissions() {
    return firebase.messaging().hasPermission();
  }

  /**
   * @return {Promise<boolean>}
   */
  async refresh() {
    if (await this.hasPermissions()) {
      return true;
    }
    if (isIOS) {
      try {
        await firebase.messaging().requestPermission();
      } catch (error) {
        const isPermissionsRequestedValue = await AsyncStorage.getItem(PERMISSIONS_REQUESTED_ST_KEY);
        const isPermissionsRequested = isPermissionsRequestedValue != null;
        if (!isPermissionsRequested) {
          Alert.alert(
            'Уведомления отключены',
            'Включите уведомления, чтобы не пропустить действия других пользователей',
            [
              {
                text: 'Открыть настройки',
                onPress: Linking.openSettings
              },
              {
                text: 'Отмена'
              }
            ]
          );
          AsyncStorage.setItem(PERMISSIONS_REQUESTED_ST_KEY, 'true');
        }
        return false;
      }
    }

    this.register();
    return true;
  }

  public checkIsNotificationTapped() {
    return firebase.notifications().getInitialNotification().then(open => {
     // console.log('checkIsNotificationTapped');
      if (open) {
        this.notificationTapHandle(open.notification);
      }
    });
  }

  destroy() {
    // this.notificationListener();
  }

  async kill() {
    await devicesRepository.delete({ id: this.device.registrationId });
  }

  async destroyFCMToken() {
    if (this.token) {
      await devicesRepository.deleteDevice(this.token)
      this.token = '';
      this.device = null;
      firebase.notifications().removeAllDeliveredNotifications();
    }
  }

  getToken() {
    console.log('getToken');
    return firebase.messaging().getToken();
  }

  notificationTapHandle(notification: any) {
    const notificationData = notification.data;
 //   console.log('notficationTapHandle', notification.data);
    if (this.rootStore) {
      switch(notificationData.typeEvent) {
        case NOTIFICATION_EVENTS.FAVORITE_ADD_PHOTO:
        case NOTIFICATION_EVENTS.FAVORITE_CHANGE_AVATAR:
        case NOTIFICATION_EVENTS.FAVORITE_CREATE_EVENT:
        case NOTIFICATION_EVENTS.FAVORITE_IS_ONLINE: {
          this.rootStore.profileManager.openProfile(notificationData.userId);
          break;
        }
        case NOTIFICATION_EVENTS.LIKE: {
          if (notificationData.fulfilled === 'false') {
            this.rootStore.profileManager.openConnectionsWithFieldsError();
          } else {
            this.rootStore.profileManager.openProfile(notificationData.userId);
          }
          break;
        }
        case NOTIFICATION_EVENTS.FAVORITE_MARK_ON_MAP: {
          // this.rootStore.profileManager.openProfile(notificationData.userId);
          if (notificationData.latitude && notificationData.longitude &&
              notificationData.latitude !== null && notificationData.longitude !== null) {
            console.log('notificationData',notificationData, notificationData.latitude !== null)
            this.rootStore.profileManager.openUserOnMap({latitude: Number(notificationData.latitude),
              longitude: Number(notificationData.longitude)})
            if (window.goToPushUser) {
              setTimeout(() => {
                window.goToPushUser();
              }, 600)
            }
          } else {
            this.rootStore.profileManager.openProfile(notificationData.userId);
          }

          break;
        }
        case NOTIFICATION_EVENTS.VERIFIED_OK:
          this.rootStore.verificationStore.verificationStatus = 'ready';
          this.rootStore.currentUserStore.setUserVerification(true);
          this.rootStore.navigationStore.navigate(screenNames.VERIFICATION_FIRST_STEP_SCREEN);
          break;
        case NOTIFICATION_EVENTS.VERIFIED_NO_OK:
          this.rootStore.verificationStore.verificationStatus = 'denied';
          this.rootStore.currentUserStore.setUserVerification(false);
          this.rootStore.navigationStore.navigate(screenNames.VERIFICATION_FIRST_STEP_SCREEN);
          break;
        case NOTIFICATION_EVENTS.MESSAGE: {
         // console.log('NOTIFICATION_EVENTS.MESSAGE', notificationData.sender)
        //  this.rootStore.navigationStore.navigate(screenNames.MESSAGES_TAB);
          this.rootStore.chatsStore.openChat(notificationData.sender, notificationData.chat, true);
          break;
        }
        case NOTIFICATION_EVENTS.PAYMENT_OK: {
          this.rootStore.currentUserStore.setUserPayment(true);
          break;
        }
        case NOTIFICATION_EVENTS.PAYMENT_NO_OK: {
          this.rootStore.currentUserStore.setUserPayment(false);
          break;
        }
      }
      firebase.notifications().cancelNotification(notification.notificationId);
    } else { setTimeout(() => { this.notificationTapHandle(notification); }, 100) };
  }

  notificationShowHandle(notification: any) {
   // console.log('NOTIFICATION_showHandle');
    const notificationData = notification.data;
    if (this.rootStore) {
      switch(notificationData.typeEvent) {
        case NOTIFICATION_EVENTS.VERIFIED_OK:
          this.rootStore.verificationStore.verificationStatus = 'ready';
          this.rootStore.currentUserStore.setUserVerification(true);
          break;
        case NOTIFICATION_EVENTS.VERIFIED_NO_OK:
          this.rootStore.verificationStore.verificationStatus = 'denied';
          this.rootStore.currentUserStore.setUserVerification(false);
          break;
        case NOTIFICATION_EVENTS.PAYMENT_OK: {
          this.rootStore.currentUserStore.setUserPayment(true);
          break;
        }
        case NOTIFICATION_EVENTS.PAYMENT_NO_OK: {
          this.rootStore.currentUserStore.setUserPayment(false);
          break;
        }
      }
    }
  }

  register() {
    const channel = new firebase.notifications.Android.Channel(
      'messages_channel',
      'Messages Channel',
      firebase.notifications.Android.Importance.Max)
      .setDescription('Notifications about new comments');

    firebase.notifications().android.createChannel(channel);

    firebase.messaging().ios.registerForRemoteNotifications();

    if (!this.onTokenRefresh) {
      this.onTokenRefresh = firebase.messaging().onTokenRefresh(token => {
        console.log('onTokenRefresh', token);
        this.updateToken(token);
      });
    }

    if (!this.onNotificationDisplayed) {
      this.onNotificationDisplayed = firebase.notifications().onNotificationDisplayed((notification) => {
        this.notificationShowHandle(notification)
      });
    }

    if (!this.onNotificationOpened) {
      this.onNotificationOpened = firebase.notifications().onNotificationOpened(open => {
        this.notificationTapHandle(open.notification);
      });
    }

    if (!this.onNotification) {
      //console.log('!onNotification');
      this.onNotification = firebase.notifications().onNotification(notification => {
        const local = new firebase.notifications.Notification()
          .setNotificationId(notification.notificationId)
          .setTitle(notification.title)
          .setBody(notification.body)
          .setData(notification.data)
          .android.setAutoCancel(true);

        local.android.setChannelId('messages_channel');
        local.android.setColor("#ffbd21");
        local.android.setSmallIcon("@drawable/ic_stat_email");

        return firebase.notifications()
          .displayNotification(local)
          .catch(e => console.log(e));
      })
    }

    this.updateToken();
  }

  private async findDevice(uniqueId: string) {
    let devices: { results: Device[] };
    try {
      devices = await devicesRepository.load();
    } catch(e) { }
    return devices.results.find(device => device.deviceId === uniqueId);
  }

  async updateToken(token?: string) {
    this.device = await this.findDevice(getUniqueId());

    const oldToken = this.device?.registrationId;

    if (!token) {
      token = await firebase.messaging().getToken();
    }
    this.token = token;

    const data = {
      deviceId: getUniqueId(),
      type: Platform.OS,
      active: true,
      registrationId: token // token can be null
    };
   // console.log('deviceId', data.deviceId);
   // console.log('FCMToken:', token);

    if (!oldToken) {
      try { await devicesRepository.create(data); }
      catch(e) { console.log(e)}
    } else {
      try { await devicesRepository.updateDevice(oldToken, data); }
      catch(e) { console.log(e) }
    }
  }

}

const notifications = new Notifications();
export default notifications;

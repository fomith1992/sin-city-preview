import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../core/infrastructure/httpResource";

class NotificationResource extends BaseRestResource {
    constructor() {
        super(httpResource, 'notifications');
    }
}

const notificationResource = new NotificationResource();

export default notificationResource;
import { BaseRestResource } from "../../../../infrastructure/BaseRestResource";
import httpResource from "../../../core/infrastructure/httpResource";

class DevicesResource extends BaseRestResource {
    constructor() {
        super(httpResource, 'devices', {});
    }
}

const devicesResource = new DevicesResource();

export default devicesResource;
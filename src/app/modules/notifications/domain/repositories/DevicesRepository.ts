import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import devicesResource from "../resources/DevicesResource";

class DevicesRepository extends BaseRepository {
  constructor() {
    super(devicesResource)
  }

  updateDevice(registration_id: string, data: any) {
    return this.resource(registration_id).patch(data)
      .then((response: any) => this.processResponse(response));
  }

  deleteDevice(registration_id: any) {
    return this.resource(registration_id).delete()
      .then((response: any) => this.processResponse(response));
  }
}

const devicesRepository = new DevicesRepository();

export default devicesRepository; 
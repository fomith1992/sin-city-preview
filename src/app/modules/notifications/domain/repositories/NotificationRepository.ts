import { BaseRepository } from "../../../../infrastructure/BaseRepository";
import notificationResource from "../resources/NotificationResource";

class NotificationRepository extends BaseRepository {
  constructor() {
    super(notificationResource)
  }
}

const notificationRepository = new NotificationRepository();

export default notificationRepository; 
export interface Push {
  isPushMessage: boolean
  isPushNewLike: boolean
  isPushFavorAddPhoto: boolean
  isPushFavorChangeAvatar: boolean
  isPushFavorCreateEvent: boolean
  isPushFavorMarkOnMap: boolean
  isPushFavorLoggedIn: boolean
}

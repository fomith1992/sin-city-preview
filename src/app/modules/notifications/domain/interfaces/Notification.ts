import { Photo } from './../../../upload/domain/interfaces/Photo';

export interface Notification {
  id: number,
  author: {
    id: number,
    login: string,
    photo: Photo,
    lastActivity: string,
    online: boolean,
    heFavorite: boolean
  },
  title: string,
  body: string,
  date: string
}
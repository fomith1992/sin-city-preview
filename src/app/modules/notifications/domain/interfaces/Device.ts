export interface Device{
  id: number,
  name: string,
  registrationId: string,
  deviceId: string,
  active: boolean,
  dateCreated: string,
  type: "android" | "ios"
}
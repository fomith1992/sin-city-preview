import { Push } from '../interfaces/Push';

export class PushFactory {
  constructor() { }

  static create(): Push {
    return {
      isPushFavorAddPhoto: false,
      isPushFavorChangeAvatar: false,
      isPushFavorCreateEvent: false,
      isPushFavorMarkOnMap: false,
      isPushMessage: false,
      isPushNewLike: false,
      isPushFavorLoggedIn: false
    }
  }
}

import { Notification } from './../domain/interfaces/Notification';
import { showMessage } from 'react-native-flash-message';
import { PushFactory } from '../domain/factories/PushFactory';
import { Push } from './../../notifications/domain/interfaces/Push';
import { observable, action, computed } from 'mobx';
import { TYPES, ITYPES } from '../../../store/store-types';
import { BaseStore } from './../../../store/BaseStore';
import userRepository from '../../user/domain/repositories/UserRepository';
import notificationRepository from '../domain/repositories/NotificationRepository';
import * as screenNames from '../../navigation/screen-names';
import UserSearchStore from '../../search/store/UsersSearchStore';

export class NotificationsStore extends BaseStore {
  private nextPage = 1;
  private isNextPageLoading = false;

  @observable notifications: Notification[] = [];

  @observable push: Push = PushFactory.create();

  private authStore: ITYPES['AuthUser'];
  private layout: ITYPES['Layout'];
  private navigation: ITYPES['Navigation'];
  private userSearchStore: UserSearchStore;
  private profileManager: ITYPES['ProfileManager'];

  storeInit() {
    this.authStore = this.root.resolve(TYPES.AuthUser);
    this.layout = this.root.resolve(TYPES.Layout);
    this.navigation = this.root.resolve(TYPES.Navigation);
    this.userSearchStore = this.root.usersSearchStore;
    this.profileManager = this.root.resolve(TYPES.ProfileManager);
  }

  @computed get pushesInfo(): any[] {
    return [
      { key: 'isPushMessage', label: 'Новые сообщения', isActive: this.push.isPushMessage },
      { key: 'isPushNewLike', label: 'Вашей анкете/фотографии поставили лайк', isActive: this.push.isPushNewLike },
      { key: 'isPushFavorCreateEvent', label: 'Избранный пользователь создал событие', isActive: this.push.isPushFavorCreateEvent },
      { key: 'isPushFavorMarkOnMap', label: 'Избранный пользователь появился на карте', isActive: this.push.isPushFavorMarkOnMap },
      { key: 'isPushFavorAddPhoto', label: 'Избранный пользователь добавил фотографии', isActive: this.push.isPushFavorAddPhoto },
      { key: 'isPushFavorChangeAvatar', label: 'Избранный пользователь изменил аватар', isActive: this.push.isPushFavorChangeAvatar },
      { key: 'isPushFavorLoggedIn', label: 'Избранный пользователь появился онлайн', isActive: this.push.isPushFavorLoggedIn },
    ]
  }
  private getPushSettings() {
    return {
      'new_message': this.parseOnOffFromBool(this.push.isPushMessage) ,
        'profile_like':  this.parseOnOffFromBool(this.push.isPushNewLike) ,
        'favorite_user_add_event':  this.parseOnOffFromBool(this.push.isPushFavorCreateEvent) ,
        'favorite_user_map':  this.parseOnOffFromBool(this.push.isPushFavorMarkOnMap) ,
        'favorite_user_add_photo':  this.parseOnOffFromBool(this.push.isPushFavorAddPhoto) ,
        'favorite_user_change_avatar': this.parseOnOffFromBool(this.push.isPushFavorChangeAvatar),
        'is_push_favor_logged_in': this.parseOnOffFromBool(this.push.isPushFavorLoggedIn)
    }
  }
  private parseOnOffFromBool(value) {
    return value ?  'on' : 'off'
  }
  savePush() {
    const { userId } = this.authStore;
    this.layout.showOverflowLoader();
    userRepository.updatePush(userId, this.push)
      .then(() => {
        window.fLogEventObj('push_clickSavePush',
          this.getPushSettings());
        showMessage({ message: 'Push-уведомления успешно сохранены', type: 'success' });
        this.navigation.pop();
      })
      .catch(() => {})
      .finally(() => { this.layout.hideOverflowLoader(); })
  }
  private getPushSettingName(value) {
    switch (value) {
      case 'isPushMessage': return 'new_message';
      case 'isPushNewLike': return 'profile_like';
      case 'isPushFavorCreateEvent': return 'favorite_user_add_event';
      case 'isPushFavorMarkOnMap': return 'favorite_user_map';
      case 'isPushFavorAddPhoto': return 'favorite_user_add_photo';
      case 'isPushFavorChangeAvatar': return 'favorite_user_change_avatar';
      case 'isPushFavorLoggedIn': return 'is_push_favor_logged_in';
      default: return value;
    }
  }
  @action
  updatePush(value: boolean, key: keyof Push) {
    window.fLogEventObj('push_changePush',
      {[this.getPushSettingName(key)]: this.parseOnOffFromBool(value)});
    this.push[key] = value;
  }

  @action
  loadPush() {
    const { userId } = this.authStore;
    this.layout.showOverflowLoader();
    userRepository.loadPush(userId)
      .then((push: Push) => { this.push = push; })
      .finally(() => { this.layout.hideOverflowLoader(); })
  }

  loadNotifications() {
    this.nextPage = 1;
    this.notifications = [];
    this.loadNextNotifications();
  }

  @action
  loadNextNotifications() {
    if (this.nextPage && !this.isNextPageLoading) {
      this.isNextPageLoading = true;
      notificationRepository.load({ page: this.nextPage})
        .then(({ next, results }) => {
          console.log('notificationRepository.load results', results) 
          this.nextPage = next ? this.nextPage + 1 : 0;
          this.notifications = [...this.notifications, ...results];
        })
        .catch(() => {})
        .finally(() => { this.isNextPageLoading = false; });
    }
  }

  @action
  openPaymentRequiredScreen() {
    this.navigation.navigate(screenNames.USERS_SCREENS_INFO_FIELDS_REQUIRED_SCREEN);
  }

  onNotificationPress(id: number) {
    this.profileManager.openProfile(id);
  }
}

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  backgroundImage: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#000'
  },
});
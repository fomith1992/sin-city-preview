import React, { useEffect, useCallback } from 'react';
import { ImageBackground, SafeAreaView, ScrollView, FlatList } from 'react-native';
import { inject, observer } from 'mobx-react';

import icons from '../../../../icons';
import styles from './style';
import Header from '../../../../layout/ui/components/header';
import NotificationsListItem from '../../components/notifications-list-item';
import { TYPES, ITYPES } from '../../../../../store/store-types';
import RootStore from "../../../../../rootStore";

interface props {
  navigation: any
  notificationsStore: ITYPES['NotificationsStore'],
    profileManager: ITYPES['ProfileManager'],
}

const NotificationsScreen = inject(TYPES.NotificationsStore, TYPES.ProfileManager)(observer(
  ({ navigation, notificationsStore, profileManager }: props) => {
    useEffect(() => {
      notificationsStore.loadNotifications();
    }, []);
    const onEndReached = useCallback(() => {
      notificationsStore.loadNextNotifications();
    }, []);
    const onNotificationPress = useCallback((id: number, item) => {
      if (!id) {
          notificationsStore.openPaymentRequiredScreen();
      }
      if (id != null) {
          if (item !== null && item.latitude !== null && item.longitude !== null) {
                  profileManager.openUserOnMap({latitude: Number(item.latitude),
                      longitude: Number(item.longitude)})
                  if (window.goToPushUser) {
                      setTimeout(() => {
                          window.goToPushUser();
                      }, 600)
                  }
              } else {
                notificationsStore.onNotificationPress(id);
              }
      }
    }, []);

    return (
      <>
        <ImageBackground source={icons.rain}
                         style={styles.backgroundImage}/>
        <SafeAreaView style={{ flex: 1 }}>
          <Header title="Последние уведомления" onBack={() => {
            navigation.pop();
          }}/>
          <FlatList data={notificationsStore.notifications}
                    onEndReached={onEndReached}
                    keyExtractor={(item) => String(item.id)}
                    renderItem={({ item }) =>
                      <NotificationsListItem
                        body={item.body}
                        date={item.date}
                        author={item.author}
                        onPress={() => {
                          onNotificationPress(item.author.id, item)

                        }}/>
                    }/>
        </SafeAreaView>
      </>);
  }
));

export default NotificationsScreen;

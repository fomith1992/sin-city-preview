import { StyleSheet } from 'react-native';
import { getBottomSpace } from 'react-native-iphone-x-helper';
import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    justifyContent: 'space-between'
  },
  scrollView: {

    backgroundColor: '#000',
  },
  contentContStyle: {
    flexGrow: 1,
  },
  bottomContainer: {
    flexGrow: 1,
    backgroundColor: '#282828',
    justifyContent: 'space-evenly'
  },
  description: {
    marginTop: 18,
    fontSize: 14,
    lineHeight: 22,
    ...fontFamily('fontBaseMedium'),
    color: '#fff',
    textAlign: 'center',
    marginHorizontal: 20
  },
  saveButton: {
    marginTop: 20,
    marginBottom: getBottomSpace() + 20,
    marginHorizontal: 40
  }
})

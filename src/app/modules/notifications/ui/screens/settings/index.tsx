import React, { useCallback, useEffect } from 'react';
import { SafeAreaView, Text, View, ScrollView, Dimensions } from 'react-native';
import { inject, observer } from 'mobx-react';

import styles from './style';
import Header from '../../../../layout/ui/components/header';
import UDButton from '../../../../ud-ui/components/ud-button';
import SettingsListItem from '../../components/settings-list-item';

import { TYPES, ITYPES } from '../../../../../store/store-types';
const wH = Dimensions.get('window').height;

interface props {
  navigation: any
  notificationsStore: ITYPES['NotificationsStore']
}

const NotificationsSettingsScreen = inject(TYPES.NotificationsStore)(observer(
  ({ navigation, notificationsStore }: props) => {

    const onSettingValueChange = useCallback((value, key) => { notificationsStore.updatePush(value, key); }, [])
    useEffect(() => {
      notificationsStore.loadPush();
      window.fLogEventObj('push_openScreen',
        {screen_name: 'push_settings'});
      }, []);

    return (
      <>
      <View style={{ flex: 1 }}>
        <SafeAreaView style={styles.container} >
          <Header title="Push-уведомления"
                  onBack={() => { navigation.pop(); }}/>
          <ScrollView style={styles.scrollView}
                      contentContainerStyle={[styles.contentContStyle, wH > 700 ? { height: '100%' } : null]}
                      bounces={false}
                      showsHorizontalScrollIndicator={false}>
            {notificationsStore.pushesInfo.map((setting, index) =>
              <SettingsListItem {...setting}
                                key={index}
                                onValueChange={(value: boolean) => { notificationsStore.updatePush(value, setting.key) }}/>
            )}
            <View style={styles.bottomContainer}>
              <Text style={styles.description}>
                Получайте уведомления только о том, что действительно важно для вас
              </Text>
              <UDButton style={styles.saveButton}
                        title="Сохранить"
                        onPress={() => { notificationsStore.savePush(); }}/>
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
      </>
    );
  }
))

export default NotificationsSettingsScreen;

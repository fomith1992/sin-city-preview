import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderBottomColor: '#ffffff33',
    borderBottomWidth: 1,
    backgroundColor: '#000'
  },
  text: {
    flex: 1,
    flexWrap: 'wrap',
    ...fontFamily('fontBaseMedium'),
    fontSize: 16,
    color: '#fff',
    paddingHorizontal: 10
  }
})
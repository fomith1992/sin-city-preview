import React from 'react';
import { View, Text, Switch } from 'react-native';

import styles from './style';

type props = { 
  label: string, 
  isActive: boolean, 
  onValueChange: any 
};

const SettingsListItem = ({ label, isActive, onValueChange }: props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{label}</Text>
      <Switch value={isActive}
              ios_backgroundColor="#999"
              thumbColor="#fff"
              trackColor={{ true: '#ffbd21', false : "#999" }}
              onValueChange={onValueChange}/>
    </View>
  );
}

export default SettingsListItem;
import { StyleSheet } from 'react-native';
import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#ffffff33'
  },
  date: {
    ...fontFamily('fontBaseMedium'),
    color: '#999',
    marginBottom: 6
  },
  info: {
    width: '70%',
    flexWrap: 'wrap',
    ...fontFamily('fontBaseMedium'),
    color: '#fff',
    marginRight: 40
  },
  photo: {
    width: 43,
    height: 43,
    borderRadius: 43,
    overflow: 'hidden',
  },
  // Android <7 fix (not around avatar)
  fixOverlay: {
    position: 'absolute',
    zIndex: 20,
    width: 43,
    height: 43,
    borderRadius: 22,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'transparent'
  },
  avatar: {
    width: 43,
    height: 43,
    borderRadius: 22,
    marginRight: 14,
    backgroundColor: '#000'
  },
  photoPlaceholderWrap: {
    width: 43,
    height: 43,
    backgroundColor: '#2D2D2D',
    alignItems: 'center',
    justifyContent: 'center'
  },
  photoPlaceholder: {
    width: 30,
    height: 22,
  },
})

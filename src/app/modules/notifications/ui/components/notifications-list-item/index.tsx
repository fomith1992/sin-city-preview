import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import moment from 'moment';

import styles from './style';
import icons from '../../../../icons';
import FastImage from 'react-native-fast-image';


function capitalize(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

const NotificationsListItem = ({ body, date, author: { photo }, onPress }: any & { onPress: any }) => {
  const formattedDate = capitalize(moment.utc(date).local().format('dddd, D MMMM HH:mm'));

  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.fixOverlay} />
      <FastImage source={photo ? { uri: photo.photoThumbnailUrl } : icons.profilePlaceholder} style={styles.avatar}/>
      <View>
        <Text style={styles.date}>{formattedDate}</Text>
        <Text style={styles.info}>{body}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default NotificationsListItem;
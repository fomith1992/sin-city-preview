import { TYPES, ITYPES } from '../../../store/store-types';
import { BaseStore } from './../../../store/BaseStore';
import { observable } from "mobx";

export interface ModalMessage {
  message: string;
  positiveButton: ModalButton;
  negativeButton: ModalButton;
}

export interface ModalButton {
  text: string;
  onPress: () => void;
}

export class LayoutStore extends BaseStore {
  @observable isOverflowLoaderShown: boolean = false;
  @observable isModalMessageShown: boolean = false;
  @observable modalMessageContent: string = '';
  @observable indicateActivity: boolean = true;
  @observable isTwoButtons: boolean = false;
  @observable modalPositiveButton: ModalButton | null = null;
  @observable modalNegativeButton: ModalButton | null = null;

  private navigation: ITYPES['Navigation'];

  storeInit() {
    this.navigation = this.root.resolve(TYPES.Navigation);
  }

  public showOverflowLoader(indicateActivity: boolean = true): void {
    this.indicateActivity = indicateActivity;
    this.isOverflowLoaderShown = true;
  }

  public hideOverflowLoader(): void {
    this.indicateActivity = true;
    this.isOverflowLoaderShown = false;
  }

  public showModalMessage(message: string | ModalMessage): void {
    if (typeof message === "string") {
      this.modalMessageContent = message;
      this.isTwoButtons = false;
    } else {
      this.modalMessageContent = message.message;
      this.modalPositiveButton = message.positiveButton;
      this.modalNegativeButton = message.negativeButton;
      this.isTwoButtons = true;
    }

    this.isModalMessageShown = true;
  }

  public hideModalMessage(): void {
    this.navigation.pop();
    this.isModalMessageShown = false;
  }

}
export default LayoutStore;

import React, { useState, useRef, useCallback } from 'react';
import {
  View,
  TouchableHighlight,
  Text,
  Dimensions,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import styles from './style';
import UDButton from '../../../../ud-ui/components/ud-button';
import * as screenNames from '../../../../navigation/screen-names';
import {useFocusEffect} from 'react-navigation-hooks';

const gradient = require('./img/gradient.png');
const pic1 = require('./img/pic-1.png');
const pic2 = require('./img/pic-2.png');
const pic3 = require('./img/pic-3.png');

const entries = [pic1, pic2, pic3];
const descriptions = [
  'Подтверди аккаунт и получи доступ к настоящим анкетам и фотографиям',
  'Преврати желания в намерения одним нажатием. Узнай на карте с кем они совпадают и действуй!',
  'Построить планы на вечер? Легко! Все вечеринки твоего города в разделе События'
];

const wW = Dimensions.get('screen').width;

const _renderItem = ({ item }: { item: any }) => {
  return (
    <FastImage style={{ marginTop: '20%', height: '70%', width: '100%' }} resizeMode='contain' source={item}/>
  );
};

const Welcome = ({ navigation }: any) => {

  const onScreenFocus = useCallback(() => {
    console.log('welcome_screen')
   // navigation.navigate('PROFILE_FISRT_STEP_SCREEN');
    if (!!window.closeWsConnect) {
      window.closeWsConnect();
    } else {
      console.log("func_doesnt_ex");
    }
    if (window.searchScreenChangeIsFirstLoadInit) {
      window.searchScreenChangeIsFirstLoadInit(true);
    }
  }, []);

  useFocusEffect(onScreenFocus);

  const [activeSlide, setActiveSlide] = useState(0);
  let carouselRef = useRef(null);
  const onNextPress = useCallback(() => {
    window.fLogEvent('onboarding_clickNext', 'WELCOME');
    if (activeSlide < 2) {
      (carouselRef as any).current.snapToNext()
      window.fLogEvent('onboarding_viewScreen', activeSlide + 2);
    }
    else {
      navigation.navigate(screenNames.PHONE_INPUT);
    }
  }, [activeSlide]);
  return (
    <SafeAreaView style={styles.safe}>
      <ScrollView bounces={false}
                  contentContainerStyle={styles.scrollable}>

        <View style={styles.container}>
          <FastImage style={{ height: '56%', width: '100%', flexDirection: 'row', paddingBottom: 40 }}
                     resizeMode='stretch'
                     source={gradient}>
            <Carousel
              ref={carouselRef}
              data={entries}
              renderItem={_renderItem}
              sliderWidth={wW}
              itemWidth={wW}
              loop={false}
              onSnapToItem={setActiveSlide}/>
          </FastImage>
          <View style={styles.bottomContainer}>
            <View style={styles.descWrap}>
              <Text style={styles.description}>{descriptions[activeSlide]}</Text>
            </View>
            <Pagination
              dotsLength={entries.length}
              activeDotIndex={activeSlide}
              dotStyle={styles.paginatoionDot}
              dotContainerStyle={{ marginHorizontal: 5, marginBottom: 20 }}
              inactiveDotOpacity={0.6}
              inactiveDotScale={1}
            />
            <UDButton title="Далее"
                      style={{ alignSelf: 'stretch' }}
                      onPress={onNextPress}/>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

export default Welcome;

import { StyleSheet, Dimensions } from 'react-native';
import fontFamily from '../../../../../styles/fonts';

const wH = Dimensions.get('window').height;
const isSmallDevices = wH < 700;

export default StyleSheet.create({
  safe: {
    flex: 1,
    backgroundColor: '#000',
    overflow: 'hidden'
  },
  container: {
  },
  descWrap: {
    flexDirection: 'row',
  },
  description: {
    ...fontFamily('fontTitleBold'),
    fontSize: isSmallDevices ? 16 : 20,
    color: '#fff', 
    textAlign: 'center', 
    marginTop: 5,
    minHeight: isSmallDevices ? 50 : 110,
  },
  paginatoionDot: {
    width: 7,
    height: 7,
    borderRadius: 5,
    marginHorizontal: 0,
    margin: 0,
    backgroundColor: '#fff'
  },
  bottomContainer: {
    flex: 1, 
    marginTop: 40,
    paddingHorizontal: '5%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  scrollable: {
    flexGrow: 1,
    paddingBottom: 40,
    justifyContent: 'space-between',
  }
})
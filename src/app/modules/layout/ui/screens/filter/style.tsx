import { StyleSheet, Dimensions } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export const activeColor = '#ffbd21';
export const inactiveColor = '#ffffff33';

export default StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: '#000',
    paddingTop: 15,
    paddingLeft: 20,
    paddingRight: 0
  },
  pickerInput: {
    color: '#fff',
    fontSize: 16,
    ...fontFamily('fontBaseMedium')
  },
  marker: {
    height: 19,
    width: 19,
    backgroundColor: '#fff'
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 7
  },
  listItemText: {
    color: '#fff',
    fontSize: 16,
    ...fontFamily('fontBaseMedium')
  },
  availibilityContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 17,
    marginBottom: 13,
  },
  bottomButtonContainer: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    paddingHorizontal: 40,
    paddingVertical: 10
  },
  datingPurposeTagsCont: {
    display: 'flex',
    marginTop: 10,
    marginRight: 20,
    marginBottom: 10,
   // paddingLeft
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  //  backgroundColor: '#ffffff',
    flexWrap: 'wrap',
  },
  datingPurposeItem: {
    display: 'flex',
    marginTop: 10,
    marginLeft: 10,
    padding: 5,
    paddingBottom: 6,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderRadius: 50,
    borderColor: '#474747'

  },
  datingPurposeItemActive: {
    borderColor: activeColor,
  },
  datingPurposeItemLabel: {
    color: '#ababab',
    fontSize: 12,
    ...fontFamily('fontBaseRegular')
  }
});


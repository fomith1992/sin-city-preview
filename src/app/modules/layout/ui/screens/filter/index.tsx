import React, {useCallback, useEffect, useState} from 'react';
import {
  Text,
  ScrollView,
  SafeAreaView,
  Image,
  Dimensions,
  Switch,
  View,
  TouchableOpacity,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {inject, observer} from 'mobx-react';

import icons from '../../../../icons';
import pickerStyles from '../../../../../styles/picker';
import styles from './style';
import UDButton from '../../../../ud-ui/components/ud-button';
import UDSlider from '../../../../ud-ui/components/ud-slider';
import Header from '../../components/header';
import ListItem from '../../../../auth/ui/components/profile-list-item';
import {
  ProfileGendersEnum,
  GendersList,
} from '../../../../user/domain/enums/Gender';
import {Attitudes} from '../../../../user/domain/enums/Attitudes';
import {Orientations} from '../../../../user/domain/enums/Orientation';
import {GenderFilter} from '../../../../search/domain/interface/Filter';
import {FamilyStatus} from '../../../../user/domain/enums/FamilyStatus';
import {PurposeDatingForPicker} from '../../../../user/domain/enums/PurposeDating';

import SearchStore from '../../../../search/store/SearchStore';
import DictionariesStore from '../../../../dictionaries/store/DictionariesStore';
import UDIconButton from '../../../../ud-ui/components/ud-icon-button';

const wW = Dimensions.get('window').width;
const sliderWidth = wW - 61;

const children = [
  {label: 'Да', value: true},
  {label: 'Нет', value: false},
];

const GenderFilterForm = observer(
  ({genderFilter}: {genderFilter: GenderFilter}) => {
    if (!genderFilter.gender) {
      return <></>;
    }
    const [figureTypes, setFigureTypes] = React.useState<any[]>([]);
    const [bodyTypes, setBodyTypes] = React.useState<any[]>([]);
    const [facialTypes, setFacialTypes] = React.useState<any[]>([]);
    React.useEffect(() => {
      switch (genderFilter.gender) {
        case 'M':
          setBodyTypes(icons.bodyTypes.manBodyTypes);
          setFigureTypes(icons.figureTypes.manFigureTypeImages);
          setFacialTypes(icons.manFacialHairs);
          break;
        case 'W':
          setBodyTypes(icons.bodyTypes.womanBodyTypes);
          setFigureTypes(icons.figureTypes.womanFigureTypeImages);
          setFacialTypes([]);
          break;
      }
    }, [genderFilter.gender]);
    const onFigurePress = (index: number) => {
      const typeFigure = figureTypes[index].value;
      Object.assign(genderFilter, {
        type_figure:
          typeFigure === genderFilter.type_figure ? undefined : typeFigure,
      });
    };
    const onBodyPress = (index: number) => {
      const typePhysiques = bodyTypes[index].value;
      Object.assign(genderFilter, {
        type_physiques:
          typePhysiques === genderFilter.type_physiques
            ? undefined
            : typePhysiques,
      });
    };
    const onFacialPress = (index: number) => {
      const facialHair = facialTypes[index].value;
      Object.assign(genderFilter, {
        facial_hair:
          facialHair === genderFilter.facial_hair ? undefined : facialHair,
      });
    };
    const onAgeChange = (age: number[]) => Object.assign(genderFilter, {age});
    const onHeightChange = (height: number[]) => {
      Object.assign(genderFilter, {height});
    };
    const onWeightChange = (weight: number[]) => {
      Object.assign(genderFilter, {weight});
    };

    return (
      <>
        <Text
          style={[
            styles.listItemText,
            {
              marginTop: 40,
              marginBottom: 20,
              marginRight: 20,
              textAlign: 'right',
            },
          ]}>
          {ProfileGendersEnum[genderFilter.gender]}
        </Text>
        <ListItem
          title="ВОЗРАСТ"
          postTitle={`от ${genderFilter.age[0]} до ${genderFilter.age[1]}`}
          withoutBorder>
          <UDSlider
            {...{
              values: genderFilter.age,
              onValuesChange: onAgeChange,
              sliderLength: sliderWidth,
              min: 18,
              max: 80,
            }}
          />
        </ListItem>
        <ListItem
          title="РОСТ"
          withoutBorder
          postTitle={`от ${genderFilter.height[0]} до ${genderFilter.height[1]}`}>
          <UDSlider
            {...{
              values: genderFilter.height,
              onValuesChange: onHeightChange,
              sliderLength: sliderWidth,
              min: 115,
              max: 245,
            }}
          />
        </ListItem>
        <ListItem
          title="ВЕС"
          withoutBorder
          postTitle={`от ${genderFilter.weight[0]} до ${genderFilter.weight[1]}`}>
          <UDSlider
            {...{
              values: genderFilter.weight,
              onValuesChange: onWeightChange,
              sliderLength: sliderWidth,
              min: 30,
              max: 180,
            }}
          />
        </ListItem>
        <ListItem title="ТИП ФИГУРЫ" withoutBorder>
          <ScrollView
            horizontal={true}
            style={{paddingTop: 20, flexDirection: 'row'}}>
            {figureTypes.map((figureType, index) => {
              const active = genderFilter.type_figure === figureType.value;
              const image = active
                ? figureType.images.off
                : figureType.images.on;
              return (
                <UDIconButton
                  key={index}
                  {...{
                    image,
                    key: index,
                    index,
                    onPress: onFigurePress,
                    active,
                  }}
                />
              );
            })}
          </ScrollView>
        </ListItem>
        <ListItem title="ТИП ТЕЛОСЛОЖЕНИЯ" withoutBorder>
          <ScrollView
            horizontal={true}
            style={{paddingTop: 20, paddingBottom: 15, flexDirection: 'row'}}>
            {bodyTypes.map((bodyType, index) => {
              const image = bodyType.image;
              const active = bodyType.value === genderFilter.type_physiques;
              return (
                <UDIconButton
                  key={index}
                  {...{image, key: index, index, onPress: onBodyPress, active}}
                />
              );
            })}
          </ScrollView>
        </ListItem>
        {Boolean(facialTypes.length) && (
          <ListItem title="РАСТИТЕЛЬНОСТЬ НА ЛИЦЕ" withoutBorder>
            <ScrollView
              horizontal={true}
              style={{paddingTop: 20, paddingBottom: 15, flexDirection: 'row'}}>
              {facialTypes.map((facialType, index) => {
                const image = facialType.image;
                const active = genderFilter.facial_hair === facialType.value;
                return (
                  <UDIconButton
                    key={index}
                    {...{
                      image,
                      key: index,
                      index,
                      onPress: onFacialPress,
                      active,
                    }}
                  />
                );
              })}
            </ScrollView>
          </ListItem>
        )}
        <ListItem title="ОРИЕНТАЦИЯ">
          <Image source={icons.gender} />
          <RNPickerSelect
            placeholder={{label: 'Выберите...'}}
            value={genderFilter.orientation}
            style={pickerStyles}
            onValueChange={(orientation) =>
              Object.assign(genderFilter, {orientation})
            }
            items={Orientations}
          />
        </ListItem>
        <ListItem title="ОТНОШЕНИЕ К АЛКОГОЛЮ">
          <Image source={icons.alcohol} />
          <RNPickerSelect
            placeholder={{label: 'Выберите...'}}
            value={genderFilter.attitude_to_alcohol}
            style={pickerStyles}
            onValueChange={(attitude_to_alcohol) =>
              Object.assign(genderFilter, {attitude_to_alcohol})
            }
            items={Attitudes}
          />
        </ListItem>
        <ListItem title="ОТНОШЕНИЕ К КУРЕНИЮ">
          <Image source={icons.smoking} />
          <RNPickerSelect
            placeholder={{label: 'Выберите...'}}
            value={genderFilter.attitude_to_smoking}
            style={pickerStyles}
            onValueChange={(attitude_to_smoking) =>
              Object.assign(genderFilter, {attitude_to_smoking})
            }
            items={Attitudes}
          />
        </ListItem>
      </>
    );
  },
);

const FilterModal = inject((stores: any, props: any) => {
  return {
    searchStore: stores[props.navigation.getParam('store')],
    dictionariesStore: stores.dictionariesStore,
  };
})(
  observer(
    ({
      navigation,
      searchStore,
      dictionariesStore,
    }: {
      searchStore: SearchStore;
      navigation: any;
      dictionariesStore: DictionariesStore;
    }) => {
      const filters = searchStore.temporaryFilter;
      const {getCities} = dictionariesStore;
      const [selectedDPurs, setSelectedDPurs] = useState([]);

      useEffect(() => {
        searchStore.prepareFilters();
        setSelectedDPurs(filters.purpose_dating);
      }, []);

      const onDPurItemClick = (value) => {
        const array = selectedDPurs;
        const index = array && array.indexOf(value);
        // console.log('index', index)
        if (index < 0) {
          array.push(value);
        } else {
          array.splice(index, 1);
        }
        setSelectedDPurs([...array]);
        Object.assign(filters, {purpose_dating: array});
      };

      const onResetFiltersPressed = useCallback(() => {
        window.fLogEventObj('search_clickFilterDrop', {});
        searchStore.dissmissFilterChanges();
        setSelectedDPurs([]);
      }, []);
      const onBackPressed = useCallback(() => {
        navigation.goBack();
      }, []);
      const onGenderValueChange = useCallback(
        (gender) => {
          if (gender && gender !== searchStore.getFilterGender) {
            searchStore.setFilterGender(gender.split(''));
          }
        },
        [searchStore.getFilterGender],
      );
      const onSave = () => {
        searchStore.applyFilterChanges();
        window.fLogEventObj(
          'search_clickFilterApply', //  \"
          {
            filter_values: JSON.stringify(searchStore.filters).replace(
              /"/g,
              '',
            ),
          },
        );
      };

      return (
        <SafeAreaView
          style={{flex: 1, backgroundColor: '#000'}}
          emulateUnlessSupported={false}>
          <Header
            title="Фильтр"
            height={70}
            onPress={onResetFiltersPressed}
            onBack={onBackPressed}
            rightLabel="Сбросить"
          />
          <ScrollView
            style={styles.scrollView}
            contentContainerStyle={{paddingBottom: 60}}>
            <ListItem title="С КЕМ ХОТИТЕ ПОЗНАКОМИТЬСЯ">
              <Image source={icons.gender} />
              <RNPickerSelect
                placeholder={{label: 'Выбор пола...', value: 'nope'}}
                value={searchStore.getFilterGender}
                style={pickerStyles}
                onValueChange={onGenderValueChange}
                items={GendersList}
              />
            </ListItem>
            <ListItem title="ГОРОД">
              <Image source={icons.location} />
              <RNPickerSelect
                placeholder={{label: 'Выбор города...'}}
                value={filters.city}
                style={pickerStyles}
                onValueChange={(city) => Object.assign(filters, {city})}
                items={getCities}
              />
            </ListItem>
            <ListItem title="ЦЕЛЬ ЗНАКОМСТВА">
              <Image source={icons.targetWhite} />
              {/*<RNPickerSelect*/}
              {/*    placeholder={{ label: 'Цель знакомства...' }}*/}
              {/*    value={filters.purpose_dating}*/}
              {/*    style={pickerStyles}*/}
              {/*    onValueChange={(purpose_dating) => Object.assign(filters, { purpose_dating: ['SWING_OPEN', 'SWING_CLOSE'] })}*/}
              {/*    items={PurposeDatingForPicker} />*/}
              <View style={styles.datingPurposeTagsCont}>
                {PurposeDatingForPicker.map((item, id) => {
                  return (
                    <TouchableOpacity
                      style={[
                        styles.datingPurposeItem,
                        selectedDPurs && selectedDPurs.indexOf(item.value) > -1
                          ? styles.datingPurposeItemActive
                          : null,
                      ]}
                      key={id}
                      onPress={() => {
                        onDPurItemClick(item.value);
                      }}>
                      <Text style={styles.datingPurposeItemLabel}>
                        {item.label}
                      </Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </ListItem>
            <ListItem title="СЕМЕЙНОЕ ПОЛОЖЕНИЕ">
              <Image source={icons.marriage} />
              <RNPickerSelect
                placeholder={{label: 'Выбор семейного положения...'}}
                value={filters.family_status}
                style={pickerStyles}
                onValueChange={(family_status) =>
                  Object.assign(filters, {family_status})
                }
                items={FamilyStatus}
              />
            </ListItem>
            <ListItem title="ДЕТИ">
              <Image source={icons.kids} />
              <RNPickerSelect
                placeholder={{label: 'Наличие детей...'}}
                value={filters.has_children}
                style={pickerStyles}
                onValueChange={(has_children) =>
                  Object.assign(filters, {has_children})
                }
                items={children}
              />
            </ListItem>
            {/*<ListItem title="ДОСТУПНОСТЬ">*/}
            {/*  <View style={styles.availibilityContainer}>*/}
            {/*    <View style={{ flexDirection: 'row', width: '70%' }}>*/}
            {/*      <Image source={icons.availability}/>*/}
            {/*      <Text style={[styles.listItemText, { marginLeft: 10, width: 225, flexWrap: 'wrap', flex: 1 }]}>*/}
            {/*        Показать только тех, кто сейчас онлайн*/}
            {/*      </Text>*/}
            {/*    </View>*/}
            {/*    <Switch thumbColor="#fff"*/}
            {/*            value={filters.online}*/}
            {/*            ios_backgroundColor="#333"*/}
            {/*            style={{ marginRight: 20 }}*/}
            {/*            trackColor={{ true: "#ffbd21", false: "#333" }}*/}
            {/*            onValueChange={(online) => Object.assign(filters, { online })}/>*/}
            {/*  </View>*/}
            {/*</ListItem>*/}
            {filters.genders.map((genderFilter: GenderFilter, index) => (
              <GenderFilterForm key={index} {...{genderFilter}} />
            ))}
          </ScrollView>
          <View style={styles.bottomButtonContainer}>
            <UDButton title="Применить" onPress={onSave} />
          </View>
        </SafeAreaView>
      );
    },
  ),
);
export default FilterModal;

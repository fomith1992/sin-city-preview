import React from 'react';
import {View, Text, Image, TouchableOpacity, ViewStyle} from 'react-native';

import styles from './style';
import icons from '../../../../icons';

type prop = {
  title: string
  subtitle?: string
  onPress?: any
  icon?: number
  onBack?: any
  onBackSecond?: any
  withoutBorder?: boolean
  leftIcon?: number
  leftIconSecond?: number
  rightLabel?: string
  titleContainerStyle?: ViewStyle
  height?: number
  subtitleColor?: string
  onTitlePress?: () => void
  isProfileHeader?: boolean
  isMyProfile?: boolean
}

const LayoutComponentsHeader = (props: prop) => {
  const {
    title,
    subtitle,
    onPress,
    icon,
    onBack,
    onBackSecond,
    withoutBorder,
    leftIcon,
    leftIconSecond,
    rightIconSecond,
    onRightIconSecondPress,
    rightLabel,
    titleContainerStyle,
    height,
    subtitleColor,
    onTitlePress,
    isProfileHeader = false,
    isMyProfile = false,
  } = props;

  return (
    <>
      <View style={[styles.container, {borderBottomWidth: withoutBorder ? 0 : 1, height: height ? height : 50}]}>
        {onBack &&
        <TouchableOpacity style={[styles.leftContainer,
          isProfileHeader ? {
            backgroundColor: '#00000034',
            borderRadius: 12,
            paddingHorizontal: 15,
            paddingVertical: 6,
            paddingLeft: 13,
        } : {}]}
           onPress={onBack}
        >
          <TouchableOpacity onPress={onBack}>
            <Image source={leftIcon ? leftIcon : icons.navBack}/>
          </TouchableOpacity>
        </TouchableOpacity>
        }
        {onBackSecond &&
        <TouchableOpacity style={[styles.leftContainerSecond,
          isProfileHeader ? {
            backgroundColor: '#00000034',
            borderRadius: 12,
            paddingHorizontal: 15,
            paddingVertical: 6,
            paddingLeft: 13,
          } : {}]}
          onPress={onBackSecond}
        >
          <TouchableOpacity onPress={onBackSecond}>
            <Image source={leftIconSecond ? leftIconSecond : icons.navBack}/>
          </TouchableOpacity>
        </TouchableOpacity>
        }
        {(title || subtitle) &&
        <TouchableOpacity style={titleContainerStyle} activeOpacity={1} onPress={onTitlePress ? onTitlePress : null}>
          <Text style={styles.title}>{title}</Text>
          {!!subtitle && <Text style={[styles.subtitle, {color: subtitleColor}]}>{subtitle}</Text>}
        </TouchableOpacity>
        }
        {onRightIconSecondPress &&
        <View style={[styles.rightSecondContainer,
          isProfileHeader ? {
            backgroundColor: '#00000034',
            borderRadius: 12,
            paddingHorizontal: 15,
            paddingVertical: 5,
            // paddingVertical: 8,
            paddingLeft: 13,
            // width: 400,
          } : null, isMyProfile ? {
            paddingVertical: 8,
          } : null]}>
          <TouchableOpacity activeOpacity={0.8}
              //   style={styles.rightContainer}
                            onPress={onRightIconSecondPress}>
            {Boolean(rightLabel) && <Text style={styles.rightLabel}>{rightLabel}</Text>}
            {rightIconSecond ? <Image source={rightIconSecond}/> : <></>}
          </TouchableOpacity>
        </View>}
        {onPress &&
        <View style={[styles.rightContainer,
          isProfileHeader ? {
            backgroundColor: '#00000034',
            borderRadius: 12,
            paddingHorizontal: 15,
            paddingVertical: 5,
           // paddingVertical: 8,
            paddingLeft: 13,
           // width: 400,
          } : null, isMyProfile ? {
             paddingVertical: 8,
          } : null]}>
        <TouchableOpacity activeOpacity={0.8}
                       //   style={styles.rightContainer}
                          onPress={onPress}>
          {Boolean(rightLabel) && <Text style={styles.rightLabel}>{rightLabel}</Text>}
          {icon ? <Image source={icon}/> : <></>}
        </TouchableOpacity>
        </View>}
      </View>
    </>);
};

export default LayoutComponentsHeader;

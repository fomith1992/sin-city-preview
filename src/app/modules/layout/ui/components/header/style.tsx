import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    zIndex: 10,
    height: 70,
    borderBottomColor: '#ffffff33',
    justifyContent: 'center',
    alignItems: 'center'
  },
  leftContainer: {
    position: 'absolute',
    left: 10,
    padding: 10,
   // backgroundColor: 'red',

  },
  leftContainerSecond: {
    position: 'absolute',
    left: 31,
    padding: 31,
    // backgroundColor: 'red',

  },
  title: {
    ...fontFamily('fontTitleBold'),
    fontSize: 16,
    color: '#fff'
  },
  subtitle: {
    marginTop: 5,
    ...fontFamily('fontBaseMedium'),
    fontSize: 12,
    color: '#999'
  },
  rightContainer: {
    position: 'absolute',
    right: 10,
    padding: 10
  },
  rightSecondContainer: {
    position: 'absolute',
    right: 31,
    padding: 31
  },
  rightLabel: {
    ...fontFamily('fontTitleBold'),
    color: '#e84747',
    fontSize: 16
  }
})

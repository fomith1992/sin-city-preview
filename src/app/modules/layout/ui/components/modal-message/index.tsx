import React, { useCallback } from 'react';
import { Modal, View, Text, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import { inject, observer } from 'mobx-react';
import { TYPES } from '../../../../../store/store-types';

import styles from './style';
import icons from '../../../../icons';
import LayoutStore from '../../../store/LayoutStore';

interface LayoutComponentModalMessageProps {
    layoutStore: LayoutStore;
}

const LayoutComponentModalMessage = inject(TYPES.Layout)(observer(
  ({ layoutStore }: LayoutComponentModalMessageProps) => {
    const onClosePress = useCallback(() => { layoutStore.hideModalMessage(); }, []);
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={layoutStore.isModalMessageShown}>
        <View style={styles.container}>
          <View style={[
            styles.content,
            layoutStore.isTwoButtons && { height: 450 }
          ]}>
            <FastImage source={icons.iconSuccess} style={styles.successIcon}/>
            <Text style={styles.primary}>{layoutStore.modalMessageContent}</Text>
            {
              layoutStore.isTwoButtons
                ? (
                    <View style={{ flexDirection: 'row' }}>
                      <TouchableOpacity
                          onPress={layoutStore.modalNegativeButton.onPress}
                          style={styles.button}
                      >
                        <Text style={styles.closeButtonText}>
                          {layoutStore.modalNegativeButton.text}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={layoutStore.modalPositiveButton.onPress}
                        style={styles.button}
                      >
                        <Text style={styles.closeButtonText}>
                          {layoutStore.modalPositiveButton.text}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )
                  : (
                    <TouchableOpacity onPress={onClosePress} style={styles.closeButton}>
                      <Text style={styles.closeButtonText}>Закрыть</Text>
                    </TouchableOpacity>
                  )
            }
          </View>
        </View>
      </Modal>
    );
  }
));

export default LayoutComponentModalMessage;

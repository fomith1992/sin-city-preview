import { StyleSheet } from 'react-native';

import fontFamily from '../../../../../styles/fonts';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#000000e5',
    justifyContent: 'center'
  },
  content: {
    height: 350,
    marginHorizontal: 40,
    paddingHorizontal: 20,
    backgroundColor: '#fff',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 70,
    paddingBottom: 50
  },
  successIcon: {
    width: 52,
    height: 52
  },
  primary: {
    ...fontFamily('fontTitleBold'),
    fontSize: 19,
    color: '#000',
    textAlign: 'center'
  },
  closeButton: {
    width: '100%',
    paddingVertical: 20
  },
  button: {
    width: '50%',
    paddingVertical: 10
  },
  closeButtonText: {
    ...fontFamily('fontTitleBold'),
    fontSize: 14,
    color: '#999',
    textAlign: 'center'
  },
})

import React, {useRef, useEffect}  from "react";
import { ActivityIndicator, Animated } from 'react-native';
import styles from './style';
import { inject, observer } from "mobx-react";
import { TYPES, ITYPES } from '../../../../../store/store-types';

const LayoutComponentsOverflowLoader =
  inject(TYPES.Layout)
  (observer(({ layoutStore }: { layoutStore: ITYPES['Layout'] }) => {

    const fadeAnim = useRef(new Animated.Value(0));
    useEffect(() => {
      if(layoutStore.isOverflowLoaderShown){
        Animated.timing(
          fadeAnim.current,
          {
            toValue: 1,
            duration: 200,
          }
        ).start();
      }
    }, [layoutStore.isOverflowLoaderShown]);

    if (layoutStore.isOverflowLoaderShown) {
      return (
        <Animated.View style={[styles.container, {opacity: fadeAnim.current}]}>
          {layoutStore.indicateActivity && <ActivityIndicator color="#ffbd21" size="large"/>}
        </Animated.View>
      );
    }
  }));

export default LayoutComponentsOverflowLoader;

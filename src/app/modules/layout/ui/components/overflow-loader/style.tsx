import { StyleSheet, Dimensions } from 'react-native';

let {height, width} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    position: 'absolute',
    opacity: 0,
    top: 0,
    left: 0,
    zIndex: 1000,
    width,
    height,
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

import { StyleSheet } from 'react-native';

import fontFamily from '../../../styles/fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '100%'
  },
  errorsContainer: {
    marginBottom: 5
  },
  textError: {
    ...fontFamily('fontBaseRegular'),
    color: 'red',
    fontSize: 12
  }
})
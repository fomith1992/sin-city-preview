import React from 'react';
import { View, Text } from 'react-native';

import styles from './style';

type props = {
  children: any
  errors: string[]
  canShow: boolean
};

const ValidationControl = ({ children, errors, canShow }: props) => {
  return (
    <View style={styles.container}>
      {children}
      { canShow && errors && errors.length > 0 && (
        <View style={styles.errorsContainer}>
          <Text style={styles.textError}>{ errors.join(', ') }</Text>
        </View>
      ) }
    </View>
  );
};

export default ValidationControl;
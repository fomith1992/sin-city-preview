export function createErrorMessages(fieldName: string, errors: any, messages: any) {
  const fieldErrors = errors && errors[fieldName];
  const fieldMessages = messages && messages[fieldName];

  let result = [];
  if (fieldErrors) {
    result = fieldErrors.reduce((result: any, error: any) => {
      const message = fieldMessages && fieldMessages[error.key]
      if (message) {
        result.push(message)
      }
      return result
    }, [])
  }

  return result
}

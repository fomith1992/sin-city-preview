const _isArray = require('lodash/isArray');


// examples
// schema = [{
//   name: [Validators.required],
//   phone: [Validators.required, Validators.mobilePhone]
// }, { validators: [() => {}] }]; // entire form validators

// schema = {
//   name: [Validators.required],
//   phone: [Validators.required, Validators.mobilePhone]
// }

export class Validator {
  protected schema: any[] | any;

  public validate(value: any, messages?: any) {
    let result = Validator.validate(this.schema, value);
    if (messages != null && result != null) {
      return Validator.createErrorMessages(result, messages);
    }
    return result;
  }

  static validate(schema: any, value: any) {
    if (_isArray(schema)) {
      return Validator.validateForm(schema as any[], value);
    }

    return Validator.validateFields(schema as any, value);
  }

  static validateFields(schema: any, value: any) {
    const fields = Object.keys(schema);
    const result = {} as any;
    let hasErrors = false;
    fields.forEach((field) => {
      const fieldValue = value[field];
      const validators = schema[field];
      if (Array.isArray(validators)) {
        validators.forEach((validator) => {
          const validatorResult = validator(fieldValue);
          if (validatorResult != null) {
            hasErrors = true;
            result[field] = result[field] || [];
            const errorKey = Object.keys(validatorResult)[0];
            result[field].push({ key: errorKey, payload: validatorResult[errorKey] });
          }
        });
      } else {
        const fieldErrors = Validator.validate(validators, fieldValue);
        if (fieldErrors) {
          const fieldErrorsArray = Object.keys(fieldErrors);
          if (fieldErrorsArray && fieldErrorsArray.length) {
            hasErrors = true;
            result[field] = fieldErrors;
          }
        }
      }
    });

    return hasErrors ? result : null;
  }

  static validateForm(schema: object[], value: any) {
    const fieldsValidationSchema = schema[0];
    const formValidationSchema = schema[1] as { validators: Function[] };
    let result = Validator.validateFields(fieldsValidationSchema, value);
    let hasErrors = result != null;

    // entire form validator(s)
    if (formValidationSchema && formValidationSchema.validators &&
      Array.isArray(formValidationSchema.validators) &&
      formValidationSchema.validators.length) {
      formValidationSchema.validators.forEach((validator) => {
        const validatorResult = validator(value);
        if (validatorResult != null) {
          hasErrors = true;
          result['$errors'] = result['$errors'] || [];
          const errorKey = Object.keys(validatorResult)[0];
          result['$errors'].push({ key: errorKey, payload: validatorResult[errorKey] });
        }
      });
    }

    return hasErrors ? result : null;
  }

  static createErrorMessages(errors: any, messages: any) {
    let messagesResult = {} as any;
    const fieldNames = Object.keys(errors);

    for (let i = 0; i < fieldNames.length; i++) {
      const fieldName = fieldNames[i];
      const fieldErrors = errors && errors[fieldName];
      const fieldMessages = messages && messages[fieldName];
      let result = [];
      if (fieldErrors) {
        result = fieldErrors.reduce((result: any, error: any) => {
          const message = fieldMessages && fieldMessages[error.key]
          if (message) {
            result.push(message)
          }
          return result
        }, [])
      }
      messagesResult[fieldName] = result;
    }

    return messagesResult
  }
}
export class Validators {
  static required(value: any) {
    const isValid = value !== '' && value != null && value !== undefined;
    return isValid ? null : { required: value };
  }

  static minLength(minLength: number) {
    return (value: any) => {
      const isValid = !value || value.length && value.length >= minLength;
      return isValid ? null : { minLength: value };
    };
  }

  static maxLength(maxLength: number) {
    return (value: any) => {
      const isValid = !value || value.length && value.length <= maxLength;
      return isValid ? null : { maxLength: value };
    };
  }

  static mobilePhone(value: any) {
    const ruMobilePhoneRegExp = /^[1-9]{1}\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}$/;     // 8(123)456-78-90
    const isValid = value === '' || value == null ? true : ruMobilePhoneRegExp.test(value);
    return isValid ? null : { mobilePhone: { mobilePhone: value } };
  }

  static excludeNonWordCharacters(value: any) {
    const nonWordCharactersRegExp = /([^а-яA-Яa-zA-Z0-9_\-|\.]+)/gm;
    const isValid = value === '' || value == null ? true : !nonWordCharactersRegExp.test(value);
    return isValid ? null : { nonWordCharacters: value.match(nonWordCharactersRegExp) };
  }
}

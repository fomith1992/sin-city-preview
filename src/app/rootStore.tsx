import httpResource, { isFuncExists } from './modules/core/infrastructure/httpResource';
import { showMessage } from 'react-native-flash-message';
import { observable } from 'mobx';
import ProfileStore from './modules/user/store/ProfileStore';
import DictionariesStore from './modules/dictionaries/store/DictionariesStore';
import UsersSearchStore from './modules/search/store/UsersSearchStore';
import CoordinatesSearchStore from './modules/search/store/CoordinatesSearchStore';
import NavigationStore from 'react-navigation-mobx-helpers';
import LayoutStore from "./modules/layout/store/LayoutStore";
import ChatsStore from './modules/messages/store/ChatStore';

import { AuthEnterStore } from "./modules/auth/store/auth-enter";
import { TYPES, ITYPES } from "./store/store-types";
import { AuthUserStore } from "./modules/auth/store/auth-user";
import { AuthRegisterStore } from "./modules/auth/store/auth-register";
import { CurrentUserStore } from "./modules/auth/store/current-user";
import { EventStore } from "./modules/events/store/EventStore";
import { EditUserStore } from "./modules/user/store/edit-user";
import { PhotoLikesStore } from './modules/user/store/photo-likes';
import { NotificationsStore } from './modules/notifications/store/NotificationsStore';
import { VerificationStore } from './modules/user/store/verification-store';
import { FavouritesStore } from './modules/user/store/favourites-store';
import { BlockedUsersStore } from './modules/user/store/blocked-users';
import ConnectionStore from "./modules/user/store/connection";
import GeolocationStore from "./modules/core/store/geolocation";
import { PurchasesStore } from './modules/user/store/purchases-store';
import { ProfileActionManager } from './modules/user/store/profile-manager';

import { User } from './modules/user/domain/interfaces/User';

export default class RootStore {

  private storesMap = {
    [TYPES.AuthEnter]: AuthEnterStore,
    [TYPES.AuthRegister]: AuthRegisterStore,
    [TYPES.AuthUser]: AuthUserStore,
    [TYPES.Dictionaries]: DictionariesStore,
    [TYPES.Navigation]: NavigationStore,
    [TYPES.Layout]: LayoutStore,
    [TYPES.CurrentUserStore]: CurrentUserStore,
    [TYPES.EventStore]: EventStore,
    [TYPES.EditUserStore]: EditUserStore,
    [TYPES.PhotoLikesStore]: PhotoLikesStore,
    [TYPES.NotificationsStore]: NotificationsStore,
    [TYPES.VerificationStore]: VerificationStore,
    [TYPES.FavouritesStore]: FavouritesStore,
    [TYPES.BlockedUsersStore]: BlockedUsersStore,
    [TYPES.ConnectionStore]: ConnectionStore,
    [TYPES.GeolocationStore]: GeolocationStore,
    [TYPES.PurchasesStore]: PurchasesStore,
    [TYPES.ProfileManager]: ProfileActionManager,

    'chatsStore': ChatsStore,
    'usersSearchStore': UsersSearchStore,
    'coordinatesSearchStore': CoordinatesSearchStore,
    'profileStore': ProfileStore,
  };

  // для поддержки обратной совместимости
  public authEnterStore: AuthEnterStore;
  public authUserStore: AuthUserStore;
  public authRegisterStore: AuthRegisterStore;
  public layoutStore: LayoutStore;
  public navigationStore: NavigationStore;
  public dictionariesStore: DictionariesStore;
  public eventStore: EventStore;
  public currentUserStore: CurrentUserStore;
  public editUserStore: EditUserStore;
  public geolocationStore: GeolocationStore;
  public photoLikesStore: ITYPES['PhotoLikesStore'];
  public notificationsStore: ITYPES['NotificationsStore'];
  public verificationStore: ITYPES['VerificationStore'];
  public favouritesStore: ITYPES['FavouritesStore'];
  public blockedUsersStore: ITYPES['BlockedUsersStore'];
  public purchasesStore: ITYPES['PurchasesStore'];
  public profileManager: ITYPES['ProfileManager'];

  public chatsStore: ChatsStore;
  public usersSearchStore: UsersSearchStore;
  public coordinatesSearchStore: CoordinatesSearchStore;
  public profileStore: ProfileStore;

  @observable users: Record<number, User> = {};

  constructor() {
    const { storesMap } = this;
    type IStoresMapKeys = keyof typeof storesMap;
    Object.keys(storesMap).forEach((storeKey: IStoresMapKeys) => {
      const StoreConstructor = storesMap[storeKey] as any;
      (this as any)[storeKey] = new StoreConstructor(this);
    });
    httpResource.setErrorHandler((e: any) => {
      let resultMessage = 'Неизвестная ошибка';
      if (e.status === 500) {
        resultMessage = 'Внутренняя ошибка сервера';
        // todo закомментил для демонстрации релиза
        // showMessage({ message: resultMessage, type: 'warning', duration: 5000 });
      } else if (e.status === 401) {
        if (this.authUserStore && this.authUserStore.userId) {
          this.authUserStore.logout();
        }
      } else if (e.status === 103) {
        console.log('BAN');
      } else if (isFuncExists(e, 'json')) {
        const promise = e.json();
        if (isFuncExists(promise, 'then')) {
          return promise.then((body: any) => {
            if (body != null) {
              resultMessage = body;
              const { message, detail } = body;
              if (detail == "Не найдено.") {

              } else if (detail != "Не найдено." && message || detail && detail !== "None") {
                window.fLogEvent('app_errorScreen', message || detail);
                showMessage({
                  message: message || detail,
                  type: 'danger',
                });
              }
              throw resultMessage;
            }
            // todo handle
          });
        } else {
          // todo handle
        }
      } else if (isFuncExists(e, 'text')) {
        return e.text().then((message: string) => {
          resultMessage = message;
          window.fLogEvent('app_errorScreen', message);
          showMessage({ message, type: 'danger' });
          throw resultMessage;
        });
      } else  {
        window.fLogEvent('app_errorScreen', resultMessage);
        showMessage({ message: resultMessage, type: 'warning', duration: 5000 });
      }
    })
  }

  public resolve = (storeKey: string): any => {
    return (this as any)[storeKey];
  };

  public getStoresProviders = (): any => {
    return Object.keys(this.storesMap).reduce((result: any, storeKey) => {
      result[storeKey] = (this as any)[storeKey];
      return result;
    }, { rootStore: this });
  };

  public emitAfterStoreInit(): void {
    Object.keys(this.storesMap).forEach((storeKey: string) => {
      if (typeof (this as any)[storeKey]['storeInit'] === 'function') {
        (this as any)[storeKey].storeInit();
      }
    });
  }
}

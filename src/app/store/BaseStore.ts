import RootStore from "../rootStore";

export class BaseStore {
  constructor(protected root: RootStore) {}
  public storeInit() {}
}
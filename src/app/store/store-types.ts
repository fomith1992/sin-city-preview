import { ProfileManager } from './../modules/user/domain/interfaces/ProfileManager';
import { FavouritesStore } from './../modules/user/store/favourites-store';
import { VerificationStore } from './../modules/user/store/verification-store';
import { NotificationsStore } from './../modules/notifications/store/NotificationsStore';
import { PhotoLikesStore } from './../modules/user/store/photo-likes';
import { EditUserStore } from './../modules/user/store/edit-user';
import { EventStore } from './../modules/events/store/EventStore';
import { CurrentUserStore } from './../modules/auth/store/current-user';
import { AuthRegisterStore } from './../modules/auth/store/auth-register';
import { AuthUserStore } from './../modules/auth/store/auth-user';
import NavigationStore from 'react-navigation-mobx-helpers';
import { AuthEnterStore } from './../modules/auth/store/auth-enter';
import { LayoutStore } from '../modules/layout/store/LayoutStore';
import { DictionariesStore } from '../modules/dictionaries/store/DictionariesStore';
import ProfileStore from '../modules/user/store/ProfileStore';
import { BlockedUsersStore } from '../modules/user/store/blocked-users';
import ConnectionStore from "../modules/user/store/connection";
import GeolocationStore from "../modules/core/store/geolocation";
import { PurchasesStore } from '../modules/user/store/purchases-store';
import ChatsStore from '../modules/messages/store/ChatStore';

const TYPES = {
  AuthEnter: 'authEnterStore',
  Layout: 'layoutStore',
  Navigation: 'navigationStore',
  AuthUser: 'authUserStore',
  AuthRegister: 'authRegisterStore',
  Dictionaries: 'dictionariesStore',
  CurrentUserStore: 'currentUserStore',
  EventStore: 'eventStore',
  EditUserStore: 'editUserStore',
  PhotoLikesStore: 'photoLikesStore',
  NotificationsStore: 'notificationsStore',
  VerificationStore: 'verificationStore',
  FavouritesStore: 'favouritesStore',
  BlockedUsersStore: 'blockedUsersStore',
  ConnectionStore: 'connectionStore',
  GeolocationStore: 'geolocationStore',
  PurchasesStore: 'purchasesStore',
  ChatsStore: 'chatsStore',
  ProfileManager: 'profileManager',
  // deprecated
  Profile: 'profileStore',
};

export interface ITYPES {
  AuthEnter: AuthEnterStore,
  Layout: LayoutStore,
  Navigation: NavigationStore,
  AuthUser: AuthUserStore,
  AuthRegister: AuthRegisterStore,
  Dictionaries: DictionariesStore,
  CurrentUserStore: CurrentUserStore,
  EventStore: EventStore,
  EditUserStore: EditUserStore,
  PhotoLikesStore: PhotoLikesStore,
  NotificationsStore: NotificationsStore,
  VerificationStore: VerificationStore,
  FavouritesStore: FavouritesStore,
  BlockedUsersStore: BlockedUsersStore,
  ConnectionsStore: ConnectionStore,
  GeolocationStore: GeolocationStore,
  PurchasesStore: PurchasesStore,
  ChatsStore: ChatsStore,
  ProfileManager: ProfileManager,
  // deprecated
  Profile: ProfileStore,
};

export { TYPES };
/**
 * @format
 */
//throw new Error("My first Sentry error!");
// import { NativeModules } from 'react-native';
//
// if (__DEV__) {
//   NativeModules.DevSettings.setIsDebuggingRemotely(true)
// }

import * as Sentry from '@sentry/react-native';

// Sentry.init({
//   dsn: 'https://2bfb982a3b474385a160ccb9efc6ca34@sentry.io/5172872',
// });

Sentry.init({
  dsn: 'https://80d8d09da51d4f6eb80501753cad8055@sentry.truemachine.ru/44',
});

import {AppRegistry, Alert} from 'react-native';
import App from './src/app/App';
import {name as appName} from './app.json';
import 'react-native-gesture-handler';
import NotificationBackground from './src/app/modules/notifications/domain/services/NotificationsBackground';


//console.log('SENTRY INIT');
//Alert.alert('SENTRY INIT');
AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => NotificationBackground)
